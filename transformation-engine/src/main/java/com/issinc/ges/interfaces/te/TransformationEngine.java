/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.commons.utils.ObjectMapperProvider;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.interfaces.te.components.RuleSet;
import com.issinc.ges.interfaces.te.generator.RulesetGenerator;
import com.issinc.ges.interfaces.te.generator.VersionDefinitionGenerator;
import com.issinc.ges.interfaces.te.generator.XSDGenerator;
import com.issinc.ges.interfaces.te.models.SchemaVersion;
import com.issinc.ges.interfaces.te.utilities.JsonConstants;
import com.issinc.ges.interfaces.te.utilities.JsonUtils;
import com.issinc.ges.interfaces.te.utilities.ModelUtils;
import com.issinc.ges.interfaces.te.utilities.VersionDefinitionUtilities;
import com.issinc.ges.reporting.dal.DALException;
import com.issinc.ges.reporting.dal.DALFactory;
import com.issinc.ges.reporting.dal.DALResult;
import com.issinc.ges.reporting.dal.DalJsonUtils;
import com.issinc.ges.reporting.dal.IRepository;
import com.issinc.ges.reporting.dal.ISchemaRepo;
import com.issinc.ges.reporting.dal.transformationengine.IQueryParamRepo;
import com.issinc.ges.reporting.dal.transformationengine.IRulesetRepo;
import com.issinc.ges.reporting.dal.transformationengine.ISchemaVersionRepo;
import com.issinc.ges.reporting.dal.transformationengine.IVersionDefinitionRepo;

/**
 * The transformation transforms string data according to predefined rulesets. It determines the rulesets that need to
 * be applied to get from the source (original) version to the destination (target) version. It then applies the
 * rulesets by applying the rules contained in each ruleset one by one until the transformation is complete.
 */
public final class TransformationEngine {

    public static final String OBJECT_ID_INSTANCE = "cidne-%s-%s-%s";
    public static final String OBJECT_ID_SCHEMA = "cidne-%s-%s-%s-schema";
    public static final String ASSOCIATION = "association";
    public static final String MEDIA = "media";
    public static final String MEDIA_REPORT = "report";
    public static final String PICKLIST = "picklist";
    public static final String DATA = "data";
    public static final String CURRENT = "CURRENT";

    private static final String REPORT_LIST = "reportlist";
    private static final String DEFAULT_LEGACY_VERSION = "2.3.5";
    private static final String DEFAULT_NATIVE_VERSION = "4.1";
    private static final String BASE_MODULE = "base.module";
    private static final String BASE_REPORT_TYPE = "base.reportType";
    private static final String BASE_CURRENT_OBJECT_STATE_OBJECT_STATE = "base.currentObjectState.objectState";
    private static final String BASE_CURRENT_OBJECT_STATE_SYSTEM = "base.currentObjectState.system";
    private static final String CIDNE_2_X_ALIAS_TO_TRANSFORM_CANNOT_BE_NULL =
            "CIDNE 2.X alias to transform cannot be null.";

    /**
     * The transformation engine can transform schemas as well as instance data
     */
    public enum InputType { INSTANCE, SCHEMA }
    private static final Logger logger = LoggerFactory.getLogger(TransformationEngine.class);

    private static final String VERSION_TO_FROM_ERROR = "%s to %s";
    private static final String WITH_VERSION = " with version ";
    private static final String MODULE = ", module ";
    private static final String AND_REPORT_TYPE = " and reportType ";
    private static final String THERE_IS_NO_4_X_MAPPING_FOR_PARAMETER = "There is no 4.X mapping for parameter ";
    private static final String NO_SCHEMA_VERSIONS_FOUND = "Error retrieving schema versions. None found.";
    private static final ObjectMapper MAPPER = ObjectMapperProvider.provide();

    private static String currentNativeVersion;
    private static String currentLegacyVersion;
    private static final ArrayList<SchemaVersion> schemaVersions = new ArrayList<>();
    private static boolean initialized;

    /**
     * Singleton
     */
    private TransformationEngine() {
    }

    /**
     * Return a single instance of the transformation engine.
     *
     * @return
     *   Transformation Engine
     */
    public static TransformationEngine getInstance() {
        return TransformationEngineHolder.INSTANCE;
    }

    /**
     * Initializes the current native and legacy versions
     * @param token The security token string
     */
    public void initialize(String token) {
        // Initialize schema version information from Mongo
        try {
            final ISchemaVersionRepo repo = DALFactory.get(ISchemaVersionRepo.class);
            final DALResult dalResult = repo.getAll(token);
            if (dalResult.getCount() > 0) {
                final List<JsonNode> nodeList = DalJsonUtils.unWrapResultNodes(dalResult.getResult());
                for (JsonNode node : nodeList) {
                    try {
                        final SchemaVersion sv = ModelUtils.toObject(node, SchemaVersion.class);
                        schemaVersions.add(sv);
                        if (sv.isCurrent()) {
                            currentLegacyVersion = sv.getLegacyVersion();
                            currentNativeVersion = sv.getNativeVersion();
                        }
                    } catch (TransformationException e) {
                        logger.warn("Error converting JsonNode to SchemaVersion POJO.");
                    }
                }
            } else {
                logger.warn(NO_SCHEMA_VERSIONS_FOUND);
            }
        } catch (DALException e) {
            logger.warn(NO_SCHEMA_VERSIONS_FOUND);
        }
        if (currentLegacyVersion == null) {
            logger.warn("Error retrieving current legacy version. Using default "  + DEFAULT_LEGACY_VERSION);
        }
        if (currentNativeVersion == null) {
            logger.warn("Error retrieving current native version. Using default " + DEFAULT_NATIVE_VERSION);
        }
        initialized = true;
    }

    /**
     * Transforms the incoming schema inputString from one version to another, then
     * returns the transformed payload as a string.
     *
     * @param inputString The schema to be transformed
     * @param module The module of the schema.
     * @param reportType The report type of the schema
     * @param srcVersionNumber The source (original or "from") version
     * @param destVersionNumber The destination (target pr "to") version to convert to
     * @param token The security token string
     * @return The transformed schema
     * @throws TransformationException if an error occurs
     */
    public String transformSchema(String inputString, String module, String reportType,
            String srcVersionNumber, String destVersionNumber, String token)
            throws TransformationException {

        final String sourceVersionString =
                buildVersionString(InputType.SCHEMA, module, reportType, srcVersionNumber, token);
        final String destinationVersionString =
                buildVersionString(InputType.SCHEMA, module, reportType, destVersionNumber, token);

        return doTransformation(inputString, sourceVersionString, destinationVersionString, token);
    }

    /**
     * Transforms the incoming report string from one version to another, then
     * returns the transformed payload as a string.
     *
     * @param inputString The report payload to be transformed
     * @param module The module of the report.
     * @param reportType The report type of the report
     * @param srcVersionNumber The source (original or "from") version
     * @param destVersionNumber The destination (target pr "to") version to convert to
     * @param token The security token string
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    public String transformReport(String inputString, String module, String reportType,
            String srcVersionNumber, String destVersionNumber, String token)
            throws TransformationException {

        final String sourceVersionString =
                buildVersionString(InputType.INSTANCE, module, reportType, srcVersionNumber, token);
        final String destinationVersionString =
                buildVersionString(InputType.INSTANCE, module, reportType, destVersionNumber, token);

        // If there is no ruleset for this transformation, generate rulesets and version definitions from the schema
        try {
            final IRulesetRepo rulesetRepo = DALFactory.get(IRulesetRepo.class);
            final DALResult dalResult = rulesetRepo.getBySourceAndDestinationVersionId(
                    sourceVersionString, destinationVersionString, token);
            if (dalResult.getCount() == 0) {
                generateDynamicComponents(module, reportType, srcVersionNumber, destVersionNumber, token);
            }
        } catch (DALException e) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_RULESET,
                    "There is no ruleset for this transformation and the ruleset could not be generated dynamically");
        }

        return doTransformation(inputString, sourceVersionString, destinationVersionString, token);
    }

    public String transformReport(String inputString, ReportVariantKey srcVariant,
                                             ReportVariantKey destVariant, String token)
            throws TransformationException {

        final String sourceVersionString = srcVariant.getVersionString(token);
        final String destinationVersionString = destVariant.getVersionString(token);

        // If there is no ruleset for this transformation, generate rulesets and version definitions from the schema
        try {
            final IRulesetRepo rulesetRepo = DALFactory.get(IRulesetRepo.class);
            final DALResult dalResult = rulesetRepo.getBySourceAndDestinationVersionId(
                    sourceVersionString, destinationVersionString, token);
            if (dalResult.getCount() == 0) {
                throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                        "Need to implement variant API for dynamic component generation");
                //generateDynamicComponents(destModule, destRptType, extSourceVersionNum, destVersionNum, token);
            }
        } catch (DALException e) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_RULESET,
                    "There is no ruleset for this transformation and the ruleset could not be generated dynamically");
        }
        return doTransformation(inputString, sourceVersionString, destinationVersionString, token);
    }


    /**
     * Transforms a single MISREP string to a Pilot Debrief report, then
     * returns the transformed payload as a string.
     *
     * @param inputString The MISREP payload to be transformed
     * @param src The source (original or "from") version
     * @param dest The destination (target or "to") version
     * @param token The security token string
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    public String transformMisrep(String inputString, String src, String dest, String token)
            throws TransformationException {

        return doTransformation(inputString, src, dest, token);
    }

    /**
     * Transforms the incoming string association from one version to another, then
     * returns the transformed payload as a string.  For Association data.
     *
     * @param association The payload to be transformed
     * @param associatedObject1 A document to be associated with another document
     * @param associatedObject2 A document to be associated with another document
     * @param srcVersionNumber The source (original or "from") version
     * @param destVersionNumber The destination (target pr "to") version to convert to
     * @param token The security token string
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    public String transformAssociation(String association, String associatedObject1, String associatedObject2,
            String srcVersionNumber, String destVersionNumber, String token) throws TransformationException {
        String payload = association;
        final String sourceVersionString =
                buildVersionString(InputType.INSTANCE, ASSOCIATION, DATA, srcVersionNumber, token);
        final String destinationVersionString =
                buildVersionString(InputType.INSTANCE, ASSOCIATION, DATA, destVersionNumber, token);

        if (associatedObject1 != null && associatedObject2 != null) {
            payload = combineAssociationData(association, associatedObject1, associatedObject2);
        }
        return doTransformation(payload, sourceVersionString, destinationVersionString, token);
    }

    private String combineAssociationData(String association, String associatedObject1, String associatedObject2)
    throws TransformationException {
        // Add the associated object information to the association for access by the ruleset
        try {
            final JsonNode associatedObject1Node = MAPPER.readTree(associatedObject1);
            final JsonNode associatedObject2Node = MAPPER.readTree(associatedObject2);
            final ObjectNode associationNode = (ObjectNode) MAPPER.readTree(association);

            // Add the associated entity data we need for 4X to 2X association transformations to the associationNode
            if (JsonUtils.hasNonNull(associatedObject1Node, BASE_MODULE)) {
                associationNode.set("sourceModule", JsonUtils.getNodeAtPath(associatedObject1Node, BASE_MODULE));
            }
            if (JsonUtils.hasNonNull(associatedObject1Node, BASE_REPORT_TYPE)) {
                associationNode.set("sourceReportType",
                        JsonUtils.getNodeAtPath(associatedObject1Node, BASE_REPORT_TYPE));
            }
            if (JsonUtils.hasNonNull(associatedObject1Node, BASE_CURRENT_OBJECT_STATE_OBJECT_STATE)) {
                associationNode.set("sourceStatus",
                        JsonUtils.getNodeAtPath(associatedObject1Node, BASE_CURRENT_OBJECT_STATE_OBJECT_STATE));
            }
            if (JsonUtils.hasNonNull(associatedObject1Node, BASE_CURRENT_OBJECT_STATE_SYSTEM)) {
                associationNode.set("sourceSystem",
                        JsonUtils.getNodeAtPath(associatedObject1Node, BASE_CURRENT_OBJECT_STATE_SYSTEM));
            }
            if (JsonUtils.hasNonNull(associatedObject2Node, BASE_MODULE)) {
                associationNode.set("targetModule", JsonUtils.getNodeAtPath(associatedObject2Node, BASE_MODULE));
            }
            if (JsonUtils.hasNonNull(associatedObject2Node, BASE_REPORT_TYPE)) {
                associationNode.set("targetReportType",
                        JsonUtils.getNodeAtPath(associatedObject2Node, BASE_REPORT_TYPE));
            }
            if (JsonUtils.hasNonNull(associatedObject2Node, BASE_CURRENT_OBJECT_STATE_OBJECT_STATE)) {
                associationNode.set("targetStatus",
                        JsonUtils.getNodeAtPath(associatedObject2Node, BASE_CURRENT_OBJECT_STATE_OBJECT_STATE));
            }
            if (JsonUtils.hasNonNull(associatedObject2Node, BASE_CURRENT_OBJECT_STATE_SYSTEM)) {
                associationNode.set("targetSystem",
                        JsonUtils.getNodeAtPath(associatedObject2Node, BASE_CURRENT_OBJECT_STATE_SYSTEM));
            }

            return associationNode.toString();
        } catch (IOException e) {
            throw new TransformationException(TransformationException.ErrorType.BAD_REQUEST,
                    "Error parsing associated objects for association");
        }
    }

    /**
     * Transforms the incoming string inputString from one version to another, then
     * returns the transformed payload as a string.  For Media reports.
     *
     * @param inputString The payload to be transformed
     * @param srcVersionNumber The source (original or "from") version
     * @param destVersionNumber The destination (target pr "to") version to convert to
     * @param token The security token string
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    public String transformMedia(String inputString, String srcVersionNumber,
                                       String destVersionNumber, String token) throws TransformationException {
        return transformReport(inputString, MEDIA, MEDIA_REPORT, srcVersionNumber, destVersionNumber, token);
    }

    /**
     * Transforms the incoming string inputString from one version to another, then
     * returns the transformed payload as a string.  For Picklist data.
     *
     * @param inputString The payload to be transformed
     * @param srcVersionNumber The source (original or "from") version
     * @param destVersionNumber The destination (target pr "to") version to convert to
     * @param token The security token string
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    public String transformPicklist(String inputString, String srcVersionNumber,
                                    String destVersionNumber, String token) throws TransformationException {
        return transformReport(inputString, PICKLIST, DATA, srcVersionNumber, destVersionNumber, token);
    }

    /**
     * Converts a CIDNE 2.X report query parameter name to the equivalent CIDNE 4.X parameter name. The mapping is
     * determined by looking up the 2.X field aliases in the schemas.
     * @param version The CIDNE 2.X version of the query parameter to be transformed ("2.3.5")
     * @param module The CIDNE report module name
     * @param reportType The CIDNE report type
     * @param twoXalias The name of the CIDNE 2.X query parameter
     * @param token The security token
     * @return The 4.x query parameter name (mapping)
     * @throws  TransformationException if an error occurs
     */
    public String transformReportQP(String version, String module, String reportType, String twoXalias, String token)
            throws TransformationException {
        if (!initialized) {
            initialize(token);
        }
        try {
            final IQueryParamRepo qpRepo = DALFactory.get(IQueryParamRepo.class);
            final DALResult dalResult = qpRepo.getReportQueryParameters(version.toLowerCase(), module.toLowerCase(),
                    reportType.toLowerCase(), token);
            // Parse DALResult and return mapping
            final String message = THERE_IS_NO_4_X_MAPPING_FOR_PARAMETER + twoXalias +
                    WITH_VERSION + version + MODULE + module + AND_REPORT_TYPE + reportType;
            return getFourXmapping(dalResult, twoXalias.toLowerCase(), message);
        } catch (DALException e) {
            throw new TransformationException(TransformationException.ErrorType.BAD_REQUEST,
                    THERE_IS_NO_4_X_MAPPING_FOR_PARAMETER + twoXalias +
                            WITH_VERSION + version + MODULE + module + AND_REPORT_TYPE + reportType);
        }
    }

    /**
     * Converts a CIDNE 2.X association query parameter name to the equivalent CIDNE 4.X parameter name. The mapping is
     * determined by looking up the 2.X field aliases in the schemas.
     * @param twoXalias The name of the CIDNE 2.X query parameter
     * @param token The security token
     * @return The 4.x query parameter name (mapping)
     * @throws  TransformationException if an error occurs
     */
    public String transformAssociationQP(String twoXalias, String token)
            throws TransformationException {
        if (!initialized) {
            initialize(token);
        }
        if (twoXalias == null) {
            throw new TransformationException(TransformationException.ErrorType.BAD_REQUEST,
                    CIDNE_2_X_ALIAS_TO_TRANSFORM_CANNOT_BE_NULL);
        }
        try {
            final IQueryParamRepo qpRepo = DALFactory.get(IQueryParamRepo.class);
            final DALResult dalResult = qpRepo.getAssociationQueryParameters(token);
            final String message = THERE_IS_NO_4_X_MAPPING_FOR_PARAMETER + twoXalias;
            return getFourXmapping(dalResult, twoXalias.toLowerCase(), message);
        } catch (DALException e) {
            throw new TransformationException(TransformationException.ErrorType.BAD_REQUEST,
                    THERE_IS_NO_4_X_MAPPING_FOR_PARAMETER + twoXalias);
        }
    }

    /**
     * Converts a CIDNE 2.X association query parameter name to the equivalent CIDNE 4.X parameter name. The mapping is
     * determined by looking up the 2.X field aliases in the schemas.
     * @param twoXalias The name of the CIDNE 2.X query parameter
     * @param token The security token
     * @return The 4.x query parameter name (mapping)
     * @throws  TransformationException if an error occurs
     */
    public String transformLocationQP(String twoXalias, String token) throws TransformationException {
        if (twoXalias == null) {
            throw new TransformationException(TransformationException.ErrorType.BAD_REQUEST,
                    CIDNE_2_X_ALIAS_TO_TRANSFORM_CANNOT_BE_NULL);
        }
        if (!initialized) {
            initialize(token);
        }
        try {
            final IQueryParamRepo qpRepo = DALFactory.get(IQueryParamRepo.class);
            final DALResult dalResult = qpRepo.getLocationQueryParameters(token);
            final String message = THERE_IS_NO_4_X_MAPPING_FOR_PARAMETER + twoXalias;
            return getFourXmapping(dalResult, twoXalias.toLowerCase(), message);
        } catch (DALException e) {
            throw new TransformationException(TransformationException.ErrorType.BAD_REQUEST,
                    THERE_IS_NO_4_X_MAPPING_FOR_PARAMETER + twoXalias);
        }
    }

    /**
     * Transform a JSON schema to a matching XSD.  The XSD describes the XML that is generated by a
     * report for this schema.
     * @param schema The JSON schema
     * @param module The module name (e.g. Operations)
     * @param reportType The report type (e.g. SIGACT)
     * @param token The seucroty token string
     * @return The XML schema string (XSD)
     * @throws TransformationException if an error occurs
     */
    public String transformSchemaToXsd(String schema, String module, String reportType, String token)
            throws TransformationException {

        final XSDGenerator xsdGenerator = new XSDGenerator();

        JsonNode schemaNode;
        try {
            schemaNode = ObjectMapperProvider.provide().readTree(schema);
        } catch (IOException e) {
            throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR,
                    "Could not convert schema to node");
        }
        final RuleSet ruleset = xsdGenerator.generateRuleset(schemaNode, module, reportType,
                "JsonSchema", "XmlSchema");   // src/dest versions really are irrelevant here...

        Object result = schema;  // start off with schema, and apply rules to convert it to an XSD
        for (Rule rule : ruleset.getRules()) {
            result = rule.applyRule(token, result);
        }
        return (String) result;
    }

    /**
     * Transforms the given query results from one version to another, then
     * returns the transformed payload as a string.
     *
     * @param inputString The payload to be transformed
     * @param srcVersionNumber The source (original or "from") version
     * @param destVersionNumber The destination (target pr "to") version to convert to
     * @param token The security token string
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    public String transformReportList(String inputString, String srcVersionNumber, String destVersionNumber,
            String token)
            throws TransformationException {
        return transformReport(inputString, REPORT_LIST, DATA, srcVersionNumber, destVersionNumber, token);
    }

    private void generateDynamicComponents(String module, String reportType, String srcVersionNumber,
            String destVersionNumber, String token) throws TransformationException {
        // See if we have a compatible schemaVersion to map between the src and destination versions
        final SchemaVersion schemaVersion =
                getSchemaVersionBySrcDestVersion(srcVersionNumber, destVersionNumber, token);
        if (schemaVersion != null) {
            try {
                // Found the schemaVersion, try to find the schema
                final ISchemaRepo sRepo = DALFactory.get(ISchemaRepo.class);
                final DALResult dalResult = sRepo.getByModuleReportTypeAndVersion(
                        module, reportType, schemaVersion.getObjectId(), token);
                if (dalResult.getCount() == 1) {
                    // We have a schema! Generate the rulesets both directions for this schema
                    final JsonNode schemaJsonNode = DalJsonUtils.unWrapResultsAsJsonNode(dalResult.getResult()).get(0);
                    final List<JsonNode> rulesets = RulesetGenerator.buildRuleSets(schemaJsonNode, token);
                    // Store the rulesets
                    final IRulesetRepo rulesetRepo = DALFactory.get(IRulesetRepo.class);
                    for (JsonNode ruleset : rulesets) {
                        insertDocument(rulesetRepo, ruleset, token);
                    }
                    // The source and/or destination version definitions may not exist either
                    final List<JsonNode> versionDefinitions =
                            VersionDefinitionGenerator.buildVersionDefinitions(schemaJsonNode, token);
                    // Store the version definitions
                    final IVersionDefinitionRepo vdRepo = DALFactory.get(IVersionDefinitionRepo.class);
                    for (JsonNode vd : versionDefinitions) {
                        insertDocument(vdRepo, vd, token);
                    }
                }
            } catch (DALException e) {
                final String message = "Error inserting dynamic ruleset or versionDefinition";
                logger.error(message);
                throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR, message);
            }
        }
    }

    private String doTransformation(String inputString, String source, String destination,
            String token) throws TransformationException {

        if (inputString == null) {
            return null;
        }

        final String sourceVersionString = source.toLowerCase();
        final String destinationVersionString = destination.toLowerCase();

        String result = inputString;

        // Determine the conversions that need to be done (rulesets that must be applied)
        final ArrayList<String> path = VersionDefinitionUtilities.getPath(sourceVersionString,
                destinationVersionString, token);

        // If we can't find a path from the source to the destination version then we don't know how to transform it =>
        // throw an exception
        if (path == null || path.size() == 0) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_RULESET,
                    "There are no known conversion paths from "
                            + String.format(VERSION_TO_FROM_ERROR, sourceVersionString, destinationVersionString));
        }
        // Perform each step of the transformation. There will generally be just one but we might chain
        // transformations when we have more versions in the picture
        for (int n = 0; n < path.size() - 1; n++) {
            result = applyRuleset(result, path.get(n), path.get(n + 1), token);
        }

        return result;
    }

    private String getFourXmapping(DALResult dalResult, String twoXalias, String message)
            throws TransformationException {
        // Parse DALResult and return mapping
        final JsonNode node = DalJsonUtils.unWrapResultsAsJsonNode(dalResult.getResult());
        if (node != null && node.isArray() && node.size() == 1 && node.get(0).has(JsonConstants.ALIASES)) {
            final ArrayNode aliases = (ArrayNode) (node.get(0).get(JsonConstants.ALIASES));
            if (aliases != null) {
                for (JsonNode aliasNode : aliases) {
                    if (twoXalias.equals(aliasNode.get(JsonConstants.TWO_X_ALIAS).asText())) {
                        return aliasNode.get(JsonConstants.FOUR_X_MAPPING).asText();
                    }
                }
            }
        }
        throw new TransformationException(TransformationException.ErrorType.BAD_REQUEST, message);
    }

    /**
     * The actual conversion method that calls the rules to convert the incoming string.
     *
     * @param inputString
     *   String to be converted.
     * @param fromVersionID
     *   From version
     * @param toVersionID
     *   To version
     * @param token
     *   Security Token
     * @return
     *   Converted String
     * @throws TransformationException
     *   If String has problems converting
     */
    private String applyRuleset(String inputString, String fromVersionID, String toVersionID, String token)
            throws TransformationException {

        Object result = inputString;

        final RuleSet ruleSet = VersionDefinitionUtilities.getRuleSet(fromVersionID, toVersionID, token);

        if (ruleSet == null) {
            logger.warn("There are no rules defined for the conversion from "
                    + String.format(VERSION_TO_FROM_ERROR, fromVersionID, toVersionID));
            return inputString;
        }

        final Rule[] rules = ruleSet.getRules();
        if (rules == null) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                    "Ruleset contained no rules; configuration issue");
        }

        for (Rule rule : rules) {
            result = rule.applyRule(token, result);
        }

        if (result == null) {
            return null;
        } else {
            return result.toString();
        }

    }

    /**
     * Generates the version to look up the ruleset and version definitions.
     *
     * @param module
     *   Report module
     * @param reportType
     *   Report type
     * @param version
     *   Version number (ie 4.1)
     * @return
     *   The version string key to pull objects from Mongo
     */
    static public String buildVersionString(InputType inputType, String module, String reportType,
            String version, String token) {

        String versionNumber = version;

        if (CURRENT.equals(versionNumber)) {
            versionNumber = getInstance().getCurrentNativeVersion(token); // get current version
        }

        if (inputType == InputType.SCHEMA) {
            return String.format(OBJECT_ID_SCHEMA, versionNumber, module, reportType).toLowerCase();
        } else {
            return String.format(OBJECT_ID_INSTANCE, versionNumber, module, reportType).toLowerCase();
        }
    }

    private static void insertDocument(IRepository repo, JsonNode node, String token) throws TransformationException {
        final String objectId = node.get(JsonConstants.OBJECT_ID).asText();
        try {
            repo.insert(node.toString(), "U", token);
            System.out.println("Inserted: " + objectId);
        } catch (DALException e) {
            if (e.getDetailedMessage().contains("DuplicateKey")) {
                logger.warn("Document not inserted because of duplicate key " + repo.toString() +
                        " with objectId " + objectId);
            } else {
                logger.error("Error inserting " + repo.toString() + " with objectId " + objectId);
                throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                        "Error inserting document to " + repo.toString());
            }
        }
    }
    /**
     * Gets the current native version
     * @param token The security token string
     * @return Current native version (e.g. 4.1)
     */
    public String getCurrentNativeVersion(final String token) {
        if (!initialized) {
            initialize(token);
        }
        return currentNativeVersion;
    }

    /**
     * Gets the current legacy version
     * @param token The security token string
     *
     * @return Current legacy version (e.g. 2.3.5)
     */
    public String getCurrentLegacyVersion(final String token) {
        if (!initialized) {
            initialize(token);
        }
        return currentLegacyVersion;
    }

    /**
     * Gets the native version for the given schema
     * @param schemaVersion The schema version (e.g. CIDNEDM235) for which to find the native version (e.g. 4.1)
     * @param token The security token string
     *
     * @return Native version (e.g. 4.1)
     * @throws TransformationException if an error occurs
     */
    public String getNativeVersion(final String schemaVersion, final String token) throws TransformationException {
        if (!initialized) {
            initialize(token);
        }
        final SchemaVersion sv = getSchemaVersionBySchemaVersion(schemaVersion, token);
        return sv.getNativeVersion();
    }

    /**
     * Gets the legacy version for the given schema
     * @param schemaVersion The schema version (e.g. CIDNEDM235) for which to find the native version (e.g. 4.1)
     * @param token The security token string
     *
     * @return Legacy version (e.g. 2.3.5)
     * @throws TransformationException if an error occurs
     */
    public String getLegacyVersion(final String schemaVersion, final String token) throws TransformationException {
        if (!initialized) {
            initialize(token);
        }
        final SchemaVersion sv = getSchemaVersionBySchemaVersion(schemaVersion, token);
        return sv.getLegacyVersion();
    }

    /**
     * Gets the schema version by objectId. Object IDs contain schema version names (e.g. CIDNEDM235)
     * @param schemaVersion The schema version (e.g. CIDNEDM235) for which to find the native version (e.g. 4.1)
     * @param token The security token string
     *
     * @return The specified SchemaVersion POJO from the cache
     * @throws TransformationException if an error occurs
     */
    public SchemaVersion getSchemaVersionBySchemaVersion(final String schemaVersion, final String token)
            throws TransformationException {

        if (schemaVersion != null) {
            if (!initialized) {
                initialize(token);
            }
            for (SchemaVersion sv : schemaVersions) {
                if (schemaVersion.equals(sv.getObjectId())) {
                    return sv;
                }
            }
        }
        return null;
    }

    /**
     * Gets the schema version with matching source and destination versions. We don't know which one is the legacy
     * version and which is the native version so check both directions.
     * @param sourceVersion The source version to be found
     * @param destinationVersion The destination version to be found
     * @param token The security token string
     *
     * @return The specified SchemaVersion POJO from the cache
     * @throws TransformationException if an error occurs
     */
    public SchemaVersion getSchemaVersionBySrcDestVersion(final String sourceVersion, final String destinationVersion,
            final String token) throws TransformationException {
        if (sourceVersion != null) {
            if (!initialized) {
                initialize(token);
            }
            for (SchemaVersion sv : schemaVersions) {
                final String legacyVersion = sv.getLegacyVersion();
                final String nativeVersion = sv.getNativeVersion();
                if ((sourceVersion.equals(legacyVersion) && destinationVersion.equals(nativeVersion)) ||
                    (sourceVersion.equals(nativeVersion) && destinationVersion.equals(legacyVersion))) {
                    return sv;
                }
            }
        }
        return null;
    }

    /**
     * Initializes singleton.
     *
     * {@link TransformationEngineHolder} is loaded on the first execution of
     * {@link TransformationEngine#getInstance()} or the first access to
     * {@link TransformationEngineHolder#INSTANCE}, not before.
     */
    /// CHECKSTYLE:OFF
    private static final class TransformationEngineHolder {
        /// CHECKSTYLE:ON
        private static final TransformationEngine INSTANCE = new TransformationEngine();
    }

} /* End of Class TransformationEngine */
