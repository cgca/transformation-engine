/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models.cdr.eval;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.issinc.ges.interfaces.te.models.ActionDate;
import com.issinc.ges.interfaces.te.models.cdr.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * Humint Eval Report details specific details
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "additionalproductscreated",
    "cite",
    "comments",
    "customernumber",
    "dateevalclosed",
    "derivedfrom",
    "draftparentserial",
    "evalclosed",
    "guidance",
    "ipsp",
    "irdt",
    "lasttimeinformationofvalue",
    "originatoremail",
    "originatorphone",
    "pass",
    "poc",
    "reference",
    "relatedrequirements",
    "reliability",
    "reliabilitychoice",
    "reporter",
    "reporterunit",
    "reporttext",
    "reportvalue",
    "responsiveness",
    "sourceparagraph",
    "utility",
    "utilitychoice",
    "validity",
    "validitychoice",
    "warning",
    "country",
    "countrypriority",
    "countrypriority2",
    "countrypriority3",
    "productuse",
    "remark",
    "source",
    "taskedunits"
})
public class EvalDetails {

    @JsonProperty("additionalproductscreated")
    private String additionalproductscreated;
    @JsonProperty("cite")
    private String cite;
    @JsonProperty("comments")
    private String comments;
    @JsonProperty("customernumber")
    private String customernumber;
    /**
     * 
     */
    @JsonProperty("dateevalclosed")
    private ActionDate dateevalclosed;
    @JsonProperty("derivedfrom")
    private String derivedfrom;
    @JsonProperty("draftparentserial")
    private String draftparentserial;
    /**
     * EvalClosed
     * 
     */
    @JsonProperty("evalclosed")
    private Boolean evalclosed;
    @JsonProperty("guidance")
    private String guidance;
    @JsonProperty("ipsp")
    private String ipsp;
    /**
     * 
     */
    @JsonProperty("irdt")
    private ActionDate irdt;
    /**
     * 
     */
    @JsonProperty("lasttimeinformationofvalue")
    private ActionDate lasttimeinformationofvalue;
    @JsonProperty("originatoremail")
    private String originatoremail;
    @JsonProperty("originatorphone")
    private String originatorphone;
    @JsonProperty("pass")
    private String pass;
    @JsonProperty("poc")
    private String poc;
    @JsonProperty("reference")
    private String reference;
    @JsonProperty("relatedrequirements")
    private String relatedrequirements;
    @JsonProperty("reliability")
    private String reliability;
    @JsonProperty("reliabilitychoice")
    private String reliabilitychoice;
    @JsonProperty("reporter")
    private String reporter;
    @JsonProperty("reporterunit")
    private String reporterunit;
    @JsonProperty("reporttext")
    private String reporttext;
    @JsonProperty("reportvalue")
    private String reportvalue;
    @JsonProperty("responsiveness")
    private String responsiveness;
    @JsonProperty("sourceparagraph")
    private String sourceparagraph;
    @JsonProperty("utility")
    private String utility;
    @JsonProperty("utilitychoice")
    private String utilitychoice;
    @JsonProperty("validity")
    private String validity;
    @JsonProperty("validitychoice")
    private String validitychoice;
    @JsonProperty("warning")
    private String warning;
    /**
     * Humint Eval Report Country specific details
     * 
     */
    @JsonProperty("country")
    private List<Country> country = new ArrayList<Country>();
    /**
     * Humint Eval Report CountryPriority specific details
     * 
     */
    @JsonProperty("countrypriority")
    private List<Countrypriority> countrypriority = new ArrayList<Countrypriority>();
    /**
     * Humint Eval Report CountryPriority2 specific details
     * 
     */
    @JsonProperty("countrypriority2")
    private List<Countrypriority2> countrypriority2 = new ArrayList<Countrypriority2>();
    /**
     * Humint Eval Report CountryPriority3 specific details
     * 
     */
    @JsonProperty("countrypriority3")
    private List<Countrypriority3> countrypriority3 = new ArrayList<Countrypriority3>();
    /**
     * Humint Eval Report ProductUse specific details
     * 
     */
    @JsonProperty("productuse")
    private List<Productuse> productuse = new ArrayList<Productuse>();
    /**
     * Humint Eval Report Remark specific details
     * 
     */
    @JsonProperty("remark")
    private List<Remark> remark = new ArrayList<Remark>();
    /**
     * Humint Eval Report Source specific details
     * 
     */
    @JsonProperty("source")
    private List<HumintSource> source = new ArrayList<HumintSource>();
    /**
     * Humint Eval Report TaskedUnits specific details
     * 
     */
    @JsonProperty("taskedunits")
    private List<Taskedunit> taskedunits = new ArrayList<Taskedunit>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public EvalDetails() {
    }

    /**
     * 
     * @param guidance
     * @param remark
     * @param reportvalue
     * @param reliability
     * @param irdt
     * @param reliabilitychoice
     * @param taskedunits
     * @param reporter
     * @param responsiveness
     * @param customernumber
     * @param validity
     * @param utilitychoice
     * @param additionalproductscreated
     * @param countrypriority3
     * @param poc
     * @param relatedrequirements
     * @param countrypriority2
     * @param dateevalclosed
     * @param ipsp
     * @param reporterunit
     * @param productuse
     * @param lasttimeinformationofvalue
     * @param sourceparagraph
     * @param reference
     * @param country
     * @param pass
     * @param reporttext
     * @param cite
     * @param originatoremail
     * @param evalclosed
     * @param derivedfrom
     * @param source
     * @param utility
     * @param originatorphone
     * @param countrypriority
     * @param draftparentserial
     * @param warning
     * @param validitychoice
     * @param comments
     */
    public EvalDetails(String additionalproductscreated, String cite, String comments, String customernumber, ActionDate dateevalclosed, String derivedfrom, String draftparentserial, Boolean evalclosed, String guidance, String ipsp, ActionDate irdt, ActionDate lasttimeinformationofvalue, String originatoremail, String originatorphone, String pass, String poc, String reference, String relatedrequirements, String reliability, String reliabilitychoice, String reporter, String reporterunit, String reporttext, String reportvalue, String responsiveness, String sourceparagraph, String utility, String utilitychoice, String validity, String validitychoice, String warning, List<Country> country, List<Countrypriority> countrypriority, List<Countrypriority2> countrypriority2, List<Countrypriority3> countrypriority3, List<Productuse> productuse, List<Remark> remark, List<HumintSource> source, List<Taskedunit> taskedunits) {
        this.additionalproductscreated = additionalproductscreated;
        this.cite = cite;
        this.comments = comments;
        this.customernumber = customernumber;
        this.dateevalclosed = dateevalclosed;
        this.derivedfrom = derivedfrom;
        this.draftparentserial = draftparentserial;
        this.evalclosed = evalclosed;
        this.guidance = guidance;
        this.ipsp = ipsp;
        this.irdt = irdt;
        this.lasttimeinformationofvalue = lasttimeinformationofvalue;
        this.originatoremail = originatoremail;
        this.originatorphone = originatorphone;
        this.pass = pass;
        this.poc = poc;
        this.reference = reference;
        this.relatedrequirements = relatedrequirements;
        this.reliability = reliability;
        this.reliabilitychoice = reliabilitychoice;
        this.reporter = reporter;
        this.reporterunit = reporterunit;
        this.reporttext = reporttext;
        this.reportvalue = reportvalue;
        this.responsiveness = responsiveness;
        this.sourceparagraph = sourceparagraph;
        this.utility = utility;
        this.utilitychoice = utilitychoice;
        this.validity = validity;
        this.validitychoice = validitychoice;
        this.warning = warning;
        this.country = country;
        this.countrypriority = countrypriority;
        this.countrypriority2 = countrypriority2;
        this.countrypriority3 = countrypriority3;
        this.productuse = productuse;
        this.remark = remark;
        this.source = source;
        this.taskedunits = taskedunits;
    }

    /**
     * 
     * @return
     *     The additionalproductscreated
     */
    @JsonProperty("additionalproductscreated")
    public String getAdditionalproductscreated() {
        return additionalproductscreated;
    }

    /**
     * 
     * @param additionalproductscreated
     *     The additionalproductscreated
     */
    @JsonProperty("additionalproductscreated")
    public void setAdditionalproductscreated(String additionalproductscreated) {
        this.additionalproductscreated = additionalproductscreated;
    }

    /**
     * 
     * @return
     *     The cite
     */
    @JsonProperty("cite")
    public String getCite() {
        return cite;
    }

    /**
     * 
     * @param cite
     *     The cite
     */
    @JsonProperty("cite")
    public void setCite(String cite) {
        this.cite = cite;
    }

    /**
     * 
     * @return
     *     The comments
     */
    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    /**
     * 
     * @param comments
     *     The comments
     */
    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * 
     * @return
     *     The customernumber
     */
    @JsonProperty("customernumber")
    public String getCustomernumber() {
        return customernumber;
    }

    /**
     * 
     * @param customernumber
     *     The customernumber
     */
    @JsonProperty("customernumber")
    public void setCustomernumber(String customernumber) {
        this.customernumber = customernumber;
    }

    /**
     * 
     * @return
     *     The dateevalclosed
     */
    @JsonProperty("dateevalclosed")
    public ActionDate getDateevalclosed() {
        return dateevalclosed;
    }

    /**
     * 
     * @param dateevalclosed
     *     The dateevalclosed
     */
    @JsonProperty("dateevalclosed")
    public void setDateevalclosed(ActionDate dateevalclosed) {
        this.dateevalclosed = dateevalclosed;
    }

    /**
     * 
     * @return
     *     The derivedfrom
     */
    @JsonProperty("derivedfrom")
    public String getDerivedfrom() {
        return derivedfrom;
    }

    /**
     * 
     * @param derivedfrom
     *     The derivedfrom
     */
    @JsonProperty("derivedfrom")
    public void setDerivedfrom(String derivedfrom) {
        this.derivedfrom = derivedfrom;
    }

    /**
     * 
     * @return
     *     The draftparentserial
     */
    @JsonProperty("draftparentserial")
    public String getDraftparentserial() {
        return draftparentserial;
    }

    /**
     * 
     * @param draftparentserial
     *     The draftparentserial
     */
    @JsonProperty("draftparentserial")
    public void setDraftparentserial(String draftparentserial) {
        this.draftparentserial = draftparentserial;
    }

    /**
     * EvalClosed
     * 
     * @return
     *     The evalclosed
     */
    @JsonProperty("evalclosed")
    public Boolean getEvalclosed() {
        return evalclosed;
    }

    /**
     * EvalClosed
     * 
     * @param evalclosed
     *     The evalclosed
     */
    @JsonProperty("evalclosed")
    public void setEvalclosed(Boolean evalclosed) {
        this.evalclosed = evalclosed;
    }

    /**
     * 
     * @return
     *     The guidance
     */
    @JsonProperty("guidance")
    public String getGuidance() {
        return guidance;
    }

    /**
     * 
     * @param guidance
     *     The guidance
     */
    @JsonProperty("guidance")
    public void setGuidance(String guidance) {
        this.guidance = guidance;
    }

    /**
     * 
     * @return
     *     The ipsp
     */
    @JsonProperty("ipsp")
    public String getIpsp() {
        return ipsp;
    }

    /**
     * 
     * @param ipsp
     *     The ipsp
     */
    @JsonProperty("ipsp")
    public void setIpsp(String ipsp) {
        this.ipsp = ipsp;
    }

    /**
     * 
     * @return
     *     The irdt
     */
    @JsonProperty("irdt")
    public ActionDate getIrdt() {
        return irdt;
    }

    /**
     * 
     * @param irdt
     *     The irdt
     */
    @JsonProperty("irdt")
    public void setIrdt(ActionDate irdt) {
        this.irdt = irdt;
    }

    /**
     * 
     * @return
     *     The lasttimeinformationofvalue
     */
    @JsonProperty("lasttimeinformationofvalue")
    public ActionDate getLasttimeinformationofvalue() {
        return lasttimeinformationofvalue;
    }

    /**
     * 
     * @param lasttimeinformationofvalue
     *     The lasttimeinformationofvalue
     */
    @JsonProperty("lasttimeinformationofvalue")
    public void setLasttimeinformationofvalue(ActionDate lasttimeinformationofvalue) {
        this.lasttimeinformationofvalue = lasttimeinformationofvalue;
    }

    /**
     * 
     * @return
     *     The originatoremail
     */
    @JsonProperty("originatoremail")
    public String getOriginatoremail() {
        return originatoremail;
    }

    /**
     * 
     * @param originatoremail
     *     The originatoremail
     */
    @JsonProperty("originatoremail")
    public void setOriginatoremail(String originatoremail) {
        this.originatoremail = originatoremail;
    }

    /**
     * 
     * @return
     *     The originatorphone
     */
    @JsonProperty("originatorphone")
    public String getOriginatorphone() {
        return originatorphone;
    }

    /**
     * 
     * @param originatorphone
     *     The originatorphone
     */
    @JsonProperty("originatorphone")
    public void setOriginatorphone(String originatorphone) {
        this.originatorphone = originatorphone;
    }

    /**
     * 
     * @return
     *     The pass
     */
    @JsonProperty("pass")
    public String getPass() {
        return pass;
    }

    /**
     * 
     * @param pass
     *     The pass
     */
    @JsonProperty("pass")
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * 
     * @return
     *     The poc
     */
    @JsonProperty("poc")
    public String getPoc() {
        return poc;
    }

    /**
     * 
     * @param poc
     *     The poc
     */
    @JsonProperty("poc")
    public void setPoc(String poc) {
        this.poc = poc;
    }

    /**
     * 
     * @return
     *     The reference
     */
    @JsonProperty("reference")
    public String getReference() {
        return reference;
    }

    /**
     * 
     * @param reference
     *     The reference
     */
    @JsonProperty("reference")
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * 
     * @return
     *     The relatedrequirements
     */
    @JsonProperty("relatedrequirements")
    public String getRelatedrequirements() {
        return relatedrequirements;
    }

    /**
     * 
     * @param relatedrequirements
     *     The relatedrequirements
     */
    @JsonProperty("relatedrequirements")
    public void setRelatedrequirements(String relatedrequirements) {
        this.relatedrequirements = relatedrequirements;
    }

    /**
     * 
     * @return
     *     The reliability
     */
    @JsonProperty("reliability")
    public String getReliability() {
        return reliability;
    }

    /**
     * 
     * @param reliability
     *     The reliability
     */
    @JsonProperty("reliability")
    public void setReliability(String reliability) {
        this.reliability = reliability;
    }

    /**
     * 
     * @return
     *     The reliabilitychoice
     */
    @JsonProperty("reliabilitychoice")
    public String getReliabilitychoice() {
        return reliabilitychoice;
    }

    /**
     * 
     * @param reliabilitychoice
     *     The reliabilitychoice
     */
    @JsonProperty("reliabilitychoice")
    public void setReliabilitychoice(String reliabilitychoice) {
        this.reliabilitychoice = reliabilitychoice;
    }

    /**
     * 
     * @return
     *     The reporter
     */
    @JsonProperty("reporter")
    public String getReporter() {
        return reporter;
    }

    /**
     * 
     * @param reporter
     *     The reporter
     */
    @JsonProperty("reporter")
    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    /**
     * 
     * @return
     *     The reporterunit
     */
    @JsonProperty("reporterunit")
    public String getReporterunit() {
        return reporterunit;
    }

    /**
     * 
     * @param reporterunit
     *     The reporterunit
     */
    @JsonProperty("reporterunit")
    public void setReporterunit(String reporterunit) {
        this.reporterunit = reporterunit;
    }

    /**
     * 
     * @return
     *     The reporttext
     */
    @JsonProperty("reporttext")
    public String getReporttext() {
        return reporttext;
    }

    /**
     * 
     * @param reporttext
     *     The reporttext
     */
    @JsonProperty("reporttext")
    public void setReporttext(String reporttext) {
        this.reporttext = reporttext;
    }

    /**
     * 
     * @return
     *     The reportvalue
     */
    @JsonProperty("reportvalue")
    public String getReportvalue() {
        return reportvalue;
    }

    /**
     * 
     * @param reportvalue
     *     The reportvalue
     */
    @JsonProperty("reportvalue")
    public void setReportvalue(String reportvalue) {
        this.reportvalue = reportvalue;
    }

    /**
     * 
     * @return
     *     The responsiveness
     */
    @JsonProperty("responsiveness")
    public String getResponsiveness() {
        return responsiveness;
    }

    /**
     * 
     * @param responsiveness
     *     The responsiveness
     */
    @JsonProperty("responsiveness")
    public void setResponsiveness(String responsiveness) {
        this.responsiveness = responsiveness;
    }

    /**
     * 
     * @return
     *     The sourceparagraph
     */
    @JsonProperty("sourceparagraph")
    public String getSourceparagraph() {
        return sourceparagraph;
    }

    /**
     * 
     * @param sourceparagraph
     *     The sourceparagraph
     */
    @JsonProperty("sourceparagraph")
    public void setSourceparagraph(String sourceparagraph) {
        this.sourceparagraph = sourceparagraph;
    }

    /**
     * 
     * @return
     *     The utility
     */
    @JsonProperty("utility")
    public String getUtility() {
        return utility;
    }

    /**
     * 
     * @param utility
     *     The utility
     */
    @JsonProperty("utility")
    public void setUtility(String utility) {
        this.utility = utility;
    }

    /**
     * 
     * @return
     *     The utilitychoice
     */
    @JsonProperty("utilitychoice")
    public String getUtilitychoice() {
        return utilitychoice;
    }

    /**
     * 
     * @param utilitychoice
     *     The utilitychoice
     */
    @JsonProperty("utilitychoice")
    public void setUtilitychoice(String utilitychoice) {
        this.utilitychoice = utilitychoice;
    }

    /**
     * 
     * @return
     *     The validity
     */
    @JsonProperty("validity")
    public String getValidity() {
        return validity;
    }

    /**
     * 
     * @param validity
     *     The validity
     */
    @JsonProperty("validity")
    public void setValidity(String validity) {
        this.validity = validity;
    }

    /**
     * 
     * @return
     *     The validitychoice
     */
    @JsonProperty("validitychoice")
    public String getValiditychoice() {
        return validitychoice;
    }

    /**
     * 
     * @param validitychoice
     *     The validitychoice
     */
    @JsonProperty("validitychoice")
    public void setValiditychoice(String validitychoice) {
        this.validitychoice = validitychoice;
    }

    /**
     * 
     * @return
     *     The warning
     */
    @JsonProperty("warning")
    public String getWarning() {
        return warning;
    }

    /**
     * 
     * @param warning
     *     The warning
     */
    @JsonProperty("warning")
    public void setWarning(String warning) {
        this.warning = warning;
    }

    /**
     * Humint Eval Report Country specific details
     * 
     * @return
     *     The country
     */
    @JsonProperty("country")
    public List<Country> getCountry() {
        return country;
    }

    /**
     * Humint Eval Report Country specific details
     * 
     * @param country
     *     The country
     */
    @JsonProperty("country")
    public void setCountry(List<Country> country) {
        this.country = country;
    }

    /**
     * Humint Eval Report CountryPriority specific details
     * 
     * @return
     *     The countrypriority
     */
    @JsonProperty("countrypriority")
    public List<Countrypriority> getCountrypriority() {
        return countrypriority;
    }

    /**
     * Humint Eval Report CountryPriority specific details
     * 
     * @param countrypriority
     *     The countrypriority
     */
    @JsonProperty("countrypriority")
    public void setCountrypriority(List<Countrypriority> countrypriority) {
        this.countrypriority = countrypriority;
    }

    /**
     * Humint Eval Report CountryPriority2 specific details
     * 
     * @return
     *     The countrypriority2
     */
    @JsonProperty("countrypriority2")
    public List<Countrypriority2> getCountrypriority2() {
        return countrypriority2;
    }

    /**
     * Humint Eval Report CountryPriority2 specific details
     * 
     * @param countrypriority2
     *     The countrypriority2
     */
    @JsonProperty("countrypriority2")
    public void setCountrypriority2(List<Countrypriority2> countrypriority2) {
        this.countrypriority2 = countrypriority2;
    }

    /**
     * Humint Eval Report CountryPriority3 specific details
     * 
     * @return
     *     The countrypriority3
     */
    @JsonProperty("countrypriority3")
    public List<Countrypriority3> getCountrypriority3() {
        return countrypriority3;
    }

    /**
     * Humint Eval Report CountryPriority3 specific details
     * 
     * @param countrypriority3
     *     The countrypriority3
     */
    @JsonProperty("countrypriority3")
    public void setCountrypriority3(List<Countrypriority3> countrypriority3) {
        this.countrypriority3 = countrypriority3;
    }

    /**
     * Humint Eval Report ProductUse specific details
     * 
     * @return
     *     The productuse
     */
    @JsonProperty("productuse")
    public List<Productuse> getProductuse() {
        return productuse;
    }

    /**
     * Humint Eval Report ProductUse specific details
     * 
     * @param productuse
     *     The productuse
     */
    @JsonProperty("productuse")
    public void setProductuse(List<Productuse> productuse) {
        this.productuse = productuse;
    }

    /**
     * Humint Eval Report Remark specific details
     * 
     * @return
     *     The remark
     */
    @JsonProperty("remark")
    public List<Remark> getRemark() {
        return remark;
    }

    /**
     * Humint Eval Report Remark specific details
     * 
     * @param remark
     *     The remark
     */
    @JsonProperty("remark")
    public void setRemark(List<Remark> remark) {
        this.remark = remark;
    }

    /**
     * Humint Eval Report Source specific details
     * 
     * @return
     *     The source
     */
    @JsonProperty("source")
    public List<HumintSource> getSource() {
        return source;
    }

    /**
     * Humint Eval Report Source specific details
     * 
     * @param source
     *     The source
     */
    @JsonProperty("source")
    public void setSource(List<HumintSource> source) {
        this.source = source;
    }

    /**
     * Humint Eval Report TaskedUnits specific details
     * 
     * @return
     *     The taskedunits
     */
    @JsonProperty("taskedunits")
    public List<Taskedunit> getTaskedunits() {
        return taskedunits;
    }

    /**
     * Humint Eval Report TaskedUnits specific details
     * 
     * @param taskedunits
     *     The taskedunits
     */
    @JsonProperty("taskedunits")
    public void setTaskedunits(List<Taskedunit> taskedunits) {
        this.taskedunits = taskedunits;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(additionalproductscreated).append(cite).append(comments).append(customernumber).append(dateevalclosed).append(derivedfrom).append(draftparentserial).append(evalclosed).append(guidance).append(ipsp).append(irdt).append(lasttimeinformationofvalue).append(originatoremail).append(originatorphone).append(pass).append(poc).append(reference).append(relatedrequirements).append(reliability).append(reliabilitychoice).append(reporter).append(reporterunit).append(reporttext).append(reportvalue).append(responsiveness).append(sourceparagraph).append(utility).append(utilitychoice).append(validity).append(validitychoice).append(warning).append(country).append(countrypriority).append(countrypriority2).append(countrypriority3).append(productuse).append(remark).append(source).append(taskedunits).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof EvalDetails) == false) {
            return false;
        }
        EvalDetails rhs = ((EvalDetails) other);
        return new EqualsBuilder().append(additionalproductscreated, rhs.additionalproductscreated).append(cite, rhs.cite).append(comments, rhs.comments).append(customernumber, rhs.customernumber).append(dateevalclosed, rhs.dateevalclosed).append(derivedfrom, rhs.derivedfrom).append(draftparentserial, rhs.draftparentserial).append(evalclosed, rhs.evalclosed).append(guidance, rhs.guidance).append(ipsp, rhs.ipsp).append(irdt, rhs.irdt).append(lasttimeinformationofvalue, rhs.lasttimeinformationofvalue).append(originatoremail, rhs.originatoremail).append(originatorphone, rhs.originatorphone).append(pass, rhs.pass).append(poc, rhs.poc).append(reference, rhs.reference).append(relatedrequirements, rhs.relatedrequirements).append(reliability, rhs.reliability).append(reliabilitychoice, rhs.reliabilitychoice).append(reporter, rhs.reporter).append(reporterunit, rhs.reporterunit).append(reporttext, rhs.reporttext).append(reportvalue, rhs.reportvalue).append(responsiveness, rhs.responsiveness).append(sourceparagraph, rhs.sourceparagraph).append(utility, rhs.utility).append(utilitychoice, rhs.utilitychoice).append(validity, rhs.validity).append(validitychoice, rhs.validitychoice).append(warning, rhs.warning).append(country, rhs.country).append(countrypriority, rhs.countrypriority).append(countrypriority2, rhs.countrypriority2).append(countrypriority3, rhs.countrypriority3).append(productuse, rhs.productuse).append(remark, rhs.remark).append(source, rhs.source).append(taskedunits, rhs.taskedunits).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
