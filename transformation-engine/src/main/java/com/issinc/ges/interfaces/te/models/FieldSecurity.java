/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "objectValue",
        "security"
})
public class FieldSecurity {

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("objectValue")
    private String objectValue;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("security")
    private Security security;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * (Required)
     *
     * @return
     * The objectValue
     */
    @JsonProperty("objectValue")
    public String getObjectValue() {
        return objectValue;
    }

    /**
     *
     * (Required)
     *
     * @param objectValue
     * The objectValue
     */
    @JsonProperty("objectValue")
    public void setObjectValue(String objectValue) {
        this.objectValue = objectValue;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The security
     */
    @JsonProperty("security")
    public Security getSecurity() {
        return security;
    }

    /**
     *
     * (Required)
     *
     * @param security
     * The security
     */
    @JsonProperty("security")
    public void setSecurity(Security security) {
        this.security = security;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
