/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.components;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.utilities.VersionDefinitionUtilities;
import com.issinc.ges.reporting.dal.DALException;
import com.issinc.ges.reporting.dal.DALFactory;
import com.issinc.ges.reporting.dal.DALResult;
import com.issinc.ges.reporting.dal.DalJsonUtils;
import com.issinc.ges.reporting.dal.transformationengine.IRulesetRepo;

/**
 * The VersionTree is a tree of VersionDefinition objects with each branch representing a ruleset to convert between
 * versions. A VersionTree represents the versions related to s specific version. It is not a web of all version
 * mappings.
 */
public class VersionTree {

    private Node root;
    private ArrayList<String> versionIdArray = new ArrayList<>();

    /**
     * Create a VersionTree for the specified version ID
     * @param versionId The version id
     * @param token The security token
     * @throws TransformationException if an error occurs
     */
    public VersionTree(String versionId, String token) throws TransformationException {
        root = new Node();
        root.versionDefinition = VersionDefinitionUtilities.getVersionDefinition(versionId, token);
        versionIdArray.add(versionId);
        loadVersionTree(root, versionId, token);
    }

    public final Node getRoot() {
        return root;
    }

    /**
     * Get the children of the specified parentNode
     * @param parentNode The parent parentNode
     * @return a list of child nodes
     */
    public final List<Node> getChildren(Node parentNode) {
        return parentNode.getChildren();
    }

    // This builds a tree structure containing all of the
    // versions to which we could possibly convert given the specified versionObjectId
    private void loadVersionTree(Node rootNode, String versionObjectId, String token) throws TransformationException {
        if (versionObjectId != null) {
            addChildren(rootNode, token);
        }
    }

    // The recursive part of the versionDefinition tree crawler (loadVersionTree)
    private void addChildren(Node node, String token) throws TransformationException {

        if (node == null) {
            return;
        }
        try {
            final IRulesetRepo rulesetRepo = DALFactory.get(IRulesetRepo.class);
            final DALResult dalResult =
                    rulesetRepo.getRelatedVersionIds(node.getVersionDefinition().getObjectId(), token);
            final JsonNode resultNode = DalJsonUtils.unWrapResultsAsJsonNode(dalResult.getResult());
            if (resultNode.isArray()) {
                for (int i = 0; i < resultNode.size(); i++) {
                    final String
                            relatedVersionObjectId = resultNode.get(i).get("destinationVersionObjectId").asText();
                    if (!versionIdArray.contains(relatedVersionObjectId)) {
                        versionIdArray.add(relatedVersionObjectId);
                        final Node child = node.addChild(
                                VersionDefinitionUtilities.getVersionDefinition(relatedVersionObjectId, token));
                        addChildren(child, token);
                    }
                }
            }
        } catch (DALException e) {
            throw new TransformationException(
                    TransformationException.ErrorType.UNKNOWN_ERROR,
                    "addChildren: Error retrieving related version IDs from the DAL ruleset collection", e);
        }

    }

    /**
     * Recurse through the version tree until the given Version Definition is found
     * @param node The current node in the tree
     * @param versionDefinition the version we are looking for
     * @return The found node or null if it is not found
     */
    private Node find(Node node, VersionDefinition versionDefinition) {

        if (versionDefinition == null) {
            return null;
        }

        if (node.getVersionDefinition().equals(versionDefinition)) {
            return node;
        }

        for (Node child : node.getChildren()) {
            if (child.getVersionDefinition().equals(versionDefinition)) {
                return child;
            }
            find(child, versionDefinition);
        }

        return null;
    }

    /**
     * A node in the version tree. Nodes keep track of the Version Definition, parent and children of each version in
     * the tree
     */
    public class Node {
        private VersionDefinition versionDefinition;
        private Node parent;
        private List<Node> children = new ArrayList<>();

        /**
         * The default constructor
         */
        public Node() {
        }

        public final List<Node> getChildren() {
            return children;
        }

        /**
         * Set the children for the specified node
         * @param children The list of children
         */
        public final void setChildren(ArrayList<VersionDefinition> children) {
            for (VersionDefinition vd : children) {
                this.addChild(vd);
            }
        }

        public final Node getParent() {
            return parent;
        }

        public final void setParent(Node parent) {
            this.parent = parent;
        }

        /**
         * Add this version as a child
         * @param childVersionDefinition The version definition for the new child
         * @return The new child node
         */
        public final Node addChild(VersionDefinition childVersionDefinition) {
            final Node child = new Node();
            child.parent = this;
            child.setVersionDefinition(childVersionDefinition);
            children.add(child);
            return child;
        }

        public final VersionDefinition getVersionDefinition() {
            return versionDefinition;
        }

        public final void setVersionDefinition(VersionDefinition versionDefinition) {
            this.versionDefinition = versionDefinition;
        }

    }

} /* End of Class VersionTree */
