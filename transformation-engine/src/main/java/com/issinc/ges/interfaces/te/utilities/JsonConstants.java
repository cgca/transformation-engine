/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.utilities;

/**
 * A central place to define the JSON field name constants
 */
public final class JsonConstants {

    public static final String DATA_MODEL = "datamodel";
    public static final String TE_DATA_MODEL_ID =
            "http://interfaces.dae.smil.mil/schemas/transformationengine/queryParameter";
    public static final String PROPERTIES = "properties";
    public static final String BASE = "base";
    public static final String DETAILS = "details";
    public static final String TWO_X_ALIAS = "fieldAlias2x";
    public static final String FOUR_X_MAPPING = "fourXmapping";
    public static final String OBJECT_ID = "objectId";
    public static final String REPORT_TYPE = "reportType";
    public static final String VERSION = "version";
    public static final String MODULE = "module";
    public static final String TYPE = "type";
    public static final String ENCODED_REFERENCE = "__ref";
    public static final String DECODED_REFERENCE = "$ref";
    public static final String ALIASES = "aliases";
    public static final String ID = "id";
    public static final String DEFINITIONS_DATE_TIME = "#/definitions/dateTime";
    public static final String ZULU_DATE_TIME = "zuluDateTime";
    public static final String OBJECT_HISTORY = "objectHistory";
    public static final String ACTION_DATE = "actionDate";

    public static final String ASSOCIATION_RELATIONSHIP_KEY = "RelationshipKey";
    public static final String ASSOCIATION_TARGET_NAME = "targetname";
    public static final String ASSOCIATION_NAME = "associationName";
    public static final String ASSOCIATION_DTG_POSTED = "DTGPosted";
    public static final String ASSOCIATION_DTG_UPDATED = "DTGUpdated";
    public static final String ASSOCIATION_GROUP_NAME = "groupname";
    public static final String ASSOCIATION_GROUP = "group";
    public static final String ASSOCIATION_PARENT_GROUP = "parentGroup";
    public static final String ASSOCIATION_ORIGINATING_NATION = "originatingnation";
    public static final String ASSOCIATION_NATIONALITY = "nationality";
    public static final String ASSOCIATION_ORIGINATING_SITE = "originatingsite";
    public static final String ASSOCIATION_SITE = "site";
    public static final String ASSOCIATION_ORIGINATING_SYSTEM = "originatingSystem";
    public static final String ASSOCIATION_SYSTEM = "system";
    public static final String ASSOCIATION_POSTED_BY = "postedBy";
    public static final String ASSOCIATION_USER = "user";
    public static final String ASSOCIATION_ORIGINATING_NETWORK = "originatingnetwork";
    public static final String ASSOCIATION_NETWORK = "network";
    public static final String ASSOCIATION_DTG_START = "DTGStart";
    public static final String ASSOCIATION_DTG_END = "DTGEnd";
    public static final String ASSOCIATION_START_DATE = "startDate";
    public static final String ASSOCIATION_END_DATE = "endDate";
    public static final String ASSOCIATION_ORIGINATING_DATE = "originatingDate";
    public static final String ASSOCIATION_DOI = "DOI";
    public static final String ASSOCIATION_SOURCE_DOI = "sourceDoi";
    public static final String ASSOCIATION_ASSOCIATION_REPORT_ID = "associationReportID";
    public static final String ASSOCIATION_ASSOCIATED_OBJECT = "associatedObject";
    public static final String ASSOCIATION_TARGET_ROLE = "targetrole";
    public static final String ASSOCIATION_ASSOCIATED_ROLE = "associatedRole";
    public static final String ASSOCIATION_ORIGINATING_GROUP = "originatingGroup";
    public static final String ASSOCIATION_ORIGINATING_USER = "originatingUser";
    public static final String ASSOCIATION_REPORT_NUMBER = "reportNumber";
    public static final String ASSOCIATION_SOURCE_IDENTIFIER = "sourceIdentifier";
    public static final String ASSOCIATION_SOURCE_TYPE = "sourceType";
    public static final String ASSOCIATION_SOURCE = "source";
    public static final String ASSOCIATION_SOURCE_ROLE = "sourceRole";
    public static final String ASSOCIATION_KEY_ASSOCIATION = "keyassociation";
    public static final String ASSOCIATION_IS_KEY_ASSOCIATION = "isKeyAssociation";
    public static final String ASSOCIATION_RECORD_DATE_TIMESTAMP = "Recorddatetimestamp";
    public static final String ASSOCIATION_OBJECT_SEQUENCE = "objectSequence";

    public static final String LOCATION = "location";
    public static final String LOCATION_LOCATION_KEY = "locationkey";
    public static final String LOCATION_ADDRESS = "address";
    public static final String LOCATION_ALTERNATE_ROUTE = "alternateRoute";
    public static final String LOCATION_AO = "ao";
    public static final String LOCATION_BASECAMP = "basecamp";
    public static final String LOCATION_CITY = "city";
    public static final String LOCATION_CONFIDENCE = "confidence";
    public static final String LOCATION_COUNTRY = "country";
    public static final String LOCATION_DATE_END_LOCATION = "dateEndLocation";
    public static final String LOCATION_DATE_START_LOCATION = "dateStartLocation";
    public static final String LOCATION_DESCRIPTION = "description";
    public static final String LOCATION_DISTANCE_FROM_CITY_CENTER = "distanceFromCityCenter";
    public static final String LOCATION_DISTANCE_TO_NEAREST_FOB = "distanceToNearestFOB";
    public static final String LOCATION_DISTANCE_TO_NEAREST_ROUTE = "distanceToNearestRoute";
    public static final String LOCATION_DISTRICT = "district";
    public static final String LOCATION_FOB = "fob";
    public static final String LOCATION_IS_PRIMARY = "isPrimaryLocation";
    public static final String LOCATION_PRIMARY_FLAG = "PrimaryLocationFlag";
    public static final String LOCATION_LATITUDE = "latitude";
    public static final String LOCATION_ACTIVE = "locationActive";
    public static final String LOCATION_TYPE = "locationType";
    public static final String LOCATION_LONGITUDE = "longitude";
    public static final String LOCATION_MSC_CURRENT = "mscCurrent";
    public static final String LOCATION_MSC_DATE_OFCURRENT = "mscDateOfCurrent";
    public static final String LOCATION_MSC = "msc";
    public static final String LOCATION_NAI = "nai";
    public static final String LOCATION_NEAREST_CITY = "nearestCity";
    public static final String LOCATION_NEAREST_FOB = "nearestFOB";
    public static final String LOCATION_NEAREST_ROUTE = "nearestRoute";
    public static final String LOCATION_NEIGHBORHOOD = "neighborhood";
    public static final String LOCATION_POPULATION_CENTER_TYPE = "populationCenterType";
    public static final String LOCATION_POPULATION_CENTER = "populationCenter";
    public static final String LOCATION_PROVINCE = "province";
    public static final String LOCATION_ROUTE = "route";
    public static final String LOCATION_STREET = "street";
    public static final String LOCATION_STRUCTURE = "structure";
    public static final String LOCATION_VILLAGE = "village";
    public static final String LOCATION_ZONE = "zone";

    public static final String CAPCO_CLASSIFICATION = "ClassificationValue";
    public static final String CAPCO_RELEASABILITY = "ClassificationReleasabilityMarkValue";

    private JsonConstants() {
    }

} /* End of Class JsonConstants */
