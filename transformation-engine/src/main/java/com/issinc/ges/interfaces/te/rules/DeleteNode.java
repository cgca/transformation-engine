/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.components.SingleNodeRule;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;

/**
 * Deletes a node from the JSON tree based on the rule configuration of oldFieldPath
 */
public class DeleteNode extends SingleNodeRule {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DeleteNode.class);

    /**
     * The default constructor
     */
    public DeleteNode() {
        super(false);
    }

    @Override
    protected final void doAction(JsonNode node, String fieldName) {
        final JsonEditor je = new JsonEditor(node);
        final JsonNode cutNode = je.cut(fieldName);
        if (cutNode == null) {
            logger.debug("Node could not be deleted as it was not found on the path " + getPath());
        }
    }

}
