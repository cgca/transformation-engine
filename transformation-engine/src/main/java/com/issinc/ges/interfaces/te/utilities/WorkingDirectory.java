/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.utilities;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WorkingDirectory {

    private static final Logger logger = LoggerFactory.getLogger(WorkingDirectory.class);

    private File workingDir = null;

    public WorkingDirectory() {
        createWorkingDirectory();
    }

    public void delete() {
        if (workingDir != null) {
            deleteDirectory(workingDir);
        }
    }

    public File getDirectory() {
        return workingDir;
    }

    public String getName() {
        return getDirectory().getName();
    }

    public String getPath() {
        return workingDir == null ? null : workingDir.getPath();
    }

    private void createWorkingDirectory() {
        File dir = new File("working");
        String workingDirectoryName = dir.getAbsolutePath() + "_" + System.currentTimeMillis();
        workingDir = new File(workingDirectoryName);
        workingDir.mkdir();
        if (!workingDir.canWrite()) {
            logger.error("Error creating the working directory [" + workingDirectoryName + "]");
            workingDir = null;
        }
    }

    private boolean deleteDirectory(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDirectory(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }
} /* End of Class WorkingDirectory */
