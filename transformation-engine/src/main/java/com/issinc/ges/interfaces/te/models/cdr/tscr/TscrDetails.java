
package com.issinc.ges.interfaces.te.models.cdr.tscr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.issinc.ges.interfaces.te.models.ActionDate;
import com.issinc.ges.interfaces.te.models.cdr.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * Humint TSCR Report details specific details
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "background",
    "cite",
    "comments",
    "daterequirementclosed",
    "derivedfrom",
    "expirationdate",
    "iirdistribution",
    "ipsp",
    "nhcd",
    "originatoremail",
    "originatornumber",
    "originatorphone",
    "pass",
    "poc",
    "priority",
    "reference",
    "releasability",
    "requirement",
    "requirementclosed",
    "serial",
    "subject",
    "sugcoll",
    "summary",
    "supersedes",
    "country",
    "countrypriority",
    "countrypriority2",
    "countrypriority3",
    "prodtask",
    "remark",
    "taskedunits"
})
public class TscrDetails implements IcrDetails {

    @JsonProperty("background")
    private String background;
    @JsonProperty("cite")
    private String cite;
    @JsonProperty("comments")
    private String comments;
    /**
     * 
     */
    @JsonProperty("daterequirementclosed")
    private ActionDate daterequirementclosed;
    @JsonProperty("derivedfrom")
    private String derivedfrom;
    /**
     * 
     */
    @JsonProperty("expirationdate")
    private ActionDate expirationdate;
    @JsonProperty("iirdistribution")
    private String iirdistribution;
    @JsonProperty("ipsp")
    private String ipsp;
    @JsonProperty("nhcd")
    private String nhcd;
    @JsonProperty("originatoremail")
    private String originatoremail;
    @JsonProperty("originatornumber")
    private String originatornumber;
    @JsonProperty("originatorphone")
    private String originatorphone;
    @JsonProperty("pass")
    private String pass;
    @JsonProperty("poc")
    private String poc;
    @JsonProperty("priority")
    private Integer priority;
    @JsonProperty("reference")
    private String reference;
    @JsonProperty("releasability")
    private String releasability;
    @JsonProperty("requirement")
    private String requirement;
    /**
     * RequirementClosed
     * 
     */
    @JsonProperty("requirementclosed")
    private Boolean requirementclosed;
    @JsonProperty("serial")
    private String serial;
    @JsonProperty("subject")
    private String subject;
    @JsonProperty("sugcoll")
    private String sugcoll;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("supersedes")
    private String supersedes;
    /**
     * Humint TSCR Report Country specific details
     * 
     */
    @JsonProperty("country")
    private List<Country> country = new ArrayList<Country>();
    /**
     * Humint TSCR Report CountryPriority specific details
     * 
     */
    @JsonProperty("countrypriority")
    private List<Countrypriority> countrypriority = new ArrayList<Countrypriority>();
    /**
     * Humint TSCR Report CountryPriority2 specific details
     * 
     */
    @JsonProperty("countrypriority2")
    private List<Countrypriority2> countrypriority2 = new ArrayList<Countrypriority2>();
    /**
     * Humint TSCR Report CountryPriority3 specific details
     * 
     */
    @JsonProperty("countrypriority3")
    private List<Countrypriority3> countrypriority3 = new ArrayList<Countrypriority3>();
    /**
     * Humint TSCR Report ProdTask specific details
     * 
     */
    @JsonProperty("prodtask")
    private List<Prodtask> prodtask = new ArrayList<Prodtask>();
    /**
     * Humint TSCR Report Remark specific details
     * 
     */
    @JsonProperty("remark")
    private List<Remark> remark = new ArrayList<Remark>();
    /**
     * Humint TSCR Report TaskedUnits specific details
     * 
     */
    @JsonProperty("taskedunits")
    private List<Taskedunit> taskedunits = new ArrayList<Taskedunit>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public TscrDetails() {
    }

    /**
     * 
     * @param summary
     * @param supersedes
     * @param remark
     * @param expirationdate
     * @param subject
     * @param nhcd
     * @param daterequirementclosed
     * @param taskedunits
     * @param priority
     * @param requirementclosed
     * @param originatornumber
     * @param countrypriority3
     * @param poc
     * @param countrypriority2
     * @param iirdistribution
     * @param ipsp
     * @param requirement
     * @param prodtask
     * @param reference
     * @param releasability
     * @param country
     * @param pass
     * @param originatoremail
     * @param cite
     * @param derivedfrom
     * @param originatorphone
     * @param countrypriority
     * @param background
     * @param serial
     * @param comments
     * @param sugcoll
     */
    public TscrDetails(String background, String cite, String comments, ActionDate daterequirementclosed, String derivedfrom, ActionDate expirationdate, String iirdistribution, String ipsp, String nhcd, String originatoremail, String originatornumber, String originatorphone, String pass, String poc, Integer priority, String reference, String releasability, String requirement, Boolean requirementclosed, String serial, String subject, String sugcoll, String summary, String supersedes, List<Country> country, List<Countrypriority> countrypriority, List<Countrypriority2> countrypriority2, List<Countrypriority3> countrypriority3, List<Prodtask> prodtask, List<Remark> remark, List<Taskedunit> taskedunits) {
        this.background = background;
        this.cite = cite;
        this.comments = comments;
        this.daterequirementclosed = daterequirementclosed;
        this.derivedfrom = derivedfrom;
        this.expirationdate = expirationdate;
        this.iirdistribution = iirdistribution;
        this.ipsp = ipsp;
        this.nhcd = nhcd;
        this.originatoremail = originatoremail;
        this.originatornumber = originatornumber;
        this.originatorphone = originatorphone;
        this.pass = pass;
        this.poc = poc;
        this.priority = priority;
        this.reference = reference;
        this.releasability = releasability;
        this.requirement = requirement;
        this.requirementclosed = requirementclosed;
        this.serial = serial;
        this.subject = subject;
        this.sugcoll = sugcoll;
        this.summary = summary;
        this.supersedes = supersedes;
        this.country = country;
        this.countrypriority = countrypriority;
        this.countrypriority2 = countrypriority2;
        this.countrypriority3 = countrypriority3;
        this.prodtask = prodtask;
        this.remark = remark;
        this.taskedunits = taskedunits;
    }

    /**
     * 
     * @return
     *     The background
     */
    @JsonProperty("background")
    public String getBackground() {
        return background;
    }

    /**
     * 
     * @param background
     *     The background
     */
    @JsonProperty("background")
    public void setBackground(String background) {
        this.background = background;
    }

    /**
     * 
     * @return
     *     The cite
     */
    @JsonProperty("cite")
    public String getCite() {
        return cite;
    }

    /**
     * 
     * @param cite
     *     The cite
     */
    @JsonProperty("cite")
    public void setCite(String cite) {
        this.cite = cite;
    }

    /**
     * 
     * @return
     *     The comments
     */
    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    /**
     * 
     * @param comments
     *     The comments
     */
    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * 
     * @return
     *     The daterequirementclosed
     */
    @JsonProperty("daterequirementclosed")
    public ActionDate getDaterequirementclosed() {
        return daterequirementclosed;
    }

    /**
     * 
     * @param daterequirementclosed
     *     The daterequirementclosed
     */
    @JsonProperty("daterequirementclosed")
    public void setDaterequirementclosed(ActionDate daterequirementclosed) {
        this.daterequirementclosed = daterequirementclosed;
    }

    /**
     * 
     * @return
     *     The derivedfrom
     */
    @JsonProperty("derivedfrom")
    public String getDerivedfrom() {
        return derivedfrom;
    }

    /**
     * 
     * @param derivedfrom
     *     The derivedfrom
     */
    @JsonProperty("derivedfrom")
    public void setDerivedfrom(String derivedfrom) {
        this.derivedfrom = derivedfrom;
    }

    /**
     * 
     * @return
     *     The expirationdate
     */
    @JsonProperty("expirationdate")
    public ActionDate getExpirationdate() {
        return expirationdate;
    }

    /**
     * 
     * @param expirationdate
     *     The expirationdate
     */
    @JsonProperty("expirationdate")
    public void setExpirationdate(ActionDate expirationdate) {
        this.expirationdate = expirationdate;
    }

    /**
     * 
     * @return
     *     The iirdistribution
     */
    @JsonProperty("iirdistribution")
    public String getIirdistribution() {
        return iirdistribution;
    }

    /**
     * 
     * @param iirdistribution
     *     The iirdistribution
     */
    @JsonProperty("iirdistribution")
    public void setIirdistribution(String iirdistribution) {
        this.iirdistribution = iirdistribution;
    }

    /**
     * 
     * @return
     *     The ipsp
     */
    @JsonProperty("ipsp")
    public String getIpsp() {
        return ipsp;
    }

    /**
     * 
     * @param ipsp
     *     The ipsp
     */
    @JsonProperty("ipsp")
    public void setIpsp(String ipsp) {
        this.ipsp = ipsp;
    }

    /**
     * 
     * @return
     *     The nhcd
     */
    @JsonProperty("nhcd")
    public String getNhcd() {
        return nhcd;
    }

    /**
     * 
     * @param nhcd
     *     The nhcd
     */
    @JsonProperty("nhcd")
    public void setNhcd(String nhcd) {
        this.nhcd = nhcd;
    }

    /**
     * 
     * @return
     *     The originatoremail
     */
    @JsonProperty("originatoremail")
    public String getOriginatoremail() {
        return originatoremail;
    }

    /**
     * 
     * @param originatoremail
     *     The originatoremail
     */
    @JsonProperty("originatoremail")
    public void setOriginatoremail(String originatoremail) {
        this.originatoremail = originatoremail;
    }

    /**
     * 
     * @return
     *     The originatornumber
     */
    @JsonProperty("originatornumber")
    public String getOriginatornumber() {
        return originatornumber;
    }

    /**
     * 
     * @param originatornumber
     *     The originatornumber
     */
    @JsonProperty("originatornumber")
    public void setOriginatornumber(String originatornumber) {
        this.originatornumber = originatornumber;
    }

    /**
     * 
     * @return
     *     The originatorphone
     */
    @JsonProperty("originatorphone")
    public String getOriginatorphone() {
        return originatorphone;
    }

    /**
     * 
     * @param originatorphone
     *     The originatorphone
     */
    @JsonProperty("originatorphone")
    public void setOriginatorphone(String originatorphone) {
        this.originatorphone = originatorphone;
    }

    /**
     * 
     * @return
     *     The pass
     */
    @JsonProperty("pass")
    public String getPass() {
        return pass;
    }

    /**
     * 
     * @param pass
     *     The pass
     */
    @JsonProperty("pass")
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * 
     * @return
     *     The poc
     */
    @JsonProperty("poc")
    public String getPoc() {
        return poc;
    }

    /**
     * 
     * @param poc
     *     The poc
     */
    @JsonProperty("poc")
    public void setPoc(String poc) {
        this.poc = poc;
    }

    /**
     * 
     * @return
     *     The priority
     */
    @JsonProperty("priority")
    public Integer getPriority() {
        return priority;
    }

    /**
     * 
     * @param priority
     *     The priority
     */
    @JsonProperty("priority")
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * 
     * @return
     *     The reference
     */
    @JsonProperty("reference")
    public String getReference() {
        return reference;
    }

    /**
     * 
     * @param reference
     *     The reference
     */
    @JsonProperty("reference")
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * 
     * @return
     *     The releasability
     */
    @JsonProperty("releasability")
    public String getReleasability() {
        return releasability;
    }

    /**
     * 
     * @param releasability
     *     The releasability
     */
    @JsonProperty("releasability")
    public void setReleasability(String releasability) {
        this.releasability = releasability;
    }

    /**
     * 
     * @return
     *     The requirement
     */
    @JsonProperty("requirement")
    public String getRequirement() {
        return requirement;
    }

    /**
     * 
     * @param requirement
     *     The requirement
     */
    @JsonProperty("requirement")
    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    /**
     * RequirementClosed
     * 
     * @return
     *     The requirementclosed
     */
    @JsonProperty("requirementclosed")
    public Boolean getRequirementclosed() {
        return requirementclosed;
    }

    /**
     * RequirementClosed
     * 
     * @param requirementclosed
     *     The requirementclosed
     */
    @JsonProperty("requirementclosed")
    public void setRequirementclosed(Boolean requirementclosed) {
        this.requirementclosed = requirementclosed;
    }

    /**
     * 
     * @return
     *     The serial
     */
    @JsonProperty("serial")
    public String getSerial() {
        return serial;
    }

    /**
     * 
     * @param serial
     *     The serial
     */
    @JsonProperty("serial")
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * 
     * @return
     *     The subject
     */
    @JsonProperty("subject")
    public String getSubject() {
        return subject;
    }

    /**
     * 
     * @param subject
     *     The subject
     */
    @JsonProperty("subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * 
     * @return
     *     The sugcoll
     */
    @JsonProperty("sugcoll")
    public String getSugcoll() {
        return sugcoll;
    }

    /**
     * 
     * @param sugcoll
     *     The sugcoll
     */
    @JsonProperty("sugcoll")
    public void setSugcoll(String sugcoll) {
        this.sugcoll = sugcoll;
    }

    /**
     * 
     * @return
     *     The summary
     */
    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    /**
     * 
     * @param summary
     *     The summary
     */
    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * 
     * @return
     *     The supersedes
     */
    @JsonProperty("supersedes")
    public String getSupersedes() {
        return supersedes;
    }

    /**
     * 
     * @param supersedes
     *     The supersedes
     */
    @JsonProperty("supersedes")
    public void setSupersedes(String supersedes) {
        this.supersedes = supersedes;
    }

    /**
     * Humint TSCR Report Country specific details
     * 
     * @return
     *     The country
     */
    @JsonProperty("country")
    public List<Country> getCountry() {
        return country;
    }

    /**
     * Humint TSCR Report Country specific details
     * 
     * @param country
     *     The country
     */
    @JsonProperty("country")
    public void setCountry(List<Country> country) {
        this.country = country;
    }

    /**
     * Humint TSCR Report CountryPriority specific details
     * 
     * @return
     *     The countrypriority
     */
    @JsonProperty("countrypriority")
    public List<Countrypriority> getCountrypriority() {
        return countrypriority;
    }

    /**
     * Humint TSCR Report CountryPriority specific details
     * 
     * @param countrypriority
     *     The countrypriority
     */
    @JsonProperty("countrypriority")
    public void setCountrypriority(List<Countrypriority> countrypriority) {
        this.countrypriority = countrypriority;
    }

    /**
     * Humint TSCR Report CountryPriority2 specific details
     * 
     * @return
     *     The countrypriority2
     */
    @JsonProperty("countrypriority2")
    public List<Countrypriority2> getCountrypriority2() {
        return countrypriority2;
    }

    /**
     * Humint TSCR Report CountryPriority2 specific details
     * 
     * @param countrypriority2
     *     The countrypriority2
     */
    @JsonProperty("countrypriority2")
    public void setCountrypriority2(List<Countrypriority2> countrypriority2) {
        this.countrypriority2 = countrypriority2;
    }

    /**
     * Humint TSCR Report CountryPriority3 specific details
     * 
     * @return
     *     The countrypriority3
     */
    @JsonProperty("countrypriority3")
    public List<Countrypriority3> getCountrypriority3() {
        return countrypriority3;
    }

    /**
     * Humint TSCR Report CountryPriority3 specific details
     * 
     * @param countrypriority3
     *     The countrypriority3
     */
    @JsonProperty("countrypriority3")
    public void setCountrypriority3(List<Countrypriority3> countrypriority3) {
        this.countrypriority3 = countrypriority3;
    }

    /**
     * Humint TSCR Report ProdTask specific details
     * 
     * @return
     *     The prodtask
     */
    @JsonProperty("prodtask")
    public List<Prodtask> getProdtask() {
        return prodtask;
    }

    /**
     * Humint TSCR Report ProdTask specific details
     * 
     * @param prodtask
     *     The prodtask
     */
    @JsonProperty("prodtask")
    public void setProdtask(List<Prodtask> prodtask) {
        this.prodtask = prodtask;
    }

    /**
     * Humint TSCR Report Remark specific details
     * 
     * @return
     *     The remark
     */
    @JsonProperty("remark")
    public List<Remark> getRemark() {
        return remark;
    }

    /**
     * Humint TSCR Report Remark specific details
     * 
     * @param remark
     *     The remark
     */
    @JsonProperty("remark")
    public void setRemark(List<Remark> remark) {
        this.remark = remark;
    }

    /**
     * Humint TSCR Report TaskedUnits specific details
     * 
     * @return
     *     The taskedunits
     */
    @JsonProperty("taskedunits")
    public List<Taskedunit> getTaskedunits() {
        return taskedunits;
    }

    /**
     * Humint TSCR Report TaskedUnits specific details
     * 
     * @param taskedunits
     *     The taskedunits
     */
    @JsonProperty("taskedunits")
    public void setTaskedunits(List<Taskedunit> taskedunits) {
        this.taskedunits = taskedunits;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(background).append(cite).append(comments).append(daterequirementclosed).append(derivedfrom).append(expirationdate).append(iirdistribution).append(ipsp).append(nhcd).append(originatoremail).append(originatornumber).append(originatorphone).append(pass).append(poc).append(priority).append(reference).append(releasability).append(requirement).append(requirementclosed).append(serial).append(subject).append(sugcoll).append(summary).append(supersedes).append(country).append(countrypriority).append(countrypriority2).append(countrypriority3).append(prodtask).append(remark).append(taskedunits).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TscrDetails) == false) {
            return false;
        }
        TscrDetails rhs = ((TscrDetails) other);
        return new EqualsBuilder().append(background, rhs.background).append(cite, rhs.cite).append(comments, rhs.comments).append(daterequirementclosed, rhs.daterequirementclosed).append(derivedfrom, rhs.derivedfrom).append(expirationdate, rhs.expirationdate).append(iirdistribution, rhs.iirdistribution).append(ipsp, rhs.ipsp).append(nhcd, rhs.nhcd).append(originatoremail, rhs.originatoremail).append(originatornumber, rhs.originatornumber).append(originatorphone, rhs.originatorphone).append(pass, rhs.pass).append(poc, rhs.poc).append(priority, rhs.priority).append(reference, rhs.reference).append(releasability, rhs.releasability).append(requirement, rhs.requirement).append(requirementclosed, rhs.requirementclosed).append(serial, rhs.serial).append(subject, rhs.subject).append(sugcoll, rhs.sugcoll).append(summary, rhs.summary).append(supersedes, rhs.supersedes).append(country, rhs.country).append(countrypriority, rhs.countrypriority).append(countrypriority2, rhs.countrypriority2).append(countrypriority3, rhs.countrypriority3).append(prodtask, rhs.prodtask).append(remark, rhs.remark).append(taskedunits, rhs.taskedunits).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
