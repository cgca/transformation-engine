/*
* Copyright (c) 2015 Intelligent Software Solutions, Inc.
* Unpublished-all rights reserved under the copyright laws of the United States.
*
* This software was developed under sponsorship from
* the CSC/42Six under:
* xxx xxx-xxxx-xxxx
*
* Contractor: Intelligent Software Solutions, Inc.
* 5450 Tech Center Drive, Suite 400
* Colorado Springs, 80919
* http://www.issinc.com
*
* Intelligent Software Solutions, Inc. has title to the rights in this computer
* software. The Government's rights to use, modify, reproduce, release, perform,
* display, or disclose these technical data are restricted by paragraph (b)(1) of
* the Rights in Technical Data-Noncommercial Items clause contained in
* Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
* Any reproduction of technical data or portions thereof marked with this legend
* must also reproduce the markings.
*
* Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
* aforementioned unlimited government rights to use, disclose, copy, or make
* derivative works of this software to parties outside the Government.
*/

package com.issinc.ges.interfaces.te.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Convenience methods for working with JSON
 */
public final class JsonUtils {

    public static final String REGEX_DOT = "\\.";

    private JsonUtils() {

    }

    /**
     * Checks to see if the JSON has a given element path. Returns true if it does and false if not.
     *
     * @param jsonNode - The JSON node tree to check
     * @param path - The path that we want to check (e.g. "base.currentObjectState")
     * @return True if the path exists. False otherwise.
     */
    public static boolean hasNonNull(JsonNode jsonNode, String path) {
        final String[] elements = path.split(REGEX_DOT);
        for (String elementName : elements) {
            if (jsonNode.hasNonNull(elementName)) {
                jsonNode = jsonNode.get(elementName);
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks to see if the JSON has a given element path. Returns the node if it exists and null if not.
     *
     * @param root - The JSON node tree to check
     * @param path - The path that we want to check (e.g. "base.currentObjectState")
     * @return The JsonNode if the path exists. Null otherwise.
     */
    public static JsonNode getNodeAtPath(JsonNode root, String path) {
        JsonNode result = root;
        final String[] elements = path.split(REGEX_DOT);
        for (String elementName : elements) {
            if (result.hasNonNull(elementName)) {
                result = result.get(elementName);
            } else {
                return null;
            }
        }
        return result;
    }

    /**
     * Retrieves the field name from the path which is the
     * first section in the path.
     *
     * @param path
     *   String from which to retrieve the fieldName
     * @return
     *   FieldName
     */
    public static String getFieldName(String path) {
        String retVal;

        if (path.contains(".")) {
            retVal = path.substring(0, path.indexOf("."));
        } else {
            retVal = path;
        }

        return retVal;
    }

    /**
     * Finds any nodes that match the specified path and returns them.  There could be more than one if there are one
     * or more arrays in the path spec.
     *
     * @param node - The node at which to start the search.
     * @param path The full path path to the node, including the name of the field (e.g. "base.objectId")
     * @return Returns a list of all of the nodes in the tree that match the given path.
     */
    public static ArrayList<JsonNode> find(JsonNode node, String path) {

        if (path == null || path.isEmpty() || node == null) {
            return new ArrayList<>();
        } else {
            return find(node, path.split(REGEX_DOT));
        }
    }

    /**
     * Finds any nodes that match the specified path and returns them.  There could be more than one if there are one
     * or more arrays in the path spec.
     *
     * @param node - The node at which to start the search.
     * @param fieldNames Variable length list of Strings
     *                     (e.g. the path segments to reportTitle are ["base", "reportTitle", "objectValue"]).
     * @return Returns a list of nodes. If none are found, returns an empty list.
     */
    private static ArrayList<JsonNode> find(JsonNode node, String... fieldNames) {

        final ArrayList<JsonNode> results = new ArrayList<>();

        final int length = fieldNames.length;
        for (int x = 0; x < length; x++) {
            final String fieldName = fieldNames[x];
            if (node.hasNonNull(fieldName)) {
                node = node.get(fieldName);
                if (x == length - 1) {
                    results.add(node);
                } else if (node.isArray()) {
                    for (JsonNode nodeFromArray : node) {
                        results.addAll(find(nodeFromArray, Arrays.copyOfRange(fieldNames, x + 1, length)));
                    }
                }
            }
        }

        return results;
    }

}
