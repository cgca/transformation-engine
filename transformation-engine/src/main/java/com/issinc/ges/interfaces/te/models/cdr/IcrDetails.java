/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.models.cdr;

import com.issinc.ges.interfaces.te.models.ActionDate;
import java.util.List;

/**
 * Common interface for ICR report details
 */
 public interface IcrDetails {

     String getBackground() ;
     void setBackground(String background) ;

     String getCite();
     void setCite(String cite);

     String getComments();
     void setComments(String comments);

     ActionDate getDaterequirementclosed();
     void setDaterequirementclosed(ActionDate daterequirementclosed);

     String getDerivedfrom();
     void setDerivedfrom(String derivedfrom);

     ActionDate getExpirationdate() ;
     void setExpirationdate(ActionDate expirationdate);

     String getIirdistribution();
     void setIirdistribution(String iirdistribution);

     String getIpsp();
     void setIpsp(String ipsp);

     String getNhcd();
     void setNhcd(String nhcd);

     String getOriginatoremail();
     void setOriginatoremail(String originatoremail);

     String getOriginatornumber();
     void setOriginatornumber(String originatornumber);

     String getOriginatorphone();
     void setOriginatorphone(String originatorphone);

     String getPass();
     void setPass(String pass);

     String getPoc();
     void setPoc(String poc);

     Integer getPriority() ;
     void setPriority(Integer priority);

     String getReference();
     void setReference(String reference);

     String getReleasability();
     void setReleasability(String releasability);

     String getRequirement();
     void setRequirement(String requirement);

     Boolean getRequirementclosed();
     void setRequirementclosed(Boolean requirementclosed);

     String getSerial();
     void setSerial(String serial);

     String getSubject() ;
     void setSubject(String subject);

     String getSugcoll();
     void setSugcoll(String sugcoll);

     String getSummary();
     void setSummary(String summary);

     String getSupersedes() ;
     void setSupersedes(String supersedes);

     List<Country> getCountry();
     void setCountry(List<Country> countries);

     List<Countrypriority> getCountrypriority() ;
     void setCountrypriority(List<Countrypriority> countrypriorities);

     List<Countrypriority2> getCountrypriority2() ;
     void setCountrypriority2(List<Countrypriority2> countrypriorities);


     List<Countrypriority3> getCountrypriority3();
     void setCountrypriority3(List<Countrypriority3> countrypriorities);



     List<Prodtask> getProdtask();
     void setProdtask(List<Prodtask> prodtasks);

     List<Remark> getRemark();
     void setRemark(List<Remark> remarks);

     List<Taskedunit> getTaskedunits();
     void setTaskedunits(List<Taskedunit> taskedunits);


}
