/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.commons.utils.ObjectMapperProvider;

public class FileUtilities {

    private static final Logger logger = LoggerFactory.getLogger(FileUtilities.class);

    public static String getWorkingFileName(String extension) {
        return getWorkingFileName("DEAI-Transformer-", extension);
    }

    public static String getWorkingFileName(String prefix, String extension) {
        if (prefix == null) {
            prefix = "";
        }
        if (extension == null) {
            extension = ".tmp";
        }
        File dir = new File("convert/.");
        String timeStamp = Long.toString(System.currentTimeMillis());
        String startPath = dir.getAbsolutePath();
        final StringBuilder fileNameSB = new StringBuilder(startPath.substring(0, startPath.length() - 1) + prefix +
                System.currentTimeMillis() + "_" + timeStamp);
        if (!extension.startsWith(".")) {
            fileNameSB.append(".");
        }
        return fileNameSB + extension;
    }

    public static boolean createZipArchive(WorkingDirectory workingDirectory, String zipFileName, boolean addEndTags) {
        boolean success = true;

        if (workingDirectory == null ||
                !workingDirectory.getDirectory().exists())
            return false;

        // Create a buffer for reading the files
        byte[] buf = new byte[1024];

        // Create the ZIP file
        InputStream in = null;
        ZipOutputStream out = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(zipFileName));

            // Add the files in the working directory to the archive
            File[] files = workingDirectory.getDirectory().listFiles();

            if (files != null) {
                for (File file : files) {
                    in = new FileInputStream(file);
                    out.putNextEntry(new ZipEntry(file.getName()));

                    // Write the file contents to the ZIP archive
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }

                    out.closeEntry();
                    in.close();
                }
            }
        } catch (Exception e) {
            success = false;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // Do nothing
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // Do nothing
                }
            }
            workingDirectory.delete();
        }
        return success;
    }

    public static void removeWorkingDirectory(WorkingDirectory workingDirectory) {
        if (workingDirectory != null && workingDirectory.getDirectory().exists()) {
            workingDirectory.delete();
        }
    }

    /**
     * Copies the data from one file to another. If the target
     * file exists or the source file does not exist (or is
     * not a file), the operation will not be carried out.
     */
    public static void copyFile(String source, String target) {
        copyFile(new File(source), new File(target));
    }

    public static void copyFile(File source, String target) {
        copyFile(source, new File(target));
    }

    public static void copyFile(String source, File target) {
        copyFile(new File(source), target);
    }

    public static void copyFile(File source, File target) {
        {
            copyFile(source, target, 10240);
        }
    }

    public static void copyFile(File source, File target, int bufsize) {
        if ((source != null) && (source.isFile()) &&
                (target != null) && (!target.exists())) {
            BufferedInputStream in = null;
            BufferedOutputStream out = null;
            try {
                in = new BufferedInputStream(new FileInputStream(source));
                out = new BufferedOutputStream(new FileOutputStream(target));

                int available = in.available();
                //int bufsize   = 10240; // 10 Kb
                byte[] buffer = new byte[bufsize];

                // Copy the data
                while (available > 0) {
                    if (bufsize > available)
                        bufsize = available;
                    in.read(buffer, 0, bufsize);
                    out.write(buffer, 0, bufsize);
                    available = in.available();
                }// while
            }// try
            catch (IOException exc) {
                logger.warn("FileUtils.copyFile IO exception", exc);
            } finally {
                // Make certain the output file is closed
                try {
                    if (out != null)
                        out.close();
                } catch (Exception exc) {
                    logger.warn("FileUtils.copyFile IO exception", exc);
                }
                // Make certain the input file is closed
                try {
                    if (in != null)
                        in.close();
                } catch (Exception exc) {
                    logger.warn("FileUtils.copyFile IO exception", exc);
                }
            }
        }
    }

    public static JsonNode readFile(final String filename) throws IOException {
        InputStream fileIS = null;
        try {
            fileIS = new FileInputStream(filename);
            final String jsonString = IOUtils.toString(fileIS);
            return ObjectMapperProvider.provide().readTree(jsonString);
        } catch (IOException ex){
            String errMsg = "Failed to load file: " + filename;
            logger.error(errMsg, ex);
            throw new RuntimeException(errMsg);
        } finally {
            if (fileIS != null) {
                IOUtils.closeQuietly(fileIS);
            }
        }
    }

    public static boolean writeFile(final String content, final String filename) throws IOException {
        PrintWriter out = null;
        boolean result = false;
        try {
            out = new PrintWriter(filename);
            out.println(content);
            result = true;
        } catch (FileNotFoundException e) {
            logger.warn("Cannot open file for write: " + filename);
        } finally {
            if (out != null) {
                out.close();
            }
        }
        return result;
    }

    public static boolean deleteFile(File file) {
        if (file.isDirectory()) {
            String[] children = file.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteFile(new File(file, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return file.delete();
    }

} /* End of Class FileUtilities */
