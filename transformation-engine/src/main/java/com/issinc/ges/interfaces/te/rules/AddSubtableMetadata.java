package com.issinc.ges.interfaces.te.rules;
/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */
import java.util.ArrayList;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.JsonRule;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;

/**
 * Rule to add metadata to every subtable.  For now the metadata that is included with every subtable is the
 * ReportKey and OriginatingSystem
 */
public class AddSubtableMetadata extends JsonRule {

    public String getSubtablePath() {
        return subtablePath;
    }

    public void setSubtablePath(String subtablePath) {
        this.subtablePath = subtablePath;
    }

    public String getReportKeyPath() {
        return reportKeyPath;
    }

    public void setReportKeyPath(String reportKeyPath) {
        this.reportKeyPath = reportKeyPath;
    }

    public String getOriginatingSystemPath() {
        return originatingSystemPath;
    }

    public void setOriginatingSystemPath(String originatingSystemPath) {
        this.originatingSystemPath = originatingSystemPath;
    }

    private String subtablePath;
    private String reportKeyPath;
    private String originatingSystemPath;


    /**
     * This is where the real content is. Every Rule must implement its own applyRule method to take a particular action
     * on the payload.
     *
     * @param token   Security token
     * @param payload The data to be transformed by the rule
     * @return The transformed payload
     * @throws com.issinc.ges.interfaces.te.TransformationException if an error occurs
     */
    @Override public JsonNode applyRule(String token, JsonNode payload) throws TransformationException {

        //if the subtable array is not empty,  then add reportkey and originating system to each element
        //find the  array node in the payload
        ArrayList<JsonNode> foundNodes = com.issinc.ges.interfaces.te.utilities.JsonUtils.find(payload,subtablePath);
        for(JsonNode fromNode : foundNodes)
        if(fromNode!=null && fromNode instanceof ArrayNode) {
            //copy in the reportKey and originating system values
            CopyNode copyRule = new CopyNode();
            copyRule.setNewFieldPath(subtablePath + ".ReportKey");
            copyRule.setOldFieldPath(reportKeyPath);
            payload = copyRule.applyRule(token, payload);

            CopyNode copyRule2 = new CopyNode();
            copyRule2.setNewFieldPath(subtablePath + ".OriginatingSystem");
            copyRule2.setOldFieldPath(originatingSystemPath);
            payload = copyRule2.applyRule(token, payload);
        }

        return payload;
    }
}
