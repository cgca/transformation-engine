/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.components;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * This is a custom deserializer for the RuleSet class. It is necessary because abstract classes cannot be
 * deserialized using the default deserializer.
 */
public class RuleDeserializer extends StdDeserializer<Rule> {

    private static final Logger logger = LoggerFactory.getLogger(RuleDeserializer.class);

    /**
     * The constructor
     * @param t The type (class) of object to be deserialized
     */
    public RuleDeserializer(Class<Rule> t) {
        super(t);
    }

    @Override
    public final Rule deserialize(JsonParser jsonParser, DeserializationContext ctx) throws IOException {
        final String methodName = "RuleDeserializer.deserialize";

        Rule ruleInstance;
        final JsonNode rule = jsonParser.readValueAsTree();
        final ObjectMapper jsonObjectMapper = new ObjectMapper();
        final JsonNode classNameNode = rule.get("className");
        if (classNameNode == null) {
            ruleInstance = jsonObjectMapper.treeToValue(rule, Rule.class);
        } else {
            final String className = classNameNode.textValue();
            try {
                ruleInstance = (Rule) jsonObjectMapper.treeToValue(rule, Class.forName(className));
            } catch (ClassNotFoundException e) {
                final String message = methodName + ": Error deserializing rule: " + className;
                logger.error(message);
                throw new IOException(message, e);
            }
        }

        return ruleInstance;
    }

} /* End of Class RuleDeserializer */
