/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.rules;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.issinc.ges.commons.utils.ObjectMapperProvider;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.StringRule;

/**
 * This rule converts a JSON string into an XML string
 */
public class JsonStringToXmlString extends StringRule {

    private static final Logger logger = LoggerFactory.getLogger(JsonStringToXmlString.class);

    private String xmlWrapper;

    /**
     * The default constructor
     */
    public JsonStringToXmlString() {
        super();
    }

    public final String getXmlWrapper() {
        return xmlWrapper;
    }

    public final void setXmlWrapper(String xmlWrapper) {
        this.xmlWrapper = xmlWrapper;
    }

    @Override
    public final String applyRule(String token, String jsonString) throws TransformationException {

        final String methodName = "JsonStringToXmlString.applyRule";
        logger.debug(methodName);

        String xmlString;

        final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        final XmlMapper xmlMapper = new XmlMapper(module);

        try {
            final JsonNode jsonNode = ObjectMapperProvider.provide().readTree(jsonString);
            xmlString = xmlMapper.writeValueAsString(jsonNode);

            if (xmlWrapper != null && xmlWrapper.length() > 0) {
                // wrap output as a simple, "<ModuleReport>...</ModuleReport>"
                xmlString = xmlString.replaceFirst("^<ObjectNode.*?>", "<" + getXmlWrapper() + ">");
                xmlString = xmlString.replaceFirst("</ObjectNode>", "</" + getXmlWrapper() + ">");
            }
        } catch (IOException e) {
            throw new TransformationException(
                    TransformationException.ErrorType.PARSING_ERROR,
                    methodName + ": " + jsonString, e);
        }
        return xmlString;
    }

} /* End of Class JsonStringToXmlString */
