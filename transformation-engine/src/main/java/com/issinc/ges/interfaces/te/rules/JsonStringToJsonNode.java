/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.rules;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.commons.utils.ObjectMapperProvider;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.Rule;

/**
 * This rule converts a string containing JSON to a JsonNode by using a mapper to read the tree
 */
public class JsonStringToJsonNode extends Rule {

    private static final Logger logger = LoggerFactory.getLogger(JsonStringToJsonNode.class);

    /**
     * The default constructor
     */
    public JsonStringToJsonNode() {
        super();
    }

    @Override
    public final Object applyRule(String token, Object payload) throws TransformationException {

        final String methodName = "JsonStringToJsonNode.applyRule";
        logger.debug(methodName);

        validatePayload(payload, String.class, methodName);

        JsonNode result;
        final String jsonString = (String) payload;
        try {
            result = ObjectMapperProvider.provide().readTree(jsonString);
        } catch (IOException e) {
            throw new TransformationException(
                    TransformationException.ErrorType.PARSING_ERROR,
                    methodName + ": " + jsonString, e);
        }

        return result;
    }

} /* End of Class JsonStringToJsonNode */
