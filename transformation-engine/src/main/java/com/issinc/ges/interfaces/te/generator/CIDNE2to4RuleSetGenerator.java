/*
* Copyright (c) 2015 Intelligent Software Solutions, Inc.
* Unpublished-all rights reserved under the copyright laws of the United States.
*
* This software was developed under sponsorship from
* the CSC/42Six under:
* xxx xxx-xxxx-xxxx
*
* Contractor: Intelligent Software Solutions, Inc.
* 5450 Tech Center Drive, Suite 400
* Colorado Springs, 80919
* http://www.issinc.com
*
* Intelligent Software Solutions, Inc. has title to the rights in this computer
* software. The Government's rights to use, modify, reproduce, release, perform,
* display, or disclose these technical data are restricted by paragraph (b)(1) of
* the Rights in Technical Data-Noncommercial Items clause contained in
* Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
* Any reproduction of technical data or portions thereof marked with this legend
* must also reproduce the markings.
*
* Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
* aforementioned unlimited government rights to use, disclose, copy, or make
* derivative works of this software to parties outside the Government.
*/
package com.issinc.ges.interfaces.te.generator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.RuleSet;
import com.issinc.ges.interfaces.te.utilities.JsonConstants;
import com.issinc.ges.interfaces.te.utilities.ModelUtils;

/**
 *  Generates the 2x to 4x rulesets from each report schema
 */
public class CIDNE2to4RuleSetGenerator{

    // Formatting for ruleset attributes
    public static final String OBJECT_ID_INSTANCE = "%s-%s-%s-%s-instance";
    public static final String DISPLAY_NAME = "%s %s from %s to %s Report ruleset";
    public static final String VERSION = "CIDNE-%s-%s-%s";

    private static final String RULESET_DATAMODEL =
            "http://interfaces.dae.smil.mil/schemas/transformationengine/ruleset";
    private static final String RULESET_OBJECT_ID = "ruleSetObjectId";
    private static final String RULESET_CLASSNAME = "className";
    private static final String RULE_ATTR_PATH_FIELD = "path";
    private static final String RULE_ATTR_NAME_FIELD = "name";
    private static final String RULE_ATTR_OLD_FIELD = "oldFieldPath";
    private static final String RULE_ATTR_NEW_FIELD = "newFieldPath";
    private static final String RULE_ATTR_CONVERSION_METHOD = "dataConversionMethodName";
    private static final String RULE_ATTR_DEFAULT_VALUE = "defaultValue";
    private static final String RULE_ATTR_CLASS_FIELD = "classificationField";
    private static final String RULE_ATTR_REL_FIELD = "releasabilityField";
    private static final String RULE_ATTR_SEC_BLOCK = "securityBlockPath";
    private static final String RULE_ATTR_CONVERSION_DIR = "conversionDirection";

    private static final String RULE_ADD_NODE = "com.issinc.ges.interfaces.te.rules.AddNode";
    private static final String RULE_DELETE_NODE = "com.issinc.ges.interfaces.te.rules.DeleteNode";
    private static final String RULE_COPY_NODE = "com.issinc.ges.interfaces.te.rules.CopyNode";
    private static final String RULE_JSON_NODE_TO_STR = "com.issinc.ges.interfaces.te.rules.JsonNodeToJsonString";
    private static final String RULE_MOVE_NODE = "com.issinc.ges.interfaces.te.rules.MoveNode";
    private static final String RULE_RENAME_NODE = "com.issinc.ges.interfaces.te.rules.RenameNode";
    private static final String RULE_XML_TO_JSON_NODE = "com.issinc.ges.interfaces.te.rules.XmlStringToJsonNode";
    private static final String RULE_DATE_XML_RULE = "com.issinc.ges.interfaces.te.rules.DateXmlRule";
    private static final String RULE_DELETE_EXTRA_4_NODE = "com.issinc.ges.interfaces.te.rules.DeleteExtra4xNodes";
    private static final String RULE_ADD_DEFAULT_ARRAY = "com.issinc.ges.interfaces.te.rules.AddDefaultArray";
    private static final String RULE_SECURITY_CONVERSION = "com.issinc.ges.interfaces.te.rules.SecurityConversionRule";
    private static final String RULE_ADD_DEFAULT_LOCATION_FIELDS = "com.issinc.ges.interfaces.te.rules.AddDefaultLocationFields";
    private static final String RULE_DELETE_EXTRA_ARRAY_NODES = "com.issinc.ges.interfaces.te.rules.DeleteExtraArrayNodes";

    private static final String RULE_DELETE_235_SECTIONS = "common_2.3.5_4.1_delete_sections";

    private static final String DATAMODEL_IS_SECTION_SECURITY = "isSectionSecurity";
    private static final String DATAMODEL_SECURITY = "security";
    private static final String DATAMODEL_HAS_SECURITY = "hasSecurity";
    private static final String DATAMODEL_IS_LOCATION = "isLocation";
    private static final String DATAMODEL_DETAILS = "details";
    private static final String DATAMODEL_2X_ALIAS = "fieldAlias2x";
    private static final String DATAMODEL_PROPERTIES = "properties";
    private static final String DATAMODEL_BASE = "base";
    private static final String DATAMODEL_2X_CLASSIFICATION_ALIAS = "classAlias2x";
    private static final String DATAMODEL_2X_REL_ALIAS = "relAlias2x";

    private static final String DEF_DATETIME = "#/definitions/dateTime";
    private static final String DEF_DATETIME_REQUIRED = "#/definitions/dateTimeRequired";
    private static final String DEF_TEXTFIELD_LEVEL_REQUIRED = "#/definitions/textFieldLevelRequired";
    private static final String DEF_SECURITY = "#/definitions/security";

    private static final Logger logger = LoggerFactory.getLogger(CIDNE4to2RuleSetGenerator.class);


    /**
     * Default constructor
     */
    public CIDNE2to4RuleSetGenerator() {

    }

    /**
     * Takes in the schema (datamodel) and converts that to a RuleSet.
     *
     * @param datamodel
     *   The schema which is the basis for the rulesets.
     * @param module
     *   The module of the report.
     * @param reportType
     *   The report type of the report.
     * @param srcVersion
     *   The version that is the "from" version for the process the ruleset will
     *   run once requested.
     * @param dstVersion
     *   The version that is the "to" version for the process the ruleset will
     *   run once requested.
     * @return
     *   The generated ruleset
     */
    public final RuleSet generateRuleset(JsonNode datamodel, String module, String reportType,
            String srcVersion, String dstVersion) {

        RuleSet convertedRuleSet = null;

        final JsonNode ruleset = generateRulesetJsonNode(datamodel, module, reportType, srcVersion,
                dstVersion);

        try {
            convertedRuleSet = ModelUtils.toObject(ruleset, RuleSet.class);
        } catch (TransformationException e) {
            logger.error("Could not convert generated rule set", e);
        }

        return convertedRuleSet;
    }

    /**
     * Takes in the schema (datamodel) and converts that to a JsonNode of rules.
     *
     * @param datamodel
     *   The schema which is the basis for the rulesets.
     * @param module
     *   The module of the report.
     * @param reportType
     *   The report type of the report.
     * @param srcVersion
     *   The version that is the "from" version for the process the ruleset will
     *   run once requested.
     * @param dstVersion
     *   The version that is the "to" version for the process the ruleset will
     *   run once requested.
     * @return
     *   The generated JsonNode ruleset
     */
    public final JsonNode generateRulesetJsonNode(JsonNode datamodel, String module, String reportType,
            String srcVersion, String dstVersion) {

        final JsonNode ruleset = initRuleSet(module, reportType, srcVersion, dstVersion);
        final ArrayNode rules = (ArrayNode) ruleset.get("rules");

        //add the datamodel to the rule
        final String dataModelId = datamodel.get("id").asText();
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "datamodel")
                .put(RULE_ATTR_DEFAULT_VALUE, dataModelId));

        //formId is a required element, but doesn't require a value
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "formId")
                .put(RULE_ATTR_DEFAULT_VALUE, ""));


        addBaseRules(rules, datamodel, module, reportType);  // create base node from top-level fields
        addLocationRules(rules);  // the top-level location node

        //add remarks and requirements section
        // (needed for report viewer even if no values later)
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_DEFAULT_ARRAY)
                .put(RULE_ATTR_PATH_FIELD, "remarks" ));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_DEFAULT_ARRAY)
                .put(RULE_ATTR_PATH_FIELD, "requirements" ));

        // Add in the report details...
        final JsonNode detailsNode = datamodel.get("properties").get(DATAMODEL_DETAILS).get("properties");
        if (detailsNode != null) {
            processDetailsSection(rules, detailsNode);
        }


        // Delete the common fields that we know should be deleted
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_OBJECT_ID, RULE_DELETE_235_SECTIONS));

        //Need to delete anything that is not one of the following from the rootNode:
        //datamodel, formId, base, details, location, security, remarks, requirements
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_EXTRA_4_NODE));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_JSON_NODE_TO_STR));

        return ruleset;
    }

    private JsonNode initRuleSet(String module, String reportType, String srcVersion, String dstVersion) {
        final ObjectNode ruleset = JsonNodeFactory.instance.objectNode()
                .put("datamodel", RULESET_DATAMODEL)
                .put("objectId",
                        String.format(OBJECT_ID_INSTANCE, module, reportType, srcVersion, dstVersion).toLowerCase())
                .put("displayName", String.format(DISPLAY_NAME, module, reportType, srcVersion, dstVersion))
                .put("sourceVersionObjectId", String.format(VERSION, srcVersion, module, reportType).toLowerCase())
                .put("destinationVersionObjectId",
                        String.format(VERSION, dstVersion, module, reportType).toLowerCase());

        final ArrayNode rulesArray = JsonNodeFactory.instance.arrayNode();

        //ReportDescription nodes may have CDATA elements that were stripped out
        //make sure to include all elements in the ReportDescription
        rulesArray.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME,
                                "com.issinc.ges.interfaces.te.rules.CombineReportDescription"));

        rulesArray.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_XML_TO_JSON_NODE)
                .put("ruleName", "xmlWrapper")
                .put("ruleValue", module + reportType));

        ruleset.set("rules", rulesArray);

        return ruleset;
    }


    private void addBaseRules(ArrayNode rules, JsonNode datamodel, String module, String reportType) {

        //default base fields
        String reportTitle = "ReportTitle";
        String reportSummaryField = "ReportDescription";
        String reportSerial = "TrackingNumber";
        String reportDate = "ReportDTG";

        //if datamodel provides the correct values for these fields, adjust them
        if (datamodel.get(DATAMODEL_PROPERTIES) != null
                && datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE) != null
                && datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES) != null){

            if (datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES).get("reportDate") != null){
                //get base.reportDate field
                 reportDate =
                        datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(
                                DATAMODEL_PROPERTIES).get("reportDate").get(DATAMODEL_2X_ALIAS).asText();
            }
            if (datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES).get("reportSerial") != null){
                //get base.reportSerial field
                reportSerial =
                        datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(
                                DATAMODEL_PROPERTIES).get("reportSerial").get(DATAMODEL_2X_ALIAS).asText();
            }
            if (datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES).get("reportSummary") != null){
                //get base.reportSummary field
                reportSummaryField =
                        datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(
                                DATAMODEL_PROPERTIES).get("reportSummary").get(DATAMODEL_2X_ALIAS).asText();
            }
            if(datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES).get("reportTitle")!=null){
                //get base.reportTitle field
                reportTitle =
                        datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(
                                DATAMODEL_PROPERTIES).get("reportTitle").get(DATAMODEL_2X_ALIAS).asText();
            }
        }

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "base.reportType" )
                .put(RULE_ATTR_DEFAULT_VALUE, reportType));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "base.module")
                .put(RULE_ATTR_DEFAULT_VALUE, module));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "ReportKey")
                .put(RULE_ATTR_NEW_FIELD, "base.reportId"));

        //NOTE: since there are versions now in 4x,  base.objectId is an
        // autogenerated GUID rather than just the reportId
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "base.objectId")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertRandomUUID"));

        //NOTE: for now this assumes, the report is a published report coming in, if this needs to change,
        // we can modify this node
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "base.currentObjectState.objectState")
                .put(RULE_ATTR_DEFAULT_VALUE, "PUBLISHED"));


        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "base.currentObjectState.nationality")
                .put(RULE_ATTR_DEFAULT_VALUE, "Unknown"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "base.currentObjectState.network")
                .put(RULE_ATTR_DEFAULT_VALUE, "Unknown"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD, "Classification" )
                .put(RULE_ATTR_REL_FIELD,"ClassificationReleasabilityMark")
                .put(RULE_ATTR_SEC_BLOCK, "security")
                .put(RULE_ATTR_CONVERSION_DIR,"2xTo4x"));


        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DatePosted" )
                .put(RULE_ATTR_NEW_FIELD, "base.currentObjectState.actionDate.zuluDateTime")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertToZuluDateTime"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "base.currentObjectState.actionDate.zuluOffset")
                .put(RULE_ATTR_DEFAULT_VALUE, 0));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DisplayIcon")
                .put(RULE_ATTR_NEW_FIELD, "base.reportSymbol"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, reportDate )
                .put(RULE_ATTR_NEW_FIELD, "base.reportDate.zuluDateTime" )
                .put(RULE_ATTR_CONVERSION_METHOD, "convertToZuluDateTime"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH_FIELD, "base.reportDate.zuluOffset")
                .put(RULE_ATTR_DEFAULT_VALUE, 0));


        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, reportSerial)
                .put(RULE_ATTR_NEW_FIELD, "base.reportSerial.objectValue")
                .put(RULE_ATTR_DEFAULT_VALUE,""));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "security.classification")
                .put(RULE_ATTR_NEW_FIELD, "base.reportSerial.security.classification"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "security.ownerProducer")
                .put(RULE_ATTR_NEW_FIELD, "base.reportSerial.security.ownerProducer"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, reportSummaryField)
                .put(RULE_ATTR_NEW_FIELD, "base.reportSummary.objectValue")
                .put(RULE_ATTR_DEFAULT_VALUE,""));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "security.classification")
                .put(RULE_ATTR_NEW_FIELD, "base.reportSummary.security.classification"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "security.ownerProducer")
                .put(RULE_ATTR_NEW_FIELD, "base.reportSummary.security.ownerProducer"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "IsTearLine")
                .put(RULE_ATTR_NEW_FIELD, "base.isTearline")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertToBoolean"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatingSite")
                .put(RULE_ATTR_NEW_FIELD, "base.currentObjectState.site"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatingSystem")
                .put(RULE_ATTR_NEW_FIELD, "base.currentObjectState.system"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorGroup")
                .put(RULE_ATTR_NEW_FIELD, "base.originatingGroup"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorName")
                .put(RULE_ATTR_NEW_FIELD, "base.originatingUser"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorName")
                .put(RULE_ATTR_NEW_FIELD, "base.currentObjectState.user"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorUnit")
                .put(RULE_ATTR_NEW_FIELD, "base.currentObjectState.parentGroup"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, reportTitle)
                .put(RULE_ATTR_NEW_FIELD, "base.reportTitle.objectValue")
                .put(RULE_ATTR_DEFAULT_VALUE,""));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "security.classification")
                .put(RULE_ATTR_NEW_FIELD, "base.reportTitle.security.classification"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "security.ownerProducer")
                .put(RULE_ATTR_NEW_FIELD, "base.reportTitle.security.ownerProducer"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "TheaterFilter")
                .put(RULE_ATTR_NEW_FIELD, "base.theaterFilter"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "UpdatedByGroup")
                .put(RULE_ATTR_NEW_FIELD, "base.currentObjectState.group"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "UpdatedByName")
                .put(RULE_ATTR_NEW_FIELD, "base.currentObjectState.user"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "UpdatedByUnit")
                .put(RULE_ATTR_NEW_FIELD, "base.currentObjectState.parentGroup"));

    }

    private void addLocationRules(ArrayNode rules) {
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "Location")
                .put(RULE_ATTR_NAME_FIELD, "location"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.AO")
                .put(RULE_ATTR_NAME_FIELD, "ao"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD, "location.Classification" )
                .put(RULE_ATTR_REL_FIELD,"location.ClassificationReleasabilityMark")
                .put(RULE_ATTR_SEC_BLOCK, "security")
                .put(RULE_ATTR_CONVERSION_DIR,"2xTo4x"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "location.Country")
                .put(RULE_ATTR_NEW_FIELD, "country"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "location.DatePosted")
                .put(RULE_ATTR_NEW_FIELD, "DateLastViewed"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.District")
                .put(RULE_ATTR_NAME_FIELD, "district"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.FOB")
                .put(RULE_ATTR_NAME_FIELD, "fob"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.Latitude")
                .put(RULE_ATTR_NAME_FIELD, "latitude")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertToNumber"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.LocationActive")
                .put(RULE_ATTR_NAME_FIELD, "locationActive")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertToBoolean"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.LocationKey")
                .put(RULE_ATTR_NAME_FIELD, "objectId"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.Longitude")
                .put(RULE_ATTR_NAME_FIELD, "longitude")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertToNumber"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.MSC")
                .put(RULE_ATTR_NAME_FIELD, "msc"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.NearestCity")
                .put(RULE_ATTR_NAME_FIELD, "nearestCity"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.PrimaryLocationFlag")
                .put(RULE_ATTR_NAME_FIELD, "isPrimaryLocation")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertToBoolean"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, "location.Province")
                .put(RULE_ATTR_NAME_FIELD, "province"));



        //add location array if it doesn't exist
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_DEFAULT_ARRAY)
                .put(RULE_ATTR_PATH_FIELD, "location" ));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_DEFAULT_LOCATION_FIELDS));
    }

    private void processDetailsSection(ArrayNode rules, JsonNode detailsNode) {
        final Iterator<Map.Entry<String, JsonNode>> iterator = detailsNode.fields();
        while (iterator.hasNext()) {
            final Map.Entry<String, JsonNode> entry = iterator.next();
            final JsonNode node = entry.getValue();
            final String nodeKey = entry.getKey();

            // Move each node into the details section
            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                    .put(RULE_ATTR_OLD_FIELD, get2xFieldAlias(node, nodeKey))
                    .put(RULE_ATTR_NEW_FIELD,  "details." + get2xFieldAlias(node, nodeKey)));

            // Now, process the node
            processNode(rules, node, nodeKey, "details." + get2xFieldAlias(node, nodeKey));

        }
    }

    private void processNode(ArrayNode rules, JsonNode node, String nodeName4x, String currentlocation2x) {

        if (isTextFieldLevelRequiredField(node)) {
            handleTextFieldLevelRequiredNode(rules, node, currentlocation2x, nodeName4x);
        }
        else if (isDate(node)) {
            handleDateNode(rules, currentlocation2x, nodeName4x);
        } else if (isLocation(node)) {
            createLocationReference(rules, node, currentlocation2x, nodeName4x);
        } else if (isSectionSecurity(node)) {

            handleSectionSecurityNode(rules,  nodeName4x,
                    get2xClassificationAlias(node, currentlocation2x),
                    get2xReleasabilityAlias(node,currentlocation2x));
        } else if (node.has("type") && node.get("type").asText().equalsIgnoreCase("array")) {

            processArray(rules, node, currentlocation2x);

            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                    .put(RULE_ATTR_PATH_FIELD, currentlocation2x)
                    .put(RULE_ATTR_NAME_FIELD, nodeName4x));

        }
        else if (hasSecurity(node)){
            handleSecurityNode(rules, node, currentlocation2x, nodeName4x);
        }
        else {
            // Not an array or special node - just rename it
            if (isBoolean(node)) {
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                        .put(RULE_ATTR_PATH_FIELD, currentlocation2x)
                        .put(RULE_ATTR_NAME_FIELD, nodeName4x)
                        .put(RULE_ATTR_CONVERSION_METHOD, "convertToBoolean"));
            }
           else if(isNumber(node)){
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                        .put(RULE_ATTR_PATH_FIELD, currentlocation2x)
                        .put(RULE_ATTR_NAME_FIELD, nodeName4x)
                        .put(RULE_ATTR_CONVERSION_METHOD, "convertToNumber"));
            } else{
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                        .put(RULE_ATTR_PATH_FIELD, currentlocation2x)
                        .put(RULE_ATTR_NAME_FIELD, nodeName4x));
            }

        }

    }

    private boolean isBoolean(JsonNode node) {
        return node.has("type") && node.get("type").asText().equalsIgnoreCase("boolean");
    }
    private boolean isNumber(JsonNode node) {
        if(node.has("type") && node.get("type").asText().equalsIgnoreCase("number") ||
                node.has("type") && node.get("type").asText().equalsIgnoreCase("integer") ||
                node.has("$ref") && node.get("$ref").asText().equalsIgnoreCase("#/definitions/positive_int") ||
                node.has("$ref") && node.get("$ref").asText().equalsIgnoreCase("#/definitions/positive_numRequired") ||
                node.has("$ref") && node.get("$ref").asText().equalsIgnoreCase("#/definitions/positive_intRequired") ||
                node.has("$ref") && node.get("$ref").asText().equalsIgnoreCase("#/definitions/positive_num") ||
                node.has("$ref") && node.get("$ref").asText().equalsIgnoreCase("#/definitions/num") ||
                node.has("$ref") && node.get("$ref").asText().equalsIgnoreCase("#/definitions/latitude_dds") ||
                node.has("$ref") && node.get("$ref").asText().equalsIgnoreCase("#/definitions/longitude_dds")
                )
            return true;
        return false;
    }

    private void processArray(ArrayNode rules, JsonNode node, String oldNodeLocation) {
        JsonNode subelementProperties = node.get("items").get("properties");
        final Iterator<Map.Entry<String, JsonNode>> iterator = subelementProperties.fields();
        String possibleNodes ="";
        while (iterator.hasNext()) {
            final Map.Entry<String, JsonNode> entry = iterator.next();
            final JsonNode arrayNode = entry.getValue();
            final String arrayNodeKey = entry.getKey();
            //keep list of node names
            if(!"".equals(possibleNodes)) {
                possibleNodes = possibleNodes + ",";
            }
            possibleNodes = possibleNodes + arrayNodeKey;

            //security node has already been handled
            if (isSecurityField(arrayNode)) {

                if(get2xFieldAlias(arrayNode,arrayNodeKey).equals(arrayNodeKey)){
                    String newNodeLocation = oldNodeLocation;
                    if (oldNodeLocation.contains(".")) {
                        newNodeLocation = oldNodeLocation + "." +
                                oldNodeLocation.substring(oldNodeLocation.lastIndexOf(".") + 1);
                    }

                    handleNodeSecuritySection(rules,
                            newNodeLocation
                            , null, DATAMODEL_SECURITY, true);
                }
                else {
                    handleNodeSecuritySection(rules,
                            oldNodeLocation + "." + get2xFieldAlias(arrayNode, arrayNodeKey)
                            , null, DATAMODEL_SECURITY, true);
               }
            }
            else {
                if (!arrayNodeKey.equals("security")) {
                    processNode(rules, arrayNode, arrayNodeKey,
                            oldNodeLocation + "." + get2xFieldAlias(arrayNode, arrayNodeKey));
                }
            }
        }

        //delete any nodes that aren't in the list of acceptable nodes
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME,RULE_DELETE_EXTRA_ARRAY_NODES)
                .put("arrayPath", oldNodeLocation)
                .put("knownFields", possibleNodes)
                .put(RULE_ATTR_DEFAULT_VALUE,""));
    }

    private String get2xFieldAlias(JsonNode node, String defaultValue) {
        if (node.has(DATAMODEL_2X_ALIAS)) {
            return node.get(DATAMODEL_2X_ALIAS).asText();
        } else {
            return defaultValue;
        }
    }
    private String get2xClassificationAlias(JsonNode node, String fieldPath) {
        //strip off the name and replace with new alias
        String strippedPath ="";
        int pathIdx = fieldPath.lastIndexOf(".");
        if(pathIdx>0){
            strippedPath=fieldPath.substring(0,pathIdx);
        }

        if (node.has(DATAMODEL_2X_CLASSIFICATION_ALIAS)) {
            return strippedPath +"."+ node.get(DATAMODEL_2X_CLASSIFICATION_ALIAS).asText();
        } else {
            return fieldPath + "Classification";
        }
    }
    private String get2xReleasabilityAlias(JsonNode node, String fieldPath) {
        //strip off the name and replace with new alias
        String strippedPath ="";
        int pathIdx = fieldPath.lastIndexOf(".");
        if(pathIdx>0){
            strippedPath=fieldPath.substring(0,pathIdx);
        }

        if (node.has(DATAMODEL_2X_REL_ALIAS)) {
            return strippedPath +"."+ node.get(DATAMODEL_2X_REL_ALIAS).asText();
        } else {
            return  fieldPath + "ClassificationReleasabilityMark";
        }
    }
    private void  createLocationReference(ArrayNode rules,JsonNode  node, String current2xNodeLocation,
            String nodeName4x){
        //The given node is a location node, therefore, the location information needs to be a "reference" to the
        // locations array rather than the actual value.


        // TODO: Currently, there is not enough information in the datamodel
        // to determine which location reference to use, so we are setting the value to empty string.
        String isLocationText = node.get(DATAMODEL_IS_LOCATION).asText();
        String[] locationFields =  isLocationText.split(",");
        String nodeToRename = locationFields[0];

        String oldpathToUse = current2xNodeLocation.replaceAll(current2xNodeLocation.substring(
                current2xNodeLocation.lastIndexOf(".")), "")+".";

        if (StringUtils.countMatches(oldpathToUse, ".") < 2) {
            oldpathToUse = oldpathToUse.replaceFirst("details.", "");
        }

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH_FIELD, oldpathToUse + nodeToRename)
                .put(RULE_ATTR_NAME_FIELD, nodeName4x)
                .put(RULE_ATTR_DEFAULT_VALUE,""));

        //remove the extra location fields
        for(int i=1;i<locationFields.length;i++){
            rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put(RULE_ATTR_PATH_FIELD, oldpathToUse + locationFields[i]));
        }

    }

    /**
     * Handle the "security" section (may be a custom-named section with "hasSecurity=true").
     * @param rules the ruleset
     * @param oldFieldPath path to the original node
     * @param newArrayLocation (optional) location for the new security fields
     * @param newSecurityPrefix prefix to use for Classification/Releasability fields
     */
    private void handleNodeSecuritySection(ArrayNode rules, String oldFieldPath,
            String newArrayLocation, String newSecurityPrefix, boolean createPath) {
        String newSecurityPath;
        if (newArrayLocation != null) {
            newSecurityPath = newArrayLocation + "." + newSecurityPrefix;
        } else {
            newSecurityPath = newSecurityPrefix;
        }

        //if the first level (not in another node)
        //strip off the "details."  from the oldFieldPath (none of the classification fields are in the
        //datamodel, so they will not have been moved into details section yet)
        String oldPathToUse = oldFieldPath;
        String newpathToUse = "";
        if (StringUtils.countMatches(oldFieldPath, ".") < 2) {
            oldPathToUse = oldFieldPath.replaceFirst("details.", "");
            newpathToUse = oldFieldPath.replaceAll(oldFieldPath.substring(oldFieldPath.lastIndexOf(".")), "")+".";
        }

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD, oldPathToUse + "Classification" )
                .put(RULE_ATTR_REL_FIELD,oldPathToUse + "ClassificationReleasabilityMark")
                .put(RULE_ATTR_SEC_BLOCK, newSecurityPath)
                .put(RULE_ATTR_CONVERSION_DIR,"2xTo4x")
                .put("createPath",createPath));
    }

    private boolean isTextFieldLevelRequiredField(JsonNode node) {
        return node.has(JsonConstants.DECODED_REFERENCE) && node.get(JsonConstants.DECODED_REFERENCE).asText().equalsIgnoreCase(DEF_TEXTFIELD_LEVEL_REQUIRED);
    }
    private boolean isSecurityField(JsonNode node) {
        return node.has(JsonConstants.DECODED_REFERENCE) && node.get(JsonConstants.DECODED_REFERENCE).asText().equalsIgnoreCase(DEF_SECURITY);
    }

    /**
     * Process a "textFieldLevelRequired" node.  These are different than standard fields because
     * the value is in an "objectValue" field, and they have a security.
     * @param rules the list of rules
     * @param node the textFieldLevelRequired node
     * @param oldNodeLocation the path to the node
     */
    private void handleTextFieldLevelRequiredNode(ArrayNode rules, JsonNode node, String oldNodeLocation, String fieldAlias) {

        // NOW, move it to its REAL location...
        //if only one level deep inside details, need to make sure to include the "details."
        String newpathToUse = "";
        if (StringUtils.countMatches(oldNodeLocation, ".") < 2) {
            newpathToUse = "details.";
        }

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, oldNodeLocation)
                .put(RULE_ATTR_NEW_FIELD, newpathToUse + fieldAlias + ".objectValue"));

        if (hasSecurity(node)) {

            handleNodeSecuritySection(rules, oldNodeLocation, null,
                    newpathToUse + fieldAlias + ".security", false);
        }

    }

    private void handleSecurityNode(ArrayNode rules, JsonNode node, String oldNodeLocation, String fieldAlias) {

        // NOW, move it to its REAL location...
        //if only one level deep inside details, need to make sure to include the "details."
        String newpathToUse = "";
        if (StringUtils.countMatches(oldNodeLocation, ".") < 2) {
            newpathToUse = "details.";
        }

        ObjectNode ruleNode = JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, oldNodeLocation)
                .put(RULE_ATTR_NEW_FIELD, newpathToUse + fieldAlias + ".objectValue");
        JsonNode valueNode = node.get("properties").get("objectValue");
        if(valueNode!=null) {
            if (isBoolean(valueNode)) {
                ruleNode.put(RULE_ATTR_CONVERSION_METHOD, "convertToBoolean");
            } else if (isNumber(valueNode)) {
                ruleNode.put(RULE_ATTR_CONVERSION_METHOD, "convertToNumber");
            }
            else if(isDate(valueNode)){
                ruleNode.put(RULE_ATTR_CONVERSION_METHOD, "convertTo4xDateTime");
            }
        }

        rules.add(ruleNode);

        handleNodeSecuritySection(rules, oldNodeLocation, null,
                    newpathToUse + fieldAlias + ".security", false);

    }


    private boolean isDate(JsonNode node) {
        return node.has(JsonConstants.DECODED_REFERENCE) &&
                (node.get(JsonConstants.DECODED_REFERENCE).asText().equals(DEF_DATETIME) ||
                        node.get(JsonConstants.DECODED_REFERENCE).asText().equals(DEF_DATETIME_REQUIRED));
    }

    private boolean hasSecurity(JsonNode node) {
        if (node.has(DATAMODEL_HAS_SECURITY)) {
            return node.get(DATAMODEL_HAS_SECURITY).asBoolean();
        }
        return false;
    }

    private boolean isLocation(JsonNode node) {
        if (node.has(DATAMODEL_IS_LOCATION)) {
            return true;
        }
        return false;
    }

    private boolean isSectionSecurity(JsonNode node) {
        if (node.has(DATAMODEL_IS_SECTION_SECURITY)) {
            return node.get(DATAMODEL_IS_SECTION_SECURITY).asBoolean();
        }
        return false;
    }


    private void handleSectionSecurityNode(ArrayNode rules,
            String newSecurityPrefix, String classifPath, String releabilityPath) {

        //if the first level (not in another node)
        //strip off the "details."  from the oldFieldPath (none of the classification fields are in the
        //datamodel, so they will not have been moved into details section yet)
        String oldClassifPathToUse = classifPath;
        String oldRelPathToUse = releabilityPath;
        String newpathToUse = "";
        if (StringUtils.countMatches(classifPath, ".") ==1) {
            oldClassifPathToUse = classifPath.replaceFirst("details.", "");
            newpathToUse = classifPath.replaceAll(classifPath.substring(classifPath.lastIndexOf(".")), "")+".";
        }
        if (StringUtils.countMatches(releabilityPath, ".") == 1) {
            oldRelPathToUse = releabilityPath.replaceFirst("details.", "");
        }
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD,oldClassifPathToUse )
                .put(RULE_ATTR_REL_FIELD,oldRelPathToUse)
                .put(RULE_ATTR_SEC_BLOCK, newpathToUse + newSecurityPrefix)
                .put(RULE_ATTR_CONVERSION_DIR,"2xTo4x"));
    }

    /**
     * Create the rule(s) for a date field conversion
     * @param rules the rules array to add the rules to
     * @param oldFieldPath src/original date field path
     * @param newFieldName new field name
     */
    private void handleDateNode(ArrayNode rules, String oldFieldPath, String newFieldName) {

        //if only one level deep inside details, need to make sure to include the "details."
        String newpathToUse = "";
        if (StringUtils.countMatches(oldFieldPath, ".") < 2) {
            newpathToUse = "details.";
        }

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, oldFieldPath)
                .put(RULE_ATTR_NEW_FIELD, newpathToUse + newFieldName)
                .put(RULE_ATTR_CONVERSION_METHOD, "convertTo4xDateTime"));


    }


}
