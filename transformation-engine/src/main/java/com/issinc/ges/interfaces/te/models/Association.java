/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * CIDNE Associations schema
 *
 */

@Generated("org.jsonschema2pojo")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "datamodel",
        "objectId",
        "languageId",
        "associationName",
        "associationType",
        "associationConfidence",
        "currentState",
        "objectHistory",
        "security",
        "startDate",
        "endDate",
        "associatedObject1",
        "associatedObject2",
        "associatedRole1",
        "associatedRole2",
        "originatingGroup",
        "originatingUser",
        "originatingDate",
        "sourceIdentifier",
        "sourceType",
        "sourceDOI",
        "sourceConfidence",
        "sourceSecurity",
        "isKeyAssociation",
        "associationLevel",
        "degree",
        "tierType",
        "tier0RelTaxCharacterization",
        "tier1RelTaxCharacterization",
        "tier2RelTaxCharacterization",
        "tier3RelTaxCharacterization",
        "objectSequence"
})
public class Association {

    @JsonProperty("datamodel")
    private String datamodel;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("objectId")
    private String objectId;
    @JsonProperty("languageId")
    private String languageId;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("associationName")
    private String associationName;
    @JsonProperty("associationType")
    private String associationType;
    @JsonProperty("associationConfidence")
    private String associationConfidence;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("currentState")
    private String currentState;
    @JsonProperty("objectHistory")
    private List<ActionInfo> objectHistory = new ArrayList<ActionInfo>();
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("security")
    private Security security;
    /**
     *
     */
    @JsonProperty("startDate")
    private ActionDate startDate;
    /**
     *
     */
    @JsonProperty("endDate")
    private ActionDate endDate;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("associatedObject1")
    private String associatedObject1;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("associatedObject2")
    private String associatedObject2;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("associatedRole1")
    private String associatedRole1;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("associatedRole2")
    private String associatedRole2;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("originatingGroup")
    private String originatingGroup;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("originatingUser")
    private String originatingUser;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("originatingDate")
    private ActionDate originatingDate;
    @JsonProperty("sourceIdentifier")
    private String sourceIdentifier;
    @JsonProperty("sourceType")
    private String sourceType;
    /**
     *
     */
    @JsonProperty("sourceDOI")
    private ActionDate sourceDOI;
    @JsonProperty("sourceConfidence")
    private String sourceConfidence;
    /**
     *
     */
    @JsonProperty("sourceSecurity")
    private Security sourceSecurity;
    /**
     * Determines if a key association
     * (Required)
     *
     */
    @JsonProperty("isKeyAssociation")
    private Boolean isKeyAssociation;
    @JsonProperty("associationLevel")
    private String associationLevel;
    @JsonProperty("degree")
    private String degree;
    @JsonProperty("tierType")
    private String tierType;
    @JsonProperty("tier0RelTaxCharacterization")
    private String tier0RelTaxCharacterization;
    @JsonProperty("tier1RelTaxCharacterization")
    private String tier1RelTaxCharacterization;
    @JsonProperty("tier2RelTaxCharacterization")
    private String tier2RelTaxCharacterization;
    @JsonProperty("tier3RelTaxCharacterization")
    private String tier3RelTaxCharacterization;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("objectSequence")
    private Integer objectSequence;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Association() {
    }

    /**
     *
     * @param associatedObject2
     * @param originatingGroup
     * @param associatedObject1
     * @param startDate
     * @param currentState
     * @param associationConfidence
     * @param endDate
     * @param security
     * @param tier0RelTaxCharacterization
     * @param objectHistory
     * @param objectId
     * @param tier2RelTaxCharacterization
     * @param sourceDOI
     * @param sourceType
     * @param tier1RelTaxCharacterization
     * @param associationType
     * @param objectSequence
     * @param sourceSecurity
     * @param tierType
     * @param languageId
     * @param sourceIdentifier
     * @param associatedRole1
     * @param isKeyAssociation
     * @param associationName
     * @param originatingUser
     * @param degree
     * @param associatedRole2
     * @param originatingDate
     * @param associationLevel
     * @param datamodel
     * @param sourceConfidence
     * @param tier3RelTaxCharacterization
     */
    public Association(String datamodel, String objectId, String languageId, String associationName, String associationType, String associationConfidence, String currentState, List<ActionInfo> objectHistory, Security security, ActionDate startDate, ActionDate endDate, String associatedObject1, String associatedObject2, String associatedRole1, String associatedRole2, String originatingGroup, String originatingUser, ActionDate originatingDate, String sourceIdentifier, String sourceType, ActionDate sourceDOI, String sourceConfidence, Security sourceSecurity, Boolean isKeyAssociation, String associationLevel, String degree, String tierType, String tier0RelTaxCharacterization, String tier1RelTaxCharacterization, String tier2RelTaxCharacterization, String tier3RelTaxCharacterization, Integer objectSequence) {
        this.datamodel = datamodel;
        this.objectId = objectId;
        this.languageId = languageId;
        this.associationName = associationName;
        this.associationType = associationType;
        this.associationConfidence = associationConfidence;
        this.currentState = currentState;
        this.objectHistory = objectHistory;
        this.security = security;
        this.startDate = startDate;
        this.endDate = endDate;
        this.associatedObject1 = associatedObject1;
        this.associatedObject2 = associatedObject2;
        this.associatedRole1 = associatedRole1;
        this.associatedRole2 = associatedRole2;
        this.originatingGroup = originatingGroup;
        this.originatingUser = originatingUser;
        this.originatingDate = originatingDate;
        this.sourceIdentifier = sourceIdentifier;
        this.sourceType = sourceType;
        this.sourceDOI = sourceDOI;
        this.sourceConfidence = sourceConfidence;
        this.sourceSecurity = sourceSecurity;
        this.isKeyAssociation = isKeyAssociation;
        this.associationLevel = associationLevel;
        this.degree = degree;
        this.tierType = tierType;
        this.tier0RelTaxCharacterization = tier0RelTaxCharacterization;
        this.tier1RelTaxCharacterization = tier1RelTaxCharacterization;
        this.tier2RelTaxCharacterization = tier2RelTaxCharacterization;
        this.tier3RelTaxCharacterization = tier3RelTaxCharacterization;
        this.objectSequence = objectSequence;
    }

    /**
     *
     * @return
     * The datamodel
     */
    @JsonProperty("datamodel")
    public String getDatamodel() {
        return datamodel;
    }

    /**
     *
     * @param datamodel
     * The datamodel
     */
    @JsonProperty("datamodel")
    public void setDatamodel(String datamodel) {
        this.datamodel = datamodel;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The objectId
     */
    @JsonProperty("objectId")
    public String getObjectId() {
        return objectId;
    }

    /**
     *
     * (Required)
     *
     * @param objectId
     * The objectId
     */
    @JsonProperty("objectId")
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     *
     * @return
     * The languageId
     */
    @JsonProperty("languageId")
    public String getLanguageId() {
        return languageId;
    }

    /**
     *
     * @param languageId
     * The languageId
     */
    @JsonProperty("languageId")
    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The associationName
     */
    @JsonProperty("associationName")
    public String getAssociationName() {
        return associationName;
    }

    /**
     *
     * (Required)
     *
     * @param associationName
     * The associationName
     */
    @JsonProperty("associationName")
    public void setAssociationName(String associationName) {
        this.associationName = associationName;
    }

    /**
     *
     * @return
     * The associationType
     */
    @JsonProperty("associationType")
    public String getAssociationType() {
        return associationType;
    }

    /**
     *
     * @param associationType
     * The associationType
     */
    @JsonProperty("associationType")
    public void setAssociationType(String associationType) {
        this.associationType = associationType;
    }

    /**
     *
     * @return
     * The associationConfidence
     */
    @JsonProperty("associationConfidence")
    public String getAssociationConfidence() {
        return associationConfidence;
    }

    /**
     *
     * @param associationConfidence
     * The associationConfidence
     */
    @JsonProperty("associationConfidence")
    public void setAssociationConfidence(String associationConfidence) {
        this.associationConfidence = associationConfidence;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The currentState
     */
    @JsonProperty("currentState")
    public String getCurrentState() {
        return currentState;
    }

    /**
     *
     * (Required)
     *
     * @param currentState
     * The currentState
     */
    @JsonProperty("currentState")
    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    /**
     *
     * @return
     * The objectHistory
     */
    @JsonProperty("objectHistory")
    public List<ActionInfo> getObjectHistory() {
        return objectHistory;
    }

    /**
     *
     * @param objectHistory
     * The objectHistory
     */
    @JsonProperty("objectHistory")
    public void setObjectHistory(List<ActionInfo> objectHistory) {
        this.objectHistory = objectHistory;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The security
     */
    @JsonProperty("security")
    public Security getSecurity() {
        return security;
    }

    /**
     *
     * (Required)
     *
     * @param security
     * The security
     */
    @JsonProperty("security")
    public void setSecurity(Security security) {
        this.security = security;
    }

    /**
     *
     * @return
     * The startDate
     */
    @JsonProperty("startDate")
    public ActionDate getStartDate() {
        return startDate;
    }

    /**
     *
     * @param startDate
     * The startDate
     */
    @JsonProperty("startDate")
    public void setStartDate(ActionDate startDate) {
        this.startDate = startDate;
    }

    /**
     *
     * @return
     * The endDate
     */
    @JsonProperty("endDate")
    public ActionDate getEndDate() {
        return endDate;
    }

    /**
     *
     * @param endDate
     * The endDate
     */
    @JsonProperty("endDate")
    public void setEndDate(ActionDate endDate) {
        this.endDate = endDate;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The associatedObject1
     */
    @JsonProperty("associatedObject1")
    public String getAssociatedObject1() {
        return associatedObject1;
    }

    /**
     *
     * (Required)
     *
     * @param associatedObject1
     * The associatedObject1
     */
    @JsonProperty("associatedObject1")
    public void setAssociatedObject1(String associatedObject1) {
        this.associatedObject1 = associatedObject1;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The associatedObject2
     */
    @JsonProperty("associatedObject2")
    public String getAssociatedObject2() {
        return associatedObject2;
    }

    /**
     *
     * (Required)
     *
     * @param associatedObject2
     * The associatedObject2
     */
    @JsonProperty("associatedObject2")
    public void setAssociatedObject2(String associatedObject2) {
        this.associatedObject2 = associatedObject2;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The associatedRole1
     */
    @JsonProperty("associatedRole1")
    public String getAssociatedRole1() {
        return associatedRole1;
    }

    /**
     *
     * (Required)
     *
     * @param associatedRole1
     * The associatedRole1
     */
    @JsonProperty("associatedRole1")
    public void setAssociatedRole1(String associatedRole1) {
        this.associatedRole1 = associatedRole1;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The associatedRole2
     */
    @JsonProperty("associatedRole2")
    public String getAssociatedRole2() {
        return associatedRole2;
    }

    /**
     *
     * (Required)
     *
     * @param associatedRole2
     * The associatedRole2
     */
    @JsonProperty("associatedRole2")
    public void setAssociatedRole2(String associatedRole2) {
        this.associatedRole2 = associatedRole2;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The originatingGroup
     */
    @JsonProperty("originatingGroup")
    public String getOriginatingGroup() {
        return originatingGroup;
    }

    /**
     *
     * (Required)
     *
     * @param originatingGroup
     * The originatingGroup
     */
    @JsonProperty("originatingGroup")
    public void setOriginatingGroup(String originatingGroup) {
        this.originatingGroup = originatingGroup;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The originatingUser
     */
    @JsonProperty("originatingUser")
    public String getOriginatingUser() {
        return originatingUser;
    }

    /**
     *
     * (Required)
     *
     * @param originatingUser
     * The originatingUser
     */
    @JsonProperty("originatingUser")
    public void setOriginatingUser(String originatingUser) {
        this.originatingUser = originatingUser;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The originatingDate
     */
    @JsonProperty("originatingDate")
    public ActionDate getOriginatingDate() {
        return originatingDate;
    }

    /**
     *
     * (Required)
     *
     * @param originatingDate
     * The originatingDate
     */
    @JsonProperty("originatingDate")
    public void setOriginatingDate(ActionDate originatingDate) {
        this.originatingDate = originatingDate;
    }

    /**
     *
     * @return
     * The sourceIdentifier
     */
    @JsonProperty("sourceIdentifier")
    public String getSourceIdentifier() {
        return sourceIdentifier;
    }

    /**
     *
     * @param sourceIdentifier
     * The sourceIdentifier
     */
    @JsonProperty("sourceIdentifier")
    public void setSourceIdentifier(String sourceIdentifier) {
        this.sourceIdentifier = sourceIdentifier;
    }

    /**
     *
     * @return
     * The sourceType
     */
    @JsonProperty("sourceType")
    public String getSourceType() {
        return sourceType;
    }

    /**
     *
     * @param sourceType
     * The sourceType
     */
    @JsonProperty("sourceType")
    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    /**
     *
     * @return
     * The sourceDOI
     */
    @JsonProperty("sourceDOI")
    public ActionDate getSourceDOI() {
        return sourceDOI;
    }

    /**
     *
     * @param sourceDOI
     * The sourceDOI
     */
    @JsonProperty("sourceDOI")
    public void setSourceDOI(ActionDate sourceDOI) {
        this.sourceDOI = sourceDOI;
    }

    /**
     *
     * @return
     * The sourceConfidence
     */
    @JsonProperty("sourceConfidence")
    public String getSourceConfidence() {
        return sourceConfidence;
    }

    /**
     *
     * @param sourceConfidence
     * The sourceConfidence
     */
    @JsonProperty("sourceConfidence")
    public void setSourceConfidence(String sourceConfidence) {
        this.sourceConfidence = sourceConfidence;
    }

    /**
     *
     * @return
     * The sourceSecurity
     */
    @JsonProperty("sourceSecurity")
    public Security getSourceSecurity() {
        return sourceSecurity;
    }

    /**
     *
     * @param sourceSecurity
     * The sourceSecurity
     */
    @JsonProperty("sourceSecurity")
    public void setSourceSecurity(Security sourceSecurity) {
        this.sourceSecurity = sourceSecurity;
    }

    /**
     * Determines if a key association
     * (Required)
     *
     * @return
     * The isKeyAssociation
     */
    @JsonProperty("isKeyAssociation")
    public Boolean getIsKeyAssociation() {
        return isKeyAssociation;
    }

    /**
     * Determines if a key association
     * (Required)
     *
     * @param isKeyAssociation
     * The isKeyAssociation
     */
    @JsonProperty("isKeyAssociation")
    public void setIsKeyAssociation(Boolean isKeyAssociation) {
        this.isKeyAssociation = isKeyAssociation;
    }

    /**
     *
     * @return
     * The associationLevel
     */
    @JsonProperty("associationLevel")
    public String getAssociationLevel() {
        return associationLevel;
    }

    /**
     *
     * @param associationLevel
     * The associationLevel
     */
    @JsonProperty("associationLevel")
    public void setAssociationLevel(String associationLevel) {
        this.associationLevel = associationLevel;
    }

    /**
     *
     * @return
     * The degree
     */
    @JsonProperty("degree")
    public String getDegree() {
        return degree;
    }

    /**
     *
     * @param degree
     * The degree
     */
    @JsonProperty("degree")
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     *
     * @return
     * The tierType
     */
    @JsonProperty("tierType")
    public String getTierType() {
        return tierType;
    }

    /**
     *
     * @param tierType
     * The tierType
     */
    @JsonProperty("tierType")
    public void setTierType(String tierType) {
        this.tierType = tierType;
    }

    /**
     *
     * @return
     * The tier0RelTaxCharacterization
     */
    @JsonProperty("tier0RelTaxCharacterization")
    public String getTier0RelTaxCharacterization() {
        return tier0RelTaxCharacterization;
    }

    /**
     *
     * @param tier0RelTaxCharacterization
     * The tier0RelTaxCharacterization
     */
    @JsonProperty("tier0RelTaxCharacterization")
    public void setTier0RelTaxCharacterization(String tier0RelTaxCharacterization) {
        this.tier0RelTaxCharacterization = tier0RelTaxCharacterization;
    }

    /**
     *
     * @return
     * The tier1RelTaxCharacterization
     */
    @JsonProperty("tier1RelTaxCharacterization")
    public String getTier1RelTaxCharacterization() {
        return tier1RelTaxCharacterization;
    }

    /**
     *
     * @param tier1RelTaxCharacterization
     * The tier1RelTaxCharacterization
     */
    @JsonProperty("tier1RelTaxCharacterization")
    public void setTier1RelTaxCharacterization(String tier1RelTaxCharacterization) {
        this.tier1RelTaxCharacterization = tier1RelTaxCharacterization;
    }

    /**
     *
     * @return
     * The tier2RelTaxCharacterization
     */
    @JsonProperty("tier2RelTaxCharacterization")
    public String getTier2RelTaxCharacterization() {
        return tier2RelTaxCharacterization;
    }

    /**
     *
     * @param tier2RelTaxCharacterization
     * The tier2RelTaxCharacterization
     */
    @JsonProperty("tier2RelTaxCharacterization")
    public void setTier2RelTaxCharacterization(String tier2RelTaxCharacterization) {
        this.tier2RelTaxCharacterization = tier2RelTaxCharacterization;
    }

    /**
     *
     * @return
     * The tier3RelTaxCharacterization
     */
    @JsonProperty("tier3RelTaxCharacterization")
    public String getTier3RelTaxCharacterization() {
        return tier3RelTaxCharacterization;
    }

    /**
     *
     * @param tier3RelTaxCharacterization
     * The tier3RelTaxCharacterization
     */
    @JsonProperty("tier3RelTaxCharacterization")
    public void setTier3RelTaxCharacterization(String tier3RelTaxCharacterization) {
        this.tier3RelTaxCharacterization = tier3RelTaxCharacterization;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The objectSequence
     */
    @JsonProperty("objectSequence")
    public Integer getObjectSequence() {
        return objectSequence;
    }

    /**
     *
     * (Required)
     *
     * @param objectSequence
     * The objectSequence
     */
    @JsonProperty("objectSequence")
    public void setObjectSequence(Integer objectSequence) {
        this.objectSequence = objectSequence;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
