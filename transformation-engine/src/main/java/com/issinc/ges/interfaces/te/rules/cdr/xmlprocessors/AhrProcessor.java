/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules.cdr.xmlprocessors;

import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.models.*;
import com.issinc.ges.interfaces.te.models.cdr.ahr.HumintAhr;
import com.issinc.ges.interfaces.te.models.cdr.ahr.AhrDetails;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;


/**
 * class to help transform CDR/ISM  reports to CIDNE 4.1  JSON strings
 */
public class AhrProcessor extends CdrProcessor {
    private static final String AHR_DATAMODEL = "http://cidne.dae.smil.mil/schemas/Humint/AHR/CIDNEDM235";

    public AhrProcessor(XPath xpath, Document doc) {
        super(xpath, doc);
    }

    @Override
    public String processXML() {
        HumintAhr ahr = new HumintAhr();
        ahr.setDatamodel(AHR_DATAMODEL);
        Security reportsecurity = getSecurity("//*[local-name()='icr']");
        ahr.setSecurity(reportsecurity);
        ahr.setFormId("");


        ReportBase base =  getIcrReportBase();
        ahr.setBase(base);

        //setup ahr details
        AhrDetails details = new AhrDetails();
        setIcrDetails(details, reportsecurity);

        ahr.setDetails(details);
        return JsonUtils.toString(ahr,false);
    }


}
