/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SchemaVersion {

    private String datamodel;

    private String objectId;

    private String legacyVersion;

    private String nativeVersion;

    private boolean isCurrent;

    private String description;

    public String getDatamodel ()
    {
        return datamodel;
    }

    public void setDatamodel (String datamodel)
    {
        this.datamodel = datamodel;
    }

    public String getObjectId ()
    {
        return objectId;
    }

    public void setObjectId (String objectId)
    {
        this.objectId = objectId;
    }

    public String getLegacyVersion()
    {
        return legacyVersion;
    }

    public void setLegacyVersion(String legacyVersion)
    {
        this.legacyVersion = legacyVersion;
    }

    public String getNativeVersion()
    {
        return nativeVersion;
    }

    public void setNativeVersion(String nativeVersion)
    {
        this.nativeVersion = nativeVersion;
    }

    public boolean isCurrent ()
    {
        return isCurrent;
    }

    public void setIsCurrent (boolean isCurrent)
    {
        this.isCurrent = isCurrent;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

} /* End of Class SchemaVersion */
