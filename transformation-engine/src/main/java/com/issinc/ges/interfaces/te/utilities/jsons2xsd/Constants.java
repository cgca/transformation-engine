/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.utilities.jsons2xsd;

/**
 * Constants used in the converter
 */
public interface Constants {

    String JSON_SCHEMA_DEFINITIONS = "#/definitions/";
    String JSON_SCHEMA_REFERENCE = "$ref";

    String TYPE = "type";
    String FORMAT = "format";
    String MAX_ITEMS = "maxItems";
    String UNBOUNDED = "unbounded";
    String MIN_ITEMS = "minItems";
    String MIN_OCCURS = "minOccurs";
    String MAX_OCCURS = "maxOccurs";
    String OBJECT = "object";
    String DEFINITIONS = "definitions";
    String PROPERTIES = "properties";

    String NAME = "name";
    String COMPLEX_TYPE = "complexType";
    String REQUIRED = "required";
    String ELEMENT = "element";
    String REFERENCE = "reference";
    String ARRAY = "array";
    String DECIMAL = "decimal";
    String ENUM = "enum";
    String INT = "int";
    String STRING = "string";
    String MIN_LENGTH = "minLength";
    String MAX_LENGTH = "maxLength";
    String VALUE = "value";
    String PATTERN = "pattern";
    String SIMPLE_TYPE = "simpleType";
    String RESTRICTION = "restriction";
    String BASE = "base";
    String ENUMERATION = "enumeration";
    String MINIMUM = "minimum";
    String MAXIMUM = "maximum";
    String MIN_INCLUSIVE = "minInclusive";
    String MAX_INCLUSIVE = "maxInclusive";
    String ONEOF = "oneOf";
    String ITEMS = "items";
    String ITEM = "item";
    String ZERO = "0";
    String ONE = "1";
    String SCHEMA = "schema";
    String QUALIFIED = "qualified";
    String DATE = "date";
    String TIME = "time";
    String DATETIME = "datetime";
    String BOOLEAN = "boolean";

} /* End of interface Constants */
