/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules;

import java.util.ArrayList;
import java.util.UUID;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.JsonRule;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;
import com.issinc.ges.interfaces.te.utilities.JsonUtils;

/**
 * a location rule developed for MAT which will create location records based off of a specific MAT data node
 */
public class MATLocation extends JsonRule {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MATLocation.class);

    //the path to node with location information (e.g. acloss, atkobs)
    private String fromPath;

    //the path to the latitude field
    private String latitudePath;

    //the path to the longitude field
    private String longitudePath;

    //The path to location ID
    //NOTE: if not provided, a random UUID will be generated
    private String idPath;

    //the location type (e.g. A/C Loss, Attack OBS)
    private String locationType;
    //comma separated paths to each field that makes up the description
    private String descriptionPaths;
    private String format;

    /**
     * The default constructor
     */
    public MATLocation() {
        super();
    }

    /**
     * This is where the real content is. Every Rule must implement its own applyRule method to take a particular action
     * on the payload.
     *
     * @param token   Security token
     * @param payload The data to be transformed by the rule
     * @return The transformed payload
     * @throws com.issinc.ges.interfaces.te.TransformationException if an error occurs
     */
    @Override public JsonNode applyRule(String token, JsonNode payload) throws TransformationException {

        //find the atkobs array node in the payload

        JsonEditor je = new JsonEditor(payload);
        JsonNode fromNode = je.find(fromPath, false);

        if (fromNode instanceof  ArrayNode) {
            //for each object in the array, create a location
            for (int i = 0; i < fromNode.size(); i++) {
                JsonNode item = fromNode.get(i);
                addLocationRecord(payload, item, je);
            }
        }
        else {
            addLocationRecord(payload, fromNode, je);
        }

        return payload;
    }

    private void addLocationRecord(JsonNode payload, JsonNode item,  JsonEditor je){
        //now get the pieces we need for a description field
        String[] descriptionFields = descriptionPaths.split(",");

        String[] descriptionItems = new String[descriptionFields.length];
        for (int i = 0; i < descriptionFields.length; i++) {
            if (item.has(descriptionFields[i]) && item.get(descriptionFields[i]) != null
                    && item.get(descriptionFields[i]).asText() != null) {
                descriptionItems[i] = item.get(descriptionFields[i]).asText();
            }
            else {
                descriptionItems[i] = "";
            }
        }

        //format the description field
        String description = String.format(format, descriptionItems);

        //create a JSON object
        ObjectNode locationNode = JsonNodeFactory.instance.objectNode();
        //create a description node
        locationNode.put("description", description);

        //add locationType
        locationNode.put("locationType", getLocationType());

        //copy latitude node
        ArrayList<JsonNode> latNodes =  JsonUtils.find(item, latitudePath);
        if (latNodes.size()<1) {
            return;
        }
        int latitude = latNodes.get(0).asInt();
        locationNode.put("latitude", latitude);

        //copy longitude node
        ArrayList<JsonNode> lngNodes =  JsonUtils.find(item, longitudePath);
        if (lngNodes.size() < 1) {
            return;
        }
        int longitude = lngNodes.get(0).asInt();
        locationNode.put("longitude", longitude);

        //copy id node to objectId
        if (idPath == null || item.get(idPath) == null) {
            locationNode.put("objectId", UUID.randomUUID().toString());
        }
        else {
            String id = item.get(idPath).asText();
            locationNode.put("objectId", id);
        }

        //this field is required for all location
        locationNode.put("isPrimaryLocation", false);

        //make sure location array exists and if not create it
        if (!payload.has("location")) {
            //location array does not exist
            ArrayNode emptyArray = JsonNodeFactory.instance.arrayNode();
            je.paste("location", emptyArray);
        }

        ArrayNode locationArrayNode = (ArrayNode) payload.get("location");

        //add new json node to location array
        locationArrayNode.add(locationNode);
    }

    public String getFromPath() {
        return fromPath;
    }

    public void setFromPath(String fromPath) {
        this.fromPath = fromPath;
    }

    public String getDescriptionPaths() {
        return descriptionPaths;
    }

    public void setDescriptionPaths(String descriptionPaths) {
        this.descriptionPaths = descriptionPaths;
    }

    public String getLongitudePath() {
        return longitudePath;
    }

    public void setLongitudePath(String longitudePath) {
        this.longitudePath = longitudePath;
    }

    public String getLatitudePath() {
        return latitudePath;
    }

    public void setLatitudePath(String latitudePath) {
        this.latitudePath = latitudePath;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

}
