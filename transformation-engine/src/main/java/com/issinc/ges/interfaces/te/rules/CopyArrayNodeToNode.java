/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules;

import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;
import com.issinc.ges.interfaces.te.utilities.JsonUtils;

/**
 * Copies a single object node in an array, based on filterByPath and filterByValue, to
 * outside the array based on oldFieldPath and newFieldPath.
 */
public class CopyArrayNodeToNode extends CopyNode {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CopyArrayNodeToNode.class);

    private String filterByPath;
    private String filterByValue;
    private String filterByIdPath;

    /**
     * The default constructor
     */
    public CopyArrayNodeToNode() {
        super();
    }

    public final String getFilterByPath() {
        return filterByPath;
    }

    public final void setFilterByPath(String filterByPath) {
        this.filterByPath = filterByPath;
    }

    public final String getFilterByIdPath() {
        return filterByIdPath;
    }

    public final void setFilterByIdPath(String filterByIdPath) {
        this.filterByIdPath = filterByIdPath;
    }

    public final String getFilterByValue() {
        return filterByValue;
    }

    public final void setFilterByValue(String filterByValue) {
        this.filterByValue = filterByValue;
    }

    @Override
    public JsonNode applyRule(String token, JsonNode payload) throws TransformationException {
        final String methodName = "CopyArrayNodeToNode.applyRule";
        logger.debug(methodName);

        if (getFilterByPath() != null && (getFilterByIdPath() != null || getFilterByValue() != null) &&
                getOldFieldPath() != null && getNewFieldPath() != null) {
            String filterValue = getFilterValue(payload);
            if(filterValue!=null) {
                retrieveNode(payload, payload, getOldFieldPath(), filterValue);
            }
        } else {
            throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR, "Rule "
                    + this.getClass().getSimpleName()
                    + " must have filterByPath, filterByValue or filterByIdPath, oldFieldPath, and newFieldPath set.");
        }

        return payload;
    }

    /**
     * Special retrieve; it looks for a specified array object and moves only sub-nodes on that
     * found object.
     *
     * @param workingRoot
     *  Current root to start the node setting from, ie, the
     *  absolute root or the array element root.
     * @param currentNode
     *  The current node where to check for the field name.
     * @param path
     *  The current path.
     */
    /// CHECKSTYLE:OFF
    protected void retrieveNode(JsonNode workingRoot, JsonNode currentNode, String path, String filterValue) {
        /// CHECKSTYLE:ON
        if (workingRoot == null) {
            return;
        }

        final String fieldName = JsonUtils.getFieldName(path);

        if (fieldName.equals(path)) {
            setNode(workingRoot, getNodeForAction(currentNode, fieldName), getNewFieldPath());
        } else {
            final JsonNode nextNode = currentNode.get(fieldName);
            if (nextNode == null) {
                logger.debug("Path " + path + " does not exist");
                return;
            }
            if (nextNode.getNodeType() == JsonNodeType.ARRAY) {
                handleArrayLogic(workingRoot, nextNode, path, filterValue);
            } else {
                retrieveNode(workingRoot, nextNode, path.substring(path.indexOf(".") + 1), filterValue);
            }
        }
    }

    /**
     * Moved the array handling logic to a separate method to get rid of the checkstyle
     * complaints.
     *
     * @param workingRoot
     *   Current root of the JSON tree
     * @param nextNode
     *   Current node.
     * @param path
     *   Current path.
     */
    private void handleArrayLogic(JsonNode workingRoot, JsonNode nextNode, String path, String filterValue) {
        final int cnt = nextNode.size();
        JsonNode foundNode = null;

        for (int i = 0; i < cnt; i++) {
            // Find the node we want to use.
            final JsonNode node = nextNode.get(i);
            final JsonEditor je = new JsonEditor(node);
            final JsonNode filterNode = je.find(getFilterByPath());

            if (filterNode == null) {
                // Must be in a sub-array; move on
                retrieveNode(workingRoot, nextNode, path.substring(path.indexOf(".") + 1), filterValue);
            } else {
                // Found on array, now test for value
                if (filterValue.equals(filterNode.textValue())) {
                    foundNode = node;
                    break;
                }
            }
        }

        if (foundNode != null) {
            retrieveNode(workingRoot, foundNode, path.substring(path.indexOf(".") + 1), filterValue);
        }
    }

    /**
     * Get the filter value either from the path specified by configuration
     * or a direct value specified by configuration.
     *
     * @param root
     *   Root of the JSON tree
     * @return
     *   String value to filter on.
     */
    String getFilterValue(JsonNode root) throws TransformationException {
        String filterValue;

        if (getFilterByIdPath() != null) {
            final JsonEditor je = new JsonEditor(root);
            final JsonNode filterNode = je.find(getFilterByIdPath());
            if (filterNode == null || filterNode.textValue() == null || filterNode.textValue().isEmpty()) {
               return null;
            }

            filterValue = filterNode.textValue();
        } else {
            filterValue = getFilterByValue();
        }

        return filterValue;
    }

}
