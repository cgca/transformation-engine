
package com.issinc.ges.interfaces.te.models.cdr;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "prodtask",
    "referenceid",
    "publishedlevelsubtable"
})
public class Prodtask {

    @JsonProperty("prodtask")
    private String prodtask;
    @JsonProperty("referenceid")
    private String referenceid;
    @JsonProperty("publishedlevelsubtable")
    private Integer publishedlevelsubtable;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Prodtask() {
    }

    /**
     * 
     * @param referenceid
     * @param prodtask
     * @param publishedlevelsubtable
     */
    public Prodtask(String prodtask, String referenceid, Integer publishedlevelsubtable) {
        this.prodtask = prodtask;
        this.referenceid = referenceid;
        this.publishedlevelsubtable = publishedlevelsubtable;
    }

    /**
     * 
     * @return
     *     The prodtask
     */
    @JsonProperty("prodtask")
    public String getProdtask() {
        return prodtask;
    }

    /**
     * 
     * @param prodtask
     *     The prodtask
     */
    @JsonProperty("prodtask")
    public void setProdtask(String prodtask) {
        this.prodtask = prodtask;
    }

    /**
     * 
     * @return
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public String getReferenceid() {
        return referenceid;
    }

    /**
     * 
     * @param referenceid
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public void setReferenceid(String referenceid) {
        this.referenceid = referenceid;
    }

    /**
     * 
     * @return
     *     The publishedlevelsubtable
     */
    @JsonProperty("publishedlevelsubtable")
    public Integer getPublishedlevelsubtable() {
        return publishedlevelsubtable;
    }

    /**
     * 
     * @param publishedlevelsubtable
     *     The publishedlevelsubtable
     */
    @JsonProperty("publishedlevelsubtable")
    public void setPublishedlevelsubtable(Integer publishedlevelsubtable) {
        this.publishedlevelsubtable = publishedlevelsubtable;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(prodtask).append(referenceid).append(publishedlevelsubtable).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Prodtask) == false) {
            return false;
        }
        Prodtask rhs = ((Prodtask) other);
        return new EqualsBuilder().append(prodtask, rhs.prodtask).append(referenceid, rhs.referenceid).append(publishedlevelsubtable, rhs.publishedlevelsubtable).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
