/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.generator;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.TransformationEngine;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.utilities.IdMapper;
import com.issinc.ges.interfaces.te.utilities.JsonConstants;

public class RulesetGenerator {

    private static final Logger logger = LoggerFactory.getLogger(RulesetGenerator.class);

    public static List<JsonNode> buildRuleSets(JsonNode dataModel, String token) {

        List<JsonNode> nodeList = new ArrayList<>();
        TransformationEngine te = TransformationEngine.getInstance();
        if (dataModel.hasNonNull(JsonConstants.ID)) {
            String dataModelId = dataModel.get(JsonConstants.ID).asText();
            // found the id, parse out the module and type
            final IdMapper idMap = new IdMapper(dataModelId);
            final String module = idMap.getModule();
            final String reportType = idMap.getReportType();
            final String schemaVersion = idMap.getVersion();

            // Generate rulesets for report schemas
            if (idMap.isReportDataModel()) {

                // Generate the native to legacy ruleset
                CIDNE4to2RuleSetGenerator generator4to2 = new CIDNE4to2RuleSetGenerator();
                try {
                    JsonNode ruleSet = generator4to2.generateRulesetJsonNode(dataModel, module, reportType,
                            te.getNativeVersion(schemaVersion, token),
                            te.getLegacyVersion(schemaVersion, token));
                    nodeList.add(ruleSet);
                } catch (TransformationException e) {
                    logger.warn("Error creating native to legacy ruleset for " + dataModelId);
                }

                // Generate the legacy to native ruleset
                CIDNE2to4RuleSetGenerator generator2to4 = new CIDNE2to4RuleSetGenerator();
                try {
                    JsonNode ruleSet = generator2to4.generateRulesetJsonNode(dataModel, module, reportType,
                            te.getLegacyVersion(schemaVersion, token),
                            te.getNativeVersion(schemaVersion, token));
                    nodeList.add(ruleSet);
                } catch (TransformationException e) {
                    logger.warn("Error creating legacy to native ruleset for " + dataModelId);
                }
            }
        }

        return nodeList;
    }
} /* End of Class RulesetGenerator */
