/*
 * Copyright (c) 2014 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-D-0022.
 *
 * Contractor: Intelligent Software Solutions
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005 and No. FA8750-09-D-0022.
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */

package com.issinc.ges.interfaces.te.utilities;

import java.io.IOException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.interfaces.te.components.RuleDeserializer;

/**
 * Provide convenience methods for transformation engine POJOs
 */
public final class ModelUtils {
    private static Logger logger = LoggerFactory.getLogger(ModelUtils.class);

    private static final ObjectMapper mapper = new ObjectMapper().
            configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

    /**
     * Default private constructor
     */
    private ModelUtils() {
    }

    static {
        final SimpleModule ruleDeserializationModule = new SimpleModule("RuleDeserializationModule");
        ruleDeserializationModule.addDeserializer(Rule.class, new RuleDeserializer(Rule.class));
        mapper.registerModule(ruleDeserializationModule);
    }

    /**
     * Convert an object to a JSON node
     *
     * @param obj the object to convert
     * @return JSON representation of the object
     * @throws JsonProcessingException failure to convert object to JSON
     */
    public static JsonNode toJson(Object obj) throws JsonProcessingException {
        return mapper.valueToTree(obj);
    }

    /**
     * Converts JSON node to a concrete instance of the Java. If the conversion fails, return null.
     *
     * @param jsonNode  the JSON content
     * @param javaClass the desired Java Class
     * @param <T>       the type of the concrete Java Object
     * @return the Java Object
     * @throws TransformationException if the string cannot be parsed into a Java object
     */
    public static <T> T toObject(final JsonNode jsonNode, Class<T> javaClass) throws TransformationException {
        return toObject(jsonNode.toString(), javaClass);
    }

    /**
     * Convert a map into a JAVA object
     *
     * @param map       the map
     * @param javaClass the desired Java class
     * @param <T>       the type of the concrete Java Object
     * @return the Java Object
     * @throws TransformationException if the string cannot be parsed into a Java object
     */
    public static <T> T toObject(Map<String, Object> map, Class<T> javaClass) throws TransformationException {
        try {
            // convert map to JSON-formatted string
            final String str = mapper.writeValueAsString(map);
            // convert string to java object
            return mapper.readValue(str, javaClass);
        } catch (IOException e) {
            final String message = "toObject(): failed to convert map to Java object of class " + javaClass;
            logger.error(message, e);
            throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR, message, e);
        }
    }

    /**
     * Convert a JSON-formatted string into a Java object
     * @param jsonStr JSON-formatted string
     * @param javaClass the Java class to return
     * @param <T> the type of the concrete Java object
     * @return the Java object
     * @throws TransformationException if the string cannot be parsed into a Java object
     */
    public static <T> T toObject(String jsonStr, Class<T> javaClass) throws TransformationException {
        try {
            return mapper.readValue(jsonStr, javaClass);
        } catch (IOException e) {
            final String message = "toObject(): failed to convert to Java object of class " + javaClass;
            logger.error(message, e);
            throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR, message, e);
        }
    }

    /**
     * Serialize an object to JSON-formatted string
     *
     * @param obj the object to serialize
     * @return JSON-formatted string
     */
    public static String toString(final Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            final String msg = "Failed to serialize Java object: " + e.getMessage();
            logger.error("toString(): " + msg, e);
            return msg;
        }
    }

}
