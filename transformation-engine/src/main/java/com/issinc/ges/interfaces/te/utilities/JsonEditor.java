/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.utilities;

import com.fasterxml.jackson.databind.node.ArrayNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.TransformationException;

/**
 * Utility methods for manipulation nodes of a JSON tree
 */
public class JsonEditor {

    public static final String REGEX_DOT = "\\.";

    private static final Logger logger = LoggerFactory.getLogger(JsonEditor.class);

    private JsonNode root;

    /**
     * Construct a JsonEditor for the JSON string
     * @param json A JSON string to be edited
     * @throws TransformationException if the json string cannot be parsed
     */
    public JsonEditor(String json) throws TransformationException {
        try {
            root = JsonUtils.jsonStringToJsonNode(json);
        } catch (RuntimeException e) {
            throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR,
                    "Error parsing JSON in JsonEditor", e);
        }
    }

    /**
     * Construct a JsonEditor for the JSON node
     * @param jsonNode A JSON tree to be edited
     */
    public JsonEditor(JsonNode jsonNode) {
        root = jsonNode;
    }

    public final String getJson() {
        return JsonUtils.toString(root, false);
    }

    public final JsonNode getRoot() {
        return root;
    }

    /**
     * Cuts the specified field out of the tree at the specified path and returns the cut node
     * @param fieldName The name of the node (branch) to "cut" from the tree
     * @param path The path to the branch (e.g. the path to reportTitle is "base"). If the field is at the top level,
     *             use null for path.
     * @return The node that was cut from the tree. It can then be placed somewhere else in the tree using the
     * "paste" method.
     */
    public final JsonNode cut(String fieldName, String path) {
        final JsonNode parentNode = findLoose(path);
        if (parentNode != null && parentNode.isObject() && parentNode.has(fieldName)) {
            final JsonNode result = parentNode.get(fieldName);
            ((ObjectNode) parentNode).remove(fieldName);
            return result;
        }
        return null;
    }

    /**
     * Takes the full path, including the last node and turns the fullPath
     * argument into the set of arguments needed for the above cut method.
     *
     * @param fullPath
     *   Full path down to the requested node.
     * @return
     *   Cut node.
     */
    public final JsonNode cut(String fullPath) {
        JsonNode node = null;

        if (fullPath != null) {
            final int lastIndexOf = fullPath.lastIndexOf(".");
            if (fullPath.contains(".")) {
                node = cut(fullPath.substring(lastIndexOf + 1), fullPath.substring(0, lastIndexOf));
            } else {
                node = cut(fullPath, null);
            }
        }

        return node;
    }

    /**
     * Pastes a jsonNode into the tree at the designated path location.
     * @param fieldName The name of the field (node) to be pasted into the tree
     * @param fieldValue The node to paste
     * @param path The location within the tree that the node should be pasted
     * @return True if successful, false otherwise
     */
    public final boolean paste(String fieldName, JsonNode fieldValue, String path) {
        boolean success = false;
        final JsonNode parentNode = findLoose(path);
        if (parentNode != null && parentNode.isObject() && fieldValue != null) {
            final JsonNode node = findLoose(path);
            if (node.has(fieldName)) {
                JsonNode existingNode = node.get(fieldName);
                if (existingNode.isArray()) {
                    if(fieldValue.isArray()){
                        ((ArrayNode)existingNode).addAll(((ArrayNode)fieldValue));
                    }else{
                        ((ArrayNode)existingNode).add(fieldValue);
                    }
                } else {
                    logger.warn("This action would overwrite an existing node.");
                }
                success = true;
            }else{
                ((ObjectNode) node).set(fieldName, fieldValue);
                success = true;
            }
        }
        return success;
    }

    /**
     * Takes the full path, including the last node and turns the fullPath
     * argument into the set of arguments needed for the above cut method.
     *
     * @param fullPath
     *   Full path down to the requested node.
     * @param node
     *   Node to be placed
     * @return
     *   Cut node.
     */
    public final boolean paste(String fullPath, JsonNode node) {
        final int lastIndexOf = fullPath.lastIndexOf(".");
        if (fullPath.contains(".")) {
            return paste(fullPath.substring(lastIndexOf + 1), node, fullPath.substring(0, lastIndexOf));
        } else {
            return paste(fullPath, node, null);
        }
    }

    /**
     * FindStrict will only find the node if it already exists in the path.  It will not create the node
     * if the node does not exist.
     *
     * @param path
     *   The path to the branch (e.g. the path to reportTitle is "base"). If the field is at the top level,
     *   use null for path.
     * @return
     *   Returns a node, either existing or null if not found.
     */
    public final JsonNode find(String path) {
        return find(path, false);
    }

    /**
     * FindLoose will find the node or, if the node does not exist for the path,
     * will create the node for the path entry.
     *
     * @param path
     *   The path to the branch (e.g. the path to reportTitle is "base"). If the field is at the top level,
     *   use null for path.
     * @return
     *   Returns a node, either existing or a new node.
     */
    public final JsonNode findLoose(String path) {
        return find(path, true);
    }

    /**
     * Finds the specified node (by path) and returns it.  If the path does not exist, may or may not create the
     * node based upon the doCreateIfNotFound parameter.
     *
     * @param path
     *   The path to the branch (e.g. the path to reportTitle is "base"). If the field is at the top level,
     *   use null for path.
     * @param doCreateIfNotFound
     *   Tells the method whether or not to try to create the node.
     * @return
     *   Returns a node, either created, existing, or null.
     */
    public final JsonNode find(String path, boolean doCreateIfNotFound) {
        JsonNode node = root;

        if (path == null) {
            return node;
        }

        final String[] fieldNames = path.split(REGEX_DOT);
        for (String fieldName : fieldNames) {
            if (node.has(fieldName)) {
                node = node.get(fieldName);
            } else if (doCreateIfNotFound) {
                logger.debug(String.format("Node for path %s not found; creating node %s", path, fieldName));
                node = ((ObjectNode) node).putObject(fieldName);
            } else {
                return null;
            }
        }

        return node;
    }

} /* End of Class JsonEditor */
