/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models.cdr;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "country",
        "referenceid"
})
public class Country {

    @JsonProperty("country")
    private String country;
    @JsonProperty("referenceid")
    private String referenceid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Country() {
    }

    /**
     *
     * @param referenceid
     * @param country
     */
    public Country(String country, String referenceid) {
        this.country = country;
        this.referenceid = referenceid;
    }

    /**
     *
     * @return
     *     The country
     */
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     *     The country
     */
    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public String getReferenceid() {
        return referenceid;
    }

    /**
     *
     * @param referenceid
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public void setReferenceid(String referenceid) {
        this.referenceid = referenceid;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
