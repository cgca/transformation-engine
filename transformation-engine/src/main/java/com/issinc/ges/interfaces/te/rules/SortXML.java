/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.rules;


import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.Rule;


/**
 * Rule to sort 2x XML string alphabetically by keynames
 */
public class SortXML extends Rule {


    @Override
    public final Object applyRule(String token, Object payload) throws TransformationException {

        String returnXML = null;
        String payloadStr = (String)payload;
        DOMParser parser = new DOMParser();
    try{
            parser.parse(new InputSource(new java.io.StringReader(payloadStr)));
            Document doc = parser.getDocument();
            sortXml(doc);
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            returnXML = writer.toString();
        } catch (SAXException | IOException | TransformerException e) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR, "Error sorting XML", e);
        }

        //The transformer adds the XML header of <?xml version="1.0" encoding="UTF-8"?>
        //this needs to be removed.
        if(returnXML!=null) {
            returnXML = returnXML.substring(38);  //remove the XML header
        }

        return returnXML;
    }


    private static void sortXml(Document document)
    {
        sortXml(document.getDocumentElement());
    }

    private static void sortXml(Node rootNode)
    {
        sortElements(rootNode);
        NodeList nodeList = rootNode.getChildNodes();
        for(int i=0;i<nodeList.getLength();i++){

            sortXml(nodeList.item(i));
        }
    }

    private static void sortElements(Node node)
    {
        boolean changed = true;
        while (changed)
        {
            changed = false;
            NodeList nodeList = node.getChildNodes();
            for (int i = 1; i < nodeList.getLength(); i++)
            {
                if(compareNodes(nodeList.item(i), nodeList.item(i - 1))<0){
                    node.insertBefore(nodeList.item(i),nodeList.item(i-1));
                    changed = true;
                }
            }
        }
    }



    private static int compareNodes(Node node1, Node node2){

        //we assume there are no complex objects in the XML
        //our XML arrays will always have at least 2 children, the objectId of the element and any additional fields
        boolean isNode1array = node1.getChildNodes().getLength()>1;
        boolean isNode2array = node2.getChildNodes().getLength()>1;

        if(isNode1array && !isNode2array){
            return 1;
        }

        if(isNode2array && !isNode1array){
            return -1;
        }

        return node1.getNodeName().compareToIgnoreCase(node2.getNodeName());

    }
}
