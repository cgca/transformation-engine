/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */
package com.issinc.ges.interfaces.te.generator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.RuleSet;
import com.issinc.ges.interfaces.te.utilities.JsonConstants;
import com.issinc.ges.interfaces.te.utilities.ModelUtils;


/**
 * Generates the rulesets from each report schema
 */
public class CIDNE4to2RuleSetGenerator {

    // Formatting for ruleset attributes
    public static final String OBJECT_ID_INSTANCE = "%s-%s-%s-%s-instance";
    public static final String DISPLAY_NAME = "%s %s from %s to %s Report ruleset";
    public static final String VERSION = "CIDNE-%s-%s-%s";

    private static final String RULESET_DATAMODEL =
            "http://interfaces.dae.smil.mil/schemas/transformationengine/ruleset";
    private static final String RULESET_OBJECT_ID = "ruleSetObjectId";
    private static final String RULESET_CLASSNAME = "className";

    private static final String RULE_ATTR_OLD_FIELD = "oldFieldPath";
    private static final String RULE_ATTR_NEW_FIELD = "newFieldPath";
    private static final String RULE_ATTR_CONVERSION_METHOD = "dataConversionMethodName";
    private static final String RULE_ATTR_DEFAULT_VALUE = "defaultValue";
    private static final String RULE_ATTR_PATH = "path";
    private static final String RULE_ATTR_NAME = "name";
    private static final String RULE_ATTR_FILTER_BY = "filterByPath";
    private static final String RULE_ATTR_FILTER_BY_ID = "filterByIdPath";
    private static final String RULE_ATTR_TO_ARRAY = "toArrayPath";
    private static final String RULE_ATTR_XML_WRAPPER = "xmlWrapper";
    private static final String RULE_ATTR_CONVERSION_DIR = "conversionDirection";
    private static final String RULE_ATTR_CLASS_FIELD = "classificationField";
    private static final String RULE_ATTR_REL_FIELD = "releasabilityField";
    private static final String RULE_ATTR_SEC_BLOCK = "securityBlockPath";
    private static final String RULE_ATTR_PARAM_1 = "parameter1";
    private static final String RULE_ATTR_PARAM_2 = "parameter2";

    private static final String RULE_SECURITY_CONVERSION = "com.issinc.ges.interfaces.te.rules.SecurityConversionRule";
    private static final String RULE_ADD_NODE = "com.issinc.ges.interfaces.te.rules.AddNode";
    private static final String RULE_COPY_NODE = "com.issinc.ges.interfaces.te.rules.CopyNode";
    private static final String RULE_COPY_ARRAY_NODE_TO_NODE = "com.issinc.ges.interfaces.te.rules.CopyArrayNodeToNode";
    private static final String RULE_COPY_ARRAY_NODE_TO_ARRAY_NODE =
            "com.issinc.ges.interfaces.te.rules.CopyArrayNodeToArrayNode";
    private static final String RULE_DELETE_NODE = "com.issinc.ges.interfaces.te.rules.DeleteNode";
    private static final String RULE_JSON_NODE_TO_STR = "com.issinc.ges.interfaces.te.rules.JsonNodeToJsonString";
    private static final String RULE_JSON_STR_TO_NODE = "com.issinc.ges.interfaces.te.rules.JsonStringToJsonNode";
    private static final String RULE_JSON_STR_TO_XML = "com.issinc.ges.interfaces.te.rules.JsonStringToXmlString";
    private static final String RULE_MOVE_NODE = "com.issinc.ges.interfaces.te.rules.MoveNode";
    private static final String RULE_RENAME_NODE = "com.issinc.ges.interfaces.te.rules.RenameNode";
    private static final String RULE_SORT_NODE = "com.issinc.ges.interfaces.te.rules.SortXML";
    private static final String RULE_ADD_SUBTABLEMETA = "com.issinc.ges.interfaces.te.rules.AddSubtableMetadata";
    private static final String RULE_ATTR_SUBTABLE_PATH = "subtablePath";
    private static final String RULE_ATTR_REPORTKEY_LOC = "reportKeyPath";
    private static final String RULE_ATTR_ORIGINATINGSYS_LOC = "originatingSystemPath";
    private static final String RULE_DELETE_SECTIONS = "cidne_4.1_2.3.5_delete_sections";

    private static final String CONVERT_LAT_LON_TO_MGRS = "convertLatLonToMGRS";

    private static final String DATAMODEL_IS_SECTION_SECURITY = "isSectionSecurity";
    private static final String DATAMODEL_SECURITY = "security";
    private static final String DATAMODEL_HAS_SECURITY = "hasSecurity";
    private static final String DATAMODEL_IS_LOCATION = "isLocation";
    private static final String DATAMODEL_DETAILS = "details";
    private static final String DATAMODEL_2X_ALIAS = "fieldAlias2x";
    private static final String DATAMODEL_PROPERTIES = "properties";
    private static final String DATAMODEL_BASE = "base";

    private static final String DEF_DATETIME = "#/definitions/dateTime";
    private static final String DEF_DATETIME_REQUIRED = "#/definitions/dateTimeRequired";
    private static final String DEF_TEXTFIELD_LEVEL_REQUIRED = "#/definitions/textFieldLevelRequired";
    private static final String DEF_SECURITY = "#/definitions/security";

    private static final Logger logger = LoggerFactory.getLogger(CIDNE4to2RuleSetGenerator.class);

    /**
     * Default constructor
     */
    public CIDNE4to2RuleSetGenerator() {

    }

    /**
     * Takes in the schema (datamodel) and converts that to a RuleSet.
     *
     * @param datamodel
     *   The schema which is the basis for the rulesets.
     * @param module
     *   The module of the report.
     * @param reportType
     *   The report type of the report.
     * @param srcVersion
     *   The version that is the "from" version for the process the ruleset will
     *   run once requested.
     * @param dstVersion
     *   The version that is the "to" version for the process the ruleset will
     *   run once requested.
     * @return
     *   The generated ruleset
     */
    public final RuleSet generateRuleset(JsonNode datamodel, String module, String reportType,
            String srcVersion, String dstVersion) {

        RuleSet convertedRuleSet = null;

        final JsonNode ruleset = generateRulesetJsonNode(datamodel, module, reportType, srcVersion,
                dstVersion);

        try {
            convertedRuleSet = ModelUtils.toObject(ruleset, RuleSet.class);
        } catch (TransformationException e) {
            logger.error("Could not convert generated rule set", e);
        }

        return convertedRuleSet;
    }

    /**
     * Takes in the schema (datamodel) and converts that to a JsonNode.
     *
     * @param datamodel
     *   The schema which is the basis for the rulesets.
     * @param module
     *   The module of the report.
     * @param reportType
     *   The report type of the report.
     * @param srcVersion
     *   The version that is the "from" version for the process the ruleset will
     *   run once requested.
     * @param dstVersion
     *   The version that is the "to" version for the process the ruleset will
     *   run once requested.
     * @return
     *   The generated JsonNode ruleset
     */
    public final JsonNode generateRulesetJsonNode(JsonNode datamodel, String module, String reportType,
            String srcVersion, String dstVersion) {
        final JsonNode ruleset = initRuleSet(module, reportType, srcVersion, dstVersion);
        final ArrayNode rules = (ArrayNode) ruleset.get("rules");

        addBaseRules(rules, datamodel);  // convert base table to top-level fields
        addLocationRules(rules, datamodel);  // the top-level location

        // Add in the report details...
        final JsonNode detailsNode = datamodel.get("properties").get(DATAMODEL_DETAILS).get("properties");
        if (detailsNode != null) {
            processDetailsSection(rules, detailsNode);
        }

        // Add in the report remarks...
        final JsonNode remarksNode = datamodel.get("properties").get("remarks").get("items").get("properties");
        if (remarksNode != null) {
            processRemarksSection(rules);
        }

        // Add in the report requirements...
        final JsonNode requirementsNode = datamodel.get("properties").get("requirements").get("items").get("properties");
        if (requirementsNode != null) {
            processRequirementsSection(rules);
        }

        // Remove left-over sections, and convert the output to a string
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_OBJECT_ID, RULE_DELETE_SECTIONS));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_JSON_NODE_TO_STR));


        String wrapperReport = module + reportType;
        //2x uses Target instead of "Entity" for module
        if("entity".equalsIgnoreCase(module)){
           wrapperReport= "Target"+reportType;
        }

        // Wrap in the appropriate Module/ReportType and convert to XML
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_JSON_STR_TO_XML)
                .put(RULE_ATTR_XML_WRAPPER, wrapperReport));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SORT_NODE));

        return ruleset;
    }

    private JsonNode initRuleSet(String module, String reportType, String srcVersion, String dstVersion) {
        final ObjectNode ruleset = JsonNodeFactory.instance.objectNode()
                .put("datamodel", RULESET_DATAMODEL)
                .put("objectId",
                        String.format(OBJECT_ID_INSTANCE, module, reportType, srcVersion, dstVersion).toLowerCase())
                .put("displayName", String.format(DISPLAY_NAME, module, reportType, srcVersion, dstVersion))
                .put("sourceVersionObjectId", String.format(VERSION, srcVersion, module, reportType).toLowerCase())
                .put("destinationVersionObjectId",
                        String.format(VERSION, dstVersion, module, reportType).toLowerCase());

        final ArrayNode rulesArray = JsonNodeFactory.instance.arrayNode();
        rulesArray.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_JSON_STR_TO_NODE));

        ruleset.set("rules", rulesArray);

        return ruleset;
    }

    private void addBaseRules(ArrayNode rules, JsonNode datamodel) {

        //since we know what fields will always be in base, just go through them directly rather than iterating
        //use logic based on certain transformation assumptions

        //default base fields
        String reportTitle = "ReportTitle";
        String reportSummaryField = "ReportDescription";
        String reportSerial = "TrackingNumber";
        String reportDate = "ReportDTG";

        //if datamodel provides the correct values for these fields, adjust them
        if (datamodel.get(DATAMODEL_PROPERTIES) != null
                && datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE) != null
                && datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES) != null){

            if (datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES).get("reportDate")
                    != null){
                //get base.reportDate field
                reportDate =
                        datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(
                                DATAMODEL_PROPERTIES).get("reportDate").get(DATAMODEL_2X_ALIAS).asText();
            }
            if (datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES).get("reportSerial")
                    != null){
                //get base.reportSerial field
                reportSerial =
                        datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(
                                DATAMODEL_PROPERTIES).get("reportSerial").get(DATAMODEL_2X_ALIAS).asText();
            }
            if (datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES).get("reportSummary")
                    != null){
                //get base.reportSummary field
                reportSummaryField =
                        datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(
                                DATAMODEL_PROPERTIES).get("reportSummary").get(DATAMODEL_2X_ALIAS).asText();
            }
            if (datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(DATAMODEL_PROPERTIES).get("reportTitle")
                    != null){
                //get base.reportTitle field
                reportTitle =
                        datamodel.get(DATAMODEL_PROPERTIES).get(DATAMODEL_BASE).get(
                                DATAMODEL_PROPERTIES).get("reportTitle").get(DATAMODEL_2X_ALIAS).asText();
            }
        }

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD, "Classification" )
                .put(RULE_ATTR_REL_FIELD,"ClassificationReleasabilityMark")
                .put(RULE_ATTR_SEC_BLOCK, "security")
                .put(RULE_ATTR_CONVERSION_DIR,"4xto2x"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.currentObjectState.actionDate.zuluDateTime")
                .put(RULE_ATTR_NEW_FIELD, "DateLastViewed")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertZuluDateTime"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DateLastViewed")
                .put(RULE_ATTR_NEW_FIELD, "DatePosted"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DateLastViewed")
                .put(RULE_ATTR_NEW_FIELD, "DatePostedZulu"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DateLastViewed")
                .put(RULE_ATTR_NEW_FIELD, "DateUpdated"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DateLastViewed")
                .put(RULE_ATTR_NEW_FIELD, "DateUpdatedZulu"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.reportSymbol")
                .put(RULE_ATTR_NEW_FIELD, "DisplayIcon"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.reportSerial.objectValue")
                .put(RULE_ATTR_NEW_FIELD, reportSerial));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.reportSummary.objectValue")
                .put(RULE_ATTR_NEW_FIELD, reportSummaryField));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.currentObjectState.objectState")
                .put(RULE_ATTR_NEW_FIELD, "isDeprecated")
                .put(RULE_ATTR_CONVERSION_METHOD,"convertDeprecated"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.isTearline")
                .put(RULE_ATTR_NEW_FIELD, "IsTearLine")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertBoolean"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.currentObjectState.site")
                .put(RULE_ATTR_NEW_FIELD, "OriginatingSite"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.currentObjectState.system")
                .put(RULE_ATTR_NEW_FIELD, "OriginatingSystem"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.originatingGroup")
                .put(RULE_ATTR_NEW_FIELD, "OriginatorGroup"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.currentObjectState.user")
                .put(RULE_ATTR_NEW_FIELD, "OriginatorName"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.currentObjectState.parentGroup")
                .put(RULE_ATTR_NEW_FIELD, "OriginatorUnit"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.reportDate.zuluDateTime")
                .put(RULE_ATTR_NEW_FIELD, reportDate)
                .put(RULE_ATTR_CONVERSION_METHOD, "convertZuluDateTime"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.reportId")
                .put(RULE_ATTR_NEW_FIELD, "ReportKey"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.theaterFilter")
                .put(RULE_ATTR_NEW_FIELD, "TheaterFilter"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorGroup")
                .put(RULE_ATTR_NEW_FIELD, "UpdatedByGroup"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorName")
                .put(RULE_ATTR_NEW_FIELD, "UpdatedByName"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorUnit")
                .put(RULE_ATTR_NEW_FIELD, "UpdatedByUnit"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "base.reportTitle.objectValue")
                .put(RULE_ATTR_NEW_FIELD, reportTitle));
    }

    private void addLocationRules(ArrayNode rules, JsonNode datamodel) {
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "location")
                .put(RULE_ATTR_NAME, "Location"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.ao")
                .put(RULE_ATTR_NAME, "AO"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD, "Classification" )
                .put(RULE_ATTR_REL_FIELD,"ClassificationReleasabilityMark")
                .put(RULE_ATTR_SEC_BLOCK, "Location.security")
                .put(RULE_ATTR_CONVERSION_DIR,"4xto2x"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, "Location.country")
                .put(RULE_ATTR_NEW_FIELD, "Country"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DateLastViewed")
                .put(RULE_ATTR_NEW_FIELD, "Location.DatePosted"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DateLastViewed")
                .put(RULE_ATTR_NEW_FIELD, "Location.DatePostedZulu"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DateLastViewed")
                .put(RULE_ATTR_NEW_FIELD, "Location.DateUpdated"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "DateLastViewed")
                .put(RULE_ATTR_NEW_FIELD, "Location.DateUpdatedZulu"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.district")
                .put(RULE_ATTR_NAME, "District"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.fob")
                .put(RULE_ATTR_NAME, "FOB"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH, "Location.isDeprecated")
                .put(RULE_ATTR_DEFAULT_VALUE, 0));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.latitude")
                .put(RULE_ATTR_NAME, "Latitude"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.locationActive")
                .put(RULE_ATTR_NAME, "LocationActive")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertBoolean"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.objectId")
                .put(RULE_ATTR_NAME, "LocationKey"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.longitude")
                .put(RULE_ATTR_NAME, "Longitude"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.msc")
                .put(RULE_ATTR_NAME, "MSC"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.nearestCity")
                .put(RULE_ATTR_NAME, "NearestCity"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatingSite")
                .put(RULE_ATTR_NEW_FIELD, "Location.OriginatingSite"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatingSystem")
                .put(RULE_ATTR_NEW_FIELD, "Location.OriginatingSystem"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorGroup")
                .put(RULE_ATTR_NEW_FIELD, "Location.OriginatorGroup"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorName")
                .put(RULE_ATTR_NEW_FIELD, "Location.OriginatorName"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorUnit")
                .put(RULE_ATTR_NEW_FIELD, "Location.OriginatorUnit"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.isPrimaryLocation")
                .put(RULE_ATTR_NAME, "PrimaryLocationFlag")
                .put(RULE_ATTR_CONVERSION_METHOD, "convertBoolean"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Location.province")
                .put(RULE_ATTR_NAME, "Province"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "ReportKey")
                .put(RULE_ATTR_NEW_FIELD, "Location.ReportKey"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "TheaterFilter")
                .put(RULE_ATTR_NEW_FIELD, "Location.TheaterFilter"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorGroup")
                .put(RULE_ATTR_NEW_FIELD, "Location.UpdatedByGroup"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorName")
                .put(RULE_ATTR_NEW_FIELD, "Location.UpdatedByName"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                .put(RULE_ATTR_OLD_FIELD, "OriginatorUnit")
                .put(RULE_ATTR_NEW_FIELD, "Location.UpdatedByUnit"));
    }

    private void processDetailsSection(ArrayNode rules, JsonNode detailsNode) {
        final Iterator<Map.Entry<String, JsonNode>> iterator = detailsNode.fields();
        final List<NodeInfo> arraysToProcess = new ArrayList<>();
        while (iterator.hasNext()) {
            final Map.Entry<String, JsonNode> entry = iterator.next();
            final JsonNode node = entry.getValue();
            final String nodeKey = entry.getKey();

            // Move each node OUT of the details section, so we can process it
            // without having to worry about, "does it start with 'details', or not..."
            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                    .put(RULE_ATTR_OLD_FIELD, "details." + nodeKey)
                    .put(RULE_ATTR_NEW_FIELD, nodeKey));

            // Now, process the node
            processNode(rules, node, nodeKey, get2xFieldAlias(node, nodeKey), arraysToProcess);

        }
        //handle arrays
        for (NodeInfo subarrayNodeInfo : arraysToProcess){
            processArray(rules, subarrayNodeInfo.node, subarrayNodeInfo.oldNodeLocation, subarrayNodeInfo.newNodeName, null);
            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                    .put(RULE_ATTR_PATH, subarrayNodeInfo.oldNodeLocation)
                    .put(RULE_ATTR_NAME, subarrayNodeInfo.newNodeName));
        }
    }

    private void processRemarksSection(ArrayNode rules) {
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put(RULE_ATTR_PATH, "remarks"));
    }

    private void processRequirementsSection(ArrayNode rules) {

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "requirements")
                .put(RULE_ATTR_NAME, "Requirements"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Requirements.objectId")
                .put(RULE_ATTR_NAME, "RequirementsKey"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Requirements.requirement")
                .put(RULE_ATTR_NAME, "Requirement"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Requirements.requirementType")
                .put(RULE_ATTR_NAME, "RequirementType"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "Requirements.comment")
                .put(RULE_ATTR_NAME, "Comment"));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD, "Classification" )
                .put(RULE_ATTR_REL_FIELD,"ClassificationReleasabilityMark")
                .put(RULE_ATTR_SEC_BLOCK, "Requirements.security")
                .put(RULE_ATTR_CONVERSION_DIR,"4xto2x"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_SUBTABLEMETA)
                .put(RULE_ATTR_SUBTABLE_PATH, "Requirements")
                .put(RULE_ATTR_REPORTKEY_LOC, "ReportKey")
                .put(RULE_ATTR_ORIGINATINGSYS_LOC, "OriginatingSystem"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put(RULE_ATTR_PATH, "Requirements.referenceId"));
    }

    protected class NodeInfo{
        JsonNode node = null;
        String oldNodeLocation = null;
        String newNodeName = null;
        public NodeInfo(JsonNode node, String oldNodeLocation, String newNodeName){
            this.node = node;
            this.oldNodeLocation = oldNodeLocation;
            this.newNodeName = newNodeName;
        }
    }


    private void processNode(ArrayNode rules, JsonNode node, String oldNodeLocation, String newNodeName, List<NodeInfo> arraysToProcess) {

        if (isTextFieldLevelRequiredField(node)) {
            handleTextFieldLevelRequiredNode(rules, node, oldNodeLocation);
        } else if (isDate(node)) {
            handleDateNode(rules, oldNodeLocation, newNodeName);
        } else if (isLocation(node)) {
            handleLocationLookup(rules, node, oldNodeLocation);
        } else if (isSectionSecurity(node)) {
            handleSectionSecurityNode(rules, oldNodeLocation, newNodeName);
        } else if (node.has("type") && node.get("type").asText().equalsIgnoreCase("array")) {
            arraysToProcess.add(new NodeInfo(node, oldNodeLocation, newNodeName));
        } else {
            // Not an array or special node - just rename it
            if (isBoolean(node)) {
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                        .put(RULE_ATTR_PATH, oldNodeLocation)
                        .put(RULE_ATTR_NAME, newNodeName)
                        .put(RULE_ATTR_CONVERSION_METHOD, "convertBoolean"));
            } else {
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                        .put(RULE_ATTR_PATH, oldNodeLocation)
                        .put(RULE_ATTR_NAME, newNodeName));
            }
        }
    }

    private boolean isBoolean(JsonNode node) {
        return node.has("type") && node.get("type").asText().equalsIgnoreCase("boolean");
    }

    private boolean isSecurityField(JsonNode node) {
        return node.has(JsonConstants.DECODED_REFERENCE) && node.get(JsonConstants.DECODED_REFERENCE).asText().equalsIgnoreCase(DEF_SECURITY);
    }
    private void processArray(ArrayNode rules, JsonNode node, String oldNodeLocation, String newNodeName,
            NodeInfo parentNode) {
        final List<NodeInfo> subarraysToProcess = new ArrayList<>();
        final JsonNode subelementProperties = node.get("items").get("properties");
        final Iterator<Map.Entry<String, JsonNode>> iterator = subelementProperties.fields();
        NodeInfo referenceId = null;
        while (iterator.hasNext()) {
            final Map.Entry<String, JsonNode> entry = iterator.next();
            final JsonNode arrayNode = entry.getValue();
            final String arrayNodeKey = entry.getKey();

            if (isSecurityField(arrayNode)) {

                if (get2xFieldAlias(arrayNode, arrayNodeKey).equals(arrayNodeKey)){

                    handleNodeSecuritySection(rules, oldNodeLocation + "." + arrayNodeKey, null, newNodeName);
                }
                else {
                    handleNodeSecuritySection(rules, oldNodeLocation + "." + arrayNodeKey, null,
                            get2xFieldAlias(arrayNode, arrayNodeKey));
                }
            }
            else if (arrayNodeKey.equals("referenceid")){
                //if the reference id, save for later processing
                referenceId = new NodeInfo(arrayNode, oldNodeLocation + "." + arrayNodeKey,
                        get2xFieldAlias(arrayNode, arrayNodeKey));
            }
            else {
                processNode(rules, arrayNode, oldNodeLocation + "." + arrayNodeKey,
                        get2xFieldAlias(arrayNode, arrayNodeKey), subarraysToProcess);
            }
        }

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_SUBTABLEMETA)
                .put(RULE_ATTR_SUBTABLE_PATH, oldNodeLocation)
                .put(RULE_ATTR_REPORTKEY_LOC, "ReportKey")
                .put(RULE_ATTR_ORIGINATINGSYS_LOC, "OriginatingSystem"));

        //handle parentkey
        if (parentNode != null){

            final String oldArrayName = oldNodeLocation.substring(oldNodeLocation.lastIndexOf(".") + 1);
            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_COPY_NODE)
                    .put(RULE_ATTR_OLD_FIELD, parentNode.oldNodeLocation)
                    .put(RULE_ATTR_NEW_FIELD, oldArrayName + "." + parentNode.newNodeName)
                    .put("createPath", false)); //only copy the parent key if the array has values
        }
        //handle subarrays
        for (NodeInfo subarrayNodeInfo : subarraysToProcess){
            processArray(rules, subarrayNodeInfo.node, subarrayNodeInfo.oldNodeLocation, subarrayNodeInfo.newNodeName, referenceId);
            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                    .put(RULE_ATTR_PATH, subarrayNodeInfo.oldNodeLocation)
                    .put(RULE_ATTR_NAME, subarrayNodeInfo.newNodeName));
        }

        //handle reference node
        if (referenceId != null){
            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                    .put(RULE_ATTR_PATH, referenceId.oldNodeLocation)
                    .put(RULE_ATTR_NAME, referenceId.newNodeName));
        }


    }

    private String get2xFieldAlias(JsonNode node, String defaultValue) {
        if (node.has(DATAMODEL_2X_ALIAS)) {
            return node.get(DATAMODEL_2X_ALIAS).asText();
        } else {
            return defaultValue;
        }
    }

    /**
     * Handle the "security" section (may be a custom-named section with "hasSecurity=true").
     * @param rules the ruleset
     * @param oldFieldPath path to the original node
     * @param newArrayLocation (optional) location for the new security fields
     * @param newSecurityPrefix prefix to use for Classification/Releasability fields
     */
    private void handleNodeSecuritySection(ArrayNode rules, String oldFieldPath,
            String newArrayLocation, String newSecurityPrefix) {

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD,  newSecurityPrefix + "Classification" )
                .put(RULE_ATTR_REL_FIELD, newSecurityPrefix + "ClassificationReleasabilityMark")
                .put(RULE_ATTR_SEC_BLOCK, oldFieldPath)
                .put(RULE_ATTR_CONVERSION_DIR, "4xto2x"));
    }

    private boolean isTextFieldLevelRequiredField(JsonNode node) {
        return node.has(JsonConstants.DECODED_REFERENCE) && node.get(JsonConstants.DECODED_REFERENCE).asText().equalsIgnoreCase(DEF_TEXTFIELD_LEVEL_REQUIRED);
    }

    /**
     * Process a "textFieldLevelRequired" node.  These are different than standard fields because
     * the value is in an "objectValue" field, and they have a security.
     * @param rules the list of rules
     * @param node the textFieldLevelRequired node
     * @param oldNodeLocation the path to the node
     */
    private void handleTextFieldLevelRequiredNode(ArrayNode rules, JsonNode node, String oldNodeLocation) {

        // Because a node may have the same 2.X and 4.X name, we need to use a temporary
        // location, and then move it to it's final location at the very end.  This is because
        // TABLE.FIELD1.objectValue can't be put in to TABLE.FIELD1, or we'll use any TABLE.FIELD1.security values.
        String temporaryLocation;
        String finalLocation;
        String rootLoc = null;

        if (!oldNodeLocation.contains(".")) {
            temporaryLocation = "__TMP__" + get2xFieldAlias(node, oldNodeLocation);
            finalLocation = get2xFieldAlias(node, oldNodeLocation);

            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                    .put(RULE_ATTR_OLD_FIELD, oldNodeLocation + ".objectValue")
                    .put(RULE_ATTR_NEW_FIELD, temporaryLocation));
        } else {
            // oldNodeLoc:  table0.table1.table2.table3.table4.field
            //
            // move:        table0.table1.table2.table3.table4.field.objectValue
            // to:          table1.table2.table3.table4.CIDNE2Alias
            //
            // oldNodeLoc:  table0.field
            // move:        table0.field.objectValue
            // to:          CIDNE2Alias
            //
            // strip off "field", and make relative to "table0"

            String newFieldLocAndName;

            // replace table0.table1.table2.<field> with table0.table1.table2.<CIDNE2xAlias>
            newFieldLocAndName =
                    oldNodeLocation.replaceAll(oldNodeLocation.substring(oldNodeLocation.lastIndexOf(".") + 1),
                            get2xFieldAlias(node, oldNodeLocation));

            // keep the "root" table, since we'll need it when copying temporary location to the final one...
            rootLoc = newFieldLocAndName.substring(0, newFieldLocAndName.lastIndexOf("."));

            // replace table0.table1.table2.CIDNE2xAlias with table1.table2.CIDNE2xAlias
            newFieldLocAndName = newFieldLocAndName.substring(newFieldLocAndName.indexOf(".") + 1);

            temporaryLocation = "__TMP__" + newFieldLocAndName.substring(newFieldLocAndName.lastIndexOf(".") + 1);

            finalLocation = newFieldLocAndName.substring(newFieldLocAndName.lastIndexOf(".") + 1);

            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                    .put(RULE_ATTR_OLD_FIELD, oldNodeLocation + ".objectValue")
                    .put(RULE_ATTR_NEW_FIELD, temporaryLocation));
        }

        if (hasSecurity(node)) {
            if (StringUtils.countMatches(oldNodeLocation, ".") < 2) {
                //
                // if oldNodeLocation is "table" or "table.field", then we'll pass in null as the new array
                // location, since the rule to move the node should look like:  MOVE table.field -> 2xAliasField
                //
                handleNodeSecuritySection(rules, oldNodeLocation + "." + DATAMODEL_SECURITY, null,
                        ".."+get2xFieldAlias(node, oldNodeLocation));
            } else {
                // if the oldNodeLocation is "table0.table1.table2.field", pass in a relative path to
                // this (i.e. table1.table2), so the rule to move looks like:
                //     MOVE table0.table1.table2.field -> table1.table2.2xAliasField
                String newSecLoc;
                // replace table0.table1.table2.<field> with table0.table1.table2
                newSecLoc =
                        oldNodeLocation.replaceAll(oldNodeLocation.substring(oldNodeLocation.lastIndexOf(".")), "");

                // replace table0.table1.table2 with table1.table2
                newSecLoc = newSecLoc.substring(newSecLoc.indexOf(".") + 1);
                handleNodeSecuritySection(rules, oldNodeLocation + "." + DATAMODEL_SECURITY,
                        newSecLoc, ".."+get2xFieldAlias(node, oldNodeLocation));
            }
        }

        // remove the original node, now that we've moved everything out
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put(RULE_ATTR_PATH, oldNodeLocation));

        // NOW, move it to its REAL location...
        if (rootLoc == null) {
            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                    .put(RULE_ATTR_OLD_FIELD, temporaryLocation)
                    .put(RULE_ATTR_NEW_FIELD, finalLocation));
        } else {
            rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                    .put(RULE_ATTR_OLD_FIELD, rootLoc + "." + temporaryLocation)
                    .put(RULE_ATTR_NEW_FIELD, finalLocation));
        }
    }

    private boolean isDate(JsonNode node) {
        return node.has(JsonConstants.DECODED_REFERENCE) &&
                (node.get(JsonConstants.DECODED_REFERENCE).asText().equals(DEF_DATETIME) ||
                        node.get(JsonConstants.DECODED_REFERENCE).asText().equals(DEF_DATETIME_REQUIRED));
    }

    private boolean hasSecurity(JsonNode node) {
        return node.has(DATAMODEL_HAS_SECURITY) && node.get(DATAMODEL_HAS_SECURITY).asBoolean();
    }

    private boolean isLocation(JsonNode node) {
        return node.has("isLocation");
    }

    private boolean isSectionSecurity(JsonNode node) {
        return node.has(DATAMODEL_IS_SECTION_SECURITY) && node.get(DATAMODEL_IS_SECTION_SECURITY).asBoolean();
    }

    private void handleSectionSecurityNode(ArrayNode rules, String oldFieldPath,
            String newSecurityPrefix) {

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_SECURITY_CONVERSION)
                .put(RULE_ATTR_CLASS_FIELD,  newSecurityPrefix + "Classification" )
                .put(RULE_ATTR_REL_FIELD,newSecurityPrefix + "ClassificationReleasabilityMark")
                .put(RULE_ATTR_SEC_BLOCK, oldFieldPath)
                .put(RULE_ATTR_CONVERSION_DIR,"4xto2x"));
    }

    /**
     * Create the rule(s) for a date field conversion
     * @param rules the rules array to add the rules to
     * @param oldFieldPath src/original date field path
     * @param newFieldName new field name
     */
    private void handleDateNode(ArrayNode rules, String oldFieldPath, String newFieldName) {
        String newPath = getRelativeTargetPath(oldFieldPath, newFieldName);
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                .put(RULE_ATTR_OLD_FIELD, oldFieldPath + ".zuluDateTime")
                .put(RULE_ATTR_NEW_FIELD, newFieldName)
                .put(RULE_ATTR_CONVERSION_METHOD, "convertZuluDateTime"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put(RULE_ATTR_PATH, oldFieldPath));
    }

    /**
     * Given an original (src) path, create a relative target path that includes the field name.
     * @param path the original path
     * @param newFieldName the new field name
     * @return The relative path including field name
     */
    private String getRelativeTargetPath(String path, String newFieldName) {
        if (StringUtils.countMatches(path, ".") > 1) {
            // for paths like:  table0.table1.table2.oldName, the rule should look like:
            // MOVE table0.table1.table2.oldName -> table1.table2.newName
            String relativePath;
            relativePath = path.substring(path.indexOf(".") + 1);  // remove root table
            relativePath = relativePath.substring(0, relativePath.lastIndexOf(".") + 1);
            return relativePath + newFieldName;
        } else {
            return newFieldName;
        }
    }

    /**
     * Handle location lookups.  The "isLocation" field contains the desired format for the output.
     * isLocation has the following formats:
     *    1.  <MGRS Label>,<Latitude Label>,<Longitude Label>
     *    2.  *empty* - use 2xFieldAlias plus "Mgrs", "Latitude" and "Longitude"
     * @param rules rules array to add the rule to
     * @param node node containing the location element
     * @param locationIdPath path for the location ID (reference an element in "locations")
     */
    private void handleLocationLookup(ArrayNode rules, JsonNode node, String locationIdPath) {
        final String locationFormat = node.get(DATAMODEL_IS_LOCATION).asText();
        String mgrsLabel;
        String latLabel = null;
        String lngLabel = null;

        String locationIdTable = null;
        if (locationIdPath.contains(".")) {
            locationIdTable = locationIdPath.substring(0, locationIdPath.lastIndexOf("."));
        }

        // Compute MGRS from Lat/Lon and add it as a node so we can copy it as we do lat and lon
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put(RULE_ATTR_PATH, "Location.mgrs")
                .put(RULE_ATTR_CONVERSION_METHOD, CONVERT_LAT_LON_TO_MGRS)
                .put(RULE_ATTR_PARAM_1, "Latitude")
                .put(RULE_ATTR_PARAM_2, "Longitude"));

        if (locationFormat.indexOf(",") > 0) {  // MGRS,LAT,LNG format
            final String[] locationFields = locationFormat.split(",");
            if (locationFields.length == 3) {
                mgrsLabel = locationFields[0];
                latLabel = locationFields[1];
                lngLabel = locationFields[2];
            } else {
                logger.error("Invalid # of fields in isLocation.  Needs to be MGRS,LAT,LON, or just MGRS");
                return;
            }

            if (!locationIdPath.contains(".")) {  // Copying to a single node location...
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_COPY_ARRAY_NODE_TO_NODE)
                        .put(RULE_ATTR_FILTER_BY, "LocationKey")  // LocationKey is relative to the OLD_FIELD root
                        .put(RULE_ATTR_FILTER_BY_ID, locationIdPath)  // reference to the location ID
                        .put(RULE_ATTR_OLD_FIELD, "Location.Latitude")  // "location" has been moved to "Location"
                        .put(RULE_ATTR_NEW_FIELD, latLabel));

                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_COPY_ARRAY_NODE_TO_NODE)
                        .put(RULE_ATTR_FILTER_BY, "LocationKey")  // LocationKey is relative to the OLD_FIELD root
                        .put(RULE_ATTR_FILTER_BY_ID, locationIdPath)  // reference to the location ID
                        .put(RULE_ATTR_OLD_FIELD, "Location.Longitude")  // "location" has been moved to "Location"
                        .put(RULE_ATTR_NEW_FIELD, lngLabel));

                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_COPY_ARRAY_NODE_TO_NODE)
                        .put(RULE_ATTR_FILTER_BY, "LocationKey")  // LocationKey is relative to the OLD_FIELD root
                        .put(RULE_ATTR_FILTER_BY_ID, locationIdPath)  // reference to the location ID
                        .put(RULE_ATTR_OLD_FIELD, "Location.mgrs")  // "location" has been moved to "Location"
                        .put(RULE_ATTR_NEW_FIELD, mgrsLabel));

            } else {  // Copying to an array location

                final String locationIdField = locationIdPath.substring(locationIdPath.lastIndexOf(".") +  1);

                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_COPY_ARRAY_NODE_TO_ARRAY_NODE)
                        .put(RULE_ATTR_OLD_FIELD, "Location.Latitude")  // "longitude" has been renamed "Longitude"
                        .put(RULE_ATTR_TO_ARRAY, locationIdTable)
                        .put(RULE_ATTR_FILTER_BY, "LocationKey")  // LocationKey is relative to the OLD_FIELD root
                        .put(RULE_ATTR_FILTER_BY_ID, locationIdField)
                        .put(RULE_ATTR_NEW_FIELD, latLabel));

                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_COPY_ARRAY_NODE_TO_ARRAY_NODE)
                        .put(RULE_ATTR_OLD_FIELD, "Location.Longitude")  // "longitude" has been renamed "Longitude"
                        .put(RULE_ATTR_TO_ARRAY, locationIdTable)
                        .put(RULE_ATTR_FILTER_BY, "LocationKey")  // LocationKey is relative to the OLD_FIELD root
                        .put(RULE_ATTR_FILTER_BY_ID, locationIdField)
                        .put(RULE_ATTR_NEW_FIELD, lngLabel));

                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_COPY_ARRAY_NODE_TO_ARRAY_NODE)
                        .put(RULE_ATTR_OLD_FIELD, "Location.mgrs")  // "longitude" has been renamed "Longitude"
                        .put(RULE_ATTR_TO_ARRAY, locationIdTable)
                        .put(RULE_ATTR_FILTER_BY, "LocationKey")  // LocationKey is relative to the OLD_FIELD root
                        .put(RULE_ATTR_FILTER_BY_ID, locationIdField)
                        .put(RULE_ATTR_NEW_FIELD, mgrsLabel));


            }

        } else {
            // MGRS attribute only
            mgrsLabel = get2xFieldAlias(node, locationIdPath) + "Mgrs";

            if (!locationIdPath.contains(".")) {
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_COPY_ARRAY_NODE_TO_NODE)
                        .put(RULE_ATTR_FILTER_BY, "LocationKey")  // LocationKey is relative to the OLD_FIELD root
                        .put(RULE_ATTR_FILTER_BY_ID, locationIdPath)  // reference to the location ID
                        .put(RULE_ATTR_OLD_FIELD, "Location.mgrs")  // "location" has been moved to "Location"
                        .put(RULE_ATTR_NEW_FIELD, mgrsLabel));
            } else {
                final String locationIdField = locationIdPath.substring(locationIdPath.lastIndexOf(".") +  1);
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_COPY_ARRAY_NODE_TO_ARRAY_NODE)
                        .put(RULE_ATTR_OLD_FIELD, "Location.mgrs")  // "longitude" has been renamed "Longitude"
                        .put(RULE_ATTR_TO_ARRAY, locationIdTable)
                        .put(RULE_ATTR_FILTER_BY, "LocationKey")  // LocationKey is relative to the OLD_FIELD root
                        .put(RULE_ATTR_FILTER_BY_ID, locationIdField)
                        .put(RULE_ATTR_NEW_FIELD, mgrsLabel));


            }
        }

        // done with it - delete the location reference
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put(RULE_ATTR_PATH, locationIdPath));

        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put(RULE_ATTR_PATH, "Location.mgrs"));
    }

}
