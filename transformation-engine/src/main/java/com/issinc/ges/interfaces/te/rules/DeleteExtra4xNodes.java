/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.JsonRule;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;

/**
 * Rule to delete unknown nodes from 4x JSON
 */
public class DeleteExtra4xNodes extends JsonRule {

    List<String> knownFields = Arrays.asList("datamodel", "formId", "base", "details", "location", "security", "remarks",
            "requirements");

    /**
     * does the actual deletion of the unknown nodes
     *
     * @param token   Security token
     * @param payload The data to be transformed by the rule
     * @return The transformed payload
     * @throws com.issinc.ges.interfaces.te.TransformationException if an error occurs
     */
    @Override public JsonNode applyRule(String token, JsonNode payload) throws TransformationException {

        final JsonEditor je = new JsonEditor(payload);
        Iterator<String> fieldIter = payload.fieldNames();
        List<String> fieldsToCut = new ArrayList<>();

        while(fieldIter.hasNext()) {
            String fieldName = fieldIter.next();
            if(!knownFields.contains(fieldName)) {
                fieldsToCut.add(fieldName);
            }
        }

        for(String fieldName: fieldsToCut){
            final JsonNode cutNode = je.cut(fieldName);
        }

        return payload;
    }
}
