/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules.cdr;

import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.interfaces.te.rules.cdr.xmlprocessors.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.StringReader;

/**
 * A tranformation rule to convert CDR XML reports to CIDNE 4.1 JSON
 */
public class Cdr2Cidne extends Rule {

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    private String reportType;

    private XPath xpath;
    private Document doc;
    public void setupXMLParse(String xmlString) throws TransformationException {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = dbFactory.newDocumentBuilder();

            InputSource is = new InputSource(new StringReader(xmlString));
            doc = builder.parse(is);

            XPathFactory factory = XPathFactory.newInstance();
            xpath = factory.newXPath();
        } catch (SAXException |ParserConfigurationException | IOException e1) {
            throw new TransformationException(TransformationException.ErrorType.PAYLOAD_ERROR,
                    "unable to parse XML for transformation");
        }
    }
    @Override public Object applyRule(String token, Object payload) throws TransformationException {

        setupXMLParse((String)payload);

        String cidne4Json = "";
        CdrProcessor processor = null;
        switch(reportType)
        {
            case "IIR":
                processor = new IIRProcessor(xpath,doc);
                break;
            case "EVAL":
               processor = new EvalProcessor(xpath,doc);
                break;
            case "ICR":
                String category = getCategory();
                switch(category) {
                    case "SDR":
                        processor = new SdrProcessor(xpath, doc);
                        break;
                    case "AHR":
                        processor = new AhrProcessor(xpath, doc);
                        break;
                    case "HCR":
                        processor = new HcrProcessor(xpath, doc);
                        break;
                    case "TSCR":
                        processor = new TscrProcessor(xpath, doc);
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
        if(processor!=null) {
            cidne4Json = processor.processXML();
        }
        else{
            throw new TransformationException(TransformationException.ErrorType.PAYLOAD_ERROR,
                    "Unsupported report type, cannot transform report.");
        }

        return cidne4Json;
    }

    private String getCategory() throws TransformationException {
        String category="";
        try{
            XPathExpression expr =
                    xpath.compile("//*[local-name()='icr']//*[local-name()='requirement_info']//*[local-name()='requirement_type']"+"/text()");

            category = (String)expr.evaluate(doc, XPathConstants.STRING);


        } catch ( XPathExpressionException e) {
           throw new TransformationException(TransformationException.ErrorType.PAYLOAD_ERROR,
                   "unable to determine report type from payload. Missing requirement_category");
        }


        return category;
    }


}
