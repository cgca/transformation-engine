/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The core metadata used within all report definitions
 *
 */
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "objectId",
        "reportId",
        "parentVersionId",
        "module",
        "reportType",
        "reportSerial",
        "reportTitle",
        "reportSummary",
        "reportDate",
        "reportSymbol",
        "reportImage",
        "reportCountry",
        "reportLatitude",
        "reportLongitude",
        "geoLocation",
        "currentObjectState",
        "originatingGroup",
        "originatingUser",
        "isTearline",
        "theaterFilter",
        "warehouseUri"
})
public class ReportBase {
/**
 *
 * (Required)
 *
 */
@JsonProperty("objectId")
private String objectId;
/**
 *
 * (Required)
 *
 */
@JsonProperty("reportId")
private String reportId;
@JsonProperty("parentVersionId")
private String parentVersionId;
/**
 *
 * (Required)
 *
 */
@JsonProperty("module")
private String module;
/**
 *
 * (Required)
 *
 */
@JsonProperty("reportType")
private String reportType;
/**
 *
 * (Required)
 *
 */
@JsonProperty("reportSerial")
private FieldSecurity reportSerial;
/**
 *
 * (Required)
 *
 */
@JsonProperty("reportTitle")
private FieldSecurity reportTitle;
/**
 *
 * (Required)
 *
 */
@JsonProperty("reportSummary")
private FieldSecurity reportSummary;
/**
 *
 * (Required)
 *
 */
@JsonProperty("reportDate")
private ActionDate reportDate;
@JsonProperty("reportSymbol")
private String reportSymbol;
@JsonProperty("reportImage")
private String reportImage;
@JsonProperty("reportCountry")
private String reportCountry;
@JsonProperty("reportLatitude")
private double reportLatitude;
@JsonProperty("reportLongitude")
private double reportLongitude;
/**
 * Geo JSON object
 * <p>
 * Schema for a Geo JSON object
 *
 */
@JsonProperty("geoLocation")
private String geoLocation;
/**
 *
 * (Required)
 *
 */
@JsonProperty("currentObjectState")
private ActionInfo currentObjectState;
/**
 *
 * (Required)
 *
 */
@JsonProperty("originatingGroup")
private String originatingGroup;
/**
 *
 * (Required)
 *
 */
@JsonProperty("originatingUser")
private String originatingUser;
/**
 * Determines whether a report is a tearline
 * (Required)
 *
 */
@JsonProperty("isTearline")
private boolean isTearline;
/**
 *
 * (Required)
 *
 */
@JsonProperty("theaterFilter")
private String theaterFilter="";
@JsonProperty("warehouseUri")
private String warehouseUri;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
 *
 * (Required)
 *
 * @return
 * The objectId
 */
@JsonProperty("objectId")
public String getObjectId() {
        return objectId;
        }

/**
 *
 * (Required)
 *
 * @param objectId
 * The objectId
 */
@JsonProperty("objectId")
public void setObjectId(String objectId) {
        this.objectId = objectId;
        }

/**
 *
 * (Required)
 *
 * @return
 * The reportId
 */
@JsonProperty("reportId")
public String getReportId() {
        return reportId;
        }

/**
 *
 * (Required)
 *
 * @param reportId
 * The reportId
 */
@JsonProperty("reportId")
public void setReportId(String reportId) {
        this.reportId = reportId;
        }

/**
 *
 * @return
 * The parentVersionId
 */
@JsonProperty("parentVersionId")
public String getParentVersionId() {
        return parentVersionId;
        }

/**
 *
 * @param parentVersionId
 * The parentVersionId
 */
@JsonProperty("parentVersionId")
public void setParentVersionId(String parentVersionId) {
        this.parentVersionId = parentVersionId;
        }

/**
 *
 * (Required)
 *
 * @return
 * The module
 */
@JsonProperty("module")
public String getModule() {
        return module;
        }

/**
 *
 * (Required)
 *
 * @param module
 * The module
 */
@JsonProperty("module")
public void setModule(String module) {
        this.module = module;
        }

/**
 *
 * (Required)
 *
 * @return
 * The reportType
 */
@JsonProperty("reportType")
public String getReportType() {
        return reportType;
        }

/**
 *
 * (Required)
 *
 * @param reportType
 * The reportType
 */
@JsonProperty("reportType")
public void setReportType(String reportType) {
        this.reportType = reportType;
        }

/**
 *
 * (Required)
 *
 * @return
 * The reportSerial
 */
@JsonProperty("reportSerial")
public FieldSecurity getReportSerial() {
        return reportSerial;
        }

/**
 *
 * (Required)
 *
 * @param reportSerial
 * The reportSerial
 */
@JsonProperty("reportSerial")
public void setReportSerial(FieldSecurity reportSerial) {
        this.reportSerial = reportSerial;
        }

/**
 *
 * (Required)
 *
 * @return
 * The reportTitle
 */
@JsonProperty("reportTitle")
public FieldSecurity getReportTitle() {
        return reportTitle;
        }

/**
 *
 * (Required)
 *
 * @param reportTitle
 * The reportTitle
 */
@JsonProperty("reportTitle")
public void setReportTitle(FieldSecurity reportTitle) {
        this.reportTitle = reportTitle;
        }

/**
 *
 * (Required)
 *
 * @return
 * The reportSummary
 */
@JsonProperty("reportSummary")
public FieldSecurity getReportSummary() {
        return reportSummary;
        }

/**
 *
 * (Required)
 *
 * @param reportSummary
 * The reportSummary
 */
@JsonProperty("reportSummary")
public void setReportSummary(FieldSecurity reportSummary) {
        this.reportSummary = reportSummary;
        }

/**
 *
 * (Required)
 *
 * @return
 * The reportDate
 */
@JsonProperty("reportDate")
public ActionDate getReportDate() {
        return reportDate;
        }

/**
 *
 * (Required)
 *
 * @param reportDate
 * The reportDate
 */
@JsonProperty("reportDate")
public void setReportDate(ActionDate reportDate) {
        this.reportDate = reportDate;
        }

/**
 *
 * @return
 * The reportSymbol
 */
@JsonProperty("reportSymbol")
public String getReportSymbol() {
        return reportSymbol;
        }

/**
 *
 * @param reportSymbol
 * The reportSymbol
 */
@JsonProperty("reportSymbol")
public void setReportSymbol(String reportSymbol) {
        this.reportSymbol = reportSymbol;
        }

/**
 *
 * @return
 * The reportImage
 */
@JsonProperty("reportImage")
public String getReportImage() {
        return reportImage;
        }

/**
 *
 * @param reportImage
 * The reportImage
 */
@JsonProperty("reportImage")
public void setReportImage(String reportImage) {
        this.reportImage = reportImage;
        }

/**
 *
 * @return
 * The reportCountry
 */
@JsonProperty("reportCountry")
public String getReportCountry() {
        return reportCountry;
        }

/**
 *
 * @param reportCountry
 * The reportCountry
 */
@JsonProperty("reportCountry")
public void setReportCountry(String reportCountry) {
        this.reportCountry = reportCountry;
        }

/**
 *
 * @return
 * The reportLatitude
 */
@JsonProperty("reportLatitude")
public double getReportLatitude() {
        return reportLatitude;
        }

/**
 *
 * @param reportLatitude
 * The reportLatitude
 */
@JsonProperty("reportLatitude")
public void setReportLatitude(double reportLatitude) {
        this.reportLatitude = reportLatitude;
        }

/**
 *
 * @return
 * The reportLongitude
 */
@JsonProperty("reportLongitude")
public double getReportLongitude() {
        return reportLongitude;
        }

/**
 *
 * @param reportLongitude
 * The reportLongitude
 */
@JsonProperty("reportLongitude")
public void setReportLongitude(double reportLongitude) {
        this.reportLongitude = reportLongitude;
        }

/**
 * Geo JSON object
 * <p>
 * Schema for a Geo JSON object
 *
 * @return
 * The geoLocation
 */
@JsonProperty("geoLocation")
public String getGeoLocation() {
        return geoLocation;
        }

/**
 * Geo JSON object
 * <p>
 * Schema for a Geo JSON object
 *
 * @param geoLocation
 * The geoLocation
 */
@JsonProperty("geoLocation")
public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
        }

/**
 *
 * (Required)
 *
 * @return
 * The currentObjectState
 */
@JsonProperty("currentObjectState")
public ActionInfo getCurrentObjectState() {
        return currentObjectState;
        }

/**
 *
 * (Required)
 *
 * @param currentObjectState
 * The currentObjectState
 */
@JsonProperty("currentObjectState")
public void setCurrentObjectState(ActionInfo currentObjectState) {
        this.currentObjectState = currentObjectState;
        }

/**
 *
 * (Required)
 *
 * @return
 * The originatingGroup
 */
@JsonProperty("originatingGroup")
public String getOriginatingGroup() {
        return originatingGroup;
        }

/**
 *
 * (Required)
 *
 * @param originatingGroup
 * The originatingGroup
 */
@JsonProperty("originatingGroup")
public void setOriginatingGroup(String originatingGroup) {
        this.originatingGroup = originatingGroup;
        }

/**
 *
 * (Required)
 *
 * @return
 * The originatingUser
 */
@JsonProperty("originatingUser")
public String getOriginatingUser() {
        return originatingUser;
        }

/**
 *
 * (Required)
 *
 * @param originatingUser
 * The originatingUser
 */
@JsonProperty("originatingUser")
public void setOriginatingUser(String originatingUser) {
        this.originatingUser = originatingUser;
        }

/**
 * Determines whether a report is a tearline
 * (Required)
 *
 * @return
 * The isTearline
 */
@JsonProperty("isTearline")
public boolean isIsTearline() {
        return isTearline;
        }

/**
 * Determines whether a report is a tearline
 * (Required)
 *
 * @param isTearline
 * The isTearline
 */
@JsonProperty("isTearline")
public void setIsTearline(boolean isTearline) {
        this.isTearline = isTearline;
        }

/**
 *
 * (Required)
 *
 * @return
 * The theaterFilter
 */
@JsonProperty("theaterFilter")
public String getTheaterFilter() {
        return theaterFilter;
        }

/**
 *
 * (Required)
 *
 * @param theaterFilter
 * The theaterFilter
 */
@JsonProperty("theaterFilter")
public void setTheaterFilter(String theaterFilter) {
        this.theaterFilter = theaterFilter;
        }

/**
 *
 * @return
 * The warehouseUri
 */
@JsonProperty("warehouseUri")
public String getWarehouseUri() {
        return warehouseUri;
        }

/**
 *
 * @param warehouseUri
 * The warehouseUri
 */
@JsonProperty("warehouseUri")
public void setWarehouseUri(String warehouseUri) {
        this.warehouseUri = warehouseUri;
        }

@Override
public String toString() {
        return ToStringBuilder.reflectionToString(this);
        }

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
        }

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        }

        }