/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models.cdr.hcr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.issinc.ges.interfaces.te.models.cdr.Remark;
import com.issinc.ges.interfaces.te.models.cdr.tscr.TscrDetails;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.issinc.ges.interfaces.te.models.ActionInfo;
import com.issinc.ges.interfaces.te.models.ReportBase;
import com.issinc.ges.interfaces.te.models.Security;

/**
 * CIDNE Humint HCR Report Definition
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "datamodel",
        "formId",
        "base",
        "location",
        "remarks",
        "requirements",
        "security",
        "objectHistory",
        "details"
})
public class HumintHcr {

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("datamodel")
    private String datamodel;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("formId")
    private String formId;
    /**
     * The core metadata used within all report definitions
     * (Required)
     *
     */
    @JsonProperty("base")
    private ReportBase base;
    /**
     * All locations for the report instance
     * (Required)
     *
     */
    @JsonProperty("location")
    private List<String> location = new ArrayList<String>();
    @JsonProperty("remarks")
    private List<Remark> remarks = new ArrayList<Remark>();
    @JsonProperty("requirements")
    private List<String> requirements = new ArrayList<String>();
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("security")
    private Security security;
    @JsonProperty("objectHistory")
    private List<ActionInfo> objectHistory = new ArrayList<ActionInfo>();
    /**
     * Humint HCR Report details specific details
     * (Required)
     *
     */
    @JsonProperty("details")
    private HcrDetails details;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public HumintHcr() {
    }

    /**
     *
     * @param details
     * @param location
     * @param formId
     * @param datamodel
     * @param objectHistory
     * @param base
     * @param remarks
     * @param security
     * @param requirements
     */
    public HumintHcr(String datamodel, String formId, ReportBase base, List<String> location, List<Remark> remarks, List<String> requirements, Security security, List<ActionInfo> objectHistory, HcrDetails details) {
        this.datamodel = datamodel;
        this.formId = formId;
        this.base = base;
        this.location = location;
        this.remarks = remarks;
        this.requirements = requirements;
        this.security = security;
        this.objectHistory = objectHistory;
        this.details = details;
    }

    /**
     *
     * (Required)
     *
     * @return
     *     The datamodel
     */
    @JsonProperty("datamodel")
    public String getDatamodel() {
        return datamodel;
    }

    /**
     *
     * (Required)
     *
     * @param datamodel
     *     The datamodel
     */
    @JsonProperty("datamodel")
    public void setDatamodel(String datamodel) {
        this.datamodel = datamodel;
    }

    /**
     *
     * (Required)
     *
     * @return
     *     The formId
     */
    @JsonProperty("formId")
    public String getFormId() {
        return formId;
    }

    /**
     *
     * (Required)
     *
     * @param formId
     *     The formId
     */
    @JsonProperty("formId")
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * The core metadata used within all report definitions
     * (Required)
     *
     * @return
     *     The base
     */
    @JsonProperty("base")
    public ReportBase getBase() {
        return base;
    }

    /**
     * The core metadata used within all report definitions
     * (Required)
     *
     * @param base
     *     The base
     */
    @JsonProperty("base")
    public void setBase(ReportBase base) {
        this.base = base;
    }

    /**
     * All locations for the report instance
     * (Required)
     *
     * @return
     *     The location
     */
    @JsonProperty("location")
    public List<String> getLocation() {
        return location;
    }

    /**
     * All locations for the report instance
     * (Required)
     *
     * @param location
     *     The location
     */
    @JsonProperty("location")
    public void setLocation(List<String> location) {
        this.location = location;
    }

    /**
     *
     * @return
     *     The remarks
     */
    @JsonProperty("remarks")
    public List<Remark> getRemarks() {
        return remarks;
    }

    /**
     *
     * @param remarks
     *     The remarks
     */
    @JsonProperty("remarks")
    public void setRemarks(List<Remark> remarks) {
        this.remarks = remarks;
    }

    /**
     *
     * @return
     *     The requirements
     */
    @JsonProperty("requirements")
    public List<String> getRequirements() {
        return requirements;
    }

    /**
     *
     * @param requirements
     *     The requirements
     */
    @JsonProperty("requirements")
    public void setRequirements(List<String> requirements) {
        this.requirements = requirements;
    }

    /**
     *
     * (Required)
     *
     * @return
     *     The security
     */
    @JsonProperty("security")
    public Security getSecurity() {
        return security;
    }

    /**
     *
     * (Required)
     *
     * @param security
     *     The security
     */
    @JsonProperty("security")
    public void setSecurity(Security security) {
        this.security = security;
    }

    /**
     *
     * @return
     *     The objectHistory
     */
    @JsonProperty("objectHistory")
    public List<ActionInfo> getObjectHistory() {
        return objectHistory;
    }

    /**
     *
     * @param objectHistory
     *     The objectHistory
     */
    @JsonProperty("objectHistory")
    public void setObjectHistory(List<ActionInfo> objectHistory) {
        this.objectHistory = objectHistory;
    }

    /**
     * Humint HCR Report details specific details
     * (Required)
     *
     * @return
     *     The details
     */
    @JsonProperty("details")
    public HcrDetails getDetails() {
        return details;
    }

    /**
     * Humint HCR Report details specific details
     * (Required)
     *
     * @param details
     *     The details
     */
    @JsonProperty("details")
    public void setDetails(HcrDetails details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
