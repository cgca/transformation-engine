/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */

package com.issinc.ges.interfaces.te.components;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.utilities.ModelUtils;
import com.issinc.ges.reporting.dal.DALException;
import com.issinc.ges.reporting.dal.DALFactory;
import com.issinc.ges.reporting.dal.DALResult;
import com.issinc.ges.reporting.dal.DalJsonUtils;
import com.issinc.ges.reporting.dal.transformationengine.IRulesetRepo;

/**
 * This is the base class (and POJO) for rules. A RuleSet has a collection of these that perform a particular
 * transformation.  This class can also act as a ruleset in that it can point to another ruleset via
 * ruleSetObjectId.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Rule {

    private static final Logger logger = LoggerFactory.getLogger(Rule.class);

    // Name of the class to create
    private String className;
    // Pointer to another set of rules
    private String ruleSetObjectId;

    // Sub rules are other rule classes that the this rule points to.
    private List<Rule> ruleList;

    /**
     * The default constructor
     */
    public Rule() {
    }

    public final String getClassName() {
        return className;
    }

    public final void setClassName(String className) {
        this.className = className;
    }

    public final String getRuleSetObjectId() {
        return ruleSetObjectId;
    }

    public final void setRuleSetObjectId(String ruleSetObjectId) {
        this.ruleSetObjectId = ruleSetObjectId;
    }

    /**
     * A rule might be a pointer to another set of rules; this method
     * pulls all the child rules to an unbounded (at this point) depth.
     *
     * @param token
     *   Security token for DB call.
     * @return
     *   List of rules.
     * @throws TransformationException
     *   Error retrieving rules
     */
    public final List<Rule> getRulesForNestedRuleset(String token) throws TransformationException {
        if (ruleList == null && ruleSetObjectId != null) {
            ruleList = new ArrayList<>();
            RuleSet ruleSet = null;
            try {
                final IRulesetRepo ruleSetRepo = DALFactory.get(IRulesetRepo.class);
                final DALResult dalResult =
                        ruleSetRepo.getByObjectId(ruleSetObjectId, token);
                if (dalResult.getCount() == 0) {
                    throw new TransformationException(TransformationException.ErrorType.UNKNOWN_RULESET,
                            "Ruleset " + ruleSetObjectId + " not found.");
                }
                final JsonNode jsonNode = DalJsonUtils.unWrapResultsAsJsonNode(dalResult.getResult());
                if (jsonNode.isArray() && jsonNode.size() > 0) {
                    ruleSet = ModelUtils.toObject(jsonNode.get(0), RuleSet.class);
                }
            } catch (DALException e) {
                throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                        "Error retrieving ruleset " + ruleSetObjectId, e);
            }

            if (ruleSet == null || ruleSet.getRules() == null) {
                throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                        "Ruleset contained no rules; configuration issue");
            }

            ruleList.addAll(Arrays.asList(ruleSet.getRules()));
        }

        return ruleList;
    }

    /**
     * This is where the real content is. Every Rule must implement its own applyRule method to take a particular action
     * on the payload.
     * @param token Security token
     * @param payload The data to be transformed by the rule
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    /// CHECKSTYLE:OFF
    public Object applyRule(String token, Object payload) throws TransformationException {
        Object retVal = payload;

        /// CHECKSTYLE:ON
        final List<Rule> nestedRules = getRulesForNestedRuleset(token);
        for (Rule rule : nestedRules) {
            retVal = rule.applyRule(token, retVal);
        }

        return retVal;
    }

    /**
     * Validation of the payload to not be null and to be the expected type.
     *
     * @param payload
     *  Transformation data
     * @param type
     *  Class that the payload is expected to be.
     * @param methodName
     *  Method name of the calling method (for the exception)
     * @throws TransformationException
     *  Exception to be thrown
     */
    protected final void validatePayload(Object payload, Class<?> type, String methodName)
            throws TransformationException {
        if (payload == null || !(type.isInstance(payload))) {
            String message;
            if (payload == null) {
                message = ": Payload is null";
            } else {
                message = ": Invalid payload. Expected " + type.getName() + " but payload is " +
                        payload.getClass().getName();
            }
            logger.error(message);
            throw new TransformationException(TransformationException.ErrorType.PAYLOAD_ERROR,  methodName + message);
        }
    }

} /* End of Class Rule */
