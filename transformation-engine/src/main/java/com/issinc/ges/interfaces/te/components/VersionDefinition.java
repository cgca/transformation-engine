/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.components;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The VersionDefinition class represents a version of something in our system that we recognize. It is a specific
 * version and has a specific format. Examples include a specific report instance or schema in CIDNE 4.1 or a
 * CIDNE 2.3.5 SIGACT report.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VersionDefinition {

    /**
     * The schema that defines the structure of this VersionDefinition. The DAL uses the datamodel to validate the
     * VersionDefinition on insert. VersionDefinitions cannot be inserted into the system unless they validate against
     * the version definition schema.
     */
    private String datamodel;

    /**
     * The objectId is the unique identifier for the particular version. Note that it conforms with the rest of the
     * data managed by the DAL so that later on, we might easily start replicating our transformation engine data.
     */
    private String objectId;

    /**
     * This is a textual description of the version.
     */
    private String description;

    /**
     * The displayName is a more human readbale name for the version. It is used whenever a versionDefinition is
     * displayed to the user. If we create a user interface for defining versions or editing rulesets this is what
     * will be displayed to the user, rather than the objectId which is just a UUID and not ata ll descriptive.
     */
    private String displayName;

    /**
     * The default constructor
     */
    public VersionDefinition() {
    }

    public final String getDatamodel() {
        return datamodel;
    }

    public final void setDatamodel(String datamodel) {
        this.datamodel = datamodel;
    }

    public final String getObjectId() {
        return objectId;
    }

    public final void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public final String getDescription() {
        return description;
    }

    public final void setDescription(String description) {
        this.description = description;
    }

    public final String getDisplayName() {
        return displayName;
    }

    public final void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public final String toString() {
        return "VersionDefinition POJO [" +
                "datamodel = " + datamodel +
                ", objectId = " + objectId +
                ", description = " + description +
                ", displayName = " + displayName +
                "]";
    }

} /* End of Class VersionDefinition */
