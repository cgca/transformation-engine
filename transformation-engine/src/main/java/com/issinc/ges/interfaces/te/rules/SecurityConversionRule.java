/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.JsonRule;
import com.issinc.ges.interfaces.te.models.Security;
import com.issinc.ges.interfaces.te.utilities.DataConversionUtils;
import com.issinc.ges.interfaces.te.utilities.JsonConstants;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;
import com.issinc.ges.interfaces.te.utilities.JsonUtils;
import com.issinc.ges.reporting.dal.DALException;
import com.issinc.ges.reporting.dal.DALFactory;
import com.issinc.ges.reporting.dal.DALResult;
import com.issinc.ges.reporting.dal.DalJsonUtils;
import com.issinc.ges.reporting.dal.transformationengine.ICapcoMappingRepo;

/**
 * A rule for between 2x and 4x security objects
 */
public class SecurityConversionRule extends JsonRule {

    private static final Logger logger = LoggerFactory.getLogger(SecurityConversionRule.class);

    private static final String ERROR_QUERYING_FOR_CAPCO_MAPPINGS = "Error querying for CAPCO Mappings";
    private static final String DOT_DOT = "..";

    private String classificationField;
    private String releasabilityField;
    private String securityBlockPath;
    private String conversionDirection;

    public boolean isCreatePath() {
        return createPath;
    }

    public void setCreatePath(boolean createPath) {
        this.createPath = createPath;
    }

    //if the objects leading up to the new field path should be created or not
    private boolean createPath = true;
    /**
     * The default constructor
     */
    public SecurityConversionRule() {
        super();
    }

    /**
     * The constructor
     * @param classificationField The classification field
     * @param releasabilityField  The releasability field
     * @param securityBlockPath The path to the ISM Security Markings
     * @param conversionDirection "2xTo4x" or "4xto2x"
     */
    public SecurityConversionRule(String classificationField, String releasabilityField,
            String securityBlockPath, String conversionDirection) {
        super();
        setClassName(SecurityConversionRule.class.getName());
        setClassificationField(classificationField);
        setReleasabilityField(releasabilityField);
        setSecurityBlockPath(securityBlockPath);
        setConversionDirection(conversionDirection);
    }

    public final String getClassificationField() {
        return classificationField;
    }

    public final void setClassificationField(String classificationField) {
        this.classificationField = classificationField;
    }

    public final String getReleasabilityField() {
        return releasabilityField;
    }

    public final void setReleasabilityField(String releasabilityField) {
        this.releasabilityField = releasabilityField;
    }

    public final String getSecurityBlockPath() {
        return securityBlockPath;
    }

    public final void setSecurityBlockPath(String securityBlockPath) {
        this.securityBlockPath = securityBlockPath;
    }

    public final String getConversionDirection() {
        return conversionDirection;
    }

    public final void setConversionDirection(String conversionDirection) {
        this.conversionDirection = conversionDirection;
    }


    /**
     * This is where the real content is. Every Rule must implement its own applyRule method to take a particular action
     * on the payload.
     *
     * @param token   Security token
     * @param payload The data to be transformed by the rule
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    @Override public final JsonNode applyRule(String token, JsonNode payload) throws TransformationException {

        if ("2xTo4x".equals(conversionDirection)) {
            convert2xTo4xSecurity(payload, token);
        } else if ("4xto2x".equals(conversionDirection)) {
            convert4xto2xSecurity(payload, token);
        } else {
            throw new TransformationException(TransformationException.ErrorType.ATTRIBUTE_ERROR,
                    "missing conversion direction");
        }


        return payload;
    }

    private void convert2xTo4xSecurity(JsonNode payload, String token) throws TransformationException {
        //find the node that holds the class/rel
        String parentNodeStr = null;
        String classificationName = null;
        String releasabilityFieldName = null;
        if (classificationField.contains(".")) {
            parentNodeStr = classificationField.substring(0, classificationField.lastIndexOf("."));
            classificationName = classificationField.substring(classificationField.lastIndexOf(".") + 1);
            releasabilityFieldName = releasabilityField.substring(releasabilityField.lastIndexOf(".") + 1);
        }

        if (parentNodeStr != null) {
            final ArrayList<JsonNode> parentNodes = JsonUtils.find(payload, parentNodeStr);
            for (JsonNode parentNode1 : parentNodes) {
                if (parentNode1 instanceof ArrayNode) {
                    for (int j = 0; j < parentNode1.size(); j++) {
                        final ObjectNode parentNode = (ObjectNode) parentNode1.get(j);
                        handle2xSecurityNode(parentNode, classificationName, releasabilityFieldName, token);
                    }
                } else {
                    final ObjectNode parentNode = (ObjectNode) parentNode1;
                    handle2xSecurityNode(parentNode, classificationName, releasabilityFieldName, token);
                }
            }

        } else {
            handle2xSecurityNode((ObjectNode) payload, classificationField, releasabilityField, token);
        }
    }

    private void handle2xSecurityNode(ObjectNode parentNode, String classificationName,
            String releasabilityFieldName, String token)
            throws TransformationException {
        final JsonNode classNode = parentNode.get(classificationName);
        final JsonNode relNode = parentNode.get(releasabilityFieldName);



        ObjectNode nodeToAddSecurity = parentNode;
        String pathToSecurity = securityBlockPath;

        //if securityBlockPath has ".", need to put security into parent node
        if (securityBlockPath != null && securityBlockPath.contains(".")) {
            final String parentNodeStr = securityBlockPath.substring(0, securityBlockPath.lastIndexOf("."));

            final JsonEditor je = new JsonEditor(parentNode);
            ObjectNode fieldLevelParent = (ObjectNode) je.find(parentNodeStr);

            if (fieldLevelParent == null&& createPath) {
                //parent doesn't exist, need to create it
                parentNode.set(parentNodeStr, JsonNodeFactory.instance.objectNode());
                fieldLevelParent = (ObjectNode) parentNode.get(parentNodeStr);
            }
            pathToSecurity =
                    securityBlockPath.substring(securityBlockPath.lastIndexOf(".") + 1);

            nodeToAddSecurity = fieldLevelParent;
        }

        if(nodeToAddSecurity!=null) {
            if (classNode == null || relNode == null) {
                //security can be optional, so at least add the security node in
                nodeToAddSecurity.set(pathToSecurity, null);
            } else {
                final JsonNode securityBlock = lookupSecurityBlock(classNode.asText(), relNode.asText(), token);
                nodeToAddSecurity.set(pathToSecurity, securityBlock.get("ismSecurityMarkingExample"));
            }
        }

        parentNode.remove(classificationName);
        parentNode.remove(releasabilityFieldName);
    }

    /**
     * Lookup the security block in the capco mapping repo by classification and releasability
     * @param classification Security classification
     * @param releasability Releasability
     * @param token Security token
     * @return JsonNode containing the security block
     * @throws TransformationException if an error occurs
     */
    public static JsonNode lookupSecurityBlock(String classification, String releasability, String token)
        throws TransformationException {

        JsonNode securityBlockNode;
        final String notFoundMessage = "No capco mappings found for classification: " + classification +
                " and releasability: " + releasability + " - Using simplified approach.";
        try {
            final ICapcoMappingRepo capcoMappingRepo = DALFactory.get(ICapcoMappingRepo.class);
            final DALResult dalResult =
                    capcoMappingRepo.getByClassificationAndReleasability(classification, releasability, token);
            if (dalResult.getCount() > 0) {
                securityBlockNode = DalJsonUtils.unWrapResultNodes(dalResult.getResult()).get(0);
            } else {
                logger.warn(notFoundMessage);
                securityBlockNode = createSimpleSecurityBlock(classification, releasability);
            }
        } catch (DALException e) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                    ERROR_QUERYING_FOR_CAPCO_MAPPINGS, e);
        }
        return securityBlockNode;
    }

    // If there is no capco mapping for the given classification and releasability, use this simplified approach
    private static JsonNode createSimpleSecurityBlock(String classification, String releasability) {
        Security security;
        JsonNode result;
        final String convertedClassification = DataConversionUtils.convertClassification(classification);
        security = new Security();
        security.setClassification(convertedClassification);
        security.setOwnerProducer(releasability);
        result = com.issinc.ges.commons.utils.JsonUtils.objectToJson(security);
        return result;
    }

    //very basic classification releasability mapping
    private String createClassification(Security security) {
        String classification;
        classification = DataConversionUtils.convertClassification(security.getClassification());
        return classification;
    }

    //very basic default basic classification releasability mapping
    private String createReleasability(Security security) {
        return security.getOwnerProducer();
    }

    private void convert4xto2xSecurity(JsonNode payload, String token) throws TransformationException {

        //find the node that holds the class/rel
        String parentNodeStr = null;
        String grandparentNodeStr = null;
        String securityPathName = securityBlockPath;
        if (securityBlockPath.contains(".")) {
            parentNodeStr = securityBlockPath.substring(0, securityBlockPath.lastIndexOf("."));

            securityPathName = securityBlockPath.substring(securityBlockPath.lastIndexOf(".") +  1);
        }

        //if there is a leading ".." in the classification field then the classification and releasability need
        // saved to the grandparent node rather than just the parent.  This is the case for field level security nodes.
        if (classificationField.contains(DOT_DOT)) {
            classificationField = classificationField.replace(DOT_DOT, "");
            releasabilityField = releasabilityField.replace(DOT_DOT, "");
            if (parentNodeStr != null && parentNodeStr.contains(".")) {
                grandparentNodeStr = parentNodeStr.substring(0, parentNodeStr.lastIndexOf("."));
                parentNodeStr = parentNodeStr.substring(parentNodeStr.lastIndexOf(".") + 1);
            }
            if (grandparentNodeStr != null) {
                final ArrayList<JsonNode> grandParentNodes = JsonUtils.find(payload, grandparentNodeStr);
                for (JsonNode grandParentNode1 : grandParentNodes) {
                    if (grandParentNode1.isArray()) {
                        for (int j = 0; j < grandParentNode1.size(); j++) {
                            final ObjectNode grandparentNode = (ObjectNode) grandParentNode1.get(j);
                            iterateParentNodes(grandparentNode, parentNodeStr, securityPathName, token);
                        }
                    } else {
                        final ObjectNode grandparentNode = (ObjectNode) grandParentNode1;
                        iterateParentNodes(grandparentNode, parentNodeStr, securityPathName, token);

                    }
                }

            } else {
                //case where the parent is at the top level
                if (parentNodeStr != null) {
                    handleTopLevelParents(payload, token, parentNodeStr, securityPathName);
                }
            }
        } else {
            //just saving 2x nodes to the parent node
            if (parentNodeStr != null) {
                handleTopLevelParents(payload, token, parentNodeStr, securityPathName);
            } else {
                //case to handle top level
                handle4xSecurityNode((ObjectNode) payload, securityPathName, (ObjectNode) payload, token);
            }
        }

    }

    private void handleTopLevelParents(JsonNode payload, String token, String parentNodeStr, String securityPathName)
            throws TransformationException {
        final ArrayList<JsonNode> parentNodes = JsonUtils.find(payload, parentNodeStr);
        for (JsonNode parentNode1 : parentNodes) {
            if (parentNode1.isArray()) {
                for (int j = 0; j < parentNode1.size(); j++) {
                    final ObjectNode parentNode = (ObjectNode) parentNode1.get(j);
                    handle4xSecurityNode(parentNode, securityPathName, (ObjectNode) payload, token);
                }
            } else {
                final ObjectNode parentNode = (ObjectNode) parentNode1;
                handle4xSecurityNode(parentNode, securityPathName, (ObjectNode) payload, token);
            }
        }
    }

    private void iterateParentNodes(ObjectNode grandparentNode, String parentNodeStr, String securityPathName,
            String token)
            throws TransformationException {
        final JsonNode parentNode1 = grandparentNode.get(parentNodeStr);
        if (parentNode1 instanceof ArrayNode) {
            for (int k = 0; k < parentNode1.size(); k++) {
                final ObjectNode parentNode = (ObjectNode) parentNode1.get(k);
                handle4xSecurityNode(parentNode, securityPathName, grandparentNode, token);
            }
        } else {
            final ObjectNode parentNode = (ObjectNode) parentNode1;
            handle4xSecurityNode(parentNode, securityPathName, grandparentNode, token);
        }
    }


    private void handle4xSecurityNode(ObjectNode parentNode, String securityName, ObjectNode newParentNode,
            String token)
            throws TransformationException {
        final JsonNode securityNode = parentNode.get(securityName);
        if(securityNode==null||securityNode.isNull()){
            return;
        }
        final String notFoundMessage = "No capco mappings found for security block: " + securityNode.toString() +
                " - Using simplified approach";

        String classification;
        String releasability;

        // Lookup capcoMarkings by security block
        try {
            final ICapcoMappingRepo capcoMappingRepo = DALFactory.get(ICapcoMappingRepo.class);
            final DALResult dalResult = capcoMappingRepo.getByISMSecurityMarking(securityNode.toString(), token);
            if (dalResult.getCount() > 0) {
                final JsonNode capcoMapping = DalJsonUtils.unWrapResultNodes(dalResult.getResult()).get(0);
                classification = capcoMapping.get(JsonConstants.CAPCO_CLASSIFICATION).asText();
                releasability = capcoMapping.get(JsonConstants.CAPCO_RELEASABILITY).asText();
            } else {
                logger.warn(notFoundMessage);
                final Security security = com.issinc.ges.commons.utils.JsonUtils.toObject(securityNode, Security.class);
                classification = createClassification(security);
                releasability = createReleasability(security);
            }
        } catch (DALException e) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                    ERROR_QUERYING_FOR_CAPCO_MAPPINGS, e);
        }

        newParentNode.put(classificationField, classification);
        newParentNode.put(releasabilityField, releasability);
        parentNode.remove(securityName);
    }

}
