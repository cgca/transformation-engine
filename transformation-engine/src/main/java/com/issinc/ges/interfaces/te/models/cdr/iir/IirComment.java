/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models.cdr.iir;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.issinc.ges.interfaces.te.models.ActionDate;
import com.issinc.ges.interfaces.te.models.Security;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "commentsdata",
        "referenceid",
        "commentsorder",
        "tearlinecommentsreportdateupdated",
        "security"
})
public class IirComment {

    @JsonProperty("commentsdata")
    private String commentsdata;
    @JsonProperty("referenceid")
    private String referenceid;
    @JsonProperty("commentsorder")
    private Integer commentsorder;
    /**
     *
     */
    @JsonProperty("tearlinecommentsreportdateupdated")
    private ActionDate tearlinecommentsreportdateupdated;
    @JsonProperty("security")
    private Security security;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public IirComment() {
    }

    /**
     *
     * @param tearlinecommentsreportdateupdated
     * @param referenceid
     * @param security
     * @param commentsdata
     * @param commentsorder
     */
    public IirComment(String commentsdata, String referenceid, Integer commentsorder, ActionDate tearlinecommentsreportdateupdated, Security security) {
        this.commentsdata = commentsdata;
        this.referenceid = referenceid;
        this.commentsorder = commentsorder;
        this.tearlinecommentsreportdateupdated = tearlinecommentsreportdateupdated;
        this.security = security;
    }

    /**
     *
     * @return
     *     The commentsdata
     */
    @JsonProperty("commentsdata")
    public String getCommentsdata() {
        return commentsdata;
    }

    /**
     *
     * @param commentsdata
     *     The commentsdata
     */
    @JsonProperty("commentsdata")
    public void setCommentsdata(String commentsdata) {
        this.commentsdata = commentsdata;
    }

    /**
     *
     * @return
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public String getReferenceid() {
        return referenceid;
    }

    /**
     *
     * @param referenceid
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public void setReferenceid(String referenceid) {
        this.referenceid = referenceid;
    }

    /**
     *
     * @return
     *     The commentsorder
     */
    @JsonProperty("commentsorder")
    public Integer getCommentsorder() {
        return commentsorder;
    }

    /**
     *
     * @param commentsorder
     *     The commentsorder
     */
    @JsonProperty("commentsorder")
    public void setCommentsorder(Integer commentsorder) {
        this.commentsorder = commentsorder;
    }

    /**
     *
     * @return
     *     The tearlinecommentsreportdateupdated
     */
    @JsonProperty("tearlinecommentsreportdateupdated")
    public ActionDate getTearlinecommentsreportdateupdated() {
        return tearlinecommentsreportdateupdated;
    }

    /**
     *
     * @param tearlinecommentsreportdateupdated
     *     The tearlinecommentsreportdateupdated
     */
    @JsonProperty("tearlinecommentsreportdateupdated")
    public void setTearlinecommentsreportdateupdated(ActionDate tearlinecommentsreportdateupdated) {
        this.tearlinecommentsreportdateupdated = tearlinecommentsreportdateupdated;
    }

    /**
     *
     * @return
     *     The security
     */
    @JsonProperty("security")
    public Security getSecurity() {
        return security;
    }

    /**
     *
     * @param security
     *     The security
     */
    @JsonProperty("security")
    public void setSecurity(Security security) {
        this.security = security;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
