/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.rules;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.StringRule;
import com.issinc.ges.interfaces.te.utilities.jsons2xsd.Json2XsdSchemaConverter;
import com.issinc.ges.interfaces.te.utilities.jsons2xsd.XmlUtil;

/**
 * This rule takes a string containing a JSON schema and transforms it to an XML schema string
 */
public class JsonSchemaToXmlSchema extends StringRule {

    private static final Logger logger = LoggerFactory.getLogger(JsonSchemaToXmlSchema.class);
    private String wrapperName;

    /**
     * The default constructor
     */
    public JsonSchemaToXmlSchema() {
        super();
    }

    @Override
    public final String applyRule(String token, String jsonString) throws TransformationException {

        final String methodName = "JsonSchemaToXmlSchema.applyRule";
        logger.debug(methodName);
        String result = null;

        Reader schemaReader = null;
        try {
            schemaReader = new StringReader(jsonString);
            final String targetNameSpaceUri = "urn:cidneNS";
            final Json2XsdSchemaConverter.OuterWrapping wrapping = Json2XsdSchemaConverter.OuterWrapping.ELEMENT;
            final String name = getWrapperName();
            final Document xsdDocument =
                    Json2XsdSchemaConverter.convert(schemaReader, targetNameSpaceUri, wrapping, name);
            result = XmlUtil.asXmlString(xsdDocument.getDocumentElement());
        } catch (IOException e) {
            throw new TransformationException(
                    TransformationException.ErrorType.PARSING_ERROR,
                    methodName + ": " + jsonString, e);
        } finally {
            try {
                if (schemaReader != null) {
                    schemaReader.close();
                }
            } catch (IOException e) {
                // Do nothing
            }
        }

        return result;

    }

    public final String getWrapperName() {
        return wrapperName;
    }
    public final void setWrapperName(String wrapperName) {
        this.wrapperName = wrapperName;
    }
} /* End of Class JsonSchemaToXmlSchema */
