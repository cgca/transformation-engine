/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.RuleSet;
import com.issinc.ges.interfaces.te.components.VersionDefinition;
import com.issinc.ges.interfaces.te.components.VersionTree;
import com.issinc.ges.reporting.dal.DALException;
import com.issinc.ges.reporting.dal.DALFactory;
import com.issinc.ges.reporting.dal.DALResult;
import com.issinc.ges.reporting.dal.DalJsonUtils;
import com.issinc.ges.reporting.dal.transformationengine.IRulesetRepo;
import com.issinc.ges.reporting.dal.transformationengine.IVersionDefinitionRepo;

/**
 * This is the version definition POJO. It defines a version that can be converted to/from
 */
public final class VersionDefinitionUtilities {

    private static final Logger logger = LoggerFactory.getLogger(VersionDefinitionUtilities.class);

    private static Hashtable<String, VersionDefinition> versionHash = new Hashtable<>();

    private VersionDefinitionUtilities() {
    }

    /**
     * Gets the specified version from the DAL
     * @param versionObjectId The unique id identifying the version to be returned
     * @param token The security token string
     * @return The specific version definition object
     * @throws TransformationException Exception for issues resolving the version object.
     */
    public static VersionDefinition getVersionDefinition(String versionObjectId, String token)
            throws TransformationException {

        VersionDefinition versionDefinition = null;

        if (versionHash.contains(versionObjectId)) {
            versionDefinition = versionHash.get(versionObjectId);
        } else {
            DALResult dalResult;
            try {
                final IVersionDefinitionRepo repository = DALFactory.get(IVersionDefinitionRepo.class);

                // Get the version definition from Mongo
                dalResult = repository.getByObjectId(versionObjectId, token);
                final JsonNode jsonNode = DalJsonUtils.unWrapResultsAsJsonNode(dalResult.getResult());
                if (jsonNode.isArray() && jsonNode.size() > 0) {
                    versionDefinition =  ModelUtils.toObject(jsonNode.get(0), VersionDefinition.class);
                    versionHash.put(versionDefinition.getObjectId(), versionDefinition);
                }
            } catch (DALException e) {
                logger.error("Error retrieving version definition: " + versionObjectId);
                return null;
            }
        }

        if (versionDefinition == null) {
            throw new TransformationException(TransformationException.ErrorType.ATTRIBUTE_ERROR,
                    String.format("Version '%s' not found", versionObjectId));
        }

        return versionDefinition;
    }

    /**
     * This method "crawls" through the version definition files to determine all of the versions to which we could
     * possibly convert given the specified versionObjectId
     * @param versionObjectId The version id of the version for which we should find related versions
     * @param token The security token string
     * @return A list of version definition objects for versions that are related to the one specified
     * @throws TransformationException if an error occurs
     */
    public static ArrayList<VersionDefinition> getCompatibleVersions(String versionObjectId, String token)
            throws TransformationException {

        ArrayList<VersionDefinition> compatibleVersions = new ArrayList<>();

        if (versionObjectId != null) {
            final ArrayList<String> versionArray = new ArrayList<>();
            compatibleVersions = getCompatibleVersionsR(versionObjectId, versionArray, token);
        }

        return compatibleVersions;
    }

    /**
     * This method returns a Map to versions to which we could possibly
     * convert given the specified versionObjectId using the displayName as the key for the map.
     * @param versionObjectId The versionObjectId of the version for which we want to find compatible versions
     * @param  token The security token string
     * @return A map of potential destination versions
     * @throws TransformationException if an error occurs
     */
    public static Map<String, VersionDefinition> getCompatibleVersionMap(String versionObjectId, String token)
            throws TransformationException {

        final Map<String, VersionDefinition> versionDefinitionMap = new HashMap<>();

        if (versionObjectId != null) {
            final List<VersionDefinition> versionDefinitionList = getCompatibleVersions(versionObjectId, token);
            for (VersionDefinition versionDefinition : versionDefinitionList) {
                versionDefinitionMap.put(versionDefinition.getDisplayName(), versionDefinition);
            }
        }

        return versionDefinitionMap;
    }

    /**
     * This method returns a an array of display name string for the versions to which we could possibly
     * convert given the specified versionObjectId.
     * @param versionObjectId The versionObjectId of the version for which we want to find compatible versions
     * @param  token The security token string
     * @return A map of potential destination versions
     * @throws TransformationException if an error occurs
     */
    public static String[] getCompatibleVersionDisplayNames(String versionObjectId, String token)
            throws TransformationException {
        final ArrayList<VersionDefinition> defs = getCompatibleVersions(versionObjectId, token);
        final String[] versions = new String[defs.size()];
        for (int i = 0; i < defs.size(); i++) {
            versions[i] = defs.get(i).getDisplayName();
        }
        return versions;
    }

    /**
     * Gets the specified ruleset from the DAL
     * @param sourceVersionId The source version of the ruleset to be returned
     * @param destinationVersionId The destination version of the ruleset to be returned
     * @param token The security token string
     * @return The specified version definition object
     * @throws TransformationException if an error occurs
     */
    public static RuleSet getRuleSet(String sourceVersionId, String destinationVersionId, String token)
            throws TransformationException {

        RuleSet ruleSet = null;
        try {
            final IRulesetRepo rulesetRepo = DALFactory.get(IRulesetRepo.class);
            final DALResult dalResult =
                    rulesetRepo.getBySourceAndDestinationVersionId(sourceVersionId, destinationVersionId, token);
            final JsonNode jsonNode = DalJsonUtils.unWrapResultsAsJsonNode(dalResult.getResult());
            if (jsonNode.isArray() && jsonNode.size() > 0) {
                ruleSet = ModelUtils.toObject(jsonNode.get(0), RuleSet.class);
            }
        } catch (DALException | TransformationException e) {
            final String message = "Error retrieving ruleset to convert from " + sourceVersionId + " to " +
                    destinationVersionId;
            logger.error(message);
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR, message, e);
        }

        return ruleSet;

    }

    /**
     * Get the path from the sourceVersion to the destinationVersion and return a list of version objectIds
     * @param sourceVersionId The version we are starting with
     * @param destinationVersionId The version we are trying to get to
     * @param token The security token string
     * @return A list of version objectIds representing the transformations needed to transform the data
     * @throws TransformationException If an error occurs
     */
    public static ArrayList<String> getPath(String sourceVersionId, String destinationVersionId, String token)
            throws TransformationException {

        // Initialize the result path
        final ArrayList<String> versionPath = new ArrayList<>();

        // Get the version tree for the source version
        final VersionTree sourceVersionTree = new VersionTree(sourceVersionId, token);

        // Initialize path array before starting recursion
        final ArrayList<VersionTree.Node> path = new ArrayList<>();

        getPathToVersion(destinationVersionId, sourceVersionTree.getRoot(), path);
        for (VersionTree.Node node : path) {
            versionPath.add(node.getVersionDefinition().getObjectId());
        }
        return versionPath;
    }

    //  The recursive part of the getPath method
    private static boolean getPathToVersion(String destinationVersionId,
            VersionTree.Node node,
            ArrayList<VersionTree.Node> path) {

        boolean found = false;

        // Is this the version we have been looking for?
        if (node.getVersionDefinition().getObjectId().equals(destinationVersionId)) {
            path.add(node);
            return true;
        }

        // Guess not, keep traversing the tree
        for (VersionTree.Node child : node.getChildren()) {
            if (!found) {
                found = getPathToVersion(destinationVersionId, child, path);
                if (found) {
                    path.add(0, node);
                }
            }
        }
        return found;
    }

    // The recursive part of the version compatibility crawler (getCompatibleVersions)
    private static ArrayList<VersionDefinition> getCompatibleVersionsR(String versionId,
            ArrayList<String> versionArray, String token) throws TransformationException {

        final ArrayList<VersionDefinition> compatibleVersions = new ArrayList<>();

        if (versionId == null) {
            return compatibleVersions;
        }

        final VersionDefinition versionDefinition = getVersionDefinition(versionId, token);

        if (versionDefinition != null) {

            // Everything is compatible with itself
            versionArray.add(versionId);
            compatibleVersions.add(versionDefinition);

            try {
                final IRulesetRepo rulesetRepo = DALFactory.get(IRulesetRepo.class);
                final DALResult dalResult = rulesetRepo.getRelatedVersionIds(versionId, token);
                final List<String> versionList = DalJsonUtils.unWrapResults(dalResult.getResult());
                for (String compVersionId : versionList) {
                    if (!versionArray.contains(compVersionId)) {
                        final ArrayList<VersionDefinition> compVersions =
                                getCompatibleVersionsR(compVersionId, versionArray, token);
                        for (VersionDefinition vd : compVersions) {
                            compatibleVersions.add(vd);
                            versionArray.add(versionId);
                        }
                    }
                }
            } catch (DALException e) {
                throw new TransformationException(
                        TransformationException.ErrorType.UNKNOWN_ERROR,
                        "getCompatibleVersionsR: " +
                                "Error retrieving related version IDs from the DAL ruleset collection",
                        e);
            }

        }

        return compatibleVersions;
    }

} /* End of Class VersionDefinitionUtilities */
