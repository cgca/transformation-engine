/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.components;

import java.util.Hashtable;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;
import com.issinc.ges.interfaces.te.utilities.JsonUtils;

/**
 * Parent class to AddNode, RenameNode, and DeleteNode to put common logic that
 * operates against a single node.
 */
public abstract class SingleNodeRule extends JsonRule {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SingleNodeRule.class);

    // The path to the location in the json that contains the data needed
    private String path;
    // The conversion method to call to convert the data to a different format
    private String dataConversionMethodName;
    // The default value for the data if nothing is found or a new node is needed
    private Object defaultValue;
    // Parameters used by the data conversion method (optional)
    private Object parameter1;
    private Object parameter2;
    // not an external parameter; defines whether or not the path should be
    // created as we follow it.  Used for AddNode.
    private boolean doCreatePath;

    /**
     * The default constructor
     *
     * @param doCreatePath
     *   Whether the path should be created as we follow it
     */
    public SingleNodeRule(boolean doCreatePath) {
        super();

        this.doCreatePath = doCreatePath;
    }

    public final String getPath() {
        return path;
    }

    public final void setPath(String path) {
        this.path = path;
    }

    public final String getDataConversionMethodName() {
        return dataConversionMethodName;
    }

    public final void setDataConversionMethodName(String dataConversionMethodName) {
        this.dataConversionMethodName = dataConversionMethodName;
    }

    public final Object getDefaultValue() {
        return defaultValue;
    }

    public final void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    public final Object getParameter1() {
        return parameter1;
    }

    public final void setParameter1(Object parameter1) {
        this.parameter1 = parameter1;
    }

    public final Object getParameter2() {
        return parameter2;
    }

    public final void setParameter2(Object parameter2) {
        this.parameter2 = parameter2;
    }

    public final boolean isDoCreatePath() {
        return doCreatePath;
    }

    public final void setDoCreatePath(boolean doCreatePath) {
        this.doCreatePath = doCreatePath;
    }

    @Override
    public final JsonNode applyRule(String token, JsonNode payload) throws TransformationException {
        final String methodName = "SingleNodeRule.applyRule";
        logger.debug(methodName);
        if (path != null) {
            setNode(payload, path);
        } else {
            throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR,
                    "Rule " + this.getClassName() + " must have path set.");
        }

        return payload;
    }

    /**
     * Method to traverse through the JsonNode tree to find the requested node.
     *
     * @param currentNode
     *   Current node in the recursion.
     * @param path
     *   The current path being recursed.
     */
    /// CHECKSTYLE:OFF
    protected void setNode(JsonNode currentNode, String path) throws TransformationException {
        /// CHECKSTYLE:ON
        if (currentNode == null) {
            return;
        }

        final String fieldName = JsonUtils.getFieldName(path);

        if (fieldName.equals(path)) {
            doAction(currentNode, fieldName);
        } else {
            final JsonEditor je = new JsonEditor(currentNode);
            final JsonNode nextNode = je.find(fieldName, isDoCreatePath());

            try {
                if (nextNode != null && nextNode.isArray()) {
                    final int cnt = nextNode.size();
                    for (int i = 0; i < cnt; i++) {
                        setNode(nextNode.get(i), path.substring(path.indexOf(".") + 1));
                    }
                } else {
                    setNode(nextNode, path.substring(path.indexOf(".") + 1));
                }
            } catch (NullPointerException e) {
                final String message = String.format("Rule %s path %s does not exist",
                        this.getClass().getSimpleName(), getPath());
                logger.error(message);
                throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR, message);
            }
        }
    }

    /**
     * A hook to allow each class to perform an action on the node being manipulated.
     *
     * @param node
     *   Node being manipulated
     * @param fieldName
     *   Field name where the node is to be placed.
     */
    protected abstract void doAction(JsonNode node, String fieldName);

}
