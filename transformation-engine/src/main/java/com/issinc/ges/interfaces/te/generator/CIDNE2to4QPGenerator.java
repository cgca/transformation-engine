/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.generator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.commons.utils.ObjectMapperProvider;
import com.issinc.ges.interfaces.te.TransformationEngine;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.utilities.IdMapper;
import com.issinc.ges.interfaces.te.utilities.JsonConstants;
import com.issinc.ges.interfaces.te.utilities.JsonUtils;

/**
 * Generates the QueryParameters from a given report schema
 */
public final class CIDNE2to4QPGenerator {

    public enum QPType { REPORT, ASSOCIATION, LOCATION }

    private static final Logger logger = LoggerFactory.getLogger(CIDNE2to4QPGenerator.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String DASH = "-";

    private CIDNE2to4QPGenerator() {
    }

    /**
     * Gets the list of dateTime QueryParameters defined in the data model
     *
     * @return A JSON node containing all of the query parameter mappings for associations
     */
    public static JsonNode createAssociationQueryParameters() {

        final ArrayList<JsonNode> queryParams = new ArrayList<>();

        JsonNode qpNode = buildQPAlias(JsonConstants.ASSOCIATION_RELATIONSHIP_KEY, JsonConstants.OBJECT_ID);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_TARGET_NAME, JsonConstants.ASSOCIATION_NAME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_DTG_POSTED,
                JsonConstants.OBJECT_HISTORY + "." +
                        JsonConstants.ACTION_DATE + "." + JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_DTG_UPDATED,
                JsonConstants.OBJECT_HISTORY + "." +
                        JsonConstants.ACTION_DATE + "." + JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_GROUP_NAME,
                JsonConstants.OBJECT_HISTORY + "." + JsonConstants.ASSOCIATION_GROUP);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_GROUP_NAME,
                JsonConstants.OBJECT_HISTORY + "." + JsonConstants.ASSOCIATION_PARENT_GROUP);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_ORIGINATING_NATION,
                JsonConstants.OBJECT_HISTORY + "." + JsonConstants.ASSOCIATION_NATIONALITY);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_ORIGINATING_NETWORK,
                JsonConstants.OBJECT_HISTORY + "." + JsonConstants.ASSOCIATION_NETWORK);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_ORIGINATING_SITE,
                JsonConstants.OBJECT_HISTORY + "." + JsonConstants.ASSOCIATION_SITE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_ORIGINATING_SYSTEM,
                JsonConstants.OBJECT_HISTORY + "." + JsonConstants.ASSOCIATION_SYSTEM);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_POSTED_BY,
                JsonConstants.OBJECT_HISTORY + "." + JsonConstants.ASSOCIATION_USER);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_DTG_START,
                JsonConstants.ASSOCIATION_START_DATE + "." + JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_DTG_END,
                JsonConstants.ASSOCIATION_END_DATE + "." + JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_ASSOCIATION_REPORT_ID + "1",
                JsonConstants.ASSOCIATION_ASSOCIATED_OBJECT + "1");
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_ASSOCIATION_REPORT_ID + "2",
                JsonConstants.ASSOCIATION_ASSOCIATED_OBJECT + "2");
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_SOURCE_ROLE, JsonConstants.ASSOCIATION_ASSOCIATED_ROLE + "1");
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_TARGET_ROLE, JsonConstants.ASSOCIATION_ASSOCIATED_ROLE + "2");
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_GROUP_NAME, JsonConstants.ASSOCIATION_ORIGINATING_GROUP);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_POSTED_BY, JsonConstants.ASSOCIATION_ORIGINATING_USER);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_DTG_POSTED,
                JsonConstants.ASSOCIATION_ORIGINATING_DATE + "." + JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_REPORT_NUMBER, JsonConstants.ASSOCIATION_SOURCE_IDENTIFIER);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_SOURCE_TYPE, JsonConstants.ASSOCIATION_SOURCE_TYPE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_SOURCE, JsonConstants.ASSOCIATION_SOURCE_ROLE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_DOI,
                JsonConstants.ASSOCIATION_SOURCE_DOI + "." + JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_KEY_ASSOCIATION, JsonConstants.ASSOCIATION_IS_KEY_ASSOCIATION);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.ASSOCIATION_RECORD_DATE_TIMESTAMP,
                JsonConstants.ASSOCIATION_OBJECT_SEQUENCE);
        addIfUnique(qpNode, queryParams);

        return createNonReportQPNode(QPType.ASSOCIATION, queryParams);

    }

    /**
     * Gets the list of dateTime QueryParameters defined in the data model
     *
     * @return A JSON node containing all of the query parameter mappings for associations
     */
    public static JsonNode createLocationQueryParameters() {

        final ArrayList<JsonNode> queryParams = new ArrayList<>();

        JsonNode qpNode = buildQPAlias(JsonConstants.LOCATION_LOCATION_KEY,
                JsonConstants.LOCATION + "." + JsonConstants.OBJECT_ID);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_ADDRESS,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_ADDRESS);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_ALTERNATE_ROUTE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_ALTERNATE_ROUTE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_AO,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_AO);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_BASECAMP,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_BASECAMP);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_CITY,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_CITY);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_CONFIDENCE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_CONFIDENCE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_COUNTRY,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_COUNTRY);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_DATE_END_LOCATION,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_DATE_END_LOCATION + "." +
                        JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_DATE_START_LOCATION,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_DATE_START_LOCATION + "." +
                        JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_DESCRIPTION,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_DESCRIPTION);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_DISTANCE_FROM_CITY_CENTER,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_DISTANCE_FROM_CITY_CENTER);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_DISTANCE_TO_NEAREST_FOB,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_DISTANCE_TO_NEAREST_FOB);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_DISTANCE_TO_NEAREST_ROUTE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_DISTANCE_TO_NEAREST_ROUTE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_DISTRICT,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_DISTRICT);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_FOB,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_FOB);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_PRIMARY_FLAG,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_IS_PRIMARY);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_LATITUDE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_LATITUDE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_ACTIVE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_ACTIVE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_TYPE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_TYPE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_LONGITUDE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_LONGITUDE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_MSC_CURRENT,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_MSC_CURRENT);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_MSC_DATE_OFCURRENT,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_MSC_DATE_OFCURRENT + "." +
                        JsonConstants.ZULU_DATE_TIME);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_MSC,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_MSC);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_NAI,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_NAI);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_NEAREST_CITY,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_NEAREST_CITY);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_NEAREST_FOB,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_NEAREST_FOB);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_NEAREST_ROUTE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_NEAREST_ROUTE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_NEIGHBORHOOD,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_NEIGHBORHOOD);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_POPULATION_CENTER_TYPE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_POPULATION_CENTER_TYPE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_POPULATION_CENTER,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_POPULATION_CENTER);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_PROVINCE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_PROVINCE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_ROUTE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_ROUTE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_STREET,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_STREET);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_STRUCTURE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_STRUCTURE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_VILLAGE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_VILLAGE);
        addIfUnique(qpNode, queryParams);

        qpNode = buildQPAlias(JsonConstants.LOCATION_ZONE,
                JsonConstants.LOCATION + "." + JsonConstants.LOCATION_ZONE);
        addIfUnique(qpNode, queryParams);

        return createNonReportQPNode(QPType.LOCATION, queryParams);

    }

    /**
     * Gets the list of dateTime QueryParameters defined in the data model
     *
     * @param dataModel - The data model for which to generate query parameters
     * @return A JSON node containing all of the dateTime query parameter mappings for the given schema
     */
    public static JsonNode generateQueryParameters(JsonNode dataModel, String token) {

        final ArrayList<JsonNode> queryParams = new ArrayList<>();
        final String dataModelId = dataModel.get("id").asText();
        final IdMapper idMapper = new IdMapper(dataModelId);
        if (dataModelId == null) {
            logger.error("generateQueryParameters: 'id' attribute missing from data model.");
        }

        // Every schema that has a base.currentObjectState property needs datePosted to be hardcoded
        if (JsonUtils.hasNonNull(dataModel, "properties.base.properties.currentObjectState")) {
            dataModel.get("properties").get("base").get("properties").get("currentObjectState");
            addIfUnique(buildQPAlias("datePosted", "base.currentObjectState.actionDate.zuluDateTime"), queryParams);
        }

        // Every schema that has a base.reportDate property needs dateUpdated to be hardcoded
        if (JsonUtils.hasNonNull(dataModel, "properties.base.properties.reportDate")) {
            dataModel.get("properties").get("base").get("properties").get("reportDate");
            addIfUnique(buildQPAlias("dateUpdated", "base.reportDate.zuluDateTime"), queryParams);
        }

        // Process 2.X aliases in the 'base' section...
        processSection(JsonConstants.BASE, dataModel, queryParams);

        // Process 2.X aliases in the 'details' section...
        processSection(JsonConstants.DETAILS, dataModel, queryParams);

        // Squash the many query parameters into one for the given schema with an array of aliases
        String version;
        try {
            version = TransformationEngine.getInstance().getLegacyVersion(idMapper.getVersion(), token);
            return createReportQPNode(idMapper, queryParams, version);
        } catch (TransformationException e) {
            logger.error("Error looking up version for schemaVersion: " + idMapper.getVersion());
        }
        return null;
    }

    // Creates a single JSON node containing all of the query parameter mappings for a given schema
    private static JsonNode createReportQPNode(IdMapper idMapper, ArrayList<JsonNode> queryParams, String version) {
        final ObjectNode queryParametersForSchema = ObjectMapperProvider.provide().createObjectNode();
        final ArrayNode parameterArrayNode = ObjectMapperProvider.provide().createArrayNode();
        queryParametersForSchema.put(JsonConstants.DATA_MODEL, JsonConstants.TE_DATA_MODEL_ID);
        queryParametersForSchema.put(JsonConstants.TYPE, idMapper.getType().toLowerCase());
        queryParametersForSchema.put(JsonConstants.VERSION, version.toLowerCase());
        queryParametersForSchema.put(JsonConstants.MODULE, idMapper.getModule().toLowerCase());
        queryParametersForSchema.put(JsonConstants.REPORT_TYPE, idMapper.getReportType().toLowerCase());
        queryParametersForSchema.put(JsonConstants.OBJECT_ID, buildObjectId(idMapper).toLowerCase());
        queryParametersForSchema.set(JsonConstants.ALIASES, parameterArrayNode);
        parameterArrayNode.addAll(queryParams);
        return queryParametersForSchema;
    }

    // Creates a single JSON node containing all of the query parameter mappings for a given type (assoc or location)
    private static JsonNode createNonReportQPNode(QPType type, ArrayList<JsonNode> queryParams) {
        final String typeLC = type.toString().toLowerCase();
        final ObjectNode queryParameters = ObjectMapperProvider.provide().createObjectNode();
        final ArrayNode parameterArrayNode = ObjectMapperProvider.provide().createArrayNode();
        queryParameters.put(JsonConstants.DATA_MODEL, JsonConstants.TE_DATA_MODEL_ID);
        queryParameters.put(JsonConstants.TYPE, typeLC);
        queryParameters.put(JsonConstants.OBJECT_ID, typeLC);
        queryParameters.set(JsonConstants.ALIASES, parameterArrayNode);
        parameterArrayNode.addAll(queryParams);
        return queryParameters;
    }

    private static void processSection(String sectionName, JsonNode dataModel, ArrayList<JsonNode> queryParams) {
        if (dataModel.has(JsonConstants.PROPERTIES) && dataModel.get(JsonConstants.PROPERTIES).has(sectionName)) {
            final JsonNode properties =
                    dataModel.get(JsonConstants.PROPERTIES).get(sectionName).get(JsonConstants.PROPERTIES);
            if (properties != null) {
                processDateTimeProperties(properties, sectionName, queryParams);
            }
        }
    }

    private static void processDateTimeProperties(final JsonNode properties,
            final String path, ArrayList<JsonNode> queryParams) {
        final Iterator<Map.Entry<String, JsonNode>> fields = properties.fields();
        while (fields.hasNext()) {
            final Map.Entry<String, JsonNode> entry = fields.next();
            final JsonNode property = entry.getValue();
            // If this property has an alias, grab it
            if (property.has(JsonConstants.TWO_X_ALIAS) && property.has(JsonConstants.DECODED_REFERENCE)) {
                String typeName = property.get(JsonConstants.DECODED_REFERENCE).asText();
                if (typeName.startsWith(JsonConstants.DEFINITIONS_DATE_TIME)) {
                    final JsonNode qpNode = buildQPAlias(property.get(JsonConstants.TWO_X_ALIAS).asText(),
                            path + "." + entry.getKey() + "." + JsonConstants.ZULU_DATE_TIME);
                    addIfUnique(qpNode, queryParams);
                }
            }
        }
    }

    private static JsonNode buildQPAlias(String twoXalias, String fourXmapping) {

        final ObjectNode qpNode = MAPPER.createObjectNode();

        qpNode.put(JsonConstants.TWO_X_ALIAS, twoXalias.toLowerCase());
        qpNode.put(JsonConstants.FOUR_X_MAPPING, fourXmapping);

        return qpNode;
    }

    /**
     * Builds the appropriate objectId for a query parameter
     * @return The objectId string
     */
    private static String buildObjectId(IdMapper idMapper) {
        return idMapper.getVersion() + DASH + idMapper.getType() + DASH +
                idMapper.getModule() + DASH + idMapper.getReportType();
    }

    private static void addIfUnique(JsonNode qpNode, ArrayList<JsonNode> queryParams) {
        // If this QP is already in our list, skip it
        final String newObjectId = qpNode.get(JsonConstants.TWO_X_ALIAS).asText();
        for  (JsonNode node : queryParams) {
            final String tmpObjectId = node.get(JsonConstants.TWO_X_ALIAS).asText();
            if (tmpObjectId.equals(newObjectId)) {
                return;
            }
        }
        queryParams.add(qpNode);
    }
} /* End of Class CIDNE2to4QPGenerator */
