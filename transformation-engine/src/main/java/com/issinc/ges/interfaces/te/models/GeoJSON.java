/*
 * Copyright (c) 2014 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The GeoServer GeoJSON response
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "type",
        "totalFeatures",
        "features",
        "properties"
})
public class GeoJSON {

    /**
     * GeoServer response is in GeoJSON
     * type is GeoJSON type, usually FeatureCollection
     */
    @JsonProperty("type")
    private String type;
    /**
     * Number of features in the GeoServer response 
     */
    @JsonProperty("totalFeatures")
    private int totalFeatures;
    /**
     * Number of features in the GeoServer response 
     */
    @JsonProperty("features")
    private List<GeoJSONFeature> features = new ArrayList<GeoJSONFeature>();
    
    /**
     * The GeoJSON Feature properties 
     */
    @JsonProperty("properties")
    private Map<String, Object> properties = new HashMap<String, Object>();


    /**
     * All features for the GeoServer response
     */
    @JsonProperty("features")
    public List<GeoJSONFeature> getFeatures() {
        return features;
    }

    /**
     * All features for the GeoServer response
     */
    @JsonProperty("features")
    public void setFeatures(List<GeoJSONFeature> features) {
        this.features = features;
    }
    
    @JsonProperty("properties")
    public  Map<String, Object> getProperties() {
        return properties;
    }

    @JsonProperty("properties")
    public void setProperties(String name, Object value) {
        this.properties.put(name, value);
    }    

}
