/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "actionDate",
        "group",
        "parentGroup",
        "nationality",
        "network",
        "objectState",
        "site",
        "system",
        "user"
})
public class ActionInfo {

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("actionDate")
    private ActionDate actionDate;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("group")
    private String group;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("parentGroup")
    private String parentGroup="";
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("nationality")
    private String nationality="";
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("network")
    private String network="";
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("objectState")
    private String objectState;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("site")
    private String site="";
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("system")
    private String system="";
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("user")
    private String user;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public ActionInfo() {
    }

    /**
     *
     * @param site
     * @param system
     * @param nationality
     * @param actionDate
     * @param objectState
     * @param parentGroup
     * @param group
     * @param user
     * @param network
     */
    public ActionInfo(ActionDate actionDate, String group, String parentGroup, String nationality, String network,
            String objectState, String site, String system, String user) {
        this.actionDate = actionDate;
        this.group = group;
        this.parentGroup = parentGroup;
        this.nationality = nationality;
        this.network = network;
        this.objectState = objectState;
        this.site = site;
        this.system = system;
        this.user = user;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The actionDate
     */
    @JsonProperty("actionDate")
    public ActionDate getActionDate() {
        return actionDate;
    }

    /**
     *
     * (Required)
     *
     * @param actionDate
     * The actionDate
     */
    @JsonProperty("actionDate")
    public void setActionDate(ActionDate actionDate) {
        this.actionDate = actionDate;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The group
     */
    @JsonProperty("group")
    public String getGroup() {
        return group;
    }

    /**
     *
     * (Required)
     *
     * @param group
     * The group
     */
    @JsonProperty("group")
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The parentGroup
     */
    @JsonProperty("parentGroup")
    public String getParentGroup() {
        return parentGroup;
    }

    /**
     *
     * (Required)
     *
     * @param parentGroup
     * The parentGroup
     */
    @JsonProperty("parentGroup")
    public void setParentGroup(String parentGroup) {
        this.parentGroup = parentGroup;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The nationality
     */
    @JsonProperty("nationality")
    public String getNationality() {
        return nationality;
    }

    /**
     *
     * (Required)
     *
     * @param nationality
     * The nationality
     */
    @JsonProperty("nationality")
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The network
     */
    @JsonProperty("network")
    public String getNetwork() {
        return network;
    }

    /**
     *
     * (Required)
     *
     * @param network
     * The network
     */
    @JsonProperty("network")
    public void setNetwork(String network) {
        this.network = network;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The objectState
     */
    @JsonProperty("objectState")
    public String getObjectState() {
        return objectState;
    }

    /**
     *
     * (Required)
     *
     * @param objectState
     * The objectState
     */
    @JsonProperty("objectState")
    public void setObjectState(String objectState) {
        this.objectState = objectState;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The site
     */
    @JsonProperty("site")
    public String getSite() {
        return site;
    }

    /**
     *
     * (Required)
     *
     * @param site
     * The site
     */
    @JsonProperty("site")
    public void setSite(String site) {
        this.site = site;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The system
     */
    @JsonProperty("system")
    public String getSystem() {
        return system;
    }

    /**
     *
     * (Required)
     *
     * @param system
     * The system
     */
    @JsonProperty("system")
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The user
     */
    @JsonProperty("user")
    public String getUser() {
        return user;
    }

    /**
     *
     * (Required)
     *
     * @param user
     * The user
     */
    @JsonProperty("user")
    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
