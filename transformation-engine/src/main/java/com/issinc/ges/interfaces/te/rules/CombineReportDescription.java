/*
* Copyright (c) 2015 Intelligent Software Solutions, Inc.
* Unpublished-all rights reserved under the copyright laws of the United States.
*
* This software was developed under sponsorship from
* the CSC/42Six under:
* xxx xxx-xxxx-xxxx
*
* Contractor: Intelligent Software Solutions, Inc.
* 5450 Tech Center Drive, Suite 400
* Colorado Springs, 80919
* http://www.issinc.com
*
* Intelligent Software Solutions, Inc. has title to the rights in this computer
* software. The Government's rights to use, modify, reproduce, release, perform,
* display, or disclose these technical data are restricted by paragraph (b)(1) of
* the Rights in Technical Data-Noncommercial Items clause contained in
* Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
* Any reproduction of technical data or portions thereof marked with this legend
* must also reproduce the markings.
*
* Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
* aforementioned unlimited government rights to use, disclose, copy, or make
* derivative works of this software to parties outside the Government.
*/
package com.issinc.ges.interfaces.te.rules;

import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.Rule;

/**
 * Rule to wrap any data in the ReportDescription element as CDATA
 */
public class CombineReportDescription extends Rule {

    /**
     * The default constructor
     */
    public CombineReportDescription() {
        super();
    }


    //accepts an xml string and returns the string with the Report description text captured in
    // CDATA element
    @Override
    public final Object applyRule(String token, Object payload) throws TransformationException {

        String REPORTDESC = "<ReportDescription>";
        String REPORTDESC_END = "</ReportDescription>";
        String CDATA_START = "<![CDATA[";
        String CDATA_END = "]]>";
        String payloadStr = (String) payload;
        int idx = payloadStr.indexOf(REPORTDESC)+REPORTDESC.length();
        int endIdx = payloadStr.indexOf(REPORTDESC_END);
        int cdataIdx = payloadStr.indexOf(CDATA_START, idx);

        //make sure there is a report description and it doesn't already have
        //CDATA tag
        if(idx>0  && endIdx >0 && (cdataIdx <idx || cdataIdx > endIdx)) {
            payloadStr = payloadStr.substring(0, idx) + CDATA_START +
                    payloadStr.substring(idx, endIdx) + CDATA_END +
                    payloadStr.substring(endIdx);
        }
        return payloadStr;
    }
}
