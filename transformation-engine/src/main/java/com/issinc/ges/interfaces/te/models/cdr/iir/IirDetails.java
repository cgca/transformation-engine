/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models.cdr.iir;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.issinc.ges.interfaces.te.models.cdr.Country;
import com.issinc.ges.interfaces.te.models.cdr.HumintSource;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.issinc.ges.interfaces.te.models.ActionDate;
import com.issinc.ges.interfaces.te.models.FieldSecurity;

/**
 * Humint IIR Report details specific details
 *
 */

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "acquiredlocation",
        "cite",
        "classifiedby",
        "countrysecurity",
        "derivedfrom",
        "disseminate",
        "instructionsyesno",
        "m3distrolist",
        "nmhd208version",
        "passto",
        "pla",
        "precedence",
        "priorityintelrequirements",
        "reason",
        "sourceparagraph",
        "reporttitlesecurity",
        "reportsummarysecurity",
        "acquireddate",
        "agency",
        "coll",
        "cutoffdate",
        "doi",
        "enclosures",
        "instructions",
        "ipsp",
        "jointrep",
        "military",
        "poc",
        "preparedby",
        "relatedrequirements",
        "reporttext",
        "statelocal",
        "topic",
        "usmission",
        "warning",
        "warningoptional",
        "comments",
        "country",
        "source",
        "text"
})
public class IirDetails {

    @JsonProperty("acquiredlocation")
    private String acquiredlocation;
    @JsonProperty("cite")
    private String cite;
    @JsonProperty("classifiedby")
    private String classifiedby;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("countrysecurity")
    private Object countrysecurity;
    @JsonProperty("derivedfrom")
    private String derivedfrom;
    @JsonProperty("disseminate")
    private String disseminate;
    @JsonProperty("instructionsyesno")
    private String instructionsyesno;
    @JsonProperty("m3distrolist")
    private String m3distrolist;
    /**
     * NMHD208Version
     *
     */
    @JsonProperty("nmhd208version")
    private Boolean nmhd208version;
    @JsonProperty("passto")
    private String passto;
    @JsonProperty("pla")
    private String pla;
    @JsonProperty("precedence")
    private String precedence;
    @JsonProperty("priorityintelrequirements")
    private String priorityintelrequirements;
    @JsonProperty("reason")
    private String reason;
    @JsonProperty("sourceparagraph")
    private String sourceparagraph;
    @JsonProperty("reporttitlesecurity")
    private Object reporttitlesecurity;
    @JsonProperty("reportsummarysecurity")
    private Object reportsummarysecurity;
    /**
     * AcquiredDate
     *
     */
    @JsonProperty("acquireddate")
    private ActionDate acquireddate;
    /**
     * Agency
     *
     */
    @JsonProperty("agency")
    private FieldSecurity agency;
    /**
     * Coll
     *
     */
    @JsonProperty("coll")
    private FieldSecurity coll;
    /**
     * CutOffDate
     *
     */
    @JsonProperty("cutoffdate")
    private ActionDate cutoffdate;
    /**
     * DOI
     *
     */
    @JsonProperty("doi")
    private FieldSecurity doi;
    /**
     * Enclosures
     *
     */
    @JsonProperty("enclosures")
    private FieldSecurity enclosures;
    /**
     * Instructions
     *
     */
    @JsonProperty("instructions")
    private FieldSecurity instructions;
    /**
     * IPSP
     *
     */
    @JsonProperty("ipsp")
    private FieldSecurity ipsp;
    /**
     * JointRep
     *
     */
    @JsonProperty("jointrep")
    private FieldSecurity jointrep;
    /**
     * Military
     *
     */
    @JsonProperty("military")
    private FieldSecurity military;
    /**
     * Poc
     *
     */
    @JsonProperty("poc")
    private FieldSecurity poc;
    /**
     * PreparedBy
     *
     */
    @JsonProperty("preparedby")
    private FieldSecurity preparedby;
    /**
     * RelatedRequirements
     *
     */
    @JsonProperty("relatedrequirements")
    private FieldSecurity relatedrequirements;
    /**
     * ReportText
     *
     */
    @JsonProperty("reporttext")
    private FieldSecurity reporttext;
    /**
     * StateLocal
     *
     */
    @JsonProperty("statelocal")
    private FieldSecurity statelocal;
    /**
     * Topic
     *
     */
    @JsonProperty("topic")
    private FieldSecurity topic;
    /**
     * USMission
     *
     */
    @JsonProperty("usmission")
    private FieldSecurity usmission;
    /**
     * Warning
     *
     */
    @JsonProperty("warning")
    private FieldSecurity warning;
    /**
     * WarningOptional
     *
     */
    @JsonProperty("warningoptional")
    private FieldSecurity warningoptional;
    /**
     * Humint IIR Report Comments specific details
     *
     */
    @JsonProperty("comments")
    private List<IirComment> comments = new ArrayList<IirComment>();
    /**
     * Humint IIR Report Country specific details
     *
     */
    @JsonProperty("country")
    private List<Country> country = new ArrayList<Country>();
    /**
     * Humint IIR Report Source specific details
     *
     */
    @JsonProperty("source")
    private List<HumintSource> source = new ArrayList<HumintSource>();
    /**
     * Humint IIR Report Text specific details
     *
     */
    @JsonProperty("text")
    private List<IirText> text = new ArrayList<IirText>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public IirDetails() {
    }

    /**
     *
     * @param cutoffdate
     * @param pla
     * @param jointrep
     * @param instructions
     * @param reason
     * @param precedence
     * @param nmhd208version
     * @param military
     * @param usmission
     * @param reporttitlesecurity
     * @param reportsummarysecurity
     * @param disseminate
     * @param coll
     * @param relatedrequirements
     * @param poc
     * @param passto
     * @param m3distrolist
     * @param topic
     * @param ipsp
     * @param text
     * @param classifiedby
     * @param preparedby
     * @param agency
     * @param warningoptional
     * @param acquiredlocation
     * @param statelocal
     * @param sourceparagraph
     * @param enclosures
     * @param country
     * @param reporttext
     * @param cite
     * @param instructionsyesno
     * @param derivedfrom
     * @param source
     * @param acquireddate
     * @param priorityintelrequirements
     * @param countrysecurity
     * @param comments
     * @param warning
     * @param doi
     */
    public IirDetails(String acquiredlocation, String cite, String classifiedby, Object countrysecurity, String derivedfrom, String disseminate, String instructionsyesno, String m3distrolist, Boolean nmhd208version, String passto, String pla, String precedence, String priorityintelrequirements, String reason, String sourceparagraph, Object reporttitlesecurity, Object reportsummarysecurity, ActionDate acquireddate, FieldSecurity agency, FieldSecurity coll, ActionDate cutoffdate, FieldSecurity doi, FieldSecurity enclosures, FieldSecurity instructions, FieldSecurity ipsp, FieldSecurity jointrep, FieldSecurity military, FieldSecurity poc, FieldSecurity preparedby, FieldSecurity relatedrequirements, FieldSecurity reporttext, FieldSecurity statelocal, FieldSecurity topic, FieldSecurity usmission, FieldSecurity warning, FieldSecurity warningoptional, List<IirComment> comments, List<Country> country, List<HumintSource> source, List<IirText> text) {
        this.acquiredlocation = acquiredlocation;
        this.cite = cite;
        this.classifiedby = classifiedby;
        this.countrysecurity = countrysecurity;
        this.derivedfrom = derivedfrom;
        this.disseminate = disseminate;
        this.instructionsyesno = instructionsyesno;
        this.m3distrolist = m3distrolist;
        this.nmhd208version = nmhd208version;
        this.passto = passto;
        this.pla = pla;
        this.precedence = precedence;
        this.priorityintelrequirements = priorityintelrequirements;
        this.reason = reason;
        this.sourceparagraph = sourceparagraph;
        this.reporttitlesecurity = reporttitlesecurity;
        this.reportsummarysecurity = reportsummarysecurity;
        this.acquireddate = acquireddate;
        this.agency = agency;
        this.coll = coll;
        this.cutoffdate = cutoffdate;
        this.doi = doi;
        this.enclosures = enclosures;
        this.instructions = instructions;
        this.ipsp = ipsp;
        this.jointrep = jointrep;
        this.military = military;
        this.poc = poc;
        this.preparedby = preparedby;
        this.relatedrequirements = relatedrequirements;
        this.reporttext = reporttext;
        this.statelocal = statelocal;
        this.topic = topic;
        this.usmission = usmission;
        this.warning = warning;
        this.warningoptional = warningoptional;
        this.comments = comments;
        this.country = country;
        this.source = source;
        this.text = text;
    }

    /**
     *
     * @return
     *     The acquiredlocation
     */
    @JsonProperty("acquiredlocation")
    public String getAcquiredlocation() {
        return acquiredlocation;
    }

    /**
     *
     * @param acquiredlocation
     *     The acquiredlocation
     */
    @JsonProperty("acquiredlocation")
    public void setAcquiredlocation(String acquiredlocation) {
        this.acquiredlocation = acquiredlocation;
    }

    /**
     *
     * @return
     *     The cite
     */
    @JsonProperty("cite")
    public String getCite() {
        return cite;
    }

    /**
     *
     * @param cite
     *     The cite
     */
    @JsonProperty("cite")
    public void setCite(String cite) {
        this.cite = cite;
    }

    /**
     *
     * @return
     *     The classifiedby
     */
    @JsonProperty("classifiedby")
    public String getClassifiedby() {
        return classifiedby;
    }

    /**
     *
     * @param classifiedby
     *     The classifiedby
     */
    @JsonProperty("classifiedby")
    public void setClassifiedby(String classifiedby) {
        this.classifiedby = classifiedby;
    }

    /**
     *
     * (Required)
     *
     * @return
     *     The countrysecurity
     */
    @JsonProperty("countrysecurity")
    public Object getCountrysecurity() {
        return countrysecurity;
    }

    /**
     *
     * (Required)
     *
     * @param countrysecurity
     *     The countrysecurity
     */
    @JsonProperty("countrysecurity")
    public void setCountrysecurity(Object countrysecurity) {
        this.countrysecurity = countrysecurity;
    }

    /**
     *
     * @return
     *     The derivedfrom
     */
    @JsonProperty("derivedfrom")
    public String getDerivedfrom() {
        return derivedfrom;
    }

    /**
     *
     * @param derivedfrom
     *     The derivedfrom
     */
    @JsonProperty("derivedfrom")
    public void setDerivedfrom(String derivedfrom) {
        this.derivedfrom = derivedfrom;
    }

    /**
     *
     * @return
     *     The disseminate
     */
    @JsonProperty("disseminate")
    public String getDisseminate() {
        return disseminate;
    }

    /**
     *
     * @param disseminate
     *     The disseminate
     */
    @JsonProperty("disseminate")
    public void setDisseminate(String disseminate) {
        this.disseminate = disseminate;
    }

    /**
     *
     * @return
     *     The instructionsyesno
     */
    @JsonProperty("instructionsyesno")
    public String getInstructionsyesno() {
        return instructionsyesno;
    }

    /**
     *
     * @param instructionsyesno
     *     The instructionsyesno
     */
    @JsonProperty("instructionsyesno")
    public void setInstructionsyesno(String instructionsyesno) {
        this.instructionsyesno = instructionsyesno;
    }

    /**
     *
     * @return
     *     The m3distrolist
     */
    @JsonProperty("m3distrolist")
    public String getM3distrolist() {
        return m3distrolist;
    }

    /**
     *
     * @param m3distrolist
     *     The m3distrolist
     */
    @JsonProperty("m3distrolist")
    public void setM3distrolist(String m3distrolist) {
        this.m3distrolist = m3distrolist;
    }

    /**
     * NMHD208Version
     *
     * @return
     *     The nmhd208version
     */
    @JsonProperty("nmhd208version")
    public Boolean getNmhd208version() {
        return nmhd208version;
    }

    /**
     * NMHD208Version
     *
     * @param nmhd208version
     *     The nmhd208version
     */
    @JsonProperty("nmhd208version")
    public void setNmhd208version(Boolean nmhd208version) {
        this.nmhd208version = nmhd208version;
    }

    /**
     *
     * @return
     *     The passto
     */
    @JsonProperty("passto")
    public String getPassto() {
        return passto;
    }

    /**
     *
     * @param passto
     *     The passto
     */
    @JsonProperty("passto")
    public void setPassto(String passto) {
        this.passto = passto;
    }

    /**
     *
     * @return
     *     The pla
     */
    @JsonProperty("pla")
    public String getPla() {
        return pla;
    }

    /**
     *
     * @param pla
     *     The pla
     */
    @JsonProperty("pla")
    public void setPla(String pla) {
        this.pla = pla;
    }

    /**
     *
     * @return
     *     The precedence
     */
    @JsonProperty("precedence")
    public String getPrecedence() {
        return precedence;
    }

    /**
     *
     * @param precedence
     *     The precedence
     */
    @JsonProperty("precedence")
    public void setPrecedence(String precedence) {
        this.precedence = precedence;
    }

    /**
     *
     * @return
     *     The priorityintelrequirements
     */
    @JsonProperty("priorityintelrequirements")
    public String getPriorityintelrequirements() {
        return priorityintelrequirements;
    }

    /**
     *
     * @param priorityintelrequirements
     *     The priorityintelrequirements
     */
    @JsonProperty("priorityintelrequirements")
    public void setPriorityintelrequirements(String priorityintelrequirements) {
        this.priorityintelrequirements = priorityintelrequirements;
    }

    /**
     *
     * @return
     *     The reason
     */
    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     *     The reason
     */
    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     *     The sourceparagraph
     */
    @JsonProperty("sourceparagraph")
    public String getSourceparagraph() {
        return sourceparagraph;
    }

    /**
     *
     * @param sourceparagraph
     *     The sourceparagraph
     */
    @JsonProperty("sourceparagraph")
    public void setSourceparagraph(String sourceparagraph) {
        this.sourceparagraph = sourceparagraph;
    }

    /**
     *
     * @return
     *     The reporttitlesecurity
     */
    @JsonProperty("reporttitlesecurity")
    public Object getReporttitlesecurity() {
        return reporttitlesecurity;
    }

    /**
     *
     * @param reporttitlesecurity
     *     The reporttitlesecurity
     */
    @JsonProperty("reporttitlesecurity")
    public void setReporttitlesecurity(Object reporttitlesecurity) {
        this.reporttitlesecurity = reporttitlesecurity;
    }

    /**
     *
     * @return
     *     The reportsummarysecurity
     */
    @JsonProperty("reportsummarysecurity")
    public Object getReportsummarysecurity() {
        return reportsummarysecurity;
    }

    /**
     *
     * @param reportsummarysecurity
     *     The reportsummarysecurity
     */
    @JsonProperty("reportsummarysecurity")
    public void setReportsummarysecurity(Object reportsummarysecurity) {
        this.reportsummarysecurity = reportsummarysecurity;
    }

    /**
     * AcquiredDate
     *
     * @return
     *     The acquireddate
     */
    @JsonProperty("acquireddate")
    public ActionDate getAcquireddate() {
        return acquireddate;
    }

    /**
     * AcquiredDate
     *
     * @param acquireddate
     *     The acquireddate
     */
    @JsonProperty("acquireddate")
    public void setAcquireddate(ActionDate acquireddate) {
        this.acquireddate = acquireddate;
    }

    /**
     * Agency
     *
     * @return
     *     The agency
     */
    @JsonProperty("agency")
    public FieldSecurity getAgency() {
        return agency;
    }

    /**
     * Agency
     *
     * @param agency
     *     The agency
     */
    @JsonProperty("agency")
    public void setAgency(FieldSecurity agency) {
        this.agency = agency;
    }

    /**
     * Coll
     *
     * @return
     *     The coll
     */
    @JsonProperty("coll")
    public FieldSecurity getColl() {
        return coll;
    }

    /**
     * Coll
     *
     * @param coll
     *     The coll
     */
    @JsonProperty("coll")
    public void setColl(FieldSecurity coll) {
        this.coll = coll;
    }

    /**
     * CutOffDate
     *
     * @return
     *     The cutoffdate
     */
    @JsonProperty("cutoffdate")
    public ActionDate getCutoffdate() {
        return cutoffdate;
    }

    /**
     * CutOffDate
     *
     * @param cutoffdate
     *     The cutoffdate
     */
    @JsonProperty("cutoffdate")
    public void setCutoffdate(ActionDate cutoffdate) {
        this.cutoffdate = cutoffdate;
    }

    /**
     * DOI
     *
     * @return
     *     The doi
     */
    @JsonProperty("doi")
    public FieldSecurity getDoi() {
        return doi;
    }

    /**
     * DOI
     *
     * @param doi
     *     The doi
     */
    @JsonProperty("doi")
    public void setDoi(FieldSecurity doi) {
        this.doi = doi;
    }

    /**
     * Enclosures
     *
     * @return
     *     The enclosures
     */
    @JsonProperty("enclosures")
    public FieldSecurity getEnclosures() {
        return enclosures;
    }

    /**
     * Enclosures
     *
     * @param enclosures
     *     The enclosures
     */
    @JsonProperty("enclosures")
    public void setEnclosures(FieldSecurity enclosures) {
        this.enclosures = enclosures;
    }

    /**
     * Instructions
     *
     * @return
     *     The instructions
     */
    @JsonProperty("instructions")
    public FieldSecurity getInstructions() {
        return instructions;
    }

    /**
     * Instructions
     *
     * @param instructions
     *     The instructions
     */
    @JsonProperty("instructions")
    public void setInstructions(FieldSecurity instructions) {
        this.instructions = instructions;
    }

    /**
     * IPSP
     *
     * @return
     *     The ipsp
     */
    @JsonProperty("ipsp")
    public FieldSecurity getIpsp() {
        return ipsp;
    }

    /**
     * IPSP
     *
     * @param ipsp
     *     The ipsp
     */
    @JsonProperty("ipsp")
    public void setIpsp(FieldSecurity ipsp) {
        this.ipsp = ipsp;
    }

    /**
     * JointRep
     *
     * @return
     *     The jointrep
     */
    @JsonProperty("jointrep")
    public FieldSecurity getJointrep() {
        return jointrep;
    }

    /**
     * JointRep
     *
     * @param jointrep
     *     The jointrep
     */
    @JsonProperty("jointrep")
    public void setJointrep(FieldSecurity jointrep) {
        this.jointrep = jointrep;
    }

    /**
     * Military
     *
     * @return
     *     The military
     */
    @JsonProperty("military")
    public FieldSecurity getMilitary() {
        return military;
    }

    /**
     * Military
     *
     * @param military
     *     The military
     */
    @JsonProperty("military")
    public void setMilitary(FieldSecurity military) {
        this.military = military;
    }

    /**
     * Poc
     *
     * @return
     *     The poc
     */
    @JsonProperty("poc")
    public FieldSecurity getPoc() {
        return poc;
    }

    /**
     * Poc
     *
     * @param poc
     *     The poc
     */
    @JsonProperty("poc")
    public void setPoc(FieldSecurity poc) {
        this.poc = poc;
    }

    /**
     * PreparedBy
     *
     * @return
     *     The preparedby
     */
    @JsonProperty("preparedby")
    public FieldSecurity getPreparedby() {
        return preparedby;
    }

    /**
     * PreparedBy
     *
     * @param preparedby
     *     The preparedby
     */
    @JsonProperty("preparedby")
    public void setPreparedby(FieldSecurity preparedby) {
        this.preparedby = preparedby;
    }

    /**
     * RelatedRequirements
     *
     * @return
     *     The relatedrequirements
     */
    @JsonProperty("relatedrequirements")
    public FieldSecurity getRelatedrequirements() {
        return relatedrequirements;
    }

    /**
     * RelatedRequirements
     *
     * @param relatedrequirements
     *     The relatedrequirements
     */
    @JsonProperty("relatedrequirements")
    public void setRelatedrequirements(FieldSecurity relatedrequirements) {
        this.relatedrequirements = relatedrequirements;
    }

    /**
     * ReportText
     *
     * @return
     *     The reporttext
     */
    @JsonProperty("reporttext")
    public FieldSecurity getReporttext() {
        return reporttext;
    }

    /**
     * ReportText
     *
     * @param reporttext
     *     The reporttext
     */
    @JsonProperty("reporttext")
    public void setReporttext(FieldSecurity reporttext) {
        this.reporttext = reporttext;
    }

    /**
     * StateLocal
     *
     * @return
     *     The statelocal
     */
    @JsonProperty("statelocal")
    public FieldSecurity getStatelocal() {
        return statelocal;
    }

    /**
     * StateLocal
     *
     * @param statelocal
     *     The statelocal
     */
    @JsonProperty("statelocal")
    public void setStatelocal(FieldSecurity statelocal) {
        this.statelocal = statelocal;
    }

    /**
     * Topic
     *
     * @return
     *     The topic
     */
    @JsonProperty("topic")
    public FieldSecurity getTopic() {
        return topic;
    }

    /**
     * Topic
     *
     * @param topic
     *     The topic
     */
    @JsonProperty("topic")
    public void setTopic(FieldSecurity topic) {
        this.topic = topic;
    }

    /**
     * USMission
     *
     * @return
     *     The usmission
     */
    @JsonProperty("usmission")
    public FieldSecurity getUsmission() {
        return usmission;
    }

    /**
     * USMission
     *
     * @param usmission
     *     The usmission
     */
    @JsonProperty("usmission")
    public void setUsmission(FieldSecurity usmission) {
        this.usmission = usmission;
    }

    /**
     * Warning
     *
     * @return
     *     The warning
     */
    @JsonProperty("warning")
    public FieldSecurity getWarning() {
        return warning;
    }

    /**
     * Warning
     *
     * @param warning
     *     The warning
     */
    @JsonProperty("warning")
    public void setWarning(FieldSecurity warning) {
        this.warning = warning;
    }

    /**
     * WarningOptional
     *
     * @return
     *     The warningoptional
     */
    @JsonProperty("warningoptional")
    public FieldSecurity getWarningoptional() {
        return warningoptional;
    }

    /**
     * WarningOptional
     *
     * @param warningoptional
     *     The warningoptional
     */
    @JsonProperty("warningoptional")
    public void setWarningoptional(FieldSecurity warningoptional) {
        this.warningoptional = warningoptional;
    }

    /**
     * Humint IIR Report Comments specific details
     *
     * @return
     *     The comments
     */
    @JsonProperty("comments")
    public List<IirComment> getComments() {
        return comments;
    }

    /**
     * Humint IIR Report Comments specific details
     *
     * @param comments
     *     The comments
     */
    @JsonProperty("comments")
    public void setComments(List<IirComment> comments) {
        this.comments = comments;
    }

    /**
     * Humint IIR Report Country specific details
     *
     * @return
     *     The country
     */
    @JsonProperty("country")
    public List<Country> getCountry() {
        return country;
    }

    /**
     * Humint IIR Report Country specific details
     *
     * @param country
     *     The country
     */
    @JsonProperty("country")
    public void setCountry(List<Country> country) {
        this.country = country;
    }

    /**
     * Humint IIR Report Source specific details
     *
     * @return
     *     The source
     */
    @JsonProperty("source")
    public List<HumintSource> getSource() {
        return source;
    }

    /**
     * Humint IIR Report Source specific details
     *
     * @param source
     *     The source
     */
    @JsonProperty("source")
    public void setSource(List<HumintSource> source) {
        this.source = source;
    }

    /**
     * Humint IIR Report Text specific details
     *
     * @return
     *     The text
     */
    @JsonProperty("text")
    public List<IirText> getText() {
        return text;
    }

    /**
     * Humint IIR Report Text specific details
     *
     * @param text
     *     The text
     */
    @JsonProperty("text")
    public void setText(List<IirText> text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
