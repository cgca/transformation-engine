/*
* Copyright (c) 2015 Intelligent Software Solutions, Inc.
* Unpublished-all rights reserved under the copyright laws of the United States.
*
* This software was developed under sponsorship from
* the CSC/42Six under:
* xxx xxx-xxxx-xxxx
*
* Contractor: Intelligent Software Solutions, Inc.
* 5450 Tech Center Drive, Suite 400
* Colorado Springs, 80919
* http://www.issinc.com
*
* Intelligent Software Solutions, Inc. has title to the rights in this computer
* software. The Government's rights to use, modify, reproduce, release, perform,
* display, or disclose these technical data are restricted by paragraph (b)(1) of
* the Rights in Technical Data-Noncommercial Items clause contained in
* Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
* Any reproduction of technical data or portions thereof marked with this legend
* must also reproduce the markings.
*
* Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
* aforementioned unlimited government rights to use, disclose, copy, or make
* derivative works of this software to parties outside the Government.
*/

package com.issinc.ges.interfaces.te.utilities;

import java.util.Date;
import java.util.UUID;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.commons.utils.*;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.models.ActionDate;
import com.issinc.ges.interfaces.te.models.ActionInfo;
import com.issinc.ges.interfaces.te.models.Association;
import com.issinc.ges.interfaces.te.models.FieldSecurity;
import com.issinc.ges.interfaces.te.models.MediaReport;
import com.issinc.ges.interfaces.te.models.MediaReportDetails;
import com.issinc.ges.interfaces.te.models.ReportBase;
import com.issinc.ges.interfaces.te.models.Security;
import com.issinc.ges.interfaces.te.rules.SecurityConversionRule;

/**
 * utility methods to help transform/create media data (reports/associations/etc..)
 */
public class MediaUtils {

    private static final String CURRENT_MEDIA_DATAMODEL = "http://cidne.dae.smil.mil/schemas/CIDNE/Media/CIDNEDM235";
    private static final String MEDIA_ASSOCIATION_TYPE = "REPORT_TO_MEDIA";
    /**
     * creates an association JSON for report to media association
     * @param reportId - the CIDNE report ID
     * @param mediaReportId - the media report ID
     * @param user - the user attaching media to the report
     * @param group - the group attaching media to the report
     * @return - association JSON string
     */
    public static String createMediaAssociation(String reportId, String mediaReportId, String user, String group){

        Association assoc = new Association();
        assoc.setAssociatedObject1(reportId);
        assoc.setAssociatedObject2(mediaReportId);
        assoc.setObjectId(UUID.randomUUID().toString()); //new GUID
        assoc.setOriginatingDate(new ActionDate(new Date(),0));//current time
        assoc.setAssociationType(MEDIA_ASSOCIATION_TYPE);
        assoc.setOriginatingUser(user);
        assoc.setOriginatingGroup(group);

        // association status, assumes only adding media to published reports
        assoc.setCurrentState("PUBLISHED");
        assoc.setIsKeyAssociation(Boolean.FALSE);

        //association to media by default is unclassified, if media has higher classification we can change this
        assoc.setSecurity(new Security());
        assoc.getSecurity().setClassification("U");
        assoc.getSecurity().setOwnerProducer("USA");

        assoc.setObjectSequence(1);
        assoc.setAssociatedRole1("");
        assoc.setAssociatedRole2("");
        assoc.setAssociationName("Attached media");

        return com.issinc.ges.commons.utils.JsonUtils.toString(assoc, false);
    }

    /**
     * creates a CIDNE Media Report
     * @param group - group creating media report
     * @param user - user creating media report
     * @param filename - the filename of the media
     * @param reportId - the reportId of the CIDNE Report attached to this media
     * @param mediaReportId - the reportId of this media report, and same id as used for the media chunk
     * @param fileType - the MIME type of media
     * @param description - summary about the media
     * @param fileSize - the size in bytes
     * @param classification - the security classification marking
     * @param releasability - the security releasability marking
     * @return - new CIDNE Media Report JSON string
     */
    public static String createMediaReport(String group, String user, String filename, String reportId,
                                           String mediaReportId, String fileType, String description,
                                           int fileSize, String classification,
                                           String releasability, String token) throws TransformationException {

        MediaReport report = new MediaReport();

        ReportBase reportBase = new ReportBase();

        //generate a new UUID for the media report
        String uuid = UUID.randomUUID().toString();
        reportBase.setObjectId(uuid);
        //same id as the media report chunk
        reportBase.setReportId(mediaReportId);

        reportBase.setIsTearline(false);
        reportBase.setModule("CIDNE");
        reportBase.setReportType("Media");

        ActionInfo currentState = new ActionInfo();
        currentState.setActionDate(new ActionDate(new Date(), 0));
        currentState.setGroup(group);
        currentState.setUser(user);
        currentState.setObjectState("PUBLISHED");
        reportBase.setCurrentObjectState(currentState);

        reportBase.setOriginatingGroup(group);
        reportBase.setOriginatingUser(user);
        FieldSecurity reportSerial = new FieldSecurity();
        reportSerial.setObjectValue(filename);
        reportBase.setReportSerial(reportSerial);
        reportBase.setReportDate(new ActionDate(new Date(),0));

        reportBase.setReportTitle(reportSerial);
        FieldSecurity reportSummary = new FieldSecurity();
        reportSummary.setObjectValue(description);
        reportBase.setReportSummary(reportSummary);
        reportBase.setWarehouseUri(uuid);

        report.setBase(reportBase);

        report.setDatamodel(CURRENT_MEDIA_DATAMODEL);

        MediaReportDetails reportDetails = new MediaReportDetails();
        reportDetails.setRelativeUri(uuid);
        reportDetails.setMediaUrl("MEDIA_URL_GENERATED_ON_INSERT");
        reportDetails.setMediaFileName(filename);
        reportDetails.setOriginalFileName(filename);
        reportDetails.setDateOfMedia(new ActionDate(new Date(),0));
        reportDetails.setDatePosted(new ActionDate(new Date(),0));
        reportDetails.setMediaName(filename);
        reportDetails.setMediaType(fileType);
        reportDetails.setMediaFileSize(fileSize);
        reportDetails.setMediaOriginalSize(fileSize);
        reportDetails.setEmbeddedReportId(reportId);


        report.setDetails(reportDetails);

        JsonNode securityNode = SecurityConversionRule.lookupSecurityBlock(classification,releasability, token);
        report.setSecurity(com.issinc.ges.commons.utils.JsonUtils.toObject(securityNode,Security.class));


        return com.issinc.ges.commons.utils.JsonUtils.toString(report, false);

    }

}
