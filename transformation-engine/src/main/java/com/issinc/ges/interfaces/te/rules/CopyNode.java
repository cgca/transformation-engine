/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.MultipleNodeRule;

/**
 * Copies a node from one part of the JSON tree to another part based on oldFieldPath and newFieldPath.
 * Allowed:
 *   Single node to other single node; outside array uses root, inside array uses array as root.
 *   Single node to array node, copied to all array objects
 */
public class CopyNode extends MultipleNodeRule {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CopyNode.class);

    /**
     * The default constructor
     */
    public CopyNode() {
        super();
    }

    /// CHECKSTYLE:OFF
    @Override
    public JsonNode applyRule(String token, JsonNode payload) throws TransformationException {
        /// CHECKSTYLE:ON
        final String methodName = "CopyNode.applyRule";
        logger.debug(methodName);

        if (getOldFieldPath() != null && getNewFieldPath() != null) {
            retrieveNode(payload, payload, getOldFieldPath());
        } else {
            throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR, "Rule " +
                    this.getClass().getSimpleName() + " must have oldFieldPath, and newFieldPath set.");
        }

        return payload;
    }

    /**
     * Retrieves the node as specified by the implementing class.
     *
     * @param currentNode
     *   Parent node.
     * @param fieldName
     *   Field to retrieve
     * @return
     *   The node
     */
    protected final JsonNode getNodeForAction(JsonNode currentNode, String fieldName) {
        return currentNode.get(fieldName);
    }

}
