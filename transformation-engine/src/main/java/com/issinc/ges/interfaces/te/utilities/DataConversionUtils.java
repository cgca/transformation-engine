/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */

package com.issinc.ges.interfaces.te.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.berico.coords.Coordinates;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import sun.java2d.pipe.SpanShapeRenderer;

/**
 * Methods to transform data during the execution of a rule. Note that new data conversion methods must also
 * be added to the ruleset schema or the ruleset will fail validation and not save to Mongo.
 */
public final class DataConversionUtils {

    private static final String WORD_UNCLASSIFIED = "UNCLASSIFIED";
    private static final String WORD_CLASSIFIED = "CLASSIFIED";
    private static final String WORD_TOP_SECRET = "TOP SECRET";
    private static final String WORD_SECRET = "SECRET";
    private static final String LETTER_UNCLASSIFIED = "U";
    private static final String LETTER_CLASSIFIED = "C";
    private static final String LETTER_TOP_SECRET = "TS";
    private static final String LETTER_SECRET = "S";
    private static final String NONE = "NONE";
    private static final String BOOLEAN_FALSE = "0";
    private static final String BOOLEAN_TRUE = "1";
    private static final String ZULU = "Z";
    private static final String DTG = "dtg";
    private static final String LATITUDE = "Location.Latitude";
    private static final String LONGITUDE = "Location.Longitude";

    private static final Logger logger = LoggerFactory.getLogger(DataConversionUtils.class);

    private DataConversionUtils() {

    }

    /**
     * Converts classification from 4.x to 2.x and 2.x to 4.x
     *
     * @param classificationNode Classification Node
     * @return Converted string.
     */
    public static String convertClassification(JsonNode classificationNode) {
        String convertedClassification = WORD_UNCLASSIFIED;

        if (classificationNode != null) {
            convertedClassification = convertClassification(classificationNode.textValue());
        }

        return convertedClassification;
    }


    /**
     * Converts classification from 4.x to 2.x and 2.x to 4.x
     *
     * @param classificationText Classification String
     * @return Converted string.
     */
    public static String convertClassification(String classificationText) {
        String convertedClassification = WORD_UNCLASSIFIED;
        switch (classificationText) {
            case LETTER_SECRET :
                convertedClassification = WORD_SECRET;
                break;
            case LETTER_CLASSIFIED :
                convertedClassification = WORD_CLASSIFIED;
                break;
            case LETTER_TOP_SECRET :
                convertedClassification = WORD_TOP_SECRET;
                break;
            case WORD_CLASSIFIED :
                convertedClassification = LETTER_CLASSIFIED;
                break;
            case WORD_TOP_SECRET :
                convertedClassification = LETTER_TOP_SECRET;
                break;
            case WORD_SECRET :
                convertedClassification = LETTER_SECRET;
                break;
            case WORD_UNCLASSIFIED :
                convertedClassification = LETTER_UNCLASSIFIED;
                break;
            default:
        }
        return convertedClassification;
    }




    /**
     * Converts releasableTo field from 4.x to 2.x
     * TODO: Reconcile the data mappings; releasableTo node could be much more complex.
     *
     * @param releasableToNode
     *   ReleasableTo Node
     * @return
     *   Converted String
     */
    public static String convertReleasableTo(JsonNode releasableToNode) {
        String convertedReleasableTo = "";

        if (releasableToNode != null && releasableToNode.textValue() != null &&
                !releasableToNode.textValue().isEmpty()) {
            convertedReleasableTo = releasableToNode.textValue();
        } else if (releasableToNode != null && releasableToNode.textValue() != null &&
                NONE.equals(releasableToNode.textValue())) {
            convertedReleasableTo = "";
        }

        return convertedReleasableTo;
    }

    /**
     * Converts the date from Zulu (4.x to 2.x)
     *
     * @param node
     *  Date information node
     * @return
     *  Non-zulu date
     */
    public static String convertZuluDateTime(JsonNode node) {
        String convertedDate = null;

        if (node != null && node.getNodeType() != JsonNodeType.NULL) {
            final String date = node.textValue();
            if (date != null && !date.isEmpty()) {
                convertedDate = date.replaceFirst(ZULU.concat("$"), "");
            }
        }

        return convertedDate;
    }

    /**
     * Converts long date (milliseconds since epoch) to Zulu (2.x to 4.x)
     *
     * @param node
     *  Date information node
     * @return
     *  Zulu date
     */
    public static String convertMilToZuluDateTime(JsonNode node) {
        String convertedDate;

        final DateTime date = new DateTime(node.get(DTG).asLong());
        final String dateString = date.toString();

        ((ObjectNode) node).put(DTG, dateString);

        convertedDate = convertToZuluDateTime(node.get(DTG));

        return convertedDate;
    }

    /**
     * Converts the date to Zulu (2.x to 4.x)
     *
     * @param node
     *  Date information node
     * @return
     *  Zulu date
     */
    public static String convertToZuluDateTime(JsonNode node) {
        String convertedDate = null;

        if (node == null || node.isNull() || node.textValue()==null) {
            return null;
        }

        // For now, we are assuming all 4.X dates are Zulu
        final String date = node.textValue();

        //parse into Date object, and put in correct format
        Date parsedDate = tryParse(date);
        if(parsedDate!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
            convertedDate = dateFormat.format(parsedDate);
        }
        else
         convertedDate = null;

        return convertedDate;
    }





    static Date tryParse(String dateString)
    {
        //list of acceptable dat formats
        String[] possibleDateStrings = {"yyyy-MM-dd'T'hh:mm:ss", "yyyy-MM-dd hh:mm:ss",
                "yyyy-MM-dd'T'hh:mm:ss.ZZZ", "yyyy-MM-dd hh:mm:ss.ZZZ"};
        for (String formatString : possibleDateStrings)
        {
            try
            {
                return new SimpleDateFormat(formatString).parse(dateString);
            }
            catch (ParseException e) {}
        }

        return null;
    }


    /**
     * Converts boolean fields from 4.x to 2.x
     *
     * @param booleanNode
     *   Boolean Node
     * @return
     *   Converted boolean to string
     */
    public static Integer convertBoolean(JsonNode booleanNode) {
        Integer convertedInteger = Integer.valueOf(BOOLEAN_FALSE);

        if (booleanNode != null) {
            if (booleanNode.booleanValue()) {
                convertedInteger = Integer.valueOf(BOOLEAN_TRUE);
            }
        }

        return convertedInteger;
    }

    /**
     * Converts boolean fields from 2.x to 4.x
     *
     * @param booleanNode
     *   Boolean Node
     * @return
     *   Converted string to boolean
     */
    public static Boolean convertToBoolean(JsonNode booleanNode) {
        Boolean convertedBoolean = false;

        if (booleanNode != null && BOOLEAN_TRUE.equals(booleanNode.textValue())) {
            convertedBoolean = true;
        }

        return convertedBoolean;
    }

    /**
     * Converts number fields from 2.x to 4.x
     *
     * @param numericNode
     *   Numeric Node
     * @return
     *   Converted string to boolean
     */
    public static Object convertToNumber(JsonNode numericNode) {
        Object convertedNumber = null;

        if (numericNode == null) {
            return null;
        }

        final String numberValue = numericNode.textValue();
        try {
            if (numberValue != null) {
                if (numberValue.contains(".")) {
                    convertedNumber = Double.parseDouble(numberValue);
                } else {
                    convertedNumber = Integer.parseInt(numberValue);
                }
            }
        } catch (NumberFormatException e) {
            logger.warn("Expected number but received " + numberValue);
        }

        return convertedNumber;
    }


    /**
     * Converts 2x dateNode to 4x style date
     *
     * @param dateNode
     *   2x date Node
     * @return
     *   Converted node to 4x style date
     */
    public static JsonNode convertTo4xDateTime(JsonNode dateNode) {

        if (dateNode == null || dateNode.getNodeType() == JsonNodeType.NULL) {
            return dateNode;
        }
        final ObjectNode newDateNode = JsonNodeFactory.instance.objectNode();

        newDateNode.put("zuluDateTime", convertToZuluDateTime(dateNode));
        newDateNode.put("zuluOffset", 0);

        return newDateNode;
    }

    /**
     * Converts 4x object state to isDeprecated flag.
     * if state is "DEPRECATED" then isDeprecated = 1,
     *
     * @param objectStateNode
     *   4x  object state node
     * @return
     *   Converted value
     */
    public static Object convertDeprecated(JsonNode objectStateNode) {
        Object convertedValue = null;
        if (objectStateNode != null && !objectStateNode.isNull()) {
            convertedValue = 0;
            if ("DEPRECATED".equals(objectStateNode.asText())) {
                convertedValue = 1;
            }
        }
        return convertedValue;
    }

    /**
     * Retrieves the value from the node and, if requested, calls the
     * related convert method.
     *
     * @param node
     *   Node where to find the value.
     * @param dataConversionMethodName
     *   Conversion method to use
     * @param defaultValue
     *   Default value from ruleset configuration
     * @param parentNode
     *   The parent node of the current node (parameter node)
     * @param parameters
     *   Parameters to be used in the conversion method (optional)
     * @return
     *   Value
     */
    public static JsonNode convertDataNode(JsonNode node, String dataConversionMethodName, Object defaultValue,
            JsonNode parentNode, Object... parameters) {
        JsonNode retNode = null;
        Object value = null;

        if (dataConversionMethodName != null) {
            switch (dataConversionMethodName) {
                case "convertBoolean":
                    value = convertBoolean(node);
                    break;
                case "convertClassification":
                    value = convertClassification(node);
                    break;
                case "convertReleasableTo":
                    value = convertReleasableTo(node);
                    break;
                case "convertZuluDateTime":
                    value = convertZuluDateTime(node);
                    break;
                case "convertMilToZuluDateTime":
                    value = convertMilToZuluDateTime(node);
                    break;
                case "convertToZuluDateTime":
                    value = convertToZuluDateTime(node);
                    break;
                case "convertToBoolean":
                    value = convertToBoolean(node);
                    break;
                case "convertToNumber":
                    value = convertToNumber(node);
                    break;
                case "convertTo4xDateTime":
                    retNode = convertTo4xDateTime(node);
                    break;
                case "convertDeprecated":
                    value = convertDeprecated(node);
                    break;
                case "convertRandomUUID":
                    value = UUID.randomUUID().toString();
                    break;
                case "convertLatLonToMGRS":
                    value = convertLatLonToMGRS(parentNode, (String) parameters[0], (String) parameters[1]);
                    break;
                default:
            }
        }

        if (value == null && (node == null || node.isNull()) && defaultValue != null) {
            value = defaultValue;
        }

        if (value != null && retNode == null) {
            if (value instanceof Boolean) {
                retNode = JsonNodeFactory.instance.booleanNode((Boolean) value);
            } else if (value instanceof String) {
                retNode = JsonNodeFactory.instance.textNode((String) value);
            } else if (value instanceof Double) {
                retNode = JsonNodeFactory.instance.numberNode((Double) value);
            } else if (value instanceof Integer) {
                retNode = JsonNodeFactory.instance.numberNode((Integer) value);
            }
        }

        if (retNode == null) {
            retNode = node;
        }

        return retNode;
    }

    private static String convertLatLonToMGRS(JsonNode parentNode, String pathToLatitude, String pathToLongitude) {
        String mgrs = "";
        String latitude = "";
        String longitude = "";
        try {
            latitude = JsonUtils.getNodeAtPath(parentNode, pathToLatitude).asText();
            longitude = JsonUtils.getNodeAtPath(parentNode, pathToLongitude).asText();
            final double lat = JsonUtils.getNodeAtPath(parentNode, pathToLatitude).asDouble();;
            final double lon = JsonUtils.getNodeAtPath(parentNode, pathToLongitude).asDouble();;
            mgrs = Coordinates.mgrsFromLatLon(lat, lon);
        } catch (Exception e) {
            logger.warn("Cannot create MGRS from latitude: " + latitude + " and longitude: " + longitude);
            // If latitude or longitude are missing or not valid, we cannot create an MGRS, use ""
        }
        return mgrs;
    }

}
