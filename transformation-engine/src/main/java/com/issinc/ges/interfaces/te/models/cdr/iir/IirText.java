/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models.cdr.iir;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import javax.swing.*;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.issinc.ges.interfaces.te.models.ActionDate;
import com.issinc.ges.interfaces.te.models.Security;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "tearlinetextreportdateupdated",
        "textdata",
        "referenceid",
        "textorder",
        "security"
})
public class IirText {

    /**
     *
     */
    @JsonProperty("tearlinetextreportdateupdated")
    private ActionDate tearlinetextreportdateupdated;
    @JsonProperty("textdata")
    private String textdata;
    @JsonProperty("referenceid")
    private String referenceid;
    @JsonProperty("textorder")
    private Integer textorder;
    @JsonProperty("security")
    private Security security;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public IirText() {
    }

    /**
     *
     * @param referenceid
     * @param tearlinetextreportdateupdated
     * @param security
     * @param textorder
     * @param textdata
     */
    public IirText(ActionDate tearlinetextreportdateupdated, String textdata, String referenceid, Integer textorder, Security security) {
        this.tearlinetextreportdateupdated = tearlinetextreportdateupdated;
        this.textdata = textdata;
        this.referenceid = referenceid;
        this.textorder = textorder;
        this.security = security;
    }

    /**
     *
     * @return
     *     The tearlinetextreportdateupdated
     */
    @JsonProperty("tearlinetextreportdateupdated")
    public ActionDate getTearlinetextreportdateupdated() {
        return tearlinetextreportdateupdated;
    }

    /**
     *
     * @param tearlinetextreportdateupdated
     *     The tearlinetextreportdateupdated
     */
    @JsonProperty("tearlinetextreportdateupdated")
    public void setTearlinetextreportdateupdated(ActionDate tearlinetextreportdateupdated) {
        this.tearlinetextreportdateupdated = tearlinetextreportdateupdated;
    }

    /**
     *
     * @return
     *     The textdata
     */
    @JsonProperty("textdata")
    public String getTextdata() {
        return textdata;
    }

    /**
     *
     * @param textdata
     *     The textdata
     */
    @JsonProperty("textdata")
    public void setTextdata(String textdata) {
        this.textdata = textdata;
    }

    /**
     *
     * @return
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public String getReferenceid() {
        return referenceid;
    }

    /**
     *
     * @param referenceid
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public void setReferenceid(String referenceid) {
        this.referenceid = referenceid;
    }

    /**
     *
     * @return
     *     The textorder
     */
    @JsonProperty("textorder")
    public Integer getTextorder() {
        return textorder;
    }

    /**
     *
     * @param textorder
     *     The textorder
     */
    @JsonProperty("textorder")
    public void setTextorder(Integer textorder) {
        this.textorder = textorder;
    }

    /**
     *
     * @return
     *     The security
     */
    @JsonProperty("security")
    public Security getSecurity() {
        return security;
    }

    /**
     *
     * @param security
     *     The security
     */
    @JsonProperty("security")
    public void setSecurity(Security security) {
        this.security = security;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
