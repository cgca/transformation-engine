package com.issinc.ges.interfaces.te.rules.cdr.xmlprocessors;

import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.models.*;
import com.issinc.ges.interfaces.te.models.cdr.Country;
import com.issinc.ges.interfaces.te.models.cdr.HumintSource;
import com.issinc.ges.interfaces.te.models.cdr.IcrDetails;
import com.issinc.ges.interfaces.te.models.cdr.iir.IirComment;
import org.apache.xml.dtm.ref.DTMNodeList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Base class for transforming ICR reports
 */
public abstract class CdrProcessor {

    private static final Logger logger = LoggerFactory.getLogger(CdrProcessor.class);

    public XPath getXpath() {
        return xpath;
    }

    public void setXpath(XPath xpath) {
        this.xpath = xpath;
    }

    public Document getDoc() {
        return doc;
    }

    public void setDoc(Document doc) {
        this.doc = doc;
    }

    protected XPath xpath;
    protected Document doc;

    public CdrProcessor(){

    }

    public CdrProcessor(XPath xpath, Document doc){
        this.xpath = xpath;
        this.doc = doc;
    }

    abstract public String processXML();



    protected List<HumintSource> getSource() {
        List<HumintSource> sources = new ArrayList<>();
        //
        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='eval']//*[local-name()='ref_iif']//*[local-name()='source_nbr']");

            DTMNodeList nodes = (DTMNodeList) expr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++) {
                HumintSource source = new HumintSource();
                FieldSecurity sourceSec = new FieldSecurity();
                sourceSec.setObjectValue(nodes.item(i).getTextContent());
                source.setSourcenumber(sourceSec);
                sources.add(source);
            }
        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }
        return sources;
    }
    protected FieldSecurity parseEnclosures(){
        String enctmp;
        String enc = "";

        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='full_text']/text()");

            String full_text = (String) expr.evaluate(doc, XPathConstants.STRING);

            int start = full_text.indexOf("ENCL:") + 5;
            if(start>5) {
                enctmp = full_text.substring(start);

                int end = enctmp.indexOf(".");
                int wend = enctmp.indexOf(".");
                if(end>wend){
                    end = wend-1;
                }
                enc = enctmp.substring(0, end);
            }
        } catch (XPathExpressionException e1) {
            //nothing to do just skip over
        }

        FieldSecurity result = null;
        if(!"".equals(enc)){
            result = new FieldSecurity();
            result.setObjectValue(enc);
        }

        return result;
    }


    protected FieldSecurity getFieldSecurity(String regex){
        FieldSecurity result = new FieldSecurity();
        try{
            XPathExpression expr =
                    xpath.compile(regex);

            DTMNodeList nodes = (DTMNodeList)expr.evaluate(doc, XPathConstants.NODESET);

            if(nodes.getLength()>0){
                Node node = nodes.item(0);
                result.setSecurity(getSecurityFromAttributes(node));
                result.setObjectValue(nodes.item(0).getTextContent());
            }

        } catch ( XPathExpressionException e) {
            //nothing to do just skip over
        }
        return result;

    }

    protected FieldSecurity getFieldSecurity(String regex, String valueNodeName){
        FieldSecurity result = new FieldSecurity();
        try{
            XPathExpression expr =
                    xpath.compile(regex);

            DTMNodeList nodes = (DTMNodeList)expr.evaluate(doc, XPathConstants.NODESET);

            if(nodes.getLength()>0){
                Node node = nodes.item(0);
                result.setSecurity(getSecurityFromAttributes(node));

                //get textvalue
                NodeList childNodes = node.getChildNodes();
                for(int j=0;j<childNodes.getLength();j++ ){
                    if(valueNodeName.equals(childNodes.item(j).getNodeName())){
                        result.setObjectValue(childNodes.item(j).getTextContent());
                    }
                }
            }

        } catch ( XPathExpressionException e) {
            //nothing to do just skip over
        }
        return result;

    }

    protected FieldSecurity getReporterCode(){
        return getFieldSecurity("//*[local-name()='iir']//*[local-name()='preparer']", "primary_fr");
    }
    protected List<IirComment> getComments(){
        List<IirComment> iirComments = new ArrayList<>();
        try {
            //get the security on the collection_mgmt_codes node and add it
            XPathExpression mgmtexpr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='iir_comment']//*[local-name()='comment_paragraph']");

            DTMNodeList mgmtNodes = (DTMNodeList) mgmtexpr.evaluate(doc, XPathConstants.NODESET);
            for(int i=0;i<mgmtNodes.getLength();i++){

                Security security = getSecurityFromAttributes(mgmtNodes.item(i));
                IirComment comment = new IirComment();
                comment.setSecurity(security);
                NodeList nodes = mgmtNodes.item(i).getChildNodes();
                for(int j=0; j<nodes.getLength();j++){
                    if("comment_text".equals(nodes.item(i).getNodeName())){
                        comment.setCommentsdata(nodes.item(i).getTextContent());
                    }

                }
                iirComments.add(comment);
            }

        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }
        return iirComments;
    }

    protected Security getSecurity(String regex){
        Security security = null;
        try {
            XPathExpression expr =
                    xpath.compile(regex);

            DTMNodeList nodes = (DTMNodeList) expr.evaluate(doc, XPathConstants.NODESET);
            if(nodes.getLength()>0){
                security = getSecurityFromAttributes(nodes.item(0));
            }

        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }

        return security;
    }
    protected FieldSecurity getIPSP(String prefix){
        FieldSecurity result = null;

        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='"+prefix+"']//*[local-name()='functional_codes']//*[local-name()='dit_subject']");

            DTMNodeList nodes = (DTMNodeList) expr.evaluate(doc, XPathConstants.NODESET);

            String requirmementNbs = appendNodelist(nodes);

            if(requirmementNbs!=null && !"".equals(requirmementNbs)){
                result = new FieldSecurity();
                result.setObjectValue(requirmementNbs);
                result.setSecurity(null);
            }

        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }

        return result;

    }


    protected String appendNodes(String regex){
        String appendedNodes = null;

        try {
            XPathExpression expr =
                    xpath.compile(regex);

            DTMNodeList nodes = (DTMNodeList) expr.evaluate(doc, XPathConstants.NODESET);

            appendedNodes = appendNodelist(nodes);

        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }

        return appendedNodes;

    }



    protected ActionDate getIirAcquiredDate() {
        String acqtmp;
        Date acqdate = null;

		/* There is no tag defined for Date Acquired */
		/* parse from message.                       */

        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='full_text']");

            String full_text = (String) expr.evaluate(doc, XPathConstants.STRING);


            int start = full_text.indexOf("ACQ:") + 4;
            if(start<0&& full_text.contains("DATE OF ACQUISITION:")){
                start = full_text.indexOf("DATE OF ACQUISITION:")+20;
            }

            acqtmp = full_text.substring(start);
            /* try to find the end of the date acquired */
            int acqend = acqtmp.indexOf(").");
            if (acqend < 0)
                acqend = acqtmp.indexOf(") .");
            if (acqend < 0)
                acqend = acqtmp.indexOf(".");
            if(acqend<0){
                return null;
            }

            acqtmp = acqtmp.substring(0,acqend);
            acqtmp = acqtmp.substring(acqtmp.length()-8);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            acqdate =dateFormat.parse(acqtmp);


        } catch (XPathExpressionException|ParseException e) {
            //nothing to do just skip over
        }


        ActionDate retDate=null;
        if(acqdate!=null){
            retDate= new ActionDate();
            retDate.setZuluDateTime(acqdate);
            retDate.setZuluOffset(0);
        }
        return retDate;

    }

    protected FieldSecurity getDateOfInfo()
    {
        FieldSecurity retDate = null;

        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='DateInfoCutoff']//*[local-name()='ApproximableDateTime']/text()");

            String Doitmp = (String) expr.evaluate(doc, XPathConstants.STRING);

            if(Doitmp!=null){
                retDate = new FieldSecurity();
                retDate.setObjectValue(Doitmp);
            }


        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }


        return retDate;

    }

    protected ActionDate getReportDate()
    {
        Date doiDate = null;

        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='DateInfoCutoff']//*[local-name()='ApproximableDateTime']/text()");

            String Doitmp = (String) expr.evaluate(doc, XPathConstants.STRING);


            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            doiDate =dateFormat.parse(Doitmp);


        } catch (XPathExpressionException | ParseException e) {
            //nothing to do just skip over
        }

        ActionDate retDate=null;
        if(doiDate!=null){
            retDate= new ActionDate();
            retDate.setZuluDateTime(doiDate);
            retDate.setZuluOffset(0);
        }
        return retDate;

    }
    protected ActionDate getXmlDate(String regex)
    {
        Date doiDate = null;

        try {
            XPathExpression expr =
                    xpath.compile(regex+"/text()");

            String Doitmp = (String) expr.evaluate(doc, XPathConstants.STRING);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            doiDate =dateFormat.parse(Doitmp);

        } catch (XPathExpressionException | ParseException e) {
            logger.warn("Skipping over invalid date format.");
        }

        ActionDate retDate=null;
        if(doiDate!=null){
            retDate= new ActionDate();
            retDate.setZuluDateTime(doiDate);
            retDate.setZuluOffset(0);
        }
        return retDate;
    }

    protected String getIirAcquiredLocation() {
        String acqtmp;
        String acq = "";

        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='full_text']/text()");

            String full_text = (String) expr.evaluate(doc, XPathConstants.STRING);

            int start = full_text.indexOf("ACQ:") + 4;
            acqtmp = full_text.substring(start);

            int acqend = acqtmp.indexOf(").");
            if (acqend < 0)
                acqend = acqtmp.indexOf(") .");
            if (acqend < 0)
                acqend = acqtmp.indexOf(".");

            if(acqend<0){
                return null;
            }

            acq = acqtmp.substring(0, acqend - 9);

        } catch (XPathExpressionException e1) {
            //nothing to do just skip over
        }

        return acq;
    }

    protected String formatSerial(String serial)
    {
        String retStr;
        if ( serial.length()==10)
        {

            retStr="IIR ";
            retStr= retStr + serial.substring(0,0) + " ";
            retStr= retStr + serial.substring(1,3) + " ";
            retStr= retStr + serial.substring(4,7) + " ";
            retStr= retStr + serial.substring(8) ;

        } // CIDNE JIRA CIDNE-2931    HOTR 15 Char serials
        else if  ( serial.length()==15 )
        {
            retStr="IIR ";
            retStr= retStr + serial.substring(0,0) + " ";
            retStr= retStr + serial.substring(1,3) + " ";
            retStr= retStr + serial.substring(4,7) + " ";
            retStr= retStr + serial.substring(8,9) + " ";
            retStr= retStr + serial.substring(10) + " ";
        }
        else
            retStr="";

        return retStr;
    }

    public String searchXML(String regex ){
        String result = "";

        try{
            XPathExpression expr =
                    xpath.compile(regex+"/text()");

            result = (String)expr.evaluate(doc, XPathConstants.STRING);


        } catch ( XPathExpressionException e) {
            //nothing to do just skip over
        }
        return result;

    }

    protected FieldSecurity getCollectors()
    {
        FieldSecurity result = null;

        try {
            //get the security on the collection_mgmt_codes node and add it
            XPathExpression mgmtexpr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='collection_mgmt_codes']");

            DTMNodeList mgmtNodes = (DTMNodeList) mgmtexpr.evaluate(doc, XPathConstants.NODESET);
            if(mgmtNodes.getLength()>0) {

                Security security = getSecurityFromAttributes(mgmtNodes.item(0));

                XPathExpression expr =
                        xpath.compile("//*[local-name()='iir']//*[local-name()='collection_mgmt_codes']//*[local-name()='CMC']");

                DTMNodeList cmcNodes = (DTMNodeList) expr.evaluate(doc, XPathConstants.NODESET);

                //go through each cmcNode, get the "code" attribute and append together
                String codes = "";
                for (int i = 0; i < cmcNodes.getLength(); i++) {
                    if (cmcNodes.item(i).getAttributes() != null &&
                            cmcNodes.item(i).getAttributes().getNamedItem("code") != null) {
                        String code = cmcNodes.item(i).getAttributes().getNamedItem("code").getTextContent();
                        if (!"".equals(codes)) {
                            codes = codes + ",";
                        }
                        codes = codes + code;
                    }
                }
                if(!"".equals(codes)){
                    result = new FieldSecurity();
                    result.setObjectValue(codes);
                    result.setSecurity(security);
                }
            }

        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }
        return result;
    }

    public FieldSecurity getRequirements(){
        FieldSecurity result = null;

        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='requirements']");

            DTMNodeList reqNodes = (DTMNodeList) expr.evaluate(doc, XPathConstants.NODESET);

            if(reqNodes.getLength()>0) {
                //get security
                Security security = getSecurityFromAttributes(reqNodes.item(0));

                XPathExpression reqexpr =
                        xpath.compile("//*[local-name()='iir']//*[local-name()='requirements']//*[local-name()='requirement_nbr']");

                DTMNodeList nodes = (DTMNodeList) reqexpr.evaluate(doc, XPathConstants.NODESET);

                String requirmementNbs = appendNodelist(nodes);

                if(requirmementNbs!=null && !"".equals(requirmementNbs)){
                    result = new FieldSecurity();
                    result.setObjectValue(requirmementNbs);
                    result.setSecurity(security);
                }

            }

        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }

        return result;
    }

    public static String appendNodelist(DTMNodeList nodes){
        String appendedStr = "";
        for (int i = 0; i< nodes.getLength(); i++) {

            if(i< nodes.getLength()-1) {
                appendedStr = appendedStr + nodes.item(i).getTextContent() + "; ";
            }
            else{
                appendedStr = appendedStr + nodes.item(i).getTextContent();
            }
        }
        return appendedStr;
    }


    public FieldSecurity getReportText(){

        return getFieldSecurity("//*[local-name()='iir']//*[local-name()='msg_text']//*[local-name()='paragraph']",
                "text_value");

    }

    protected Security getSecurityFromAttributes(Node node){
        Security security = new Security();
        //get security from attributes
        NamedNodeMap attributeMap = node.getAttributes();

        for(int i = 0;i<attributeMap.getLength();i++){

            String nodenames = attributeMap.item(i).getNodeName();
            int splitIdx = nodenames.indexOf(":");
            if(splitIdx<0){
                splitIdx=0;
            }
            else{
                splitIdx=splitIdx+1;
            }
            nodenames = nodenames.substring(splitIdx);

            switch(nodenames) {
                case "classification":
                    security.setClassification(attributeMap.item(i).getTextContent());
                    break;
                case "releasableTo":
                    security.setReleasableTo(attributeMap.item(i).getTextContent());
                    break;
                case "ownerProducer":
                    security.setOwnerProducer(attributeMap.item(i).getTextContent());
                    break;
                case "disseminationControls":
                    security.setDisseminationControls(attributeMap.item(i).getTextContent());
                    break;
                default:
                    break;
            }

        }

        return security;
    }

    protected boolean getRequirementClosed(String prefix)
    {
        String tmp         = searchXML("//*[local-name()='" + prefix
                + "']//*[local-name()='req_status']/text()");

        return "CLOSED".equalsIgnoreCase(tmp);
    }
    protected void setIcrDetails(IcrDetails details,
                                 Security reportsecurity){

        String documentNumber = getDocumentNumber();
        FieldSecurity summary = getSummary();


        details.setBackground(appendNodes("//*[local-name()='icr']//*[local-name()='background_text']"));
        details.setExpirationdate(getXmlDate("//*[local-name()='icr']//*[local-name()='requirement_info']//*[local-name()='expiration_date']"));
        details.setIpsp(appendNodes("//*[local-name()='icr']//*[local-name()='functional_codes']//*[local-name()='dit_subject']"));
        details.setNhcd(appendNodes("//*[local-name()='icr']//*[local-name()='nhcd']//*[local-name()='nhcdID']"));
        details.setOriginatorphone(searchXML("//*[local-name()='icr']//*[local-name()='origin_agency']//*[local-name()='phone_number']"));
        details.setOriginatornumber(searchXML("//*[local-name()='icr']//*[local-name()='origin_agency']//*[local-name()='originating_agency_nbr']"));
        details.setOriginatoremail(searchXML("//*[local-name()='icr']//*[local-name()='origin_agency']//*[local-name()='creator_email']"));
        String priority = searchXML("//*[local-name()='icr']//*[local-name()='priority']//*[local-name()='tasking_collector_code']");
        if(priority!=null){
            details.setPriority(Integer.getInteger(priority));
        }

        details.setReference(searchXML("//*[local-name()='icr']//*[local-name()='ref_iir']//*[local-name()='ref_iir_title']"));
        details.setRequirement(searchXML("//*[local-name()='icr']//*[local-name()='requirement_info']//*[local-name()='requirement_nbr']"));
        details.setRequirementclosed(getRequirementClosed("icr"));
        details.setSerial(documentNumber);
        details.setSubject(searchXML("//*[local-name()='icr']//*[local-name()='title']"));
        details.setPass(""); // no mapping in CIDNE 2
        details.setPoc("");  //no mapping in CIDNE 2
        details.setSummary(summary.getObjectValue());
        if(reportsecurity!=null) {
            details.setDerivedfrom(reportsecurity.getDerivedFrom());
            details.setReleasability(reportsecurity.getReleasableTo());
        }

        details.setSupersedes(getSpecificGuidance("SUPERSEDES","C."));
        details.setIirdistribution(getSpecificGuidance("IIR DISTRIBUTIONS","D."));

    }


    protected String getSpecificGuidance(String searchString, String endChars) {
        String result = "";

        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='iir']//*[local-name()='guidance']//*[local-name()='text_content']//*[local-name()='paragraph']//*[local-name()='text_value']");

            DTMNodeList reqNodes = (DTMNodeList) expr.evaluate(doc, XPathConstants.NODESET);

            for(int i=0; i<reqNodes.getLength();i++){
                Node node = reqNodes.item(i);
                if(node.getTextContent()!=null && node.getTextContent().contains(searchString)){

                    int start = node.getTextContent().indexOf(searchString)+searchString.length()+1;

                    int end = node.getTextContent().indexOf(endChars);
                    if(end>0){
                        result = node.getTextContent().substring(start,end);
                    }
                    else{
                        result = node.getTextContent().substring(start);
                    }
                }
            }

        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }

        return result;
    }

    protected ReportBase getIcrReportBase(){

        String documentNumber = getDocumentNumber();
        String originatorName = getOriginatorName();
        String group = getOriginatorGroup();
        String system = getSystem();
        FieldSecurity summary = getSummary();

        //setup IIRBase
        ReportBase base = new ReportBase();
        base.setReportType("HCR");
        base.setModule("Humint");
        base.setIsTearline(false);
        base.setReportId(searchXML("//*[local-name()='icr']//*[local-name()='primaryKey']"));
        base.setObjectId(UUID.randomUUID().toString());
        base.setWarehouseUri("");
        FieldSecurity reportSerial = new FieldSecurity();

        reportSerial.setObjectValue(formatSerial(documentNumber));
        base.setReportSerial(reportSerial);

        ActionDate reportDate = getXmlDate("//*[local-name()='icr']//*[local-name()='requirement_info']//*[local-name()='expiration_date']");
        if(reportDate == null){
            reportDate = new ActionDate();
            reportDate.setZuluDateTime(Calendar.getInstance().getTime());
            reportDate.setZuluOffset(0);
        }
        base.setReportDate(reportDate);
        base.setTheaterFilter("");
        base.setReportTitle(getFieldSecurity("//*[local-name()='icr']//*[local-name()='title']"));

        base.setReportSummary(summary);
        base.setOriginatingGroup(group);
        base.setOriginatingUser(originatorName);

        ActionInfo objectState = new ActionInfo();
        objectState.setSystem(system);
        objectState.setGroup(group);
        objectState.setParentGroup(group);
        Date date = Calendar.getInstance().getTime();
        ActionDate actionDate = new ActionDate();
        actionDate.setZuluDateTime(date);
        actionDate.setZuluOffset(0);
        objectState.setActionDate(actionDate); //today's date
        objectState.setNationality("");
        objectState.setNetwork("");
        objectState.setObjectState("PUBLISHED");
        objectState.setSite("");
        objectState.setUser(originatorName);
        base.setCurrentObjectState(objectState);

        return  base;
    }

    protected String getSystem(){
        return searchXML("//*[local-name()='icr']//*[local-name()='administrative_metadata']//*[local-name()='originating_system_name']");
    }
    protected String getOriginatorName(){
        return searchXML("//*[local-name()='icr']//*[local-name()='origin_agency']//*[local-name()='originator_name']//*[local-name()='originator_fullname']");
    }
    protected String getOriginatorGroup() {
        return searchXML("//*[local-name()='icr']//*[local-name()='origin_agency']//*[local-name()='originating_agency']");
    }
    protected String getDocumentNumber() {
        return searchXML("//*[local-name()='icr']//*[local-name()='original_req_nbr']");
    }
    protected FieldSecurity getSummary() {
        return getFieldSecurity("//*[local-name()='icr']//*[local-name()='summary']");
    }
    protected List<Country> getCountry() {
        List<Country> countries = new ArrayList<>();
        try {
            XPathExpression expr =
                    xpath.compile("//*[local-name()='icr']//*[local-name()='target']//*[local-name()='country']");

            DTMNodeList nodes = (DTMNodeList) expr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++) {
                NamedNodeMap attributeMap = nodes.item(i).getAttributes();
                Country country = new Country();
                Node countryName = attributeMap.getNamedItem("ISO_3166_1_country_name");
                if (countryName != null) {
                    country.setCountry(countryName.getTextContent());
                    countries.add(country);
                }
            }
        } catch (XPathExpressionException e) {
            //No action necessary
        }
        return countries;
    }
    protected String getSupportingRequirements(String startText, String endText) {
        String supportingRequirements = searchXML("//*[local-name()='icr']//*[local-name()='Full_text']");
        supportingRequirements = supportingRequirements.substring(supportingRequirements.indexOf(startText) + 1, supportingRequirements.indexOf(endText));

        return supportingRequirements;
    }
    protected String getSDRRequirements() {
        String requirementsNbr = "";
        try {
            XPathExpression reqexpr =
                    xpath.compile("//*[local-name()='icr']//*[local-name()='requirement_info']//*[local-name()='requirement_nbr']");

            DTMNodeList nodes = (DTMNodeList) reqexpr.evaluate(doc, XPathConstants.NODESET);
            requirementsNbr = appendNodelist(nodes);
        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }

        return requirementsNbr;
    }
    protected String getSDRSource() {
        String source = "";
        try {
            XPathExpression reqexpr =
                    xpath.compile("//*[local-name()='icr']//*[local-name()='source_nbr']");

            DTMNodeList nodes = (DTMNodeList) reqexpr.evaluate(doc, XPathConstants.NODESET);
            //original CIDNE 2.x grabbed the first node only.
            if (nodes.getLength() > 0) {
                source = nodes.item(0).toString();
            }
        } catch (XPathExpressionException e) {
            //nothing to do just skip over
        }

        return source;
    }
}
