package com.issinc.ges.interfaces.te.rules;


import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.Rule;


/**
 * Rule to sort 2x XML string alphabetically by keynames
 */
public class SortXSD extends Rule {


    @Override
    public final Object applyRule(String token, Object payload) throws TransformationException {

        String returnXML = null;
        String payloadStr = (String)payload;
        DOMParser parser = new DOMParser();
        try{
            parser.parse(new InputSource(new java.io.StringReader(payloadStr)));
            Document doc = parser.getDocument();
            sortXml(doc);
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            returnXML = writer.toString();
        } catch (SAXException | IOException | TransformerException e) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR, "Error sorting XML", e);
        }

        return returnXML;
    }


    private static void sortXml(Document document)
    {
        sortXml(document.getDocumentElement());

    }

    private static void sortXml(Node rootNode)
    {
        sortElements(rootNode);
        NodeList childNodeList = rootNode.getChildNodes();
        for(int i=0;i<childNodeList.getLength();i++){
            sortXml(childNodeList.item(i));
        }
    }

    private static void sortElements(Node node)
    {
        boolean changed = true;
        while (changed)
        {
            changed = false;
            NodeList childNodeList = node.getChildNodes();

            List<Node> nodeList = getValidNodes(childNodeList);

            for (int i = 1; i < nodeList.size(); i++)
            {
                if (compareNodes(nodeList.get(i), nodeList.get(i - 1)) < 0) {
                    node.insertBefore(nodeList.get(i), nodeList.get(i - 1));
                    changed = true;
                }
            }
        }
    }

    private static List<Node> getValidNodes(NodeList nodeList){
        List<Node> filteredNodes = new ArrayList<Node>();
        for (int i = 1; i < nodeList.getLength(); i++){
            Node childNode = nodeList.item(i);
            if(childNode.getNodeType()==Node.ELEMENT_NODE) {
                NamedNodeMap nodemap = childNode.getAttributes();
                if (nodemap != null && nodemap.getNamedItem("name") != null) {
                    filteredNodes.add(childNode);
                }
            }
        }
        return filteredNodes;
    }

    //Location should be the last node in the XML, so this comparison is made to ensure Location is the
    // "highest" value on sorting.  In addition, all subtables should be after simple elements.
    private static int compareNodes(Node node1, Node node2){


        NamedNodeMap nodemap = node1.getAttributes();
        NamedNodeMap nodemap2 = node2.getAttributes();
        String node1Name = nodemap.getNamedItem("name").getNodeValue();
        String node2Name = nodemap2.getNamedItem("name").getNodeValue();

        if ("WebServiceAttributes".equals(node1Name) && !"WebServiceAttributes".equals(node2Name)) {
            return -1;
        }
        if (!"WebServiceAttributes".equals(node1Name) && "WebServiceAttributes".equals(node2Name)) {
            return 1;
        }

        boolean isNode1array = false;
        boolean isNode2array = false;

        if(node1.getAttributes().getNamedItem("maxOccurs")!=null){
            isNode1array = node1.getAttributes().getNamedItem("maxOccurs").getNodeValue()!="1";
        }
        if(node2.getAttributes().getNamedItem("maxOccurs")!=null){
            isNode2array = node2.getAttributes().getNamedItem("maxOccurs").getNodeValue()!="1";
        }
        if(isNode1array && !isNode2array){
            return 1;
        }

        if(isNode2array && !isNode1array){
            return -1;
        }

        return node1Name.compareToIgnoreCase(node2Name);

    }


}
