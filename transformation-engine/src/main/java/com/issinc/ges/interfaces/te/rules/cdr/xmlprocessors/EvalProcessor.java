/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules.cdr.xmlprocessors;

import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.models.*;
import com.issinc.ges.interfaces.te.models.cdr.eval.EvalDetails;
import com.issinc.ges.interfaces.te.models.cdr.eval.HumintEval;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * class to help transform CDR/ISM eval reports to CIDNE 4.1 eval JSON strings
 */
public class EvalProcessor extends CdrProcessor{

    private static final String EVAL_DATAMODEL = "http://cidne.dae.smil.mil/schemas/Humint/Eval/CIDNEDM235";

    public EvalProcessor(XPath xpath, Document doc){
        super(xpath,doc);
    }
    @Override
    public String processXML() {
        HumintEval eval = new HumintEval();

        eval.setDatamodel(EVAL_DATAMODEL);
        Security reportsecurity = getSecurity("//*[local-name()='eval']");
        eval.setSecurity(reportsecurity);
        eval.setFormId("");

        String system = searchXML("//*[local-name()='eval']//*[local-name()='administrative_metadata']//*[local-name()='originating_system_name']");
        String originatorName = searchXML("//*[local-name()='eval']//*[local-name()='evaluator_info']//*[local-name()='evaluator_name']");
        String group = searchXML("//*[local-name()='eval']//*[local-name()='evaluator_info']//*[local-name()='evaluation_organization']");
        String documentNumber = searchXML("//*[local-name()='eval']//*[local-name()='document_nbr_eval']");

        //setup IIRBase
        ReportBase base = new ReportBase();
        base.setReportType("Eval");
        base.setModule("Humint");
        base.setIsTearline(false);
        base.setReportId(searchXML("//*[local-name()='eval']//*[local-name()='primaryKey']"));
        base.setObjectId(UUID.randomUUID().toString());
        base.setWarehouseUri("");
        FieldSecurity reportSerial = new FieldSecurity();

        reportSerial.setObjectValue(formatSerial(documentNumber));
        base.setReportSerial(reportSerial);

        base.setReportDate(getXmlDate("//*[local-name()='eval']//*[local-name()='eval_status']//*[local-name()='status_date']"));
        base.setTheaterFilter("");
        base.setReportTitle(getFieldSecurity("//*[local-name()='eval']//*[local-name()='title']"));
        FieldSecurity summary = getFieldSecurity("//*[local-name()='eval']//*[local-name()='summary']");

        base.setReportSummary(summary);
        base.setOriginatingGroup(group);
        base.setOriginatingUser(originatorName);

        ActionInfo objectState = new ActionInfo();
        objectState.setSystem(system);
        objectState.setGroup(group);
        objectState.setParentGroup(group);
        Date date = Calendar.getInstance().getTime();
        ActionDate actionDate = new ActionDate();
        actionDate.setZuluDateTime(date);
        actionDate.setZuluOffset(0);
        objectState.setActionDate(actionDate); //today's date
        objectState.setNationality("");
        objectState.setNetwork("");
        objectState.setObjectState("PUBLISHED");
        objectState.setSite("");
        objectState.setUser(originatorName);
        base.setCurrentObjectState(objectState);
        eval.setBase(base);

        //setDetails
        EvalDetails details = new EvalDetails();
        details.setDateevalclosed(getXmlDate("//*[local-name()='eval']//*[local-name()='eval_status']//*[local-name()='status_date']"));
        details.setDraftparentserial(searchXML("//*[local-name()='eval']//*[local-name()='originating_system_id']"));
        details.setGuidance(appendNodes("//*[local-name()='eval']//*[local-name()='guidance_text']//*[local-name()='paragraph']//*[local-name()='text_value']"));
        details.setIrdt(getXmlDate("//*[local-name()='eval']//*[local-name()='iir_ref']//*[local-name()='iir_ref_date']"));
        details.setLasttimeinformationofvalue(getXmlDate("//*[local-name()='eval']//*[local-name()='eval_status']//*[local-name()='status_date']"));
        details.setOriginatoremail(searchXML("//*[local-name()='eval']//*[local-name()='evaluator_info']//*[local-name()='email']")
        );
        details.setOriginatorphone(searchXML("//*[local-name()='eval']//*[local-name()='evaluator_info']//*[local-name()='phone']")
        );
        details.setReference(searchXML("//*[local-name()='eval']//*[local-name()='ref_iir']//*[local-name()='ref_iir_title']"));
        if(reportsecurity!=null) {
            details.setDerivedfrom(reportsecurity.getDerivedFrom());
        }
        details.setRelatedrequirements(appendNodes("//*[local-name()='eval']//*[local-name()='requirements']//*[local-name()='requirement_nbr']"));
        details.setReportvalue(searchXML("//*[local-name()='eval']//*[local-name()='eval_value']//*[local-name()='info_value_code']']"));
        details.setReporterunit("");
        details.setReporttext(summary.getObjectValue());
        details.setIpsp(appendNodes("//*[local-name()='eval']//*[local-name()='functional_codes']//*[local-name()='dit_subject']"));
        details.setSource(getSource());
        details.setPass(""); // no mapping in CIDNE 2
        details.setPoc("");  //no mapping in CIDNE 2
        eval.setDetails(details);
        return JsonUtils.toString(eval,false);
    }
}
