/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.components;

import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.issinc.ges.interfaces.te.utilities.DataConversionUtils;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;
import com.issinc.ges.interfaces.te.utilities.JsonUtils;

/**
 * Parent class to CopyNode, MoveNode, and CopyArrayNodeToNode to put common
 * logic that operates against two nodes, a source and destination.
 */
public abstract class MultipleNodeRule extends JsonRule {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MultipleNodeRule.class);

    // The path to the location in the json that contains the data needed
    private String oldFieldPath;
    // The path to the location in the json to put the data.
    private String newFieldPath;
    // The conversion method to call to convert the data to a different format
    private String dataConversionMethodName;
    // The default value for the data if nothing is found or a new node is needed
    private Object defaultValue;
    // Parameters used by the data conversion method (optional)
    private Object parameter1;
    private Object parameter2;

    //if the objects leading up to the new field path should be created or not
    private boolean createPath = true;

    /**
     * The default constructor
     */
    public MultipleNodeRule() {
        super();
    }

    public final boolean isCreatePath() {
        return createPath;
    }

    public final void setCreatePath(boolean createPath) {
        this.createPath = createPath;
    }

    public final String getOldFieldPath() {
        return oldFieldPath;
    }

    public final void setOldFieldPath(String oldFieldPath) {
        this.oldFieldPath = oldFieldPath;
    }

    public final String getNewFieldPath() {
        return newFieldPath;
    }

    public final void setNewFieldPath(String newFieldPath) {
        this.newFieldPath = newFieldPath;
    }

    public final String getDataConversionMethodName() {
        return dataConversionMethodName;
    }

    public final void setDataConversionMethodName(String dataConversionMethodName) {
        this.dataConversionMethodName = dataConversionMethodName;
    }

    public final Object getDefaultValue() {
        return defaultValue;
    }

    public final void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    public final Object getParameter1() {
        return parameter1;
    }

    public final void setParameter1(Object parameter1) {
        this.parameter1 = parameter1;
    }

    public final Object getParameter2() {
        return parameter2;
    }

    public final void setParameter2(Object parameter2) {
        this.parameter2 = parameter2;
    }

    /**
     * Sets the node into the appropriate location.
     *
     * @param currentNode
     *   Current node
     * @param node
     *   Node to set
     * @param path
     *   Current path
     */
    /// CHECKSTYLE:OFF
    protected void setNode(JsonNode currentNode, JsonNode node, String path) {
        /// CHECKSTYLE:ON
        final String fieldName = JsonUtils.getFieldName(path);

        //if there is no node and no default value to insert,
        // there is nothing to paste, so return
        if(node==null && getDefaultValue()==null){
            return;
        }

        if (fieldName.equals(path)) {
            final JsonEditor je = new JsonEditor(currentNode);
            je.paste(fieldName, DataConversionUtils.convertDataNode(node, getDataConversionMethodName(),
                    getDefaultValue(), currentNode, getParameter1(), getParameter2()));
        } else {
            JsonNode nextNode = currentNode.get(fieldName);
            if (nextNode == null) {
                if (!createPath){
                    return;
                }
                // If the field cannot be found create it and add it to the JSON tree
                nextNode = JsonNodeFactory.instance.objectNode();
                final JsonEditor je = new JsonEditor(currentNode);
                je.paste(fieldName, nextNode);
            }
            if (nextNode.isArray()) {
                final int cnt = nextNode.size();
                for (int i = 0; i < cnt; i++) {
                    setNode(nextNode.get(i), node, path.substring(path.indexOf(".") + 1));
                }
            } else {
                setNode(nextNode, node, path.substring(path.indexOf(".") + 1));
            }
        }
    }

    /**
     * Retrieves the node from the oldFieldPath.
     *
     * @param workingRoot
     *  Current root to start the node setting from, ie, the
     *  absolute root or the array element root.
     * @param currentNode
     *  The current node where to check for the field name.
     * @param path
     *  The current path.
     */
    /// CHECKSTYLE:OFF
    protected void retrieveNode(JsonNode workingRoot, JsonNode currentNode, String path) {
        /// CHECKSTYLE:ON
        if (workingRoot == null) {
            return;
        }

        final String fieldName = JsonUtils.getFieldName(path);

        if (fieldName.equals(path)) {
            setNode(workingRoot, getNodeForAction(currentNode, fieldName), getNewFieldPath());
        } else {
            final JsonNode nextNode = currentNode.get(fieldName);
            if (nextNode == null) {
                logger.debug("Path " + path + " does not exist");
                return;
            }
            if (nextNode.isArray()) {
                final int cnt = nextNode.size();
                for (int i = 0; i < cnt; i++) {
                    retrieveNode(nextNode.get(i), nextNode.get(i), path.substring(path.indexOf(".") + 1));
                }
            } else {
                retrieveNode(workingRoot, nextNode, path.substring(path.indexOf(".") + 1));
            }
        }
    }

    /**
     * Retrieves the node as specified by the implementing class.
     *
     * @param currentNode
     *   Parent node.
     * @param fieldName
     *   Field to retrieve
     * @return
     *   The node
     */
    /// CHECKSTYLE:OFF
    abstract protected JsonNode getNodeForAction(JsonNode currentNode, String fieldName);

}
