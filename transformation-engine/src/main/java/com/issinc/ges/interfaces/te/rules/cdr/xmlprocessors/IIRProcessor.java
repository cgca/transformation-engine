/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules.cdr.xmlprocessors;


import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.models.*;
import com.issinc.ges.interfaces.te.models.cdr.iir.HumintIir;
import com.issinc.ges.interfaces.te.models.cdr.iir.IirDetails;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * class to help transform CDR/ISM iir reports to CIDNE 4.1 iir JSON strings
 */
public class IIRProcessor extends CdrProcessor{
    private static final String IIR_DATAMODEL = "http://cidne.dae.smil.mil/schemas/Humint/IIR/CIDNEDM235";

    public IIRProcessor(XPath xpath, Document doc){
        super(xpath,doc);
    }
    @Override
    public String processXML() {
        HumintIir iir = new HumintIir();

        iir.setDatamodel(IIR_DATAMODEL);
        Security reportsecurity = getSecurity("//*[local-name()='iir']");
        iir.setSecurity(reportsecurity);
        iir.setFormId("");

        String system = searchXML("//*[local-name()='iir']//*[local-name()='administrative_metadata']//*[local-name()='originating_system_name']");
        String collectorNumber = searchXML("//*[local-name()='iir']//*[local-name()='collector']//*[local-name()='collector_number']");
        String systemId = searchXML("//*[local-name()='iir']//*[local-name()='originating_system_id']");
        String group = searchXML("//*[local-name()='iir']//*[local-name()='catalog_source_org']");
        String originatorName = systemId+"//"+collectorNumber;
        String documentNumber = searchXML("//*[local-name()='iir']//*[local-name()='document_nbr_iir']");

        //setup IIRBase
        ReportBase base = new ReportBase();
        base.setReportType("IIR");
        base.setModule("Humint");
        base.setIsTearline(false);
        base.setReportId(searchXML("//*[local-name()='iir']//*[local-name()='primaryKey']"));
        base.setObjectId(UUID.randomUUID().toString());
        base.setWarehouseUri("");
        FieldSecurity reportSerial = new FieldSecurity();

        reportSerial.setObjectValue(documentNumber);
        base.setReportSerial(reportSerial);

        base.setReportDate(getReportDate());
        base.setTheaterFilter("");
        base.setReportTitle(getFieldSecurity("//*[local-name()='iir']//*[local-name()='title']"));
        base.setReportSummary(getFieldSecurity("//*[local-name()='iir']//*[local-name()='summary']"));
        base.setOriginatingGroup(group);
        base.setOriginatingUser(originatorName);

        ActionInfo objectState = new ActionInfo();
        objectState.setSystem(system);
        objectState.setGroup(group);
        objectState.setParentGroup(system+collectorNumber);
        Date date = Calendar.getInstance().getTime();
        ActionDate actionDate = new ActionDate();
        actionDate.setZuluDateTime(date);
        actionDate.setZuluOffset(0);
        objectState.setActionDate(actionDate); //today's date
        objectState.setNationality("");
        objectState.setNetwork("");
        objectState.setObjectState("PUBLISHED");
        objectState.setSite("");
        objectState.setUser(originatorName);
        base.setCurrentObjectState(objectState);
        iir.setBase(base);

        //setDetails
        IirDetails details = new IirDetails();
        details.setAcquireddate(getIirAcquiredDate());
        details.setAcquiredlocation(getIirAcquiredLocation());
        details.setPassto("");  //the CIDNE 2x code, has no mapping for this
        details.setRelatedrequirements(getRequirements());
        details.setColl(getCollectors());
        details.setDoi(getDateOfInfo());
        details.setReporttext(getReportText());
        details.setIpsp(getIPSP("iir"));
        if(reportsecurity!=null) {
            details.setDerivedfrom(reportsecurity.getDerivedFrom());
        }
        details.setComments(getComments());
        details.setPreparedby(getReporterCode());
        details.setInstructions(getFieldSecurity("//*[local-name()='iir']//*[local-name()='instructions']//*[local-name()='contains_us_citizen_info']"));
        details.setEnclosures(parseEnclosures());
        details.setNmhd208version(true);
        details.setCountrysecurity(null);
        iir.setDetails(details);
        return JsonUtils.toString(iir,false);
    }
}
