/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models.cdr;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.issinc.ges.interfaces.te.models.ActionDate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "additionalinformation",
    "dateofinformation",
    "referenceid",
    "remarkorder",
    "remarksource",
    "remarktext",
    "remarktype"
})
public class Remark {

    @JsonProperty("additionalinformation")
    private String additionalinformation;
    /**
     * 
     */
    @JsonProperty("dateofinformation")
    private ActionDate dateofinformation;
    @JsonProperty("referenceid")
    private String referenceid;
    @JsonProperty("remarkorder")
    private Integer remarkorder;
    @JsonProperty("remarksource")
    private String remarksource;
    @JsonProperty("remarktext")
    private String remarktext;
    @JsonProperty("remarktype")
    private String remarktype;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Remark() {
    }

    /**
     * 
     * @param additionalinformation
     * @param remarktext
     * @param referenceid
     * @param remarksource
     * @param remarkorder
     * @param remarktype
     * @param dateofinformation
     */
    public Remark(String additionalinformation, ActionDate dateofinformation, String referenceid, Integer remarkorder, String remarksource, String remarktext, String remarktype) {
        this.additionalinformation = additionalinformation;
        this.dateofinformation = dateofinformation;
        this.referenceid = referenceid;
        this.remarkorder = remarkorder;
        this.remarksource = remarksource;
        this.remarktext = remarktext;
        this.remarktype = remarktype;
    }

    /**
     * 
     * @return
     *     The additionalinformation
     */
    @JsonProperty("additionalinformation")
    public String getAdditionalinformation() {
        return additionalinformation;
    }

    /**
     * 
     * @param additionalinformation
     *     The additionalinformation
     */
    @JsonProperty("additionalinformation")
    public void setAdditionalinformation(String additionalinformation) {
        this.additionalinformation = additionalinformation;
    }

    /**
     * 
     * @return
     *     The dateofinformation
     */
    @JsonProperty("dateofinformation")
    public ActionDate getDateofinformation() {
        return dateofinformation;
    }

    /**
     * 
     * @param dateofinformation
     *     The dateofinformation
     */
    @JsonProperty("dateofinformation")
    public void setDateofinformation(ActionDate dateofinformation) {
        this.dateofinformation = dateofinformation;
    }

    /**
     * 
     * @return
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public String getReferenceid() {
        return referenceid;
    }

    /**
     * 
     * @param referenceid
     *     The referenceid
     */
    @JsonProperty("referenceid")
    public void setReferenceid(String referenceid) {
        this.referenceid = referenceid;
    }

    /**
     * 
     * @return
     *     The remarkorder
     */
    @JsonProperty("remarkorder")
    public Integer getRemarkorder() {
        return remarkorder;
    }

    /**
     * 
     * @param remarkorder
     *     The remarkorder
     */
    @JsonProperty("remarkorder")
    public void setRemarkorder(Integer remarkorder) {
        this.remarkorder = remarkorder;
    }

    /**
     * 
     * @return
     *     The remarksource
     */
    @JsonProperty("remarksource")
    public String getRemarksource() {
        return remarksource;
    }

    /**
     * 
     * @param remarksource
     *     The remarksource
     */
    @JsonProperty("remarksource")
    public void setRemarksource(String remarksource) {
        this.remarksource = remarksource;
    }

    /**
     * 
     * @return
     *     The remarktext
     */
    @JsonProperty("remarktext")
    public String getRemarktext() {
        return remarktext;
    }

    /**
     * 
     * @param remarktext
     *     The remarktext
     */
    @JsonProperty("remarktext")
    public void setRemarktext(String remarktext) {
        this.remarktext = remarktext;
    }

    /**
     * 
     * @return
     *     The remarktype
     */
    @JsonProperty("remarktype")
    public String getRemarktype() {
        return remarktype;
    }

    /**
     * 
     * @param remarktype
     *     The remarktype
     */
    @JsonProperty("remarktype")
    public void setRemarktype(String remarktype) {
        this.remarktype = remarktype;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(additionalinformation).append(dateofinformation).append(referenceid).append(remarkorder).append(remarksource).append(remarktext).append(remarktype).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Remark) == false) {
            return false;
        }
        Remark rhs = ((Remark) other);
        return new EqualsBuilder().append(additionalinformation, rhs.additionalinformation).append(dateofinformation, rhs.dateofinformation).append(referenceid, rhs.referenceid).append(remarkorder, rhs.remarkorder).append(remarksource, rhs.remarksource).append(remarktext, rhs.remarktext).append(remarktype, rhs.remarktype).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
