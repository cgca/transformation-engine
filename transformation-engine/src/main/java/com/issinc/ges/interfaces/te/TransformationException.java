/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te;

/**
 * This is a custom exception for use by the transformation engine. All exceptions occurring within the transformation
 * engine are caught and converted to TransformatioNExceptions.
 */
public final class TransformationException extends Exception {

    /**
     * The type of error that occurred within the DAL
     */
    public enum ErrorType {
        BAD_REQUEST,
        PARSING_ERROR,
        ATTRIBUTE_ERROR,
        PAYLOAD_ERROR,
        UNKNOWN_RULESET,
        UNKNOWN_ERROR,
        INVALID_TOKEN,
    }

    /**
     * The type of exception thrown
     */
    private ErrorType errorType;
    /**
     * A short message describing the exception
     */
    private String message;
    /**
     * A more detailed message describing the exception
     */
    private String detailedMessage;
    /**
     * The exception that caused the TransformationEngine to throw an exception
     */
    private Exception exception;

    /**
     * Create a new DALException with the given type and message
     * @param errorType The type of exception that occurred
     * @param message A message describing the error
     */
    public TransformationException(ErrorType errorType, String message) {
        this(errorType, message, null, null);
    }

    /**
     * Create a new DALException with the given type and message
     * @param errorType The type of exception that occurred
     * @param message A message describing the error
     * @param e An exception that caused this exception, or that is wrapped by this exception.
     */
    public TransformationException(ErrorType errorType, String message, Exception e) {
        this(errorType, message, null, e);
    }

    /**
     * Create a new DALException with the given type, message, and detailed
     * message
     * @param errorType The type of exception that occurred
     * @param message A message describing the error
     * @param detailedMessage A detailed message describing the error
     * @param e An exception that caused this exception, or that is wrapped by this exception.
     */
    public TransformationException(ErrorType errorType, String message, String detailedMessage, Exception e) {
        super(e);
        this.errorType = errorType;
        this.message = message;
        this.detailedMessage = detailedMessage;
        this.exception = e;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetailedMessage() {
        return detailedMessage;
    }

    public void setDetailedMessage(String detailedMessage) {
        this.detailedMessage = detailedMessage;
    }

} /* End of Class TransformationException */
