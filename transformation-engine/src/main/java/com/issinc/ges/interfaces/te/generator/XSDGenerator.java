/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */
package com.issinc.ges.interfaces.te.generator;

import java.util.Iterator;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.RuleSet;
import com.issinc.ges.interfaces.te.utilities.ModelUtils;


/**
 * Generates the rulesets from each report schema
 */
public class XSDGenerator {

    // Formatting for ruleset attributes
    public static final String OBJECT_ID_INSTANCE = "%s-%s_%s_%s-instance";
    public static final String DISPLAY_NAME = "%s %s from %s to %s Report ruleset";
    public static final String VERSION = "CIDNE-%s-%s-%s";

    private static final String RULESET_DATAMODEL =
            "http://interfaces.dae.smil.mil/schemas/transformationengine/ruleset";
    private static final String RULESET_OBJECT_ID = "ruleSetObjectId";
    private static final String RULESET_CLASSNAME = "className";

    private static final String RULE_ATTR_OLD_FIELD = "oldFieldPath";
    private static final String RULE_ATTR_NEW_FIELD = "newFieldPath";
    private static final String RULE_ATTR_PATH = "path";
    private static final String RULE_ATTR_NAME = "name";

    private static final String RULE_ADD_NODE = "com.issinc.ges.interfaces.te.rules.AddNode";
    private static final String RULE_DELETE_NODE = "com.issinc.ges.interfaces.te.rules.DeleteNode";
    private static final String RULE_JSON_NODE_TO_STR = "com.issinc.ges.interfaces.te.rules.JsonNodeToJsonString";
    private static final String RULE_JSON_SCHEMA_TO_NODE =
            "com.issinc.ges.interfaces.te.rules.JsonSchemaToJsonString";
    private static final String RULE_JSON_STR_TO_NODE = "com.issinc.ges.interfaces.te.rules.JsonStringToJsonNode";
    private static final String RULE_MOVE_NODE = "com.issinc.ges.interfaces.te.rules.MoveNode";
    private static final String RULE_RENAME_NODE = "com.issinc.ges.interfaces.te.rules.RenameNode";


    private static final String DATAMODEL_HAS_SECURITY = "hasSecurity";
    private static final String DATAMODEL_2X_ALIAS = "fieldAlias2x";
    private static final String DATAMODEL_IS_LOCATION = "isLocation";

    private static final String DEF_DATETIME = "#/definitions/dateTime";
    private static final String DEF_DATETIME_REQUIRED = "#/definitions/dateTimeRequired";
    private static final String DEF_TEXTFIELD_LEVEL_REQUIRED = "#/definitions/textFieldLevelRequired";
    private static final String DEF_LONG_STRING_REQUIRED = "#/definitions/longStringRequired";
    private static final String DEF_SHORT_STRING_REQUIRED = "#/definitions/shortStringRequired";
    private static final String DEF_LONG_STRING = "#/definitions/longString";
    private static final String DEF_SHORT_STRING = "#/definitions/shortString";
    private static final String DEF_NUM = "#/definitions/num";
    private static final String DEF_TEXT_REQUIRED = "#/definitions/textRequired";
    private static final String DEF_SECURITY = "#/definitions/security";


    private static final Logger logger = LoggerFactory.getLogger(XSDGenerator.class);

    /**
     * Default constructor
     */
    public XSDGenerator() {
    }

    /**
     * Takes in the schema (datamodel) and converts that to a RuleSet.
     *
     * @param datamodel
     *   The schema which is the basis for the rulesets.
     * @param module
     *   The module of the report.
     * @param reportType
     *   The report type of the report.
     * @param srcVersion
     *   The version that is the "from" version for the process the ruleset will
     *   run once requested.
     * @param dstVersion
     *   The version that is the "to" version for the process the ruleset will
     *   run once requested.
     * @return
     *   The generated ruleset
     */
    public final RuleSet generateRuleset(JsonNode datamodel, String module, String reportType,
                                         String srcVersion, String dstVersion) {

        RuleSet convertedRuleSet = null;

        final JsonNode ruleset = generateRulesetJsonNode(datamodel, module, reportType, srcVersion,
                dstVersion);

        try {
            convertedRuleSet = ModelUtils.toObject(ruleset, RuleSet.class);
        } catch (TransformationException e) {
            logger.error("Could not convert generated rule set", e);
        }

        return convertedRuleSet;
    }

    /**
     * Takes in the schema (datamodel) and converts that to a JsonNode.
     *
     * @param datamodel
     *   The schema which is the basis for the rulesets.
     * @param module
     *   The module of the report.
     * @param reportType
     *   The report type of the report.
     * @param srcVersion
     *   The version that is the "from" version for the process the ruleset will
     *   run once requested.
     * @param dstVersion
     *   The version that is the "to" version for the process the ruleset will
     *   run once requested.
     * @return
     *   The generated JsonNode ruleset
     */
    public final JsonNode generateRulesetJsonNode(JsonNode datamodel, String module, String reportType,
                                                  String srcVersion, String dstVersion) {
        final JsonNode ruleset = initRuleSet(module, reportType, srcVersion, dstVersion);
        final ArrayNode rules = (ArrayNode) ruleset.get("rules");


        addBaseRules(rules);  // convert base table to top-level fields
        addLocationRules(rules);  // move "location" to "Location" and replace the fields

        // remove any node in the details section that doesn't have a CIDNE 2.X field alias.  Also, rename the
        // node to the 2.X field alias.
        processPropertiesNode(rules, datamodel.get("properties").get("details").get("properties"),
                "properties.details.properties", "properties", null, null);

        addRequirementsRules(rules);

        // Remove left-over sections
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.datamodel"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.formId"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.location"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.remarks"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.requirements"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.security"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.objectHistory"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.details"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.base"));

        // convert it to a string
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_JSON_NODE_TO_STR));

        // create an XSD & sort it
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, "com.issinc.ges.interfaces.te.rules.JsonSchemaToXmlSchema")
                .put("wrapperName", module + reportType));


        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, "com.issinc.ges.interfaces.te.rules.SortXSD"));

        return ruleset;
    }

    private void processPropertiesNode(ArrayNode rules, JsonNode propertiesNode, String nodeLocation,
                                       String tgtLocation, String parentKey, JsonNode parentNode) {

        //add parentkey node if it exists
        if(parentKey!=null){
            addSchemaNode(rules, nodeLocation +"."+parentKey, DEF_SHORT_STRING);
        }
        Iterator<Map.Entry<String, JsonNode>> iterator = propertiesNode.fields();
        String arrayKey = null;

        while (iterator.hasNext()) {
            Map.Entry<String, JsonNode> entry = iterator.next();
            final JsonNode node = entry.getValue();
            final String nodeKey = entry.getKey();
            final String nodeAlias = get2xFieldAlias(node, nodeKey);

            if (refenceType(node).equals(DEF_SECURITY)) {
                // handle security sections - can have a 2x field alias or not.
                rules.add(JsonNodeFactory.instance.objectNode()
                        .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                        .put(RULE_ATTR_PATH, joinPaths(nodeLocation, nodeKey)));

                String alias;
                if (nodeAlias == null || nodeAlias.equals("security")) {
                    if (parentNode != null) {
                        // this is a "security" section...use:  [subtable]Classification
                        alias = get2xFieldAlias(parentNode, "");
                    } else {
                        alias = "";
                    }
                } else {
                    alias = nodeAlias;
                }

                addSchemaNode(rules, joinPaths(tgtLocation, alias + "Classification"), DEF_TEXT_REQUIRED);
                addSchemaNode(rules, joinPaths(tgtLocation, alias + "ClassificationReleasabilityMark"),
                        DEF_TEXT_REQUIRED);

            }
            else if (node.has("type") && node.get("type").asText().equalsIgnoreCase("array")) {

                //
                // Array processing...
                //
                JsonNode propNode = node.get("items").get("properties");

                //add reportKey
                addSchemaNode(rules,nodeLocation + "."+nodeKey+".items.properties.ReportKey", DEF_SHORT_STRING);

                //add originatingsystem
                addSchemaNode(rules, nodeLocation + "."+nodeKey+".items.properties.OriginatingSystem", DEF_SHORT_STRING);

                processPropertiesNode(rules, propNode, nodeLocation + "." + nodeKey + ".items.properties",
                        nodeLocation + "." + nodeKey + ".items.properties", arrayKey, node);

                // move/rename array to 2x alias in tgt location
                if (nodeLocation.equals(tgtLocation)) {
                    rules.add(JsonNodeFactory.instance.objectNode()
                            .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                            .put(RULE_ATTR_PATH, nodeLocation + "." + nodeKey)
                            .put(RULE_ATTR_NAME, nodeAlias));
                } else {
                    rules.add(JsonNodeFactory.instance.objectNode()
                            .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                            .put(RULE_ATTR_OLD_FIELD, nodeLocation + "." + nodeKey)
                            .put(RULE_ATTR_NEW_FIELD, joinPaths(tgtLocation, nodeAlias)));
                }

            } else {

                if(nodeKey.equals("referenceid")){
                    arrayKey = nodeAlias;
                }

                //
                // Standard (single) node processing...
                //
                if (node.has(DATAMODEL_2X_ALIAS)) {

                    switch (refenceType(node)) {
                        case DEF_TEXTFIELD_LEVEL_REQUIRED:
                            // replace the textFieldLevelRequired with a string, and security fields
                            rules.add(JsonNodeFactory.instance.objectNode()
                                    .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                                    .put(RULE_ATTR_PATH, nodeLocation + "." + nodeKey));
                            addSchemaNode(rules, joinPaths(tgtLocation, nodeAlias), DEF_TEXT_REQUIRED);
                            if (node.has(DATAMODEL_HAS_SECURITY) &&
                                    node.get(DATAMODEL_HAS_SECURITY).asBoolean() == true) {
                                addSchemaNode(rules, joinPaths(tgtLocation, nodeAlias + "Classification"),
                                        DEF_TEXT_REQUIRED);
                                addSchemaNode(rules, joinPaths(tgtLocation,
                                                nodeAlias + "ClassificationReleasabilityMark"), DEF_TEXT_REQUIRED);
                            }
                            break;
                        case DEF_DATETIME_REQUIRED:
                        case DEF_DATETIME:
                            // replace the date with a
                            rules.add(JsonNodeFactory.instance.objectNode()
                                    .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                                    .put(RULE_ATTR_PATH, nodeLocation + "." + nodeKey));
                            addSchemaNode(rules, joinPaths(tgtLocation, nodeAlias), DEF_TEXT_REQUIRED);
                            addSchemaNode(rules, joinPaths(tgtLocation, nodeAlias + "Zulu"), DEF_TEXT_REQUIRED);
                            break;
                        default:
                            if (isLocation(node)) {
                                // remove the original node, and add items from the "isLocation" field (MGRS,lat,lng)
                                rules.add(JsonNodeFactory.instance.objectNode()
                                        .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                                        .put(RULE_ATTR_PATH, nodeLocation + "." + nodeKey));
                                String[] locField = node.get(DATAMODEL_IS_LOCATION).asText().split(",");
                                if (locField.length == 1) {
                                    addSchemaNode(rules, joinPaths(tgtLocation, locField[0]), DEF_TEXT_REQUIRED);
                                } else if (locField.length == 3) {
                                    addSchemaNode(rules, joinPaths(tgtLocation, locField[0]), DEF_TEXT_REQUIRED);
                                    addSchemaNode(rules, joinPaths(tgtLocation, locField[1]), DEF_NUM);
                                    addSchemaNode(rules, joinPaths(tgtLocation, locField[2]), DEF_NUM);
                                } else {
                                    logger.error("format for isLocation must be:  <mgrs> | <mgrs>,<lat>,<lng>");
                                }
                            } else {
                                if (nodeLocation.equals(tgtLocation)) {
                                    rules.add(JsonNodeFactory.instance.objectNode()
                                            .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                                            .put(RULE_ATTR_PATH, nodeLocation + "." + nodeKey)
                                            .put(RULE_ATTR_NAME, nodeAlias));
                                } else {
                                    rules.add(JsonNodeFactory.instance.objectNode()
                                            .put(RULESET_CLASSNAME, RULE_MOVE_NODE)
                                            .put(RULE_ATTR_OLD_FIELD, nodeLocation + "." + nodeKey)
                                            .put(RULE_ATTR_NEW_FIELD, joinPaths(tgtLocation, nodeAlias)));
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    private String joinPaths(String path1, String path2) {
        if (path1 != null && path1.length() > 0) {
            return path1 + "." + path2;
        } else {
            return path2;
        }
    }
    private String refenceType(JsonNode node) {
        if (node.has("$ref")) return node.get("$ref").asText();
        else return "";
    }

    private JsonNode initRuleSet(String module, String reportType, String srcVersion, String dstVersion) {
        final ObjectNode ruleset = JsonNodeFactory.instance.objectNode()
                .put("datamodel", RULESET_DATAMODEL)
                .put("objectId", String.format(OBJECT_ID_INSTANCE, module, reportType, srcVersion, dstVersion))
                .put("displayName", String.format(DISPLAY_NAME, module, reportType, srcVersion, dstVersion))
                .put("sourceVersionObjectId", String.format(VERSION, srcVersion, module, reportType))
                .put("destinationVersionObjectId", String.format(VERSION, dstVersion, module, reportType));

        final ArrayNode rulesArray = JsonNodeFactory.instance.arrayNode();
        rulesArray.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_JSON_STR_TO_NODE));

        ruleset.set("rules", rulesArray);

        return ruleset;
    }

    /**
     * Add rules to build the schema for the "base" section
     * @param rules
     */
    private void addBaseRules(ArrayNode rules) {
        addSchemaNode(rules, "properties.Classification", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.ClassificationReleasabilityMark", DEF_LONG_STRING_REQUIRED);
        addSchemaNode(rules, "properties.DateLastViewed", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.DateLastViewedZulu", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.DateOccurred", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.DatePosted", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.DatePostedZulu", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.DateUpdated", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.DateUpdatedZulu", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.DisplayIcon", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.TrackingNumber", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.isDeprecated", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.IsTearLine", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.OriginatingSite", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.OriginatingSystem", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.OriginatorGroup", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.OriginatorName", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.OriginatorUnit", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.ReportDescription", DEF_TEXT_REQUIRED);
        addSchemaNode(rules, "properties.ReportDTG", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.ReportKey", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.ReportSummary", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.ReportTitle", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.Summary", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.TheaterFilter", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.Title", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.UpdatedByGroup", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.UpdatedByName", DEF_SHORT_STRING_REQUIRED);
        addSchemaNode(rules, "properties.UpdatedByUnit", DEF_SHORT_STRING_REQUIRED);
    }

    private void addSchemaNode(ArrayNode rules, String nodeLocation, String nodeType) {
        // NOTE: creating FIELD.$ref automatically creates FIELD
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put("path", nodeLocation + ".$ref")
                .put("defaultValue", nodeType));
    }

    private void addLocationRules(ArrayNode rules) {
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "properties.location")
                .put(RULE_ATTR_NAME, "Location"));

        // clear out the "properties" object so we can refill it...
        rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                    .put("path", "properties.Location.items.properties"));
        rules.add(JsonNodeFactory.instance.objectNode()
                    .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                    .put("path", "properties.Location.item.properties"));

        // add nodes for the new fields
        addSchemaNode(rules, "properties.Location.items.properties.AO", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.Classification", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.ClassificationReleasabilityMark", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.Country", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.DatePosted", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.DatePostedZulu", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.DateUpdated", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.DateUpdatedZulu", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.District", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.FOB", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.isDeprecated", DEF_NUM);
        addSchemaNode(rules, "properties.Location.items.properties.Latitude", DEF_NUM);
        addSchemaNode(rules, "properties.Location.items.properties.LocationActive", DEF_NUM);
        addSchemaNode(rules, "properties.Location.items.properties.LocationKey", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.Longitude", DEF_NUM);
        addSchemaNode(rules, "properties.Location.items.properties.MSC", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.NearestCity", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.OriginatingSite", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.OriginatingSystem", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.OriginatorGroup", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.OriginatorName", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.OriginatorUnit", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.PrimaryLocationFlag", DEF_NUM);
        addSchemaNode(rules, "properties.Location.items.properties.Province", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.ReportKey", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.TheaterFilter", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.UpdatedByGroup", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.UpdatedByName", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Location.items.properties.UpdatedByUnit", DEF_SHORT_STRING);
    }

    private void addRequirementsRules(ArrayNode rules) {
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_RENAME_NODE)
                .put(RULE_ATTR_PATH, "properties.requirements")
                .put(RULE_ATTR_NAME, "Requirements"));

        // clear out the "properties" object so we can refill it...
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_DELETE_NODE)
                .put("path", "properties.Requirements.items.properties"));
        rules.add(JsonNodeFactory.instance.objectNode()
                .put(RULESET_CLASSNAME, RULE_ADD_NODE)
                .put("path", "properties.Requirements.item.properties"));

        // add nodes for the new fields
        addSchemaNode(rules, "properties.Requirements.items.properties.Classification", DEF_TEXT_REQUIRED);
        addSchemaNode(rules, "properties.Requirements.items.properties.ClassificationReleasabilityMark", DEF_TEXT_REQUIRED);
        addSchemaNode(rules, "properties.Requirements.items.properties.Comment", DEF_TEXT_REQUIRED);
        addSchemaNode(rules, "properties.Requirements.items.properties.OriginatingSystem", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Requirements.items.properties.ReportKey", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Requirements.items.properties.Requirement", DEF_TEXT_REQUIRED);
        addSchemaNode(rules, "properties.Requirements.items.properties.RequirementsKey", DEF_SHORT_STRING);
        addSchemaNode(rules, "properties.Requirements.items.properties.RequirementType", DEF_SHORT_STRING);
    }


    private String get2xFieldAlias(JsonNode node, String defaultValue) {
        if (node.has(DATAMODEL_2X_ALIAS)) {
            return node.get(DATAMODEL_2X_ALIAS).asText();
        } else {
            return defaultValue;
        }
    }

    private boolean isLocation(JsonNode node) {
        if (node.has(DATAMODEL_IS_LOCATION)) {
            return true;
        }
        return false;
    }

}
