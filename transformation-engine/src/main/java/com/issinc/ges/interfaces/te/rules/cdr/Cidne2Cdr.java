/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.rules.cdr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.interfaces.te.models.*;
import com.issinc.ges.interfaces.te.models.cdr.*;
import com.issinc.ges.interfaces.te.models.cdr.ahr.HumintAhr;
import com.issinc.ges.interfaces.te.models.cdr.eval.HumintEval;
import com.issinc.ges.interfaces.te.models.cdr.hcr.HumintHcr;
import com.issinc.ges.interfaces.te.models.cdr.iir.HumintIir;
import com.issinc.ges.interfaces.te.models.cdr.iir.IirComment;
import com.issinc.ges.interfaces.te.models.cdr.iir.IirText;
import com.issinc.ges.interfaces.te.models.cdr.sdr.HumintSdr;
import com.issinc.ges.interfaces.te.models.cdr.tscr.HumintTscr;

/**
 * rule to convert CIDNE 4.1 Humint JSON data to CDR ISMv4 xml format
 */
public class Cidne2Cdr extends Rule {

    private final static String SIMPLE_DATETIME_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";
    private final static String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";
    private final static String MONTH_DATE_FORMAT = "hhmmss'Z'MMMyy";
    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    private String reportType;


    @Override
    public Object applyRule(String token, Object payload) throws TransformationException {

        //take in a CIDNE Json string,  convert it to XML String

        String procXmlStr = "";

        switch (reportType) {
            case "IIR":
                procXmlStr = ProcessIIR((String) payload);
                break;
            case "EVAL":
                procXmlStr = ProcessEval((String) payload);
                break;
            case "SDR":
                procXmlStr = ProcessSDR((String) payload);
                break;
            case "AHR":
                procXmlStr = ProcessAHR((String) payload);
                break;
            case "HCR":
                procXmlStr = ProcessHCR((String) payload);
                break;
            case "TSCR":
                procXmlStr = ProcessTSCR((String) payload);
                break;
            default:
                break;
        }

        return procXmlStr;
    }

    private String ProcessICRDetails(IcrDetails details, ReportBase base,
                                     Security security, String relatedRequirements,
                                     List<HumintSource> humintSourceList,
                                     String sourceNumber) {
        String xmlString = append_Header(reportType, false, security, base.getReportId());
        xmlString = xmlString + append_originating_system_id(base.getCurrentObjectState().getSystem());
        xmlString = xmlString + append_originating_system_name(base.getCurrentObjectState().getSystem());
        //xmlString = xmlString + append_country(details.getCountry());
        xmlString = xmlString + append_catalog_source_org();
        xmlString = xmlString + append_system_create_date();
        xmlString = xmlString + append_last_mod_date(base.getCurrentObjectState().getActionDate());
        xmlString = xmlString + append_original_pub_date(base.getReportDate());
        xmlString = xmlString + append_message_dtg(reportType, base);
        xmlString = xmlString + append_title(base.getReportTitle());
        xmlString = xmlString + append_Summary_eval(base.getReportSummary());
        xmlString = xmlString + append_ifc_subject_vocabulary(details.getIpsp());
        xmlString = xmlString + append_product_format(reportType);
        //xmlString = xmlString + append_ism_system_domain(); not a part of v4 icr schema
        xmlString = xmlString + append_full_text(reportType, base,
                details, security, relatedRequirements,
                humintSourceList);

        xmlString = xmlString + append_version_nbr();
        xmlString = xmlString + append_nhcd(details.getNhcd());
        xmlString = xmlString + append_origin_agency(base.getOriginatingUser(),
                base.getOriginatingGroup(), base.getReportSerial());
        xmlString = xmlString + append_background_text(details.getBackground(),
                security);
        xmlString = xmlString + append_icr_req(details.getRequirementclosed());

        xmlString = xmlString + append_guidance_sdr(details.getReleasability(),
                details.getSupersedes(), details.getIirdistribution(),
                security);

        xmlString = xmlString + append_DIA_received_date();

        if ("SDR".equalsIgnoreCase(reportType)) {
            xmlString = xmlString + append_source_nbr_sdr(sourceNumber);
        }

        xmlString = xmlString + append_version();
        xmlString = xmlString + append_document_nbr_icr(base.getReportSerial());
        xmlString = xmlString + append_requirement_info(base.getReportSerial(),
                details.getExpirationdate(), security,
                details.getRemark());

        xmlString = xmlString + append_sdr_id(base.getOriginatingGroup());
        xmlString = xmlString + append_sdr_status(base.getCurrentObjectState().getActionDate());

        xmlString = xmlString + append_ref_info(details.getReference());
        xmlString = xmlString + append_Footer(reportType,
                base.getReportSerial().getObjectValue(), false);

        return xmlString;
    }

    private String ProcessSDR(String payload) {
        HumintSdr report = JsonUtils.toObject(payload, HumintSdr.class);

        return ProcessICRDetails(report.getDetails(), report.getBase(), report.getSecurity(),
                report.getDetails().getRelatedrequirements(), report.getDetails().getSource(),
                report.getDetails().getSourcenumber());
    }

    private String ProcessTSCR(String payload) {
        HumintTscr report = JsonUtils.toObject(payload, HumintTscr.class);
        return ProcessICRDetails(report.getDetails(), report.getBase(), report.getSecurity(),
                null, null, null);
    }

    private String ProcessAHR(String payload) {
        HumintAhr report = JsonUtils.toObject(payload, HumintAhr.class);
        return ProcessICRDetails(report.getDetails(), report.getBase(), report.getSecurity(),
                null, null, null);
    }

    private String ProcessHCR(String payload) {
        HumintHcr report = JsonUtils.toObject(payload, HumintHcr.class);
        return ProcessICRDetails(report.getDetails(), report.getBase(), report.getSecurity(),
                null, null, null);
    }

    private String ProcessIIR(String payload) {

        HumintIir iir = JsonUtils.toObject(payload, HumintIir.class);
        String xmlString = append_Header(reportType, false, iir.getSecurity(), iir.getBase().getReportId());

        xmlString = xmlString + append_originating_system_id(iir.getBase().getCurrentObjectState().getSystem());
        xmlString = xmlString + append_originating_system_name(iir.getBase().getCurrentObjectState().getSystem());
        //xmlString = xmlString + append_country(iir.getDetails().getCountry());
        xmlString = xmlString + append_catalog_source_org();
        xmlString = xmlString + append_system_create_date();
        xmlString = xmlString + append_last_mod_date(iir.getBase().getCurrentObjectState().getActionDate());
        xmlString = xmlString + append_original_pub_date_iir(iir.getBase().getReportDate());
        xmlString = xmlString + append_message_dtg(reportType, iir.getBase());
        xmlString = xmlString + append_subject(iir.getDetails().getTopic());
        xmlString = xmlString + append_Summary_IIR(iir.getBase().getReportSummary());

        xmlString = xmlString + new_append_ifc_subject_vocabulary(iir.getDetails().getIpsp());

        xmlString = xmlString + append_product_format(reportType);
        //xmlString = xmlString + append_ism_system_domain();  not a part of v4 iir schema
        xmlString = xmlString + append_iir_msg_text(iir);
        xmlString = xmlString + append_full_text_iir(iir);
        xmlString = xmlString + append_version_nbr();
        xmlString = xmlString + append_doc_number_iir(iir);


        xmlString = xmlString + append_requirement_nbr(iir.getDetails().getRelatedrequirements());
        xmlString = xmlString + append_source_nbr(iir.getDetails().getSource());
//        xmlString = xmlString + append_iir_status(
//                iir.getBase().getCurrentObjectState().getActionDate().getZuluDateTime());  //not used in v4
        xmlString = xmlString + append_iir_comment(iir.getDetails().getComments());
        xmlString = xmlString + append_contains_us_citizen_info(iir.getDetails().getInstructions());
        xmlString = xmlString + append_collector(iir.getDetails().getColl(),
                iir.getBase().getReportSerial().getObjectValue());

        xmlString = xmlString + append_Footer("IIR", iir.getBase().getReportSerial().getObjectValue(), false);

        return xmlString;
    }

    private String ProcessEval(String payload) {
        HumintEval eval = JsonUtils.toObject(payload, HumintEval.class);
        String xmlString = append_Header("EVAL", false, eval.getSecurity(),eval.getBase().getReportId());
        xmlString = xmlString + append_originating_system_id(eval.getBase().getCurrentObjectState().getSystem());
        xmlString = xmlString + append_originating_system_name(eval.getBase().getCurrentObjectState().getSystem());
        xmlString = xmlString + append_catalog_source_org();
        xmlString = xmlString + append_system_create_date();
        xmlString = xmlString + append_last_mod_date(eval.getBase().getCurrentObjectState().getActionDate());
        xmlString = xmlString + append_original_pub_date(eval.getBase().getReportDate());
        xmlString = xmlString + append_title(eval.getBase().getReportTitle());
        xmlString = xmlString + append_Summary_eval(eval.getBase().getReportSummary());
        xmlString = xmlString + append_ifc_subject_vocabulary(eval.getDetails().getIpsp());
        xmlString = xmlString + append_product_format(reportType);
        //xmlString = xmlString + append_ism_system_domain();  not a part of v4 eval schema
        xmlString = xmlString + append_full_text_eval(eval);
        xmlString = xmlString + append_version_nbr();
        xmlString = xmlString + append_document_nbr_eval(eval.getBase().getReportSerial());
        xmlString = xmlString + append_collector_nbr_evaluated(eval.getDetails().getCustomernumber());
        xmlString = xmlString + append_evaluator_info(eval.getBase().getOriginatingUser(), eval.getBase().getOriginatingGroup(),
                eval.getDetails().getCustomernumber());
        xmlString = xmlString + append_guidance_text_eval(eval.getDetails().getGuidance(), eval.getSecurity());
        xmlString = xmlString + append_ref_iir(eval.getDetails().getReference(), eval.getDetails().getIrdt(),
                eval.getBase().getReportTitle(), eval.getSecurity(),
                eval.getDetails().getRelatedrequirements(), eval.getDetails().getReportvalue());
        xmlString = xmlString + append_source_nbr(eval.getDetails().getSource());
        //xmlString = xmlString + append_country(eval.getDetails().getCountry());
        xmlString = xmlString + append_eval_status(eval.getDetails().getLasttimeinformationofvalue());
        xmlString = xmlString + append_Footer(reportType,
                eval.getBase().getReportSerial().getObjectValue(), false);

        return xmlString;
    }


    private String append_eval_status(ActionDate LastTimeInformationOfValue) {
        String tmp = "";
        if (LastTimeInformationOfValue != null) {
            tmp = "<ns3:eval_status>";
            tmp = tmp + "<ns1:current_status>PUBLISHED</ns1:current_status>";
            tmp = tmp + "<ns1:status_date>" + new SimpleDateFormat(SIMPLE_DATETIME_FORMAT).format(LastTimeInformationOfValue.getZuluDateTime()) + "</ns1:status_date>";
            tmp = tmp + "</ns3:eval_status>";
        }
        return tmp;
    }

    private String append_Summary_eval(FieldSecurity summary) {
        String tmp = "";
        if (summary != null && summary.getObjectValue() != null) {
            tmp = "<ns1:summary";
            tmp = tmp + appendAttrSecurity(summary.getSecurity());
            tmp = tmp + " >"+ summary.getObjectValue()+ "</ns1:summary>";
        }
        return tmp;
    }

    private String append_title(FieldSecurity title) {

        String tmp = "";
        if (title != null && title.getObjectValue() != null) {
            tmp = "<ns1:title";
            tmp = tmp + appendAttrSecurity(title.getSecurity());
            tmp = tmp + " >"+ title.getObjectValue() + "</ns1:title>";
        }

        return tmp;
    }

    private String append_original_pub_date(ActionDate reportDate) {
        String tmp = "<ns1:original_pub_date>";
        tmp = tmp +
                new SimpleDateFormat(SIMPLE_DATE_FORMAT).format(reportDate.getZuluDateTime());
        tmp = tmp + "</ns1:original_pub_date>";

        return tmp;
    }

    private String append_last_mod_date(ActionDate reportDate) {
        String tmp = "<ns1:last_modification_date>";
        tmp = tmp +
                new SimpleDateFormat(SIMPLE_DATETIME_FORMAT).format(reportDate.getZuluDateTime());
        tmp = tmp + "</ns1:last_modification_date>";

        return tmp;
    }
    private String appendAttr(String value, String tag) {
        String tmp;

        if (value == null || value.isEmpty())
            tmp = "";
        else
            tmp = " " + tag + "=" + "\"" + value + "\"";

        return tmp;
    }

    private String append_originating_system_id(String system) {
        String tmp = "";
        if (system != null)
			/* Prepend CIDNE tag to indicate its comming from CIDNE */
            tmp = "<ns1:originating_system_id>CIDNE_" +
                    system
                    + "</ns1:originating_system_id>";

        return tmp;
    }

    private String append_originating_system_name(String system) {
        String tmp = "";

        if (system != null) {
            tmp = "<ns1:administrative_metadata>";
            tmp = tmp + "<ns1:originating_system_name>" +
                    system + "</ns1:originating_system_name>";
            tmp = tmp + "</ns1:administrative_metadata>";
        }

        return tmp;
    }


    private String append_country(List<Country> countries) {
        String tmp = "";

        for (Country country : countries) {

            if (!reportType.equals("EVAL"))
                tmp = tmp + "<ns1:country";
            else
                tmp = tmp + "<ns3:full_country_name";

            tmp = tmp + appendAttr(country.getCountry(), "ISO_3166_1_country_name");

            tmp = tmp + "/>";
        }


        return tmp;
    }

    String append_catalog_source_org() {
        return "<ns1:catalog_source_org>CIDNE</ns1:catalog_source_org>";
    }

    String append_original_pub_date_iir(ActionDate reportDate) {
        String tmp = "<ns1:original_pub_date>";
        tmp = tmp + new SimpleDateFormat(SIMPLE_DATE_FORMAT).format(reportDate.getZuluDateTime());
        tmp = tmp + "</ns1:original_pub_date>";

        return tmp;
    }

    private String append_message_dtg(String report, ReportBase base) {
        Date messageDate = base.getReportDate().getZuluDateTime();
        if (!report.equals("IIR"))
            messageDate = base.getCurrentObjectState().getActionDate().getZuluDateTime();

        return "<ns1:message_dtg>" + new SimpleDateFormat(MONTH_DATE_FORMAT).format(messageDate).toUpperCase()
                + "</ns1:message_dtg>";

    }
    private String append_system_create_date() {
        Date messageDate = Calendar.getInstance().getTime();
        return "<ns1:system_create_date>" + new SimpleDateFormat(SIMPLE_DATETIME_FORMAT).format(messageDate)
                + "</ns1:system_create_date>";

    }

    String append_subject(FieldSecurity subject) {
        String tmp = "<ns1:title";
        tmp = tmp + appendAttrSecurity(subject.getSecurity());
        tmp = tmp + " >";
       // tmp = tmp + "	<ns1:title_text>" + subject.getObjectValue() + "</ns1:title_text>";
        tmp = tmp + subject.getObjectValue();
        tmp = tmp + "</ns1:title>";

        return tmp;
    }


    private String append_document_nbr_eval(FieldSecurity reportSerial) {
        if (reportSerial != null && reportSerial.getObjectValue() != null) {
            return "<ns3:document_nbr_eval>" + reportSerial.getObjectValue().replaceAll(" ", "") + "</ns3:document_nbr_eval>";
        }
        return "";
    }

    private String append_collector_nbr_evaluated(String CustomerNumber) {
        String tmp = "<ns3:collector_nbr_evaluated>";
        if (CustomerNumber != null)
            tmp = tmp + CustomerNumber.replaceAll("\\.", "");
        tmp = tmp + "</ns3:collector_nbr_evaluated>";

        return tmp;
    }

    private String append_evaluator_info(String originatingUser, String originatingGroup,
                                         String customerNum) {
        String tmp = "<ns3:evaluator_info>";

        String trimmedUser = originatingUser;
        if (originatingUser.length() > 50) {
            trimmedUser = originatingUser.substring(0, 50);
        }
        tmp = tmp + "<ns3:evaluator_name>" +
                trimmedUser
                + "</ns3:evaluator_name>";
        tmp = tmp + "<ns3:email>" + "</ns3:email>";

        tmp = tmp + "<ns3:customer_nbr>" +
                customerNum.replaceAll("\\.", "")
                + "</ns3:customer_nbr>";
        tmp = tmp + "<ns3:phone_number>" + "</ns3:phone_number>";
        tmp = tmp + "<ns3:evaluation_organization>" +
                originatingGroup + "</ns3:evaluation_organization>";
        tmp = tmp + "</ns3:evaluator_info>";

        return tmp;
    }

    private String append_ref_iir(String refIir, ActionDate irdt, FieldSecurity title,
                                  Security security, String relatedReq, String reportValue) {

        String tmp = "";
        if (refIir == null || refIir.equals("")) {
            return tmp;
        }
        String refiirtmp = refIir.replaceAll(" ,.", ",,");

        //get last 10 chars of refiir
        if (refIir.length() >= 10) {
            refiirtmp = refiirtmp.substring(refiirtmp.length() - 10);
        }

        tmp = "<ns3:ref_iir>";
        tmp = tmp + "<ns3:ref_iir_date>" + new SimpleDateFormat("YYYY-MM-DD").format(irdt.getZuluDateTime())
                + "</ns3:ref_iir_date>";
        tmp = tmp + "<ns3:ref_iir_nbr>" + refiirtmp + "</ns3:ref_iir_nbr>";
        tmp = tmp + "<ns3:ref_iir_title";

        tmp = tmp + appendAttrSecurity(security);
        tmp = tmp + ">";
        tmp = tmp + "<ns1:title_text>" + title.getObjectValue() + "</ns1:title_text>";
        tmp = tmp + "</ns3:ref_iir_title>";
        tmp = tmp + "<ns3:eval_requirements>";
        tmp = tmp + append_requirement_nbr(relatedReq);
        tmp = tmp + "</ns3:eval_requirements>";
        if (!reportValue.contains("None")) {
            String[] rvalArray = reportValue.split("-");
            if (rvalArray.length == 2) {

                tmp = tmp + "<ns3:eval_value>";

                tmp = tmp + "<ns3:info_value_code>" + rvalArray[0] + "</ns3:info_value_code>";

                tmp = tmp + "<ns3:value_statement>" + rvalArray[1] + "</ns3:value_statement>";

                tmp = tmp + "</ns3:eval_value>";
            }
        }
        tmp = tmp + "</ns3:ref_iir>";


        return tmp;
    }


    private String append_nhcd(String nhcd) {
        String tmp = "";
        if (nhcd != null && !"".equals(nhcd)) {

            String[] nhcdArr = nhcd.split(";");
            for (String nhcdString : nhcdArr) {
                tmp = tmp + "<ns1:nhcd><ns1:nhcdID>" +
                        nhcdString + "</ns1:nhcdID></ns1:nhcd>";
            }

        }

        return tmp;
    }

    private String append_origin_agency(String originatorUser, String originatorGroup,
                                        FieldSecurity serial) {

        String first = "";
        String last = "";
        String fullName = "";
        String unit = "";
        String reportSerial = "";
        if (originatorGroup != null) {
            unit = originatorGroup;
        }
        if (originatorUser != null) {
            fullName = originatorUser;
        }

        if (serial != null && serial.getObjectValue() != null) {
            reportSerial = serial.getObjectValue();
        }

        //for some reason they only want the last 10 chars for number
        if (reportSerial.length() > 10) {
            reportSerial = reportSerial.substring(reportSerial.length() - 10);
        }
       String tmp = "<ns3:origin_agency>";
        tmp = tmp + "<ns3:originating_agency_nbr>" + reportSerial + "</ns3:originating_agency_nbr>";
        tmp = tmp + "<ns3:originating_agency>" + unit + "</ns3:originating_agency>";
        tmp = tmp + "<ns3:originator_name>";
        tmp = tmp + "<ns3:originator_fullname>" + fullName + "</ns3:originator_fullname>";


        String[] namAry = fullName.split(" ");
        if (namAry.length >= 2) {
            first = namAry[1];
            last = namAry[2];
        }

        tmp = tmp + "<ns3:firstname>" + first + "</ns3:firstname>";
        tmp = tmp + "<ns3:lastname>" + last + "</ns3:lastname>";
        tmp = tmp + "</ns3:originator_name>";
        tmp = tmp + "</ns3:origin_agency>";

        return tmp;
    }

    private String appendAttrSecurity(Security security) {
        String tmp= appendAttr(security.getClassification(), " ism:classification");
        tmp = tmp + appendAttr(security.getReleasableTo(), "ism:releasableTo");
        tmp = tmp + appendAttr(security.getDisseminationControls(), "ism:disseminationControls");
        tmp = tmp + appendAttr(security.getOwnerProducer(), "ism:ownerProducer");

        return tmp;
    }

    private String append_background_text(String background, Security security) {

        String tmp = "";
        if (background != null && security != null) {

            tmp = tmp + "<ns3:background_text";
            tmp = tmp + appendAttrSecurity(security);
            tmp = tmp + ">";
            tmp = tmp + "<ns1:paragraph";
            tmp = tmp + appendAttrSecurity(security);
            tmp = tmp + ">";
            tmp = tmp + "<ns1:text_value>" + background + "</ns1:text_value>";
            tmp = tmp + "</ns1:paragraph>";

            tmp = tmp + "</ns3:background_text> ";


        }
        return tmp;

    }
//

    private String append_icr_req(Boolean requirementClosed) {

        String status;

        if (requirementClosed) {
            status = "Cancelled";
        } else {
            status = "Active";
        }
        return "<ns3:icr_req><ns3:req_status>" + status + "</ns3:req_status></ns3:icr_req>";
    }

    private String append_guidance_text_eval(String guidance, Security security) {
        if (guidance == null || security == null) {
            return "";
        }
        String tmp = "";
        tmp = tmp + "<ns3:guidance_text";
        tmp = tmp + appendAttrSecurity(security);
        tmp = tmp + ">";
        tmp = tmp + " <ns1:paragraph ";
        tmp = tmp + appendAttrSecurity(security);
        tmp = tmp + ">";
        tmp = tmp + "<ns1:text_value>" + guidance + "</ns1:text_value>";
        tmp = tmp + "</ns1:paragraph>";

        tmp = tmp + "</ns3:guidance_text> ";
        return tmp;

    }

    private String append_guidance_sdr(String releasability,
                                       String supersedes, String iirDistribution,
                                       Security security) {

        String tmp = "";
        boolean hasRel = false;
        boolean hasSuper = false;
        boolean hasIIR = false;

        if (!"".equals(releasability)) {
            hasRel = true;
        }
        if (!"".equals(supersedes)) {
            hasSuper = true;
        }
        if (!"".equals(iirDistribution)) {

            hasIIR = true;
        }

        if (hasRel || hasSuper || hasIIR) {
            tmp = tmp + "<ns3:guidance";
           // tmp = tmp + appendAttrSecurity(security);
            tmp = tmp + ">";

            if (hasRel) {
                tmp = tmp + "<text_content>";
                tmp = tmp + append_Paragraph(releasability,security);
                tmp = tmp + "</text_content>";
            }

            if (hasSuper) {
                tmp = tmp + "<text_content>";
                tmp = tmp + append_Paragraph(supersedes,security);
                tmp = tmp + "</text_content>";
            }

            if (hasIIR) {
                tmp = tmp + "<text_content>";
                tmp = tmp + append_Paragraph(iirDistribution,security);
                tmp = tmp + "</text_content>";
            }

            tmp = tmp + "</ns3:guidance> ";
        }


        return tmp;

    }

    //
    private String append_DIA_received_date() {


        Date date = Calendar.getInstance().getTime();
        return "<ns3:DIA_received_date>" +
                new SimpleDateFormat(SIMPLE_DATETIME_FORMAT).format(date) + "</ns3:DIA_received_date> ";

    }

    private String append_version() {
        return "<ns3:version>ICR NOM</ns3:version>";

    }

    private String append_document_nbr_icr(FieldSecurity reportSerial) {
        String tmp = "";
        String trimmedSerial = "";
        if (reportSerial != null && reportSerial.getObjectValue() != null) {
            trimmedSerial = reportSerial.getObjectValue().replaceAll(" ,-", ",");

        }

        tmp = tmp + "<ns3:document_nbr_icr>" + trimmedSerial + "</ns3:document_nbr_icr>";
        return tmp;

    }

    private String append_requirement_info(FieldSecurity reportSerial,
                                           ActionDate expireDate, Security security,
                                           List<Remark> remarks) {

        String trimmedSerial = "";

        if (reportSerial != null && reportSerial.getObjectValue() != null) {
            trimmedSerial = reportSerial.getObjectValue().replaceAll(" ,-", ",,");
        }

        String tmp = "<ns3:requirement_info>";

        tmp = tmp + "<ns3:requirement_nbr>" + trimmedSerial + "</ns3:requirement_nbr>";
        tmp = tmp + "<ns3:requirement_type>" + reportType + "</ns3:requirement_type>";
        if (expireDate != null && expireDate.getZuluDateTime() != null) {
            tmp = tmp + "<ns3:expiration_date>" +
                    new SimpleDateFormat(SIMPLE_DATETIME_FORMAT).format(
                            expireDate.getZuluDateTime())
                    + "</ns3:expiration_date>";
        }
        List<Remark> remarkRs = getRemarks(remarks, "CMD_COMMENT");
        tmp = tmp + processRemarks(remarkRs,security);

        remarkRs = getRemarks(remarks, "REVIEWER_COMMENT");
        tmp = tmp + processRemarks(remarkRs,security);


        tmp = tmp + "</ns3:requirement_info>";

        return tmp;

    }

    private String processRemarks(List<Remark>remarkRs, Security security){
        String tmp = "";

        for (Remark remark : remarkRs) {

            if (remark.getRemarktext() != null && !"".equals(remark.getRemarktext())) {
                tmp = tmp + "<ns3:question>";
                tmp = tmp + "<ns3:question_text";
                tmp = tmp + appendAttrSecurity(security);
                tmp = tmp + ">";

                tmp = tmp + " <ns1:paragraph";
                tmp = tmp + appendAttrSecurity(security);
                tmp = tmp + ">";
                tmp = tmp + "<ns1:text_value>" + remark.getRemarktext() + "</ns1:text_value>";
                tmp = tmp + "</ns1:paragraph>";

                tmp = tmp + "</ns3:question_text>";
                tmp = tmp + "</ns3:question>";
            }
        }

        return tmp;
    }

    private List<Remark> getRemarks(List<Remark> remarks, String type) {
        List<Remark> foundRemarks = new ArrayList<>();
        for (Remark remark : remarks) {
            if (type.equalsIgnoreCase(remark.getRemarktype())) {
                foundRemarks.add(remark);
            }
        }
        return foundRemarks;
    }

    private String append_sdr_id(String originatorGroup) {

        String originatorUnit = "";
        if (originatorGroup != null) {
            originatorUnit = originatorGroup;
        }

        String tmp = "<ns3:sdr_id>";
        tmp = tmp + "<ns1:profile_id></ns1:profile_id>";
        tmp = tmp + "<ns1:orig_id>" + originatorUnit + "</ns1:orig_id>";
        tmp = tmp + "</ns3:sdr_id>";

        return tmp;

    }

    private String append_sdr_status(ActionDate publishDate) {

        Date initialDate = Calendar.getInstance().getTime();
        if (publishDate != null && publishDate.getZuluDateTime() != null) {
            initialDate = publishDate.getZuluDateTime();
        }

        String tmp = "<ns3:sdr_status><ns1:current_status>PUBLISHED</ns1:current_status>";
        tmp = tmp + "<ns1:status_date>" +
                new SimpleDateFormat(SIMPLE_DATETIME_FORMAT).format(initialDate)
                + "</ns1:status_date></ns3:sdr_status>";


        return tmp;

    }

    private String append_ref_info(String reference) {

        String trimmedReference = "";
        if (reference != null) {
            trimmedReference = reference;
            if (reference.length() > 254) {
                trimmedReference = reference.substring(0, 254);
            }
        }

        return "<ns3:ref_info>" + trimmedReference + "</ns3:ref_info>";
    }

    /* CIDNE 11337 ifc_subject_vocabulary failing validation, added TRIM */
    private String append_ifc_subject_vocabulary(String subject) {


        String tmp = "<ns1:functional_codes>";
        if (subject != null) {
            String ipsp = subject.replaceAll(",", ";");
            ipsp = ipsp.replaceAll(":", ";");

            if (ipsp.length() > 0) {
                String[] ipspArr = ipsp.split(";");

                for (String ipspval : ipspArr) {
                    if (ipspval.contains("IFC")) {
                        tmp = "<ns1:ifc_subject_vocabulary>";
                        String tempvalue = ipspval.replaceAll("\\.", "");
                        //GETINT-159 took out test code
                        if(tempvalue.length()>20){
                            tempvalue = tempvalue.substring(0,19);
                        }
                        tmp = tmp + tempvalue;
                        tmp = tmp + "</ns1:ifc_subject_vocabulary>";
                    }
                }
            }
        }

        if ("".equals(tmp)) {
            tmp = "<ns1:ifc_subject_vocabulary></ns1:ifc_subject_vocabulary>";
        }

        tmp = tmp + "</ns1:functional_codes>";
        return tmp;

    }

    private String new_append_ifc_subject_vocabulary(FieldSecurity vocab) {

        String tmp ="<ns1:functional_codes>";
        if (vocab != null) {
            String fullIpsp = vocab.getObjectValue();
            String[] ipspArr = fullIpsp.split(":");
            for (String ipsp : ipspArr) {
                if(ipsp.length()>20){
                    ipsp = ipsp.substring(0,19);
                }
                tmp = tmp + "<ns1:ifc_subject_vocabulary>" + ipsp.replaceAll("\\.", "")
                        + "</ns1:ifc_subject_vocabulary>";
            }
        }
        if ("".equals(tmp))
            tmp = "<ns1:ifc_subject_vocabulary></ns1:ifc_subject_vocabulary>";
        tmp = tmp + "</ns1:functional_codes>";
        return tmp;

    }

    private String new_append_nipf(FieldSecurity topic) {
        String tmp = "";

        if (topic != null && topic.getObjectValue() != null) {
            String fullTopic = topic.getObjectValue();
            String[] topicArr = fullTopic.split(";");

            for (String topicElem : topicArr) {

                tmp = tmp + "<ns1:nipf><ns1:nipf_topic> " +
                        topicElem
                        + "</ns1:nipf_topic></ns1:nipf>";
            }
        }

        return tmp;

    }

    private String append_product_format(String report) {
        String tmp = "<ns1:product_format>";
        tmp = tmp + "<ns1:media_format>MESSAGE</ns1:media_format>";
        tmp = tmp + "<ns1:product_type>" + report + "</ns1:product_type>";
        tmp = tmp + "</ns1:product_format>";

        return tmp;
    }

    private String append_ism_system_domain() {
        return "<ns1:ism_system_domain>C</ns1:ism_system_domain>";
    }

    private String append_doc_number_iir(HumintIir iir) {
        return "<ns3:document_nbr_iir>" +
                iir.getBase().getReportSerial().getObjectValue().replaceAll(" ,IIR,.", ",,")
                + "</ns3:document_nbr_iir>";

    }

    //  CIDNE-6741 break up text message
    private String append_iir_msg_text(HumintIir iir) {
        String tmp = "<ns1:msg_text>";
        for (IirText text : iir.getDetails().getText()) {
            String textorder = text.getTextorder().toString();
            String textdata = text.getTextdata();
            Security security = text.getSecurity();
            //tmp = tmp + append_iir_Paragraph(queryreporttext["textorder"][row],queryreporttext["textdata"][row],queryreporttext["classification"][row],queryreporttext["classificationreleaseabilitymark"][row]);
            tmp = tmp + append_iir_Paragraph(textorder, textdata, security);

        }
        tmp = tmp + "</ns1:msg_text>";


        return tmp;
    }


    private String generateHeader(Date Dtg, String Classif, String originatorUnit) {
        String header = "01  01  ";
        String newstring = new SimpleDateFormat("ddHHMMZ  mmm  YY").format(Dtg);
        header = header + newstring + "  RR      " + Classif + "         AT        " + originatorUnit;
        return header;
    }

    private String append_full_text_iir(HumintIir iir) {
        String tmp = "<ns1:full_text>";

        String getUnitPLA = iir.getDetails().getPla();
        Date Headerdate = iir.getBase().getReportDate().getZuluDateTime();

        tmp = tmp + generateHeader(Headerdate, iir.getSecurity().getClassification(),
                iir.getBase().getOriginatingUser());
        tmp = tmp + "\n";

        String from = "FM " + getUnitPLA;
        tmp = tmp + from + "\n";
        //message      = message + "TO ";

			/* tbd
					   for (row = 1; row LTE getPLA.RecordCount; row = row + 1) {
								if (getPLA["Type"][row] EQ "INFO" AND NOT infoLineWritten) {
									message = message + "INFO ";
									infoLinewritten = true;
								}
								message = message + getPLA["Name"][row] + "\n";
					   }
			*/

        String classif = iir.getSecurity().getClassification() + " " + iir.getSecurity().getReleasableTo();
        tmp = tmp + classif + "\n";
        tmp = tmp + "QQQQ" + "\n";

        String body = generateIIRNHMD208(iir);


        tmp = tmp + body;
        tmp = tmp + "##0000" + "\n";

        tmp = tmp + "</ns1:full_text>";

        return tmp;
    }

    private String append_full_text(String report, ReportBase base,
                                    IcrDetails details, Security security, String sdrRelatedRequirements,
                                    List<HumintSource> sdrSources) {

        String tmp = "<ns1:full_text>";

        String getUnitPLA = details.getPoc();
        Date Headerdate = base.getReportDate().getZuluDateTime();

        tmp = tmp + generateHeader(Headerdate, security.getClassification(),
                base.getOriginatingUser());
        tmp = tmp + "\n";

        String from = "FM " + getUnitPLA;
        tmp = tmp + from + "\n";
        //message      = message + "TO ";


			/* tbd
					   for (row = 1; row LTE getPLA.RecordCount; row = row + 1) {
								if (getPLA["Type"][row] EQ "INFO" AND NOT infoLineWritten) {
									message = message + "INFO ";
									infoLinewritten = true;
								}
								message = message + getPLA["Name"][row] + "\n";
					   }
			*/

        String classif = security.getClassification() + " " + security.getReleasableTo();
        tmp = tmp + classif + "\n";
        tmp = tmp + "QQQQ" + "\n";

        String body = generateICR(report,
                details, security, base, sdrRelatedRequirements,
                sdrSources);

        tmp = tmp + body;
        tmp = tmp + "##0000" + "\n";

        tmp = tmp + "</ns1:full_text>";

        return tmp;

    }

    private String append_full_text_eval(HumintEval eval) {
        String tmp = "<ns1:full_text>";
        String getUnitPLA = eval.getDetails().getPoc();
        Date Headerdate = eval.getBase().getReportDate().getZuluDateTime();

        tmp = tmp + generateHeader(Headerdate, eval.getSecurity().getClassification(),
                eval.getBase().getOriginatingUser());
        tmp = tmp + "\n";

        String from = "FM " + getUnitPLA;
        tmp = tmp + from + "\n";
        //message      = message + "TO ";

			/* tbd
					   for (row = 1; row LTE getPLA.RecordCount; row = row + 1) {
								if (getPLA["Type"][row] EQ "INFO" AND NOT infoLineWritten) {
									message = message + "INFO ";
									infoLinewritten = true;
								}
								message = message + getPLA["Name"][row] + "\n";
					   }
			*/

        String classif = eval.getSecurity().getClassification() + " " + eval.getSecurity().getReleasableTo();
        tmp = tmp + classif + "\n";
        tmp = tmp + "QQQQ" + "\n";

        String body = generateEval(eval);

        tmp = tmp + body;
        tmp = tmp + "##0000" + "\n";

        tmp = tmp + "</ns1:full_text>";

        return tmp;
    }


    private String generateICR(String reportType, IcrDetails details, Security security,
                               ReportBase base, String sdrRelatedRequirements,
                               List<HumintSource> sdrSources) {


        List<Country> countryList = details.getCountry();
        List<Countrypriority> countryPriorityList = details.getCountrypriority();
        List<Countrypriority2> countryPriorityList2 = details.getCountrypriority2();
        List<Countrypriority3> countryPriorityList3 = details.getCountrypriority3();
        List<Remark> CommentQuery = details.getRemark();

        FieldSecurity reportSerial = base.getReportSerial();
        String Pass = details.getPass();
        String reference = details.getReference();
        FieldSecurity summary = base.getReportSummary();
        String ipsp = details.getIpsp();
        Integer priority = details.getPriority();
        String nhcd = details.getNhcd();
        ActionDate expireDate = details.getExpirationdate();
        String background = details.getBackground();
        String requirement = details.getRequirement();
        String supercedes = details.getSupersedes();
        String releasability = details.getReleasability();
        String IIRDistribution = details.getIirdistribution();
        String poc = details.getPoc();
        String productUseList = "";
        for (Prodtask task : details.getProdtask()) {
            if (!"".equals(productUseList)) {
                productUseList = productUseList + ",";
            }
            productUseList = productUseList + task.getProdtask();
        }

        String sugcoll = details.getSugcoll();
        String originatorNumber = details.getOriginatornumber();
        String originatorName = base.getOriginatingUser();

        String outbound = "";
        outbound = outbound + "/n";

        outbound = outbound + "SERIAL:" +
                formatHumintStringDHM1_M3(false, reportSerial.getObjectValue(), "(U).");
        outbound = outbound + "/n";

        if (Pass != null && !"".equals(Pass)) {
            outbound = outbound + "PASS:" +
                    formatHumintStringDHM1_M3(true, Pass, "NONE.");
            outbound = outbound + "/n";
        }

        String countries = "";
        for (Country country : countryList) {
            if (!"".equals(countries)) {
                countries = countries + ",";
            }
            countries = countries + country.getCountry();
        }
        outbound = outbound + "COUNTRY:" + formatHumintStringDHM1_M3(true, countries, "(U).");
        outbound = outbound + "\n";

        outbound = outbound + "REF:" + formatHumintStringDHM1_M3(false,
                reference, "");
        outbound = outbound + "\n";
        if ("SDR".equalsIgnoreCase(reportType)) {
            outbound = outbound + "REQS:" + formatHumintStringDHM1_M3(true,
                    sdrRelatedRequirements, "(U).");
            outbound = outbound + "\n";

            String sources = "";
            for (HumintSource source : sdrSources) {
                if (!"".equals(sources)) {
                    sources = sources + ",";
                }
                sources = sources + source.getSourcesummary().getObjectValue();
            }
            outbound = outbound + "SOURCE:" + formatHumintStringDHM1_M3(true, sources
                    , "(U).");
            outbound = outbound + "\n";

        }
        outbound = outbound + "SUMMARY: " +
                formatHumintStringDHM1_M3(false, summary.getObjectValue(), "(U).");
        outbound = outbound + "/n";

        outbound = outbound + "TEXT:";
        outbound = outbound + "/n";

        outbound = outbound + "IPSP:" + formatHumintStringDHM1_M3(true, ipsp, "(U).");
        outbound = outbound + "/n";

        if (priority != null) {
            outbound = outbound + "PRIORITY:" + formatHumintStringDHM1_M3(true,
                    String.valueOf(priority), "NONE.");
            outbound = outbound + "/n";
        }

        outbound = outbound + "NHCD:" + formatHumintStringDHM1_M3(true, nhcd, "(U).");
        outbound = outbound + "/n";

        Date tempDate = Calendar.getInstance().getTime();
        if (expireDate != null && expireDate.getZuluDateTime() != null) {
            tempDate = expireDate.getZuluDateTime();
        }
        outbound = outbound + "EXPYRMO: (U) " +
                new SimpleDateFormat(SIMPLE_DATETIME_FORMAT).format(tempDate) + "\\.";

        outbound = outbound + "/n";

        outbound = outbound + "BACKGD: " + formatHumintStringDHM1_M3(false, background, "(U).");
        outbound = outbound + "/n";

        outbound = outbound + "REQMT: " + formatHumintStringDHM1_M3(false, requirement, "(U).");
        outbound = outbound + "/n";

        outbound = outbound + "GUID: (U).";
        outbound = outbound + "/n";

        outbound = outbound + "RELEASABILITY: " + formatHumintStringDHM1_M3(false, releasability, "(U).");
        outbound = outbound + "/n";

        outbound = outbound + "SUPERCEDES:" + formatHumintStringDHM1_M3(true, supercedes, "(U).");
        outbound = outbound + "/n";

        //Take out DHM1 req
        //	outbound = outbound + "SUPPORT REQS: " + query.SupportingRequirements  ;
        // 	    outbound = outbound + "/n";

        outbound = outbound + "IIR DISTRIBUTION: " + formatHumintStringDHM1_M3(false, IIRDistribution, "(U).");
        outbound = outbound + "/n";

        if (countryPriorityList != null && countryPriorityList.size() > 0) {
            outbound = outbound + "COUNTRY PRIORITIES (C): ";
            outbound = outbound + "/n";
            String countrypriorities = "";
            for (Countrypriority country : countryPriorityList) {
                if (!"".equals(countrypriorities)) {
                    countrypriorities = countrypriorities + ",";
                }
                countrypriorities = countrypriorities + country.getCountrypriority();
            }
            outbound = outbound + "PRIORITY ONE: " + formatHumintStringDHM1_M3(false, countrypriorities, "(U).");
            outbound = outbound + "/n";
            if (countryPriorityList2 != null && countryPriorityList2.size() > 0) {
                String countrypriorities2 = "";
                for (Countrypriority2 country : countryPriorityList2) {
                    if (!"".equals(countrypriorities2)) {
                        countrypriorities2 = countrypriorities2 + ",";
                    }
                    countrypriorities2 = countrypriorities2 + country.getCountrypriority();
                }
                outbound = outbound + "PRIORITY TWO: " + formatHumintStringDHM1_M3(false, countrypriorities2, "(U).");
                outbound = outbound + "/n";
                if (countryPriorityList3 != null && countryPriorityList3.size() > 0) {
                    String countrypriorities3 = "";
                    for (Countrypriority3 country : countryPriorityList3) {
                        if (!"".equals(countrypriorities3)) {
                            countrypriorities3 = countrypriorities3 + ",";
                        }
                        countrypriorities3 = countrypriorities3 + country.getCountrypriority();
                    }
                    outbound = outbound + "PRIORITY THREE: " +
                            formatHumintStringDHM1_M3(false, countrypriorities3, "(U).");

                    outbound = outbound + "/n";
                }
            }
        } else {
            outbound = outbound + "(U) COUNTRY PRIORITIES: N/A. ";
            outbound = outbound + "/n";
        }

        outbound = outbound + "POC:" + formatHumintStringDHM1_M3(true, poc, "(U).");
        outbound = outbound + "/n";

        outbound = outbound + "PRODTASK:" + formatHumintStringDHM1_M3(true, productUseList, "(U).");
        outbound = outbound + "/n";

        outbound = outbound + "ORIGNO:" + formatHumintStringDHM1_M3(true, originatorNumber, "(U).");
        outbound = outbound + "/n";

        outbound = outbound + "ORIGNME:" + formatHumintStringDHM1_M3(true, originatorName, "(U).");
        outbound = outbound + "/n";


        outbound = outbound + "SUGCOLL:" + formatHumintStringDHM1_M3(true, sugcoll, "(U).");
        outbound = outbound + "/n";

        for (Remark comment : CommentQuery) {
            if ("COMMENT".equalsIgnoreCase(comment.getRemarktype())) {
                if (comment.getRemarktext() != null && !"".equals(comment.getRemarktext())) {
                    outbound = outbound + "COMMENT: " + comment.getRemarktext();
                } else {
                    outbound = outbound + "COMMENT: (U) NOT REPORTED.";
                }
                outbound = outbound + "/n";
            }
        }

        if (security.getDerivedFrom() != null) {
            outbound = outbound + "DRV FROM:" + formatHumintStringDHM1_M3(true,
                    security.getDerivedFrom(), "(U).");
        }

        return outbound;
    }

    private String generateEval(HumintEval eval) {
        String outbound = "";

        outbound = outbound + "\n";
        outbound = outbound + "\n";

        outbound = outbound + "ORIGINATING UNIT:" + formatHumintStringDHM1_M3(true, eval.getBase().getOriginatingGroup(), "");
        outbound = outbound + "\n";

        outbound = outbound + "SERIAL:" + formatHumintStringDHM1_M3(true, eval.getBase().getReportSerial().getObjectValue(), "");
        outbound = outbound + "\n";

        String countries = "";
        for (Country country : eval.getDetails().getCountry()) {
            if (!"".equals(countries)) {
                countries = countries + ",";
            }
            countries = countries + country;
        }
        outbound = outbound + "COUNTRY:" + formatHumintStringDHM1_M3(true, countries, "(U).");
        outbound = outbound + "\n";

        if (eval.getDetails().getReportvalue() != null)
            outbound = outbound + "SUBJ: " + formatHumintStringDHM1_M3(false, eval.getDetails().getReportvalue(), "");
        else
            outbound = outbound + " NONE.";
        outbound = outbound + "\n";

        outbound = outbound + "REF:" + formatHumintStringDHM1_M3(false,
                eval.getDetails().getReference(), "");
        outbound = outbound + "\n";

        outbound = outbound + "REQS:" + formatHumintStringDHM1_M3(true,
                eval.getDetails().getRelatedrequirements(), "(U).");
        outbound = outbound + "\n";

        String sources = "";
        for (HumintSource source : eval.getDetails().getSource()) {
            if (!"".equals(sources)) {
                sources = sources + ",";
            }
            sources = sources + source;
        }
        outbound = outbound + "SOURCE:" + formatHumintStringDHM1_M3(true, sources
                , "(U).");
        outbound = outbound + "\n";

        outbound = outbound + "TEXT:";
        outbound = outbound + "\n";

        outbound = outbound + "CONSRESP:" + formatHumintStringDHM1_M3(true,
                "(U).", "(U).");
        outbound = outbound + "\n";

        outbound = outbound + "REPORTER:" + formatHumintStringDHM1_M3(true,
                eval.getDetails().getReporter(), "(U).");
        outbound = outbound + "\n";


        if (eval.getDetails().getIrdt() != null && eval.getDetails().getIrdt().getZuluDateTime() != null)
            outbound = outbound + "IRDT: (U) " + new SimpleDateFormat("yyyymmdd").format(
                    eval.getDetails().getIrdt().getZuluDateTime()) + "\\.";
        else
            outbound = outbound + "IRDT:" + formatHumintStringDHM1_M3(true, "", "(U).");
        outbound = outbound + "\n";

        outbound = outbound + "GUID:" + formatHumintStringDHM1_M3(false, eval.getDetails().getGuidance()
                , "");
        outbound = outbound + "\n";

        outbound = outbound + "VALUE:" + formatHumintStringDHM1_M3(true,
                eval.getDetails().getReportvalue(), "(U).");
        outbound = outbound + "\n";


        outbound = outbound + "EVALNME:" + formatHumintStringDHM1_M3(true,
                eval.getBase().getOriginatingUser(), "");
        outbound = outbound + "\n";


        outbound = outbound + "CUSTNO:" + formatHumintStringDHM1_M3(true,
                eval.getDetails().getCustomernumber(), "(U).");
        outbound = outbound + "\n";


        outbound = outbound + "IPSP:" + formatHumintStringDHM1_M3(true,
                eval.getDetails().getIpsp(), "(U).");
        outbound = outbound + "\n";

        outbound = outbound + "WARNING:" + formatHumintStringDHM1_M3(true,
                eval.getDetails().getWarning(), "");
        outbound = outbound + "\n";

        if (eval.getSecurity().getDerivedFrom() != null) {
            outbound = outbound + "DRV FROM:" + formatHumintStringDHM1_M3(true,
                    eval.getSecurity().getDerivedFrom(), "(U).");
        }
        outbound = outbound + "\n";

        return outbound;
    }


    private String generateIIRNHMD208(HumintIir iir) {

//        TextQuery  =  executeSql("Select TextData,Classification,ClassificationReleasabilityMark from #tableName#Text where reportkey =  '#ReportKey#' order by TextOrder asc");
//        CountryClassQuery = executeSql("SELECT 	CountryKey, Classification, ClassificationReleasabilityMark FROM #tableName#Country WHERE ReportKey = '#ReportKey#'");

        String outbound = "";

        outbound = outbound + "\n";

        outbound = outbound + "SERIAL: " + formatHumintStringNMHD208(true,
                iir.getBase().getReportSerial().getObjectValue(), null, false);
        outbound = outbound + "\n";

        outbound = outbound + "DATE OF PUBLICATION: " + formatHumintStringNMHD208(true,
                iir.getBase().getReportDate().getZuluDateTime().toString(), null, false);
        outbound = outbound + "\n";

        outbound = outbound + "------------------------------------------------------" + "\n";
        outbound = outbound + "                DEPARTMENT OF DEFENSE                 " + "\n";
        outbound = outbound + "INFORMATION REPORT, NOT FINALLY EVALUATED INTELLIGENCE" + "\n";
        outbound = outbound + "------------------------------------------------------" + "\n";
        outbound = outbound + "\n";
        String countryList = "";
        for (Country country : iir.getDetails().getCountry()) {
            if (!"".equals(countryList)) {
                countryList = countryList + ",";
            }
            countryList = countryList + country.getCountry();
        }
        outbound = outbound + "COUNTRY: " + formatHumintStringNMHD208(true, countryList, null, false);
        outbound = outbound + "\n";

        outbound = outbound + "SUBJECT: " + formatHumintStringNMHD208(true,
                iir.getDetails().getTopic().getObjectValue(), iir.getDetails().getTopic().getSecurity(), false);
        outbound = outbound + "\n";

        String tempDate;
        if (iir.getDetails().getDoi() != null && iir.getDetails().getDoi() != null) {

            tempDate = iir.getDetails().getDoi().getObjectValue();

            outbound = outbound + "DATE OF INFORMATION: " + formatHumintStringNMHD208(true, tempDate, null, false);
            outbound = outbound + "\n";
        }

        if (iir.getDetails().getCutoffdate() != null && iir.getDetails().getCutoffdate().getZuluDateTime() != null) {

            tempDate = iir.getDetails().getCutoffdate().getZuluDateTime().toString();

            outbound = outbound + "CUTOFF: " + formatHumintStringNMHD208(true, tempDate, null, false);
            outbound = outbound + "\n";
        }

        outbound = outbound + "SUMMARY: ";
        if (iir.getBase().getReportSummary() != null) {
            outbound = outbound + formatHumintStringNMHD208(true,
                    iir.getBase().getReportSummary().getObjectValue(),
                    iir.getBase().getReportSummary().getSecurity(), false);
        }
        outbound = outbound + "\n";
        int cnt = 0;
        for (HumintSource source : iir.getDetails().getSource()) {
            cnt = cnt + 1;
            if (source.getSourcenumber() != null) {
                outbound = outbound + "SOURCE NUMBER " +
                        cnt + ":" + formatHumintStringNMHD208(true, source.getSourcenumber().getObjectValue()
                        , source.getSourcenumber().getSecurity(), false);
                outbound = outbound + "\n";
            }

            outbound = outbound + "SOURCE " +
                    cnt + ":" + formatHumintStringNMHD208(true, source.getSourcesummary().getObjectValue()
                    , source.getSourcesummary().getSecurity(), false);
            outbound = outbound + "\n";
            outbound = outbound + "CONTEXT " +
                    cnt + ":" + formatHumintStringNMHD208(true, source.getContext().getObjectValue()
                    , source.getContext().getSecurity(), false);
            outbound = outbound + "\n";
        }

        if (iir.getDetails().getWarningoptional() != null) {
            outbound = outbound + "WARNING: " +
                    formatHumintStringNMHD208(true,
                            iir.getDetails().getWarningoptional().getObjectValue(),
                            iir.getDetails().getWarningoptional().getSecurity(), false);
            outbound = outbound + "\n";
        }


        outbound = outbound + "TEXT: ";
        for (IirText text : iir.getDetails().getText()) {
            outbound = outbound +
                    formatHumintStringNMHD208(true, text.getTextdata(),
                            text.getSecurity(), false);
            outbound = outbound + "\n";
        }

        outbound = outbound + "COMMENTS: ";
        for (IirComment comment : iir.getDetails().getComments()) {
            outbound = outbound +
                    formatHumintStringNMHD208(true, comment.getCommentsdata(),
                            comment.getSecurity(), false);
            outbound = outbound + "\n";
        }


		/* CIDNE 11329 Attachement text not making into ISM */
        outbound = outbound + "ATTACHMENTS: " +
                formatHumintStringNMHD208(true, iir.getDetails().getEnclosures().getObjectValue(),
                        iir.getDetails().getEnclosures().getSecurity(), false);
        outbound = outbound + "\n";

        outbound = outbound + "TOPIC: " +
                formatHumintStringNMHD208(true, iir.getDetails().getTopic().getObjectValue(),
                        iir.getDetails().getTopic().getSecurity(), false);
        outbound = outbound + "\n";

        if (iir.getDetails().getIpsp() != null) {
            outbound = outbound + "FUNCTIONAL CODE: " + formatHumintStringNMHD208(true,
                    iir.getDetails().getIpsp().getObjectValue(),
                    iir.getDetails().getIpsp().getSecurity(), false);
            outbound = outbound + "\n";
        }

        if (iir.getDetails().getRelatedrequirements() != null) {
            outbound = outbound + "REQUIREMENT: " +
                    formatHumintStringNMHD208(true,
                            iir.getDetails().getRelatedrequirements().getObjectValue(),
                            iir.getDetails().getRelatedrequirements().getSecurity(), false);
            outbound = outbound + "\n";
        }

        if (iir.getDetails().getColl() != null) {
            outbound = outbound + "MGT CODE: " +
                    formatHumintStringNMHD208(true,
                            iir.getDetails().getColl().getObjectValue(),
                            iir.getDetails().getColl().getSecurity(), false);
            outbound = outbound + "\n";
        }
        if (iir.getDetails().getInstructions() != null) {
            outbound = outbound + "INSTR: " +
                    formatHumintStringNMHD208(true,
                            iir.getDetails().getInstructions().getObjectValue(),
                            iir.getDetails().getInstructions().getSecurity(), false);
            outbound = outbound + "\n";
        }
        if (iir.getDetails().getPreparedby() != null) {
            outbound = outbound + "PREP: " +
                    formatHumintStringNMHD208(true,
                            iir.getDetails().getPreparedby().getObjectValue(),
                            iir.getDetails().getPreparedby().getSecurity(), false);
            outbound = outbound + "\n";
        }
        if (iir.getDetails().getJointrep() != null) {
            outbound = outbound + "JOINT REP: " +
                    formatHumintStringNMHD208(true,
                            iir.getDetails().getJointrep().getObjectValue(),
                            iir.getDetails().getJointrep().getSecurity(), false);
            outbound = outbound + "\n";
        }

        if (iir.getDetails().getAcquireddate() != null && iir.getDetails().getAcquireddate().getZuluDateTime() != null) {
            tempDate = iir.getDetails().getAcquireddate().getZuluDateTime().toString();
            outbound = outbound + "DATE OF ACQUISITION: " +
                    formatHumintStringNMHD208(true,
                            tempDate,
                            null, false);
            outbound = outbound + "\n";
        }
        if (iir.getDetails().getPoc() != null) {
            outbound = outbound + "POC: " +
                    formatHumintStringNMHD208(true,
                            iir.getDetails().getPoc().getObjectValue(),
                            iir.getDetails().getPoc().getSecurity(), false);
            outbound = outbound + "\n";
        }

        if (iir.getDetails().getWarning() != null) {
            outbound = outbound + "WARNING: " +
                    formatHumintStringNMHD208(true,
                            iir.getDetails().getWarning().getObjectValue(),
                            iir.getDetails().getWarning().getSecurity(), false);
            outbound = outbound + "\n";
        }


        outbound = outbound + repeatString("-", 28) + "DISSEMINATION " + repeatString("-", 28);
        outbound = outbound + "\n";

        outbound = outbound + "AGENCY: " +
                formatHumintStringNMHD208(true,
                        iir.getDetails().getAgency().getObjectValue(),
                        iir.getDetails().getAgency().getSecurity(), false);
        outbound = outbound + "\n";
        outbound = outbound + "U.S. MISSION: " +
                formatHumintStringNMHD208(true,
                        iir.getDetails().getUsmission().getObjectValue(),
                        iir.getDetails().getUsmission().getSecurity(), false);
        outbound = outbound + "\n";
        outbound = outbound + "MILITARY: " +
                formatHumintStringNMHD208(true,
                        iir.getDetails().getMilitary().getObjectValue(),
                        iir.getDetails().getMilitary().getSecurity(), false);
        outbound = outbound + "\n";
        outbound = outbound + "STATE/LOCAL: " +
                formatHumintStringNMHD208(true,
                        iir.getDetails().getStatelocal().getObjectValue(),
                        iir.getDetails().getStatelocal().getSecurity(), false);
        outbound = outbound + "\n";

        outbound = outbound + repeatString("-", 27) + "CLASSIFICATION " + repeatString("-", 28);
        outbound = outbound + "\n";
        outbound = outbound + "CLASSIFIED BY: " +
                formatHumintStringNMHD208(true,
                        iir.getSecurity().getClassification(),
                        null, false);
        outbound = outbound + "\n";

        outbound = outbound + "REASON: " +
                formatHumintStringNMHD208(true,
                        iir.getSecurity().getClassificationReason(),
                        null, false);
        outbound = outbound + "\n";

        if (iir.getSecurity().getDeclassDate() != null && iir.getSecurity().getDeclassDate().getZuluDateTime() != null)
            outbound = outbound + "DECLASSIFIED ON: " +
                    formatHumintStringNMHD208(true,
                            iir.getSecurity().getDeclassDate().getZuluDateTime().toString(),
                            null, false);
        outbound = outbound + "\n";
        outbound = outbound + "\n";
        outbound = outbound + "DERIVED FROM: " +
                formatHumintStringNMHD208(true,
                        iir.getSecurity().getDerivedFrom(),
                        null, false);
        outbound = outbound + "\n";

        return outbound;
    }

    private String repeatString(String input, int count) {
        String output = "";
        for (int i = 0; i < count; i++) {
            output = output + input;
        }
        return output;
    }

    private String formatHumintStringDHM1_M3(boolean prefixClassification,
                                             String input, String defaultValue) {
//    <!---Rather then modify the "formatHumintStringDHM1" method above and have to change the parameters
//    everywhere this is called, decided to just add this method, specifically for M3.--->
//
//
//    <!--- Adds period at the end and (U) at the beginning of string
//    Had to take into account that certain fields end in ?
//    Added argrument prefixClassification: boolean determining whether
//            (U) should be added to beginning of string--->

        String defaultVal = "";
        if (defaultValue != null) {
            defaultVal = defaultValue;
        }
        if ("".equals(input) || input == null) {
            return defaultVal;
        }

        String formatString = input.replaceAll("\\.", "");
        formatString = formatString.replaceAll(";", "");
        formatString = formatString.replaceAll(",", "");
        formatString = formatString.replaceAll("//", "");


        if (!formatString.endsWith("?")) {
//    <!--- If it ends in a ? leave it, this may be different for outgoing M3 messages
//    so for formatting M3 add the period after the ? regardless as this maybe used
//    by M3 for processing.    --->
            formatString = formatString + "\\.";
        }

        if (prefixClassification && !formatString.startsWith("(U)"))
            formatString = " (U) " + formatString;


        formatString = formatString.replaceAll("<BR />", "/n");
        return formatString;
    }


    private String formatHumintStringNMHD208(boolean prefixClassification, String input,
                                             Security security, boolean addPeriod) {
// New format CIDNE -4036
//<!--- Adds period at the end and (U) at the beginning of string
//        Had to take into account that certain fields end in ?
//        Added argrument prefixClassification: boolean determining whether
//        (U) should be added to beginning of string--->

        if (input == null || "".equals(input)) {
            return "";
        }

        String formatString = input.replaceAll("\\.", "");
        formatString = formatString.replaceAll(";", "");
        formatString = formatString.replaceAll(",", "");
        formatString = formatString.replaceAll("//", "");


        if (!formatString.endsWith("?") && !formatString.endsWith("\\.") && addPeriod) {
//        <!--- If it ends in a ? leave it, this may be different for outgoing M3 messages
//        so for formatting M3 add the period after the ? regardless as this maybe used
//        by M3 for processing.    --->
            formatString = formatString + "\\.";
        }

        if (prefixClassification && security != null) {
            if (security.getReleasableTo() == null || "".equals(security.getReleasableTo())) {
                formatString = "(" + security.getClassification() + ") " + formatString;
            } else {
                formatString = "(" + security.getClassification() + "//" +
                        security.getReleasableTo() + ") " + formatString;
            }
        }
        return formatString;

    }


    private String append_version_nbr() {
        return "<ns1:version_nbr><ns1:version>1</ns1:version></ns1:version_nbr>";
    }


    private String append_contains_us_citizen_info(FieldSecurity instructions) {

        String tmp = "";
        if (instructions != null) {
            String tempInstr = instructions.getObjectValue();
            tmp = "<ns3:instructions>";
            /*Per Rob Mcgregor */
            if (tempInstr.contains("NO"))
                tmp = tmp+ "<ns3:contains_us_citizen_info>NO</ns3:contains_us_citizen_info>";
            else
                tmp = tmp+"<ns3:contains_us_citizen_info>CONTAINS INFORMATION ON US ENTITY OR CITIZEN.  AUTHORIZED EXEMPTION APPLIED PER DOD REGULATION</ns3:contains_us_citizen_info>";
            tmp = tmp + "</ns3:instructions>";
        }

        return tmp;
    }

    private String parseIIRCollNumber(String serial) {
        String collNumber = "";
        String iirSerial = serial.replace(" ,IIR,.,-", ",,,");

        if (iirSerial.length() == 10)
            collNumber = iirSerial.substring(0, 4);

        return collNumber;

    }

    private String append_collector(FieldSecurity collField, String serial) {

        String tmp = "";

        if (collField.getObjectValue() != null) {
            String coll = collField.getObjectValue();
            tmp = "<ns3:collector>";

            //Bug fix, some records use ',' instead of ';' as a delimeter

            coll = coll.replaceAll(",", ";");
            coll = coll.replaceAll(":", ";");
            coll = coll.replaceAll("NONE", "");
            coll = coll.replaceAll("COLL;", "");
            coll = coll.replaceAll("'", "");


			   /* get Collector Number */
            String collN = parseIIRCollNumber(serial);

            if (collN.length() == 4)
                tmp = tmp + "<ns1:collector_number>" + collN + "</ns1:collector_number>";

            tmp = tmp + "</ns3:collector>";
        }

        return tmp;

    }


    private String append_requirement_nbr(FieldSecurity relatedRequirements) {


        String tmp = "";
        if (relatedRequirements != null && relatedRequirements.getObjectValue() != null) {
            String req = relatedRequirements.getObjectValue();

            req = req.replace("\\.", "");

            req = req.replaceAll(",", ";");

            if (!"".equals(req)) {

                String[] reqArr = req.split(";");

                tmp = tmp + "<ns3:requirements>";
                for (String reqString : reqArr) {
                    tmp = tmp + "<ns3:requirement_nbr>" +
                            reqString.replace(".,;,-, ", ",,,") + "</ns3:requirement_nbr>";
                }
                tmp = tmp + "</ns3:requirements>";
            }
        }

        return tmp;

    }

    private String append_requirement_nbr(String relatedRequirements) {


        String tmp = "";
        if (relatedRequirements != null) {
            String req = relatedRequirements;

            req = req.replace("\\.", "");

            req = req.replaceAll(",", ";");

            if (!"".equals(req)) {

                String[] reqArr = req.split(";");


                for (String reqString : reqArr) {
                    tmp = tmp + "<ns3:requirement_nbr>" +
                            reqString.replace(".,;,-, ", ",,,") + "</ns3:requirement_nbr>";
                }
            }
        }

        return tmp;

    }

    private String append_source_nbr_sdr(String sourceNum) {


        String tmp = "";
        String trimmedSourceNum = "";
        if (sourceNum != null) {
            trimmedSourceNum = sourceNum;
            if (sourceNum.length() > 50) {
                trimmedSourceNum = sourceNum.substring(0, 50).replaceAll(" ", "");
            }
        }
        tmp = tmp + "<ns3:source_nbr>";
        tmp = tmp + trimmedSourceNum;
        tmp = tmp + "</ns3:source_nbr>";

        return tmp;


    }

    private String append_source_nbr(List<HumintSource> sources) {

        String tmp = "";


        if (sources != null && sources.size() > 0) {
            tmp = "<ns3:source_nbr>";
            boolean addSemicolon = false;
            for (HumintSource source : sources) {
                if (!addSemicolon) {
                    tmp = tmp + source.getSourcenumber();
                    addSemicolon = true;
                } else {
                    tmp = tmp + ";" + source.getSourcenumber();
                }
            }

            tmp = tmp + "</ns3:source_nbr>";
        }

        return tmp;
    }


    private String append_iir_status(Date dateUpdated) {
        String dateString = new SimpleDateFormat("yyyy-mm-dd").format(dateUpdated);

        String tmp = "<ns3:iir_status>";
        tmp = tmp + "<ns1:current_status>PUBLISHED</ns1:current_status>";
        tmp = tmp + "<ns1:status_date>" + dateString + "Z</ns1:status_date>";
        tmp = tmp + "</ns3:iir_status>";

        return tmp;

    }


    private String append_iir_comment(List<IirComment> iircomments) {
        String tmp ="";
        if(iircomments!=null && iircomments.size()>0) {
            tmp = "<ns3:iir_comment > ";
            for (IirComment comment : iircomments) {
                tmp = tmp + "<ns1:comment_paragraph";
                tmp = tmp + appendAttrSecurity(comment.getSecurity());
                tmp = tmp + ">";
                tmp = tmp + "<ns1:comment_text> ";
                tmp = tmp + comment.getCommentsdata();
                tmp = tmp + "</ns1:comment_text> ";
                tmp = tmp + "</ns1:comment_paragraph>";
            }
            tmp = tmp + "</ns3:iir_comment > ";
        }

        return tmp;

    }

    private String append_Summary_IIR(FieldSecurity summary) {
        String tmp = "<ns1:summary";
        tmp = tmp + appendAttrSecurity(summary.getSecurity());
        tmp = tmp + " >";
        tmp = tmp + summary.getObjectValue();
        tmp = tmp + "</ns1:summary>";

        return tmp;
    }


    private String append_Paragraph(String value, Security security) {
        String ptmp = "<ns1:paragraph";
        ptmp = ptmp + appendAttrSecurity(security);
        ptmp = ptmp + " >";
        ptmp = ptmp + "<ns1:text_value>" + value + "</ns1:text_value>";
        ptmp = ptmp + "<ns1:text_id>1</ns1:text_id>";
        ptmp = ptmp + "</ns1:paragraph>";
        return ptmp;
    }

    // CIDNE-6741 breakout text message
    private String append_iir_Paragraph(String textorder, String textdata, Security security) {
        String ptmp = "<ns1:paragraph";
        ptmp = ptmp + appendAttrSecurity(security);
        ptmp = ptmp + " >";
        ptmp = ptmp + "<ns1:text_value>" + textdata + "</ns1:text_value>";
        ptmp = ptmp + "<ns1:text_id>" + textorder + "</ns1:text_id>";
        ptmp = ptmp + "</ns1:paragraph>";

        return ptmp;
    }

    private String append_Header(String reportType, boolean isUpdate, Security security, String reportKey) {
        String tmp = "";

        switch (reportType) {
            case "IIR":
                tmp = tmp + "<ns3:iir";
                tmp = tmp + appendAttr("urn:us:gov:ic:ddf:humint:iir", "xmlns:ns3");
                break;
            case "SDR":
            case "AHR":
            case "HCR":
            case "TSCR":
                tmp = tmp + "<ns3:icr";
                tmp = tmp + appendAttr("urn:us:gov:ic:ddf:humint:icr", "xmlns:ns3");
                break;
            case "EVAL":
                tmp = tmp + "<ns3:eval";
                tmp = tmp + appendAttr("urn:us:gov:ic:ddf:humint:eval", "xmlns:ns3");
                break;
        }

        tmp = tmp + appendAttr("urn:us:gov:ic:irm", "xmlns:irm");
        tmp = tmp + appendAttr("urn:us:gov:ic:ism", "xmlns:ism");
        tmp = tmp + appendAttr("urn:us:gov:ic:ntk", "xmlns:ntk");
        tmp = tmp + appendAttr("urn:us:gov:ic:ddf:humint:base", "xmlns:ns1");

        tmp = tmp + appendAttrSecurity(security);
        Date currentDate = Calendar.getInstance().getTime();

        tmp = tmp + appendAttr(new SimpleDateFormat(SIMPLE_DATE_FORMAT).format(currentDate),
                "ism:createDate");
        tmp = tmp + appendAttr("true", "ism:resourceElement");

        tmp = tmp + appendAttr("4", "ddfHumintVersion");
        tmp = tmp + appendAttr("8", "irm:DESVersion");
        tmp = tmp + appendAttr("MIN_ACCESSIBLE", "irm:compliesWith");
        tmp = tmp + appendAttr("9", "ism:DESVersion");
        tmp = tmp + appendAttr("7", "ntk:DESVersion");

        tmp = tmp + ">";

        if (isUpdate) {
            tmp = tmp + "<ns1:primary_key>"+reportKey+"</ns1:primary_key>";
        }

        return tmp;

    }

    private String append_Footer(String report, String reportSerial, boolean isUpdate) {

        String tmp = "";

        switch (report) {
            case "IIR":
                tmp = tmp + "</ns3:iir>";
                break;
            case "SDR":
            case "AHR":
            case "HCR":
            case "TSCR":
                tmp = tmp + "</ns3:icr>";
                break;
            case "EVAL":
                tmp = tmp + "</ns3:eval>";
                break;
        }

        return tmp;
    }

}
