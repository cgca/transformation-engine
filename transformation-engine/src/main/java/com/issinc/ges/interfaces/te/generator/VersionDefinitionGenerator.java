/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.generator;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.interfaces.te.TransformationEngine;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.models.SchemaVersion;
import com.issinc.ges.interfaces.te.utilities.IdMapper;
import com.issinc.ges.interfaces.te.utilities.JsonConstants;

/**
 * Generates version definitions from the schemas
 */
public final class VersionDefinitionGenerator {

    private static final Logger logger = LoggerFactory.getLogger(VersionDefinitionGenerator.class);

    private static final String DATA_MODEL =
            "http://interfaces.dae.smil.mil/schemas/transformationengine/versionDefinition";
    private static final String DESCRIPTION_INSTANCE = "CIDNE %s %s %s Report";
    private static final String DISPLAY_NAME_INSTANCE = DESCRIPTION_INSTANCE;
    private static final String DESCRIPTION_SCHEMA = DESCRIPTION_INSTANCE + " Data Model";
    private static final String DISPLAY_NAME_SCHEMA = "CIDNE %s %s %s Report Data Model";

    private VersionDefinitionGenerator() {
    }

    /**
     * Entry point for building the version definitions.  The idea is to
     * take the report type schemas, pull and parse the id, then create
     * JSON to upload to the Mongodb.
     * @param dataModel The JSON schema for which to generate a VersionDefinition
     * @param token The security token string
     * @return List of JSON Nodes to upload.
     *   Exception
     */
    public static List<JsonNode> buildVersionDefinitions(JsonNode dataModel, String token) {

        final List<JsonNode> nodeList = new ArrayList<>();

        if (dataModel.get(JsonConstants.ID) != null) {
            final String id = dataModel.get(JsonConstants.ID).textValue();
            try {
                // found the id, parse out the module and type
                final IdMapper idMap = new IdMapper(id);
                if (idMap.getTokenCount() == 7) {
                    nodeList.addAll(buildVersionDefinitionNodes(idMap, token));
                }
            } catch (TransformationException e) {
                logger.warn("Error creating query parameters for " + id);
            }
        }

        return nodeList;
    }

    /**
     * Builds versionDefinitions for this schema. Creates a versionDefinition for both instance and schema for both the
     * legacy and native versions of this schema version. Note that the native version definition may already exist
     * (e.g. if we have a CIDNEDM235 and a CIDNEDM236 version of the CIDNE Simple schema, we do not want
     * to create a second 4.1 version definition when processing the 2nd schema)
     *
     * @return
     *   Nodes (schema and instance) for the given schema
     */
    private static List<JsonNode> buildVersionDefinitionNodes(IdMapper idMap, String token)
    throws TransformationException {
        final String module = idMap.getModule();
        final String reportType = idMap.getReportType();
        final String schemaVersion = idMap.getVersion();

        final List<JsonNode> nodeList = new ArrayList<>();
        final SchemaVersion sv =
                TransformationEngine.getInstance().getSchemaVersionBySchemaVersion(schemaVersion, token);

        if (sv != null) {
            final String legacyVersion = sv.getLegacyVersion();
            final String nativeVersion = sv.getNativeVersion();
            // Create instance and schema version definitions for the legacy report
            nodeList.add(buildVersionDefinitionNode(legacyVersion, module, reportType,
                    TransformationEngine.OBJECT_ID_INSTANCE, DESCRIPTION_INSTANCE, DISPLAY_NAME_INSTANCE));
            nodeList.add(buildVersionDefinitionNode(legacyVersion, module, reportType,
                    TransformationEngine.OBJECT_ID_SCHEMA, DESCRIPTION_SCHEMA, DISPLAY_NAME_SCHEMA));

            nodeList.add(buildVersionDefinitionNode(nativeVersion, module, reportType,
                    TransformationEngine.OBJECT_ID_INSTANCE, DESCRIPTION_INSTANCE, DISPLAY_NAME_INSTANCE));
            nodeList.add(buildVersionDefinitionNode(nativeVersion, module, reportType,
                    TransformationEngine.OBJECT_ID_SCHEMA, DESCRIPTION_SCHEMA, DISPLAY_NAME_SCHEMA));
        }

        return nodeList;
    }

    /**
     * Builds a single node based on the incoming values
     *
     * @param version
     *   CIDNE version
     * @param module
     *   Module of the report
     * @param reportType
     *   Type of the report
     * @param objectId
     *   Unformatted ObjectId string
     * @param description
     *   Unformatted description string
     * @param displayName
     *   Unformatted display name string
     * @return
     *   Single node
     */
    private static JsonNode buildVersionDefinitionNode(String version, String module, String reportType,
            String objectId, String description, String displayName) {
        final ObjectNode node = JsonNodeFactory.instance.objectNode();

        node.put("datamodel", DATA_MODEL);
        node.put("objectId", String.format(objectId, version, module, reportType).toLowerCase());
        node.put("description", String.format(description, version, module, reportType));
        node.put("displayName", String.format(displayName, version, module, reportType));

        return node;
    }

} /* End of Class VersionDefinitionGenerator */
