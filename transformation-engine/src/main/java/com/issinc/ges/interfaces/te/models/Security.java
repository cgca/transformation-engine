/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "atomicEnergyMarkings",
        "classificationReason",
        "classification",
        "classifiedBy",
        "compilationReason",
        "declassDate",
        "declassEvent",
        "declassException",
        "derivativelyClassifiedBy",
        "derivedFrom",
        "displayOnlyTo",
        "disseminationControls",
        "FGIsourceOpen",
        "FGIsourceProtected",
        "nonICmarkings",
        "nonUSControls",
        "ownerProducer",
        "releasableToFilterField",
        "releasableTo",
        "SARIdentifier",
        "SCIcontrols"
})
public class Security {

    @JsonProperty("atomicEnergyMarkings")
    private String atomicEnergyMarkings;
    @JsonProperty("classificationReason")
    private String classificationReason;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("classification")
    private String classification;
    @JsonProperty("classifiedBy")
    private String classifiedBy;
    @JsonProperty("compilationReason")
    private String compilationReason;
    /**
     *
     */
    @JsonProperty("declassDate")
    private ActionDate declassDate;
    @JsonProperty("declassEvent")
    private String declassEvent;
    @JsonProperty("declassException")
    private String declassException;
    @JsonProperty("derivativelyClassifiedBy")
    private String derivativelyClassifiedBy;
    @JsonProperty("derivedFrom")
    private String derivedFrom;
    @JsonProperty("displayOnlyTo")
    private String displayOnlyTo;
    @JsonProperty("disseminationControls")
    private String disseminationControls;
    @JsonProperty("FGIsourceOpen")
    private String FGIsourceOpen;
    @JsonProperty("FGIsourceProtected")
    private String FGIsourceProtected;
    @JsonProperty("nonICmarkings")
    private String nonICmarkings;
    @JsonProperty("nonUSControls")
    private String nonUSControls;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("ownerProducer")
    private String ownerProducer;
    @JsonProperty("releasableToFilterField")
    private String releasableToFilterField;
    @JsonProperty("releasableTo")
    private String releasableTo;
    @JsonProperty("SARIdentifier")
    private String SARIdentifier;
    @JsonProperty("SCIcontrols")
    private String SCIcontrols;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Security() {
    }

    /**
     *
     * @param nonICmarkings
     * @param classificationReason
     * @param ownerProducer
     * @param derivedFrom
     * @param SARIdentifier
     * @param classifiedBy
     * @param disseminationControls
     * @param SCIcontrols
     * @param nonUSControls
     * @param FGIsourceOpen
     * @param declassException
     * @param declassDate
     * @param derivativelyClassifiedBy
     * @param classification
     * @param FGIsourceProtected
     * @param compilationReason
     * @param atomicEnergyMarkings
     * @param declassEvent
     * @param releasableTo
     * @param displayOnlyTo
     * @param releasableToFilterField
     */
    public Security(String atomicEnergyMarkings, String classificationReason, String classification, String classifiedBy, String compilationReason, ActionDate declassDate, String declassEvent, String declassException, String derivativelyClassifiedBy, String derivedFrom, String displayOnlyTo, String disseminationControls, String FGIsourceOpen, String FGIsourceProtected, String nonICmarkings, String nonUSControls, String ownerProducer, String releasableToFilterField, String releasableTo, String SARIdentifier, String SCIcontrols) {
        this.atomicEnergyMarkings = atomicEnergyMarkings;
        this.classificationReason = classificationReason;
        this.classification = classification;
        this.classifiedBy = classifiedBy;
        this.compilationReason = compilationReason;
        this.declassDate = declassDate;
        this.declassEvent = declassEvent;
        this.declassException = declassException;
        this.derivativelyClassifiedBy = derivativelyClassifiedBy;
        this.derivedFrom = derivedFrom;
        this.displayOnlyTo = displayOnlyTo;
        this.disseminationControls = disseminationControls;
        this.FGIsourceOpen = FGIsourceOpen;
        this.FGIsourceProtected = FGIsourceProtected;
        this.nonICmarkings = nonICmarkings;
        this.nonUSControls = nonUSControls;
        this.ownerProducer = ownerProducer;
        this.releasableToFilterField = releasableToFilterField;
        this.releasableTo = releasableTo;
        this.SARIdentifier = SARIdentifier;
        this.SCIcontrols = SCIcontrols;
    }

    /**
     *
     * @return
     * The atomicEnergyMarkings
     */
    @JsonProperty("atomicEnergyMarkings")
    public String getAtomicEnergyMarkings() {
        return atomicEnergyMarkings;
    }

    /**
     *
     * @param atomicEnergyMarkings
     * The atomicEnergyMarkings
     */
    @JsonProperty("atomicEnergyMarkings")
    public void setAtomicEnergyMarkings(String atomicEnergyMarkings) {
        this.atomicEnergyMarkings = atomicEnergyMarkings;
    }

    /**
     *
     * @return
     * The classificationReason
     */
    @JsonProperty("classificationReason")
    public String getClassificationReason() {
        return classificationReason;
    }

    /**
     *
     * @param classificationReason
     * The classificationReason
     */
    @JsonProperty("classificationReason")
    public void setClassificationReason(String classificationReason) {
        this.classificationReason = classificationReason;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The classification
     */
    @JsonProperty("classification")
    public String getClassification() {
        return classification;
    }

    /**
     *
     * (Required)
     *
     * @param classification
     * The classification
     */
    @JsonProperty("classification")
    public void setClassification(String classification) {
        this.classification = classification;
    }

    /**
     *
     * @return
     * The classifiedBy
     */
    @JsonProperty("classifiedBy")
    public String getClassifiedBy() {
        return classifiedBy;
    }

    /**
     *
     * @param classifiedBy
     * The classifiedBy
     */
    @JsonProperty("classifiedBy")
    public void setClassifiedBy(String classifiedBy) {
        this.classifiedBy = classifiedBy;
    }

    /**
     *
     * @return
     * The compilationReason
     */
    @JsonProperty("compilationReason")
    public String getCompilationReason() {
        return compilationReason;
    }

    /**
     *
     * @param compilationReason
     * The compilationReason
     */
    @JsonProperty("compilationReason")
    public void setCompilationReason(String compilationReason) {
        this.compilationReason = compilationReason;
    }

    /**
     *
     * @return
     * The declassDate
     */
    @JsonProperty("declassDate")
    public ActionDate getDeclassDate() {
        return declassDate;
    }

    /**
     *
     * @param declassDate
     * The declassDate
     */
    @JsonProperty("declassDate")
    public void setDeclassDate(ActionDate declassDate) {
        this.declassDate = declassDate;
    }

    /**
     *
     * @return
     * The declassEvent
     */
    @JsonProperty("declassEvent")
    public String getDeclassEvent() {
        return declassEvent;
    }

    /**
     *
     * @param declassEvent
     * The declassEvent
     */
    @JsonProperty("declassEvent")
    public void setDeclassEvent(String declassEvent) {
        this.declassEvent = declassEvent;
    }

    /**
     *
     * @return
     * The declassException
     */
    @JsonProperty("declassException")
    public String getDeclassException() {
        return declassException;
    }

    /**
     *
     * @param declassException
     * The declassException
     */
    @JsonProperty("declassException")
    public void setDeclassException(String declassException) {
        this.declassException = declassException;
    }

    /**
     *
     * @return
     * The derivativelyClassifiedBy
     */
    @JsonProperty("derivativelyClassifiedBy")
    public String getDerivativelyClassifiedBy() {
        return derivativelyClassifiedBy;
    }

    /**
     *
     * @param derivativelyClassifiedBy
     * The derivativelyClassifiedBy
     */
    @JsonProperty("derivativelyClassifiedBy")
    public void setDerivativelyClassifiedBy(String derivativelyClassifiedBy) {
        this.derivativelyClassifiedBy = derivativelyClassifiedBy;
    }

    /**
     *
     * @return
     * The derivedFrom
     */
    @JsonProperty("derivedFrom")
    public String getDerivedFrom() {
        return derivedFrom;
    }

    /**
     *
     * @param derivedFrom
     * The derivedFrom
     */
    @JsonProperty("derivedFrom")
    public void setDerivedFrom(String derivedFrom) {
        this.derivedFrom = derivedFrom;
    }

    /**
     *
     * @return
     * The displayOnlyTo
     */
    @JsonProperty("displayOnlyTo")
    public String getDisplayOnlyTo() {
        return displayOnlyTo;
    }

    /**
     *
     * @param displayOnlyTo
     * The displayOnlyTo
     */
    @JsonProperty("displayOnlyTo")
    public void setDisplayOnlyTo(String displayOnlyTo) {
        this.displayOnlyTo = displayOnlyTo;
    }

    /**
     *
     * @return
     * The disseminationControls
     */
    @JsonProperty("disseminationControls")
    public String getDisseminationControls() {
        return disseminationControls;
    }

    /**
     *
     * @param disseminationControls
     * The disseminationControls
     */
    @JsonProperty("disseminationControls")
    public void setDisseminationControls(String disseminationControls) {
        this.disseminationControls = disseminationControls;
    }

    /**
     *
     * @return
     * The FGIsourceOpen
     */
    @JsonProperty("FGIsourceOpen")
    public String getFGIsourceOpen() {
        return FGIsourceOpen;
    }

    /**
     *
     * @param FGIsourceOpen
     * The FGIsourceOpen
     */
    @JsonProperty("FGIsourceOpen")
    public void setFGIsourceOpen(String FGIsourceOpen) {
        this.FGIsourceOpen = FGIsourceOpen;
    }

    /**
     *
     * @return
     * The FGIsourceProtected
     */
    @JsonProperty("FGIsourceProtected")
    public String getFGIsourceProtected() {
        return FGIsourceProtected;
    }

    /**
     *
     * @param FGIsourceProtected
     * The FGIsourceProtected
     */
    @JsonProperty("FGIsourceProtected")
    public void setFGIsourceProtected(String FGIsourceProtected) {
        this.FGIsourceProtected = FGIsourceProtected;
    }

    /**
     *
     * @return
     * The nonICmarkings
     */
    @JsonProperty("nonICmarkings")
    public String getNonICmarkings() {
        return nonICmarkings;
    }

    /**
     *
     * @param nonICmarkings
     * The nonICmarkings
     */
    @JsonProperty("nonICmarkings")
    public void setNonICmarkings(String nonICmarkings) {
        this.nonICmarkings = nonICmarkings;
    }

    /**
     *
     * @return
     * The nonUSControls
     */
    @JsonProperty("nonUSControls")
    public String getNonUSControls() {
        return nonUSControls;
    }

    /**
     *
     * @param nonUSControls
     * The nonUSControls
     */
    @JsonProperty("nonUSControls")
    public void setNonUSControls(String nonUSControls) {
        this.nonUSControls = nonUSControls;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The ownerProducer
     */
    @JsonProperty("ownerProducer")
    public String getOwnerProducer() {
        return ownerProducer;
    }

    /**
     *
     * (Required)
     *
     * @param ownerProducer
     * The ownerProducer
     */
    @JsonProperty("ownerProducer")
    public void setOwnerProducer(String ownerProducer) {
        this.ownerProducer = ownerProducer;
    }

    /**
     *
     * @return
     * The releasableToFilterField
     */
    @JsonProperty("releasableToFilterField")
    public String getReleasableToFilterField() {
        return releasableToFilterField;
    }

    /**
     *
     * @param releasableToFilterField
     * The releasableToFilterField
     */
    @JsonProperty("releasableToFilterField")
    public void setReleasableToFilterField(String releasableToFilterField) {
        this.releasableToFilterField = releasableToFilterField;
    }

    /**
     *
     * @return
     * The releasableTo
     */
    @JsonProperty("releasableTo")
    public String getReleasableTo() {
        return releasableTo;
    }

    /**
     *
     * @param releasableTo
     * The releasableTo
     */
    @JsonProperty("releasableTo")
    public void setReleasableTo(String releasableTo) {
        this.releasableTo = releasableTo;
    }

    /**
     *
     * @return
     * The SARIdentifier
     */
    @JsonProperty("SARIdentifier")
    public String getSARIdentifier() {
        return SARIdentifier;
    }

    /**
     *
     * @param SARIdentifier
     * The SARIdentifier
     */
    @JsonProperty("SARIdentifier")
    public void setSARIdentifier(String SARIdentifier) {
        this.SARIdentifier = SARIdentifier;
    }

    /**
     *
     * @return
     * The SCIcontrols
     */
    @JsonProperty("SCIcontrols")
    public String getSCIcontrols() {
        return SCIcontrols;
    }

    /**
     *
     * @param SCIcontrols
     * The SCIcontrols
     */
    @JsonProperty("SCIcontrols")
    public void setSCIcontrols(String SCIcontrols) {
        this.SCIcontrols = SCIcontrols;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}