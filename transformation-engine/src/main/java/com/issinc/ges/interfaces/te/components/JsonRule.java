/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.components;

import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.interfaces.te.TransformationException;

/**
 * This class is for rules that act on JSON nodes (as opposed to strings). It is provided so that we can avoid
 * continually parsing a JSON string into a JSON tree and vice versa between rules in a transformation ruleset.
 */
public abstract class JsonRule extends Rule {

    /**
     * This is super.applyRule which simply checks the class of the payload for compatibility with the type of rule
     * @param token Security token
     * @param payload The data to be transformed by the rule
     * @return the transformed payload
     * @throws TransformationException if the payload is not a JsonNode
     */
    public final Object applyRule(String token, Object payload) throws TransformationException {
        validatePayload(payload, JsonNode.class, "JsonRule.applyRule");
        return applyRule(token, (JsonNode) payload);
    }

    /**
     * This is where the real content is. Every Rule must implement its own applyRule method to take a particular action
     * on the payload.
     * @param token Security token
     * @param payload The data to be transformed by the rule
     * @return The transformed payload
     * @throws TransformationException if an error occurs
     */
    public abstract JsonNode applyRule(String token, JsonNode payload) throws TransformationException;

} /* End of Class JsonRule */
