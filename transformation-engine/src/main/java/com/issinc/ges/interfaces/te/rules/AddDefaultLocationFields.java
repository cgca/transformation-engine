package com.issinc.ges.interfaces.te.rules;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.JsonRule;
import com.issinc.ges.interfaces.te.models.Location;

import java.util.UUID;

/**
 * This rule will add default values to required location fields
 */
public class AddDefaultLocationFields extends JsonRule {

    @Override
    public JsonNode applyRule(String token, JsonNode payload) throws TransformationException {


        JsonNode locoationNode = payload.get("location");
        if(locoationNode instanceof ArrayNode){
            for(int i=0; i<locoationNode.size();i++){
                JsonNode node = locoationNode.get(i);
                Location location = JsonUtils.toObject(node,Location.class);
                //set defaults if not already set
                if(location.getIsPrimaryLocation()==null) {
                    location.setIsPrimaryLocation(false);
                }
                if(location.getObjectId()==null){
                    location.setObjectId(UUID.randomUUID().toString());
                }
                node = JsonUtils.objectToJson(location);
                ((ArrayNode) locoationNode).set(i,node);
            }
        }
        return payload;
    }
}
