/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * CIDNE Media specific details
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "relativeUri",
        "mediaUrl",
        "mediaName",
        "mediaFileName",
        "datePosted",
        "datePostedOffset",
        "dateOfMedia",
        "dateofMediaOffset",
        "embeddedReportId",
        "itemNumber",
        "mediaCategory",
        "mediaDescription",
        "mediaFileSize",
        "mediaOriginalSize",
        "mediaType",
        "originalFileName",
        "embeddedReferenceId"
})
public class MediaReportDetails {

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("relativeUri")
    private String relativeUri;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("mediaUrl")
    private String mediaUrl;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("mediaName")
    private String mediaName;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("mediaFileName")
    private String mediaFileName;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("datePosted")
    private ActionDate datePosted;
    /**
     * Offset in minutes for date posted
     *
     */
    @JsonProperty("datePostedOffset")
    private int datePostedOffset;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("dateOfMedia")
    private ActionDate dateOfMedia;
    /**
     * Offset in minutes for date of media
     *
     */
    @JsonProperty("dateofMediaOffset")
    private int dateofMediaOffset;
    @JsonProperty("embeddedReportId")
    private String embeddedReportId;
    @JsonProperty("itemNumber")
    private String itemNumber;
    @JsonProperty("mediaCategory")
    private String mediaCategory;
    @JsonProperty("mediaDescription")
    private String mediaDescription;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("mediaFileSize")
    private int mediaFileSize;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("mediaOriginalSize")
    private int mediaOriginalSize;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("mediaType")
    private String mediaType;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("originalFileName")
    private String originalFileName;
    @JsonProperty("embeddedReferenceId")
    private String embeddedReferenceId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * (Required)
     *
     * @return
     * The relativeUri
     */
    @JsonProperty("relativeUri")
    public String getRelativeUri() {
        return relativeUri;
    }

    /**
     *
     * (Required)
     *
     * @param relativeUri
     * The relativeUri
     */
    @JsonProperty("relativeUri")
    public void setRelativeUri(String relativeUri) {
        this.relativeUri = relativeUri;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The mediaUrl
     */
    @JsonProperty("mediaUrl")
    public String getMediaUrl() {
        return mediaUrl;
    }

    /**
     *
     * (Required)
     *
     * @param mediaUrl
     * The mediaUrl
     */
    @JsonProperty("mediaUrl")
    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The mediaName
     */
    @JsonProperty("mediaName")
    public String getMediaName() {
        return mediaName;
    }

    /**
     *
     * (Required)
     *
     * @param mediaName
     * The mediaName
     */
    @JsonProperty("mediaName")
    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The mediaFileName
     */
    @JsonProperty("mediaFileName")
    public String getMediaFileName() {
        return mediaFileName;
    }

    /**
     *
     * (Required)
     *
     * @param mediaFileName
     * The mediaFileName
     */
    @JsonProperty("mediaFileName")
    public void setMediaFileName(String mediaFileName) {
        this.mediaFileName = mediaFileName;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The datePosted
     */
    @JsonProperty("datePosted")
    public ActionDate getDatePosted() {
        return datePosted;
    }

    /**
     *
     * (Required)
     *
     * @param datePosted
     * The datePosted
     */
    @JsonProperty("datePosted")
    public void setDatePosted(ActionDate datePosted) {
        this.datePosted = datePosted;
    }

    /**
     * Offset in minutes for date posted
     *
     * @return
     * The datePostedOffset
     */
    @JsonProperty("datePostedOffset")
    public int getDatePostedOffset() {
        return datePostedOffset;
    }

    /**
     * Offset in minutes for date posted
     *
     * @param datePostedOffset
     * The datePostedOffset
     */
    @JsonProperty("datePostedOffset")
    public void setDatePostedOffset(int datePostedOffset) {
        this.datePostedOffset = datePostedOffset;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The dateOfMedia
     */
    @JsonProperty("dateOfMedia")
    public ActionDate getDateOfMedia() {
        return dateOfMedia;
    }

    /**
     *
     * (Required)
     *
     * @param dateOfMedia
     * The dateOfMedia
     */
    @JsonProperty("dateOfMedia")
    public void setDateOfMedia(ActionDate dateOfMedia) {
        this.dateOfMedia = dateOfMedia;
    }

    /**
     * Offset in minutes for date of media
     *
     * @return
     * The dateofMediaOffset
     */
    @JsonProperty("dateofMediaOffset")
    public int getDateofMediaOffset() {
        return dateofMediaOffset;
    }

    /**
     * Offset in minutes for date of media
     *
     * @param dateofMediaOffset
     * The dateofMediaOffset
     */
    @JsonProperty("dateofMediaOffset")
    public void setDateofMediaOffset(int dateofMediaOffset) {
        this.dateofMediaOffset = dateofMediaOffset;
    }

    /**
     *
     * @return
     * The embeddedReportId
     */
    @JsonProperty("embeddedReportId")
    public String getEmbeddedReportId() {
        return embeddedReportId;
    }

    /**
     *
     * @param embeddedReportId
     * The embeddedReportId
     */
    @JsonProperty("embeddedReportId")
    public void setEmbeddedReportId(String embeddedReportId) {
        this.embeddedReportId = embeddedReportId;
    }

    /**
     *
     * @return
     * The itemNumber
     */
    @JsonProperty("itemNumber")
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     *
     * @param itemNumber
     * The itemNumber
     */
    @JsonProperty("itemNumber")
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     *
     * @return
     * The mediaCategory
     */
    @JsonProperty("mediaCategory")
    public String getMediaCategory() {
        return mediaCategory;
    }

    /**
     *
     * @param mediaCategory
     * The mediaCategory
     */
    @JsonProperty("mediaCategory")
    public void setMediaCategory(String mediaCategory) {
        this.mediaCategory = mediaCategory;
    }

    /**
     *
     * @return
     * The mediaDescription
     */
    @JsonProperty("mediaDescription")
    public String getMediaDescription() {
        return mediaDescription;
    }

    /**
     *
     * @param mediaDescription
     * The mediaDescription
     */
    @JsonProperty("mediaDescription")
    public void setMediaDescription(String mediaDescription) {
        this.mediaDescription = mediaDescription;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The mediaFileSize
     */
    @JsonProperty("mediaFileSize")
    public int getMediaFileSize() {
        return mediaFileSize;
    }

    /**
     *
     * (Required)
     *
     * @param mediaFileSize
     * The mediaFileSize
     */
    @JsonProperty("mediaFileSize")
    public void setMediaFileSize(int mediaFileSize) {
        this.mediaFileSize = mediaFileSize;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The mediaOriginalSize
     */
    @JsonProperty("mediaOriginalSize")
    public int getMediaOriginalSize() {
        return mediaOriginalSize;
    }

    /**
     *
     * (Required)
     *
     * @param mediaOriginalSize
     * The mediaOriginalSize
     */
    @JsonProperty("mediaOriginalSize")
    public void setMediaOriginalSize(int mediaOriginalSize) {
        this.mediaOriginalSize = mediaOriginalSize;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The mediaType
     */
    @JsonProperty("mediaType")
    public String getMediaType() {
        return mediaType;
    }

    /**
     *
     * (Required)
     *
     * @param mediaType
     * The mediaType
     */
    @JsonProperty("mediaType")
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The originalFileName
     */
    @JsonProperty("originalFileName")
    public String getOriginalFileName() {
        return originalFileName;
    }

    /**
     *
     * (Required)
     *
     * @param originalFileName
     * The originalFileName
     */
    @JsonProperty("originalFileName")
    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    /**
     *
     * @return
     * The embeddedReferenceId
     */
    @JsonProperty("embeddedReferenceId")
    public String getEmbeddedReferenceId() {
        return embeddedReferenceId;
    }

    /**
     *
     * @param embeddedReferenceId
     * The embeddedReferenceId
     */
    @JsonProperty("embeddedReferenceId")
    public void setEmbeddedReferenceId(String embeddedReferenceId) {
        this.embeddedReferenceId = embeddedReferenceId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
