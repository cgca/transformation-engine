/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.rules;

import java.io.IOException;
import java.io.StringReader;

import org.apache.xerces.parsers.DOMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.interfaces.te.utilities.JsonEditor;

/**
 * Converts an incoming xml string to json nodes; could not use
 * Jackson as the conversion process didn't recognize arrays (ie location);
 * therefore, below contains the "manual" conversion process.
 */
public class XmlStringToJsonNode extends Rule {

    private static final Logger logger = LoggerFactory.getLogger(XmlStringToJsonNode.class);

    /**
     * The default constructor
     */
    public XmlStringToJsonNode() {
        super();
    }

    @Override
    public final Object applyRule(String token, Object payload) throws TransformationException {
        final String methodName = "XmlStringToJsonNode.applyRule";
        logger.debug(methodName);

        // Generate the root node for the json node tree
        final JsonEditor je = new JsonEditor(new ObjectMapper().createObjectNode());

        // Verify the payload is indeed a string
        validatePayload(payload, String.class, methodName);

        try {
            buildJsonNodeTreeFromXml(getXmlNodeList((String) payload), je.getRoot());
        } catch (IOException | SAXException e) {
            throw new TransformationException(
                    TransformationException.ErrorType.PARSING_ERROR,
                    methodName + ": " + payload, e);
        }

        return je.getRoot();
    }

    /**
     * Method creates the parser and parses the incoming xml string into
     * the first set of child nodes which will be the root of the xml
     * tree.
     *
     * @param xmlString
     *  Incoming report xml string
     * @return
     *  Returns the child nodes from the xml root.
     * @throws SAXException
     * @throws IOException
     */
    private NodeList getXmlNodeList(String xmlString) throws SAXException, IOException {
        final DOMParser parser = new DOMParser();
        final InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xmlString));
        parser.parse(is);
        final Document doc = parser.getDocument();
        return doc.getChildNodes().item(0).getChildNodes();
    }

    /**
     * Recursive method that traverses the xml nodes, creating the json node
     * equivalent.
     *
     * @param xmlNodeList
     *  Child node list from the current root.
     * @param jsonNode
     *  Current json node root
     */
    private void buildJsonNodeTreeFromXml(NodeList xmlNodeList, JsonNode jsonNode) {
        // Loop through all the xml nodes, looking for the element nodes
        for (int i = 0; i < xmlNodeList.getLength(); i++) {
            final Node xmlNode = xmlNodeList.item(i);
            // Only dealing with element nodes
            if (xmlNode.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            final Element value = (Element) xmlNode;
            // Having multiple child nodes indicates an array
            if (xmlNode.getChildNodes().getLength() > 1) {

                JsonNode arrayNode = jsonNode.get(value.getTagName());
                // First, see if the array exists; if not, create it.
                if (arrayNode == null || !(arrayNode instanceof ArrayNode)) {
                    arrayNode = ((ObjectNode) jsonNode).putArray(value.getTagName());
                }
                // Call this method again with the child nodes
                buildJsonNodeTreeFromXml(xmlNode.getChildNodes(), ((ArrayNode)arrayNode).addObject());
            } else {
                String content = value.getTextContent();
                if(value instanceof CharacterData){
                    content = ((CharacterData) value).getData();
                }
                // Add the element tag name and value to the json object
                ((ObjectNode) jsonNode).put(value.getTagName(), content);
            }
        }
    }

} /* End of Class JsonStringToXmlString */
