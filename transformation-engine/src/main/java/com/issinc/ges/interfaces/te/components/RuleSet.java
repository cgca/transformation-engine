/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.components;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Class to define a set of rules to translate one report version to another.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleSet {

    private String datamodel;
    private String destinationVersionObjectId;
    private String displayName;
    private String objectId;
    private String sourceVersionObjectId;
    private Rule[] rules;

    /**
     * Default Constructor
     */
    public RuleSet() {
    }

    public final String getDatamodel() {
        return datamodel;
    }

    public final void setDatamodel(String datamodel) {
        this.datamodel = datamodel;
    }

    public final String getDisplayName() {
        return displayName;
    }

    public final void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public final String getDestinationVersionObjectId() {
        return destinationVersionObjectId;
    }

    public final void setDestinationVersionObjectId(String destinationVersionObjectId) {
        this.destinationVersionObjectId = destinationVersionObjectId;
    }

    public final String getObjectId() {
        return objectId;
    }

    public final void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public final String getSourceVersionObjectId() {
        return sourceVersionObjectId;
    }

    public final void setSourceVersionObjectId(String sourceVersionObjectId) {
        this.sourceVersionObjectId = sourceVersionObjectId;
    }

    public final Rule[] getRules() {
        return rules;
    }

    public final void setRules(Rule[] rules) {
        this.rules = rules;
    }

    /// CHECKSTYLE:OFF
    @Override
    public String toString() {
        /// CHECKSTYLE:ON
        final StringBuilder sb = new StringBuilder("RuleSet POJO [" +
                "datamodel = " + datamodel +
                ", sourceVersionObjectId = " + sourceVersionObjectId +
                ", displayName = " + displayName +
                ", destinationVersionObjectId = " + destinationVersionObjectId +
                ", rules = [");
        for (Rule rule : rules) {
            sb.append(rule.toString()).append("\n");
        }
        sb.append("]");
        return sb.toString();
    }

} /* End of Class RuleSet */
