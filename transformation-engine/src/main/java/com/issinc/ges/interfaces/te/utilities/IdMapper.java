/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te.utilities;

/**
 * Parses a data model id into it's parts and provides a getter for each part
 */
public class IdMapper {

    private String type;
    private String module;
    private String reportType;
    private String version;
    private int tokenCount;
    private boolean isReportDataModel = false;

    /**
     * Constructor
     * @param dataModelId The data model ID string to be parsed
     */
    public IdMapper(final String dataModelId) {
        final String[] elements = dataModelId.split("/");
        this.tokenCount = elements.length;
        // Report schemas always have 7 tokens in the "id" attribute
        if (tokenCount == 7) {
            type = "report";
            module = elements[4];
            reportType = elements[5];
            version = elements[6];
            isReportDataModel = true;
        }
    }

    public final String getType() {
        return type;
    }

    public final String getModule() {
        return module;
    }

    public final String getReportType() {
        return reportType;
    }

    public final String getVersion() {
        return version;
    }

    public final  int getTokenCount() {
        return tokenCount;
    }

    public boolean isReportDataModel() {
        return isReportDataModel;
    }

} /* End of Class IdMapper */
