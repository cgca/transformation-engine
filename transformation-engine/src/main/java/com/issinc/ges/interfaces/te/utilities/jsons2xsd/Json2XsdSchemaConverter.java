/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te.utilities.jsons2xsd;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.XMLConstants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Schema Converter class
 */
public final class Json2XsdSchemaConverter {

    private static final String SEQUENCE = "sequence";
    private static final ObjectMapper mapper = new ObjectMapper();
    private static JsonNode definitions;
    private static final Map<String, String> typeMapping = new HashMap<>();

    /**
     * Constructor
     */
    private Json2XsdSchemaConverter() {
    }

    /**
     * The way a particular element should be wrapped
     */
    public enum OuterWrapping { ELEMENT, TYPE }

    static {
        // Primitive types
        typeMapping.put(Constants.STRING, Constants.STRING);
        typeMapping.put(Constants.OBJECT, Constants.OBJECT);
        typeMapping.put(Constants.ARRAY, Constants.ARRAY);
        typeMapping.put("number", Constants.DECIMAL);
        typeMapping.put(Constants.BOOLEAN, Constants.BOOLEAN);
        typeMapping.put("integer", Constants.INT);

        // Non-standard, often encountered in the wild
        typeMapping.put(Constants.INT, Constants.INT);
        typeMapping.put("date-time", Constants.DATETIME);
        typeMapping.put(Constants.TIME, Constants.TIME);
        typeMapping.put(Constants.DATE, Constants.DATE);


        // String formats
        typeMapping.put("string|uri", "anyURI");
        typeMapping.put("string|email", Constants.STRING);
        typeMapping.put("string|phone", Constants.STRING);
        typeMapping.put("string|date-time", Constants.DATETIME);
        typeMapping.put("string|date", Constants.DATE);
        typeMapping.put("string|time", Constants.TIME);
        typeMapping.put("string|utc-millisec", "long");
        typeMapping.put("string|regex", Constants.STRING);
        typeMapping.put("string|color", Constants.STRING);
        typeMapping.put("string|style", Constants.STRING);
    }

    /**
     * Converts a JSON Schema to XSD
     *
     * @param jsonSchema JSON Schema to be converted
     * @param targetNameSpaceUri XML Namespace
     * @param wrapping XML wrapper
     * @param name report object name
     * @return XML return object
     * @throws IOException Custom exception
     */
    public static Document convert(Reader jsonSchema, String targetNameSpaceUri, OuterWrapping wrapping, String name)
            throws IOException {
        final JsonNode rootNode = mapper.readTree(jsonSchema);
        final String type = rootNode.path(Constants.TYPE).textValue();
        Assert.isTrue(Constants.OBJECT.equals(type), "root should have type='" + Constants.OBJECT + "'");

        definitions = rootNode.get(Constants.DEFINITIONS);

        final JsonNode properties = rootNode.get(Constants.PROPERTIES);

        Assert.notNull(properties, "'" + Constants.PROPERTIES + "' property should be found in root of JSON schema'");

        final JsonNode required = rootNode.get(Constants.REQUIRED);

        final Document xsdDoc = XmlUtil.newDocument();
        xsdDoc.setXmlStandalone(true);

        final Element schemaRoot = createXsdElement(xsdDoc, Constants.SCHEMA);
        schemaRoot.setAttribute("targetNamespace", targetNameSpaceUri);
        schemaRoot.setAttribute("xmlns:xs", XMLConstants.W3C_XML_SCHEMA_NS_URI);
        schemaRoot.setAttribute("elementFormDefault", Constants.QUALIFIED);
        schemaRoot.setAttribute("attributeFormDefault", Constants.QUALIFIED);

        Element wrapper = schemaRoot;
        if (wrapping == OuterWrapping.ELEMENT) {

            // <[ModuleReportType]Reports> ...
            Element topElement = createXsdElement(schemaRoot, Constants.ELEMENT);
            topElement.setAttribute(Constants.NAME, name + "Reports");

            Element reportWrapperComplexType = createXsdElement(topElement, Constants.COMPLEX_TYPE);
            Element reportWrapperSequence = createXsdElement(reportWrapperComplexType, SEQUENCE);

            // <WebServiceAttributes> ...
            Element webserviceAttr = createXsdElement(reportWrapperSequence, Constants.ELEMENT);
            webserviceAttr.setAttribute(Constants.NAME, "WebServiceAttributes");
            webserviceAttr.setAttribute(Constants.MIN_OCCURS,"0");
            webserviceAttr.setAttribute(Constants.MAX_OCCURS, "1");

            // Everything from here on out goes under this element:
            // <[ModuleReportType]> ...
            wrapper = createXsdElement(reportWrapperSequence, Constants.ELEMENT);
            wrapper.setAttribute(Constants.NAME, name);
            wrapper.setAttribute(Constants.MIN_OCCURS,"0");
            wrapper.setAttribute(Constants.MAX_OCCURS, Constants.UNBOUNDED);
        }

        final Element schemaComplexType = createXsdElement(wrapper, Constants.COMPLEX_TYPE);

        if (wrapping == OuterWrapping.TYPE) {
            schemaComplexType.setAttribute(Constants.NAME, name);
        }
        final Element schemaSequence = createXsdElement(schemaComplexType, SEQUENCE);

        doIterate(schemaSequence, properties, required);

        return xsdDoc;
    }

    private static void doIterate(Element elem, JsonNode node, JsonNode required) {
        final Iterator<Entry<String, JsonNode>> fieldIter = node.fields();
        while (fieldIter.hasNext()) {
            final Entry<String, JsonNode> entry = fieldIter.next();
            final String key = entry.getKey();
            final JsonNode val = entry.getValue();
            doIterateSingle(key, val, elem, required);
        }
    }

    private static void doIterateSingle(String key, JsonNode val, Element elem, JsonNode required) {
        final String xsdType = determineXsdType(key, val);
        //final boolean required = requiredFields.get(val.path("parent").textValue()).contains(key);
        final Element nodeElem = createXsdElement(elem, Constants.ELEMENT);
        nodeElem.setAttribute(Constants.NAME, key);

        if (!Constants.OBJECT.equals(xsdType) && !Constants.ARRAY.equals(xsdType)
                && !Constants.REFERENCE.equals(xsdType) && !Constants.ONEOF.equals(xsdType)) {
            // Simple type
            nodeElem.setAttribute(Constants.TYPE, xsdType);
        }

        if (isRequired(required, key)) {
            nodeElem.setAttribute(Constants.MIN_OCCURS, Constants.ONE);
        } else {
            nodeElem.setAttribute(Constants.MIN_OCCURS, Constants.ZERO);
        }

        switch (xsdType) {
            case Constants.ARRAY:
                handleArray(nodeElem, val);
                break;

            case Constants.DECIMAL:
            case Constants.INT:
                handleNumber(nodeElem, xsdType, val);
                break;

            case Constants.ENUM:
                handleEnum(nodeElem, val);
                break;

            case Constants.OBJECT:
                handleObject(nodeElem, val);
                break;

            case Constants.STRING:
                handleString(nodeElem, val);
                break;

            default:
        }
    }

    private static void handleString(Element nodeElem, JsonNode val) {
        final Integer minimumLength = getIntVal(val, Constants.MIN_LENGTH);
        final Integer maximumLength = getIntVal(val, Constants.MAX_LENGTH);
        final String expression = val.path(Constants.PATTERN).textValue();

        if (minimumLength != null || maximumLength != null || expression != null) {
            nodeElem.removeAttribute(Constants.TYPE);
            final Element simpleType = createXsdElement(nodeElem, Constants.SIMPLE_TYPE);
            final Element restriction = createXsdElement(simpleType, Constants.RESTRICTION);
            restriction.setAttribute(Constants.BASE, Constants.STRING);

            if (minimumLength != null) {
                final Element min = createXsdElement(restriction, Constants.MIN_LENGTH);
                min.setAttribute(Constants.VALUE, Integer.toString(minimumLength));
            }

            if (maximumLength != null) {
                final Element max = createXsdElement(restriction, Constants.MAX_LENGTH);
                max.setAttribute(Constants.VALUE, Integer.toString(maximumLength));
            }

            if (expression != null) {
                final Element max = createXsdElement(restriction, Constants.PATTERN);
                max.setAttribute(Constants.VALUE, expression);
            }
        }
    }

    private static void handleObject(Element nodeElem, JsonNode val) {
        final Element complexType = createXsdElement(nodeElem, Constants.COMPLEX_TYPE);
        final Element sequence = createXsdElement(complexType, SEQUENCE);
        final JsonNode properties = val.get(Constants.PROPERTIES);
        final JsonNode required = val.get(Constants.REQUIRED);

        if (properties != null) {
            doIterate(sequence, properties, required);
        }
    }

    private static void handleEnum(Element nodeElem, JsonNode val) {
        nodeElem.removeAttribute(Constants.TYPE);
        final Element simpleType = createXsdElement(nodeElem, Constants.SIMPLE_TYPE);
        final Element restriction = createXsdElement(simpleType, Constants.RESTRICTION);
        restriction.setAttribute(Constants.BASE, Constants.STRING);
        final JsonNode enumNode = val.get(Constants.ENUM);
        for (int i = 0; i < enumNode.size(); i++) {
            final String enumVal = enumNode.path(i).asText();
            final Element enumElem = createXsdElement(restriction, Constants.ENUMERATION);
            enumElem.setAttribute(Constants.VALUE, enumVal);
        }
    }

    private static void handleNumber(Element nodeElem, String xsdType, JsonNode jsonNode) {
        final Integer minimum = getIntVal(jsonNode, Constants.MINIMUM);
        final Integer maximum = getIntVal(jsonNode, Constants.MAXIMUM);

        if (minimum != null || maximum != null) {
            nodeElem.removeAttribute(Constants.TYPE);
            final Element simpleType = createXsdElement(nodeElem, Constants.SIMPLE_TYPE);
            final Element restriction = createXsdElement(simpleType, Constants.RESTRICTION);
            restriction.setAttribute(Constants.BASE, xsdType);

            if (minimum != null) {
                final Element min = createXsdElement(restriction, Constants.MIN_INCLUSIVE);
                min.setAttribute(Constants.VALUE, Integer.toString(minimum));
            }

            if (maximum != null) {
                final Element max = createXsdElement(restriction, Constants.MAX_INCLUSIVE);
                max.setAttribute(Constants.VALUE, Integer.toString(maximum));
            }
        }
    }

    private static void handleArray(Element nodeElem, JsonNode jsonNode) {
        final JsonNode arrItems = jsonNode.path(Constants.ITEMS);
        deReference(arrItems);
        final String arrayXsdType = getType(arrItems.path(Constants.TYPE).textValue(),
                arrItems.path(Constants.FORMAT).textValue());

        if (Constants.OBJECT.equals(arrayXsdType)) {
            handleObject(nodeElem, arrItems);
        }

        // Minimum items
        final Integer minItems = getIntVal(jsonNode, Constants.MIN_ITEMS);
        nodeElem.setAttribute(Constants.MIN_OCCURS, minItems != null ? Integer.toString(minItems) : Constants.ZERO);

        // Max Items
        final Integer maxItems = getIntVal(jsonNode, Constants.MAX_ITEMS);
        nodeElem.setAttribute(Constants.MAX_OCCURS, maxItems != null ? Integer.toString(maxItems) : Constants.UNBOUNDED);

    }

    private static String determineXsdType(String key, JsonNode node) {
        String xsdType;
        deReference(node);
        final String jsonType = node.path(Constants.TYPE).isArray()
                ? node.path(Constants.TYPE).get(0).textValue() : node.path(Constants.TYPE).textValue();
        final String jsonFormat = node.path(Constants.FORMAT).textValue();
        final boolean isEnum = node.get(Constants.ENUM) != null;
        if (isEnum) {
            xsdType = Constants.ENUM;
        } else  if (jsonType == null && node.get(Constants.ONEOF) != null) {
            xsdType = Constants.ONEOF;
        } else {
            Assert.notNull(jsonType, "type must be specified on node '" + key + "': " + node);
            xsdType = getType(jsonType, jsonFormat);
            Assert.notNull(xsdType, "Unable to determine XSD type for json type=" + jsonType
                    + ", format=" + jsonFormat);
        }
        return xsdType;
    }

    private static void deReference(JsonNode node) {
        final JsonNode referenceNode = node.get(Constants.JSON_SCHEMA_REFERENCE);
        if (referenceNode != null) {
            final String typeName = referenceNode.asText().substring(Constants.JSON_SCHEMA_DEFINITIONS.length());
            final Iterator<Entry<String, JsonNode>> defIter = definitions.get(typeName).fields();
            while (defIter.hasNext()) {
                final Entry<String, JsonNode> entry = defIter.next();
                ((ObjectNode) node).replace(entry.getKey(), entry.getValue());
            }
            ((ObjectNode) node).remove(Constants.JSON_SCHEMA_REFERENCE);
        }
    }

    private static boolean isRequired(JsonNode required, String key) {
        if (required != null && required.isArray() && key != null) {
            for (final JsonNode keyName : required) {
                if (key.equals(keyName.textValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    private static Integer getIntVal(JsonNode node, String attribute) {
        return node.get(attribute) != null ? node.get(attribute).intValue() : null;
    }

    private static Element createXsdElement(Node element, String name) {
        return XmlUtil.createXsdElement(element, name);
    }

    private static String getType(String type, String format) {
        final String key = (type + (format != null ? ("|" + format) : "")).toLowerCase();
        return typeMapping.get(key);
    }
}
