/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */

package com.issinc.ges.interfaces.te.rules;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.issinc.ges.interfaces.te.TransformationException;
import com.issinc.ges.interfaces.te.utilities.JsonUtils;

public class CopyArrayNodeToArrayNode extends CopyArrayNodeToNode {

    private static final Logger logger = Logger.getLogger(CopyArrayNodeToArrayNode.class);
    private static final String methodName = "CopyArrayNodeToArrayNode.applyRule";

    private String toArrayPath;

    /**
     * The default constructor
     */
    public CopyArrayNodeToArrayNode() {
        super();
    }

    public CopyArrayNodeToArrayNode(String className, String toArrayPath,
            String filterByPath, String filterByValue, String filterByIdPath,
            String oldFieldPath, String newFieldPath, String dataConversionMethodName) {
        super();
        setClassName(className);
        setToArrayPath(toArrayPath);
        setFilterByPath(filterByPath);
        setFilterByValue(filterByValue);
        setFilterByIdPath(filterByIdPath);
        setOldFieldPath(oldFieldPath);
        setNewFieldPath(newFieldPath);
        setDataConversionMethodName(dataConversionMethodName);
    }

    @Override
    public final JsonNode applyRule(String token, JsonNode payload) throws TransformationException {
        logger.debug(methodName);

        // Check for valid input parameters and exit if invalid
        if (payload == null || getToArrayPath() == null || getFilterByPath() == null ||
                (getFilterByIdPath() == null && getFilterByValue() == null) ||
                getOldFieldPath() == null || getNewFieldPath() == null) {
            throw new TransformationException(TransformationException.ErrorType.PARSING_ERROR, "Rule "
                    + this.getClass().getSimpleName()
                    + " must have non null payload, toArrayPath, filterByPath,"
                    + "filterByIdField or filterByValue, oldFieldPath and newFieldPath.");
        }

        // Get all arrays that match the "to array" path
        ArrayList<JsonNode> copyToArrayList = JsonUtils.find(payload, toArrayPath);

        // For each array in the copyToArrayList
        for (JsonNode toJsonNode : copyToArrayList) {
            if (!toJsonNode.isArray()) {
                logger.warn(methodName + ": toArrayPath of " + toArrayPath + " is not an array. Rule Not applied.");
            } else {
                // For each node in the "toArray", copy a field from a matching node in some other array based on the
                // "oldFieldPath" which should specify an array, and the "filterValue"
                ArrayNode toArray = (ArrayNode)toJsonNode;
                for (JsonNode toNode : toArray) {
                    String filterValue = getFilterValue(toNode);
                    if(filterValue!=null) {
                        retrieveNode(toNode, payload, getOldFieldPath(), filterValue);
                    }
                }
            }
        }

        return payload;

    }

    public String getToArrayPath() {
        return toArrayPath;
    }

    public void setToArrayPath(String toArrayPath) {
        this.toArrayPath = toArrayPath;
    }

} /* End of Class CopyArrayNodeToArrayNode */
