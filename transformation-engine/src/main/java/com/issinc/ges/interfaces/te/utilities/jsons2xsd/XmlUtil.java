/* ****************************************************************************
 * Copyright (c) 2014 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-D-0022.
 *
 * Contractor: Intelligent Software Solutions
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005 and No. FA8750-09-D-0022.
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 * *****************************************************************************
 */

package com.issinc.ges.interfaces.te.utilities.jsons2xsd;

import java.io.IOException;
import java.io.StringWriter;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author mha
 */
public final class XmlUtil {

    /**
     * Constructor
     */
    private XmlUtil() {
    }

    /**
     * Converts a DOM node to an XML string
     * @param node The node to be converted
     * @return The XML string representation of the node
     * @throws IOException if an error occurs
     */
    public static String asXmlString(Node node) throws IOException {
        final Source source = new DOMSource(node);
        final StringWriter stringWriter = new StringWriter();
        final Result result = new StreamResult(stringWriter);
        final TransformerFactory factory = TransformerFactory.newInstance();

        try {
            final Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, result);
            return stringWriter.getBuffer().toString();
        } catch (TransformerException exc) {
            throw new IOException(exc.getMessage(), exc);
        }
    }

    /**
     * Creates a new DOM document
     * @return The new DOM document
     */
    public static Document newDocument() {
        return getBuilder().newDocument();
    }

    private static DocumentBuilder getBuilder() {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        try {
            return factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates a new XSD element
     * @param element The element node where the new element is to be created
     * @param name The name of the new element
     * @return The newly created element
     */
    public static Element createXsdElement(Node element, String name) {
        Assert.notNull(element, "element should never be null");
        final Document doc = element.getOwnerDocument() != null ? element.getOwnerDocument() : ((Document) element);
        final Element retVal = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, name);
        element.appendChild(retVal);
        return retVal;
    }

}


