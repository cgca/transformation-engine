/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.models;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "address",
        "alternateRoute",
        "ao",
        "basecamp",
        "city",
        "confidence",
        "country",
        "geoLocation",
        "dateEndLocation",
        "dateStartLocation",
        "description",
        "distanceFromCityCenter",
        "distanceToNearestFOB",
        "distanceToNearestRoute",
        "district",
        "fob",
        "house",
        "isPrimaryLocation",
        "latitude",
        "locationActive",
        "locationType",
        "longitude",
        "mscCurrent",
        "mscDateOfCurrent",
        "msc",
        "nai",
        "nearestCity",
        "nearestFOB",
        "nearestRoute",
        "neighborhood",
        "objectId",
        "populationCenterType",
        "populationCenter",
        "province",
        "rc",
        "referenceId",
        "region",
        "route",
        "security",
        "street",
        "structure",
        "village",
        "zone"
})
public class Location {

    private static final String NOT_REPORTED = "Not Reported";

    @JsonProperty("address")
    private String address;
    @JsonProperty("alternateRoute")
    private String alternateRoute;
    @JsonProperty("ao")
    private String ao;
    @JsonProperty("basecamp")
    private String basecamp;
    @JsonProperty("city")
    private String city;
    @JsonProperty("confidence")
    private String confidence;
    @JsonProperty("country")
    private String country;
    /**
     * Custom Geometry Object for Location
     */
    @JsonProperty("geoLocation")
    private GeoJSON geoLocation;
    /**
     *
     */
    @JsonProperty("dateEndLocation")
    private ActionDate dateEndLocation;
    /**
     *
     */
    @JsonProperty("dateStartLocation")
    private ActionDate dateStartLocation;
    /**
     * Location Description
     */
    @JsonProperty("description")
    private String description;
    /**
     * Distance from city center, m.
     */
    @JsonProperty("distanceFromCityCenter")
    private Integer distanceFromCityCenter;
    @JsonProperty("distanceToNearestFOB")
    private Integer distanceToNearestFOB;
    @JsonProperty("distanceToNearestRoute")
    private Integer distanceToNearestRoute;
    @JsonProperty("district")
    private String district;
    @JsonProperty("fob")
    private String fob;

    @JsonProperty("house")
    private String house;
    /**
     * Marks Primary Location
     */
    @JsonProperty("isPrimaryLocation")
    private Boolean isPrimaryLocation;
    @JsonProperty("latitude")
    private Double latitude;
    /**
     * Is Location Active?
     */
    @JsonProperty("locationActive")
    private Boolean locationActive;
    @JsonProperty("locationType")
    private String locationType;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("mscCurrent")
    private String mscCurrent;
    /**
     *
     */
    @JsonProperty("mscDateOfCurrent")
    private ActionDate mscDateOfCurrent;
    @JsonProperty("msc")
    private String msc;
    @JsonProperty("nai")
    private String nai;
    @JsonProperty("nearestCity")
    private String nearestCity;
    @JsonProperty("nearestFOB")
    private String nearestFOB;
    @JsonProperty("nearestRoute")
    private String nearestRoute;
    @JsonProperty("neighborhood")
    private String neighborhood;
    @JsonProperty("objectId")
    private String objectId;
    @JsonProperty("populationCenterType")
    private String populationCenterType;
    @JsonProperty("populationCenter")
    private String populationCenter;
    @JsonProperty("province")
    private String province;
    @JsonProperty("rc")
    private String rc;
    @JsonProperty("referenceId")
    private String referenceId;
    @JsonProperty("region")
    private String region;
    @JsonProperty("route")
    private String route;
    /**
     *
     */
    @JsonProperty("security")
    private Security security;
    @JsonProperty("street")
    private String street;
    @JsonProperty("structure")
    private String structure;
    @JsonProperty("village")
    private String village;
    @JsonProperty("zone")
    private String zone;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("alternateRoute")
    public String getAlternateRoute() {
        return alternateRoute;
    }

    @JsonProperty("alternateRoute")
    public void setAlternateRoute(String alternateRoute) {
        this.alternateRoute = alternateRoute;
    }

    @JsonProperty("ao")
    public String getAo() {
        return (String) nullCheckStringValue(ao);
    }

    @JsonProperty("ao")
    public void setAo(String ao) {
        this.ao = ao;
    }

    @JsonProperty("basecamp")
    public String getBasecamp() {
        return (String) nullCheckStringValue(basecamp);
    }

    @JsonProperty("basecamp")
    public void setBasecamp(String basecamp) {
        this.basecamp = basecamp;
    }

    @JsonProperty("city")
    public String getCity() {
        return (String) nullCheckStringValue(city);
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("confidence")
    public String getConfidence() {
        return confidence;
    }

    @JsonProperty("confidence")
    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    @JsonProperty("country")
    public String getCountry() {
        return (String) nullCheckStringValue(country);
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Custom Geometry Object for Location
     */
    @JsonProperty("geoLocation")
    public GeoJSON getGeoLocation() {
        return geoLocation;
    }

    /**
     * Custom Geometry Object for Location
     */
    @JsonProperty("geoLocation")
    public void setGeoLocation(GeoJSON geoLocation) {
        this.geoLocation = geoLocation;
    }

    /**
     *
     */
    @JsonProperty("dateEndLocation")
    public ActionDate getDateEndLocation() {
        return dateEndLocation;
    }

    /**
     *
     */
    @JsonProperty("dateEndLocation")
    public void setDateEndLocation(ActionDate dateEndLocation) {
        this.dateEndLocation = dateEndLocation;
    }

    /**
     *
     */
    @JsonProperty("dateStartLocation")
    public ActionDate getDateStartLocation() {
        return dateStartLocation;
    }

    /**
     *
     */
    @JsonProperty("dateStartLocation")
    public void setDateStartLocation(ActionDate dateStartLocation) {
        this.dateStartLocation = dateStartLocation;
    }

    /**
     * Location Description
     */
    @JsonProperty("description")
    public String getDescription() {
        return (String) nullCheckStringValue(description);
    }

    /**
     * Location Description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns the distance from city center in kilometers, instead of meters, to support the location tab UI.
     *
     * @return the distance from city center in kilometers.
     */
    @JsonProperty("distanceFromCityCenter")
    public Integer getDistanceFromCityCenter() {
        if (null == distanceFromCityCenter) {
            return distanceFromCityCenter;
        }
        return distanceFromCityCenter / 1000;
    }

    @JsonProperty("distanceFromCityCenter")
    public void setDistanceFromCityCenter(Integer distanceFromCityCenter) {
        this.distanceFromCityCenter = distanceFromCityCenter;
    }

    @JsonProperty("distanceToNearestFOB")
    public Integer getDistanceToNearestFOB() {
        return distanceToNearestFOB;
    }

    @JsonProperty("distanceToNearestFOB")
    public void setDistanceToNearestFOB(Integer distanceToNearestFOB) {
        this.distanceToNearestFOB = distanceToNearestFOB;
    }

    @JsonProperty("distanceToNearestRoute")
    public Integer getDistanceToNearestRoute() {
        return distanceToNearestRoute;
    }

    @JsonProperty("distanceToNearestRoute")
    public void setDistanceToNearestRoute(Integer distanceToNearestRoute) {
        this.distanceToNearestRoute = distanceToNearestRoute;
    }

    @JsonProperty("district")
    public String getDistrict() {
        return (String) nullCheckStringValue(district);
    }

    @JsonProperty("district")
    public void setDistrict(String district) {
        this.district = district;
    }

    @JsonProperty("fob")
    public String getFob() {
        return (String) nullCheckStringValue(fob);
    }

    @JsonProperty("fob")
    public void setFob(String fob) {
        this.fob = fob;
    }

    @JsonProperty("house")
    public String getHouse() {
        return house;
    }

    @JsonProperty("house")
    public void setHouse(String house) {
        this.house = house;
    }

    /**
     * Marks Primary Location
     */
    @JsonProperty("isPrimaryLocation")
    public Boolean getIsPrimaryLocation() {
        return isPrimaryLocation;
    }

    /**
     * Marks Primary Location
     */
    @JsonProperty("isPrimaryLocation")
    public void setIsPrimaryLocation(Boolean isPrimaryLocation) {
        this.isPrimaryLocation = isPrimaryLocation;
    }

    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Is Location Active?
     */
    @JsonProperty("locationActive")
    public Boolean getLocationActive() {
        return locationActive;
    }

    /**
     * Is Location Active?
     */
    @JsonProperty("locationActive")
    public void setLocationActive(Boolean locationActive) {
        this.locationActive = locationActive;
    }

    @JsonProperty("locationType")
    public String getLocationType() {
        return locationType;
    }

    @JsonProperty("locationType")
    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("mscCurrent")
    public String getMscCurrent() {
        return mscCurrent;
    }

    @JsonProperty("mscCurrent")
    public void setMscCurrent(String mscCurrent) {
        this.mscCurrent = mscCurrent;
    }

    /**
     *
     */
    @JsonProperty("mscDateOfCurrent")
    public ActionDate getMscDateOfCurrent() {
        return mscDateOfCurrent;
    }

    /**
     *
     */
    @JsonProperty("mscDateOfCurrent")
    public void setMscDateOfCurrent(ActionDate mscDateOfCurrent) {
        this.mscDateOfCurrent = mscDateOfCurrent;
    }

    @JsonProperty("msc")
    public String getMsc() {
        return nullCheckStringValue(msc);
    }

    @JsonProperty("msc")
    public void setMsc(String msc) {
        this.msc = msc;
    }

    @JsonProperty("nai")
    public String getNai() {
        return nullCheckStringValue(nai);
    }

    @JsonProperty("nai")
    public void setNai(String nai) {
        this.nai = nai;
    }

    @JsonProperty("nearestCity")
    public String getNearestCity() {
        return nearestCity;
    }

    @JsonProperty("nearestCity")
    public void setNearestCity(String nearestCity) {
        this.nearestCity = nearestCity;
    }

    @JsonProperty("nearestFOB")
    public String getNearestFOB() {
        return nearestFOB;
    }

    @JsonProperty("nearestFOB")
    public void setNearestFOB(String nearestFOB) {
        this.nearestFOB = nearestFOB;
    }

    @JsonProperty("nearestRoute")
    public String getNearestRoute() {
        return nearestRoute;
    }

    @JsonProperty("nearestRoute")
    public void setNearestRoute(String nearestRoute) {
        this.nearestRoute = nearestRoute;
    }

    @JsonProperty("neighborhood")
    public String getNeighborhood() {
        return nullCheckStringValue(neighborhood);
    }

    @JsonProperty("neighborhood")
    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    @JsonProperty("objectId")
    public String getObjectId() {
        return objectId;
    }

    @JsonProperty("objectId")
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    @JsonProperty("populationCenterType")
    public String getPopulationCenterType() {
        return populationCenterType;
    }

    @JsonProperty("populationCenterType")
    public void setPopulationCenterType(String populationCenterType) {
        this.populationCenterType = populationCenterType;
    }

    @JsonProperty("populationCenter")
    public String getPopulationCenter() {
        return populationCenter;
    }

    @JsonProperty("populationCenter")
    public void setPopulationCenter(String populationCenter) {
        this.populationCenter = populationCenter;
    }

    @JsonProperty("province")
    public String getProvince() {
        return nullCheckStringValue(province);
    }

    @JsonProperty("province")
    public void setProvince(String province) {
        this.province = province;
    }

    @JsonProperty("rc")
    public String getRc() {
        return rc;
    }

    @JsonProperty("rc")
    public void setRc(String rc) {
        this.rc = rc;
    }

    @JsonProperty("referenceId")
    public String getReferenceId() {
        return referenceId;
    }

    @JsonProperty("referenceId")
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty("route")
    public String getRoute() {
        return nullCheckStringValue(route);
    }

    @JsonProperty("route")
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     *
     */
    @JsonProperty("security")
    public Security getSecurity() {
        return security;
    }

    /**
     *
     */
    @JsonProperty("security")
    public void setSecurity(Security security) {
        this.security = security;
    }

    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    @JsonProperty("street")
    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("structure")
    public String getStructure() {
        return structure;
    }

    @JsonProperty("structure")
    public void setStructure(String structure) {
        this.structure = structure;
    }

    @JsonProperty("village")
    public String getVillage() {
        return nullCheckStringValue(village);
    }

    @JsonProperty("village")
    public void setVillage(String village) {
        this.village = village;
    }

    @JsonProperty("zone")
    public String getZone() {
        return nullCheckStringValue(zone);
    }

    @JsonProperty("zone")
    public void setZone(String zone) {
        this.zone = zone;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    private String nullCheckStringValue(String value) {
        if (null == value || "null".equals(value)) {
            return NOT_REPORTED;
        }
        return value;
    }

}
