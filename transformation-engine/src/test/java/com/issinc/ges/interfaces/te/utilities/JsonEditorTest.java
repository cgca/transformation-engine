package com.issinc.ges.interfaces.te.utilities;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.slf4j.LoggerFactory;
import junit.framework.TestCase;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Classification: Unclassified
 * <p/>
 * File Created: 9/9/2015
 */
public class JsonEditorTest extends TestCase {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JsonEditorTest.class);

    public void testJsonEditor() throws Exception {

        final String jsonTestString = loadFile("sampleInputReports/Operations/SIGACT/sigactReport.json");

        JsonEditor jsonEditor = new JsonEditor(jsonTestString);

        Assert.assertNotNull(jsonEditor.find("base.isTearline"));
        Assert.assertNull(jsonEditor.find("isTearline"));

        JsonNode isTearline = jsonEditor.cut("isTearline", "base");
        jsonEditor.paste("isTearline", isTearline, null);

        Assert.assertNull(jsonEditor.find("base.isTearline"));
        Assert.assertNotNull(jsonEditor.find("isTearline"));

        Assert.assertNull(jsonEditor.find("base.isTearline"));

        Assert.assertNotNull(jsonEditor.find("objectHistory"));
        Assert.assertNull(jsonEditor.find("base.objectHistory"));

        JsonNode objectHistory = jsonEditor.cut("objectHistory", null);
        jsonEditor.paste("objectHistory", objectHistory, "base");

        Assert.assertNull(jsonEditor.find("objectHistory"));
        Assert.assertNotNull(jsonEditor.find("base.objectHistory"));

    }

    /**
     * Load a file into a string.
     *
     * @param filename A file that can be found within the classloader.
     * @return the contents of a file as a string
     */
    public static String loadFile(String filename){
        try {
            final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream fileIS = classLoader.getResourceAsStream(filename);
            return IOUtils.toString(fileIS);
        } catch (IOException ex){
            String errMsg = "Failed to load file: " + filename;
            throw new RuntimeException(errMsg);
        }
    }
}