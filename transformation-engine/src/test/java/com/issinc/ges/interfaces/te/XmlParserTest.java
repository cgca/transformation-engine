/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import junit.framework.TestCase;
import org.apache.xml.dtm.ref.DTMNodeList;

public class XmlParserTest extends TestCase {

    public void testXMLIteration() throws Exception {
        final String xmlString =
                "<Department><Name>IT</Name><Employees><Employee><Name>Bob</Name></Employee><Employee><Name>Jim</Name></Employee><Employee><Name>Mel</Name></Employee></Employees></Department>";
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbFactory.newDocumentBuilder();
        InputSource is = new InputSource( new StringReader( xmlString) );
        Document doc = builder.parse( is );

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expr = xpath.compile("/Department/Employees/Employee");

        DTMNodeList nodes = (DTMNodeList)expr.evaluate(doc, XPathConstants.NODESET);
        int arraySize = nodes.getLength();
        for (int i = 0; i < arraySize; i++) {
            System.out.println(nodeToString(nodes.item(i)));
        }
    }

    private static String nodeToString(Node node)
            throws TransformerException
    {
        StringWriter buf = new StringWriter();
        Transformer xform = TransformerFactory.newInstance().newTransformer();
        xform.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        xform.transform(new DOMSource(node), new StreamResult(buf));
        return(buf.toString());
    }

} /* End of Class XmlParserTest */
