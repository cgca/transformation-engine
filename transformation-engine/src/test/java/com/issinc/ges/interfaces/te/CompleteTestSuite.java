/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 * 5450 Tech Center Drive, Suite 400
 * Colorado Springs, 80919
 * http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 */
package com.issinc.ges.interfaces.te;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Suite.SuiteClasses({
        com.issinc.ges.interfaces.te.utilities.JsonEditorTest.class,
        com.issinc.ges.interfaces.te.QPGeneratorTest.class,
        com.issinc.ges.interfaces.te.RuleSetGeneratorTest.class,
        com.issinc.ges.interfaces.te.TransformAssociationTest.class,
        com.issinc.ges.interfaces.te.TransformMediaTest.class,
        com.issinc.ges.interfaces.te.TransformationEngineTest.class,
        com.issinc.ges.interfaces.te.TransformPicklistTest.class,
        com.issinc.ges.interfaces.te.TransformReportTest.class,
        com.issinc.ges.interfaces.te.utilities.MediaUtilsTest.class,
})

public class CompleteTestSuite {
} /* End of Class CompleteTestSuite */
