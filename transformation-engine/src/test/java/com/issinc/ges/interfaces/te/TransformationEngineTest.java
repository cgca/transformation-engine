package com.issinc.ges.interfaces.te;

import com.issinc.ges.interfaces.te.models.FieldSecurity;
import com.issinc.ges.interfaces.te.rules.cdr.Cdr2Cidne;
import org.junit.Assert;
import org.junit.Test;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.commons.utils.ObjectMapperProvider;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.interfaces.te.rules.AddNode;
import com.issinc.ges.interfaces.te.rules.CopyArrayNodeToArrayNode;
import com.issinc.ges.interfaces.te.rules.XmlStringToJsonNode;
import com.issinc.ges.interfaces.te.rules.cdr.Cidne2Cdr;
import com.issinc.ges.reporting.core.util.json.validator.JsonValidator;
import com.issinc.ges.reporting.dal.BaseTest;
import com.issinc.ges.reporting.dal.DALException;
import com.issinc.ges.reporting.dal.DALFactory;
import com.issinc.ges.reporting.dal.DALResult;
import com.issinc.ges.reporting.dal.ISchemaRepo;
import com.issinc.ges.reporting.dal.transformationengine.IRulesetRepo;
import com.issinc.ges.reporting.dal.transformationengine.IVersionDefinitionRepo;

public class TransformationEngineTest extends BaseTest {

    private static String MODULE = "CIDNE";
    private static String REPORT_TYPE = "Simple";
    private static final TransformationEngine te = TransformationEngine.getInstance();

    @Test
    public void testTransformReport() throws Exception {
        // Get a CIDNE Simple report from file
        String report = loadFile("sampleInputReports/CIDNE/Simple/cidneSimpleWithLocation.json");

        // Call the transformation engine to transform it
        String result = TransformationEngine.getInstance().transformReport(report, "cidne", "simple",
                "4.1", "2.3.5", getSecurityToken());

        Assert.assertNotNull(result);

        // Get a CIDNE Simple report from file
        report = loadFile("sampleInputReports/CIDNE/Simple/cidneSimpleWithLocation.xml");

        // Call the transformation engine to transform it
        result = TransformationEngine.getInstance().transformReport(report, "cidne", "simple",
                "2.3.5", "4.1", getSecurityToken());

        Assert.assertNotNull(result);
    }

    @Test
    public void testDynamicRuleset() throws Exception {

        // Add a new 'unknown' schema
        String schema = loadFile("sampleDataModels/SchemaForFormAuthoredReport.json");

        // Here is a new 'unknown' report to be transformed from 2.3.5 to 4.1
        String report = loadFile("sampleInputReports/CIDNEExample.xml");

        String token = getSecurityToken();

        ISchemaRepo schemaRepo = DALFactory.get(ISchemaRepo.class);
        try {
            schemaRepo.insert(schema, "U", token);
            // Call the transformation engine to transform it
            String result = TransformationEngine.getInstance().transformReport(report, "cidne", "FormAuthoredReport",
                    "2.3.5", "4.1", getSecurityToken());
            Assert.assertNotNull(result);
            JsonNode reportNode = ObjectMapperProvider.provide().readTree(result);
            Assert.assertTrue("FormAuthoredReport".equals(JsonUtils.getTextField(reportNode, "base.reportType")));
        } finally {
            try {
                IVersionDefinitionRepo vdRepo = DALFactory.get(IVersionDefinitionRepo.class);
                vdRepo.delete("cidne-2.3.5-cidne-formauthoredreport", token);
                vdRepo.delete("cidne-2.3.5-cidne-formauthoredreport-schema", token);
                vdRepo.delete("cidne-4.1-cidne-formauthoredreport", token);
                vdRepo.delete("cidne-4.1-cidne-formauthoredreport-schema", token);
                IRulesetRepo rulesetRepo = DALFactory.get(IRulesetRepo.class);
                rulesetRepo.delete("cidne-formauthoredreport-4.1-2.3.5-instance", token);
                rulesetRepo.delete("cidne-formauthoredreport-2.3.5-4.1-instance", token);
                schemaRepo.delete("http://cidne.dae.smil.mil/schemas/CIDNE/FormAuthoredReport/CIDNEDM235", token);
            } catch (DALException e) {
                // Do nothing
            }
        }

    }

    @Test
    public void testTransformMisrep() throws Exception {
        // Get a MISREP from file
        String misrep = loadFile("sampleInputReports/misrep_examples.json");
        ProcessingReport report;
        final String jsonSchema = loadFile("sampleDataModels/OperationsPilotDebrief_DataModel.json");

        // Call the transformation engine to transform it
        String result = TransformationEngine.getInstance().transformMisrep(misrep, "cidne-2.3.5-misrep-data",
                "cidne-4.1-operations-pilotdebrief", getSecurityToken());

        JsonValidator.JsonValidatorResult validationResult = JsonValidator.validateJson(result, jsonSchema, JsonValidator.ValidationMode.FULL);
        report = validationResult.getReport();

        Assert.assertNotNull(result);
        Assert.assertTrue(report.isSuccess());
    }

    @Test
    public void testTransformCdir() throws Exception {
        // Get a CDIR-IIR from file
        String ism = loadFile("sampleInputReports/humintIIRv4.xml");
        ProcessingReport report;
        String jsonSchema = loadFile("sampleDataModels/HumintIIR_DataModel.json");

        Cdr2Cidne xmlrule = new Cdr2Cidne();
        xmlrule.setReportType("IIR");
        String reportJson = (String)xmlrule.applyRule(getSecurityToken(), ism);

        JsonValidator.JsonValidatorResult validationResult = JsonValidator.validateJson(reportJson, jsonSchema, JsonValidator.ValidationMode.FULL);
        report = validationResult.getReport();

        Assert.assertNotNull(reportJson);
        Assert.assertTrue(report.isSuccess());

        //test eval transformation
        ism = loadFile("sampleInputReports/humintEVALv4.xml");

        jsonSchema = loadFile("sampleDataModels/HumintEval_DataModel.json");

        xmlrule = new Cdr2Cidne();
        xmlrule.setReportType("EVAL");
        reportJson = (String)xmlrule.applyRule(getSecurityToken(), ism);

        validationResult = JsonValidator.validateJson(reportJson, jsonSchema, JsonValidator.ValidationMode.FULL);
        report = validationResult.getReport();

        Assert.assertNotNull(reportJson);
        Assert.assertTrue(report.isSuccess());

        ism = loadFile("sampleInputReports/humintAHR.xml");

        jsonSchema = loadFile("sampleDataModels/HumintAHR_DataModel.json");

        xmlrule = new Cdr2Cidne();
        xmlrule.setReportType("ICR");
        reportJson = (String)xmlrule.applyRule(getSecurityToken(), ism);

        validationResult = JsonValidator.validateJson(reportJson, jsonSchema, JsonValidator.ValidationMode.FULL);
        report = validationResult.getReport();

        Assert.assertNotNull(reportJson);
        Assert.assertTrue(report.isSuccess());

        ism = loadFile("sampleInputReports/humintHCR.xml");

        jsonSchema = loadFile("sampleDataModels/HumintHCR_DataModel.json");

        xmlrule = new Cdr2Cidne();
        xmlrule.setReportType("ICR");
        reportJson = (String)xmlrule.applyRule(getSecurityToken(), ism);

        validationResult = JsonValidator.validateJson(reportJson, jsonSchema, JsonValidator.ValidationMode.FULL);
        report = validationResult.getReport();

        Assert.assertNotNull(reportJson);
        Assert.assertTrue(report.isSuccess());

        ism = loadFile("sampleInputReports/humintSDR.xml");

        jsonSchema = loadFile("sampleDataModels/HumintSDR_DataModel.json");

        xmlrule = new Cdr2Cidne();
        xmlrule.setReportType("ICR");
        reportJson = (String)xmlrule.applyRule(getSecurityToken(), ism);

        validationResult = JsonValidator.validateJson(reportJson, jsonSchema, JsonValidator.ValidationMode.FULL);
        report = validationResult.getReport();

        Assert.assertNotNull(reportJson);
        Assert.assertTrue(report.isSuccess());

    }

    @Test
    public void testTransformCIDNEToCdir() throws Exception {
        String jsonString = loadFile("sampleInputReports/HumintIIR.json");
        Cidne2Cdr xmlrule = new Cidne2Cdr();
        xmlrule.setReportType("IIR");
        String reportXML = (String)xmlrule.applyRule(getSecurityToken(), (Object)jsonString);
        Assert.assertNotNull(reportXML);

        jsonString = loadFile("sampleInputReports/HumintEval.json");
        xmlrule = new Cidne2Cdr();
        xmlrule.setReportType("EVAL");
        reportXML = (String)xmlrule.applyRule(getSecurityToken(), (Object)jsonString);
        Assert.assertNotNull(reportXML);


        //TODO: need to generate sample reports for other Humint types (AHR, etc..)
    }

    @Test
    public void testTransformSchema() throws Exception {
        // Get a CIDNE Simple report from file
        String schema = loadFile("sampleDataModels/CIDNESimple_DataModel.json");

        // Call the transformation engine to transform it
        String result = TransformationEngine.getInstance().transformSchema(schema, "cidne", "simple",
                "4.1", "2.3.5", getSecurityToken());

        Assert.assertNotNull(result);
    }

    @Test
    public void testTransformSchemaToXsd() throws Exception {
        // Get a CIDNE Simple report from file
        String schema = loadFile("sampleDataModels/CIDNESimple_DataModel.json");

        // Call the transformation engine to transform it
        String result = TransformationEngine.getInstance().transformSchemaToXsd(schema, "cidne", "simple",
                 getSecurityToken());

        Assert.assertNotNull(result);
    }


    @Test
    public void testGetReportQueryParameter() throws Exception {
        final String token = getSecurityToken();
        String fourXmapping = te.transformReportQP("2.3.5", "CIDNE", "Simple", "ReportDTG", token);
        Assert.assertTrue("base.reportDate.zuluDateTime".equals(fourXmapping));

    }

    @Test
    public void testGetAssociationQueryParameter() throws Exception {
        final String token = getSecurityToken();
        String fourXmapping = te.transformAssociationQP("DTGPosted", token);
        Assert.assertTrue("objectHistory.actionDate.zuluDateTime".equals(fourXmapping));

        fourXmapping = te.transformAssociationQP("dtgposted", token);
        Assert.assertTrue("objectHistory.actionDate.zuluDateTime".equals(fourXmapping));
    }

    @Test
    public void testGetLocationQueryParameter() throws Exception {
        final String token = getSecurityToken();
        String fourXmapping = te.transformLocationQP("city", token);
        Assert.assertTrue("location.city".equals(fourXmapping));

        fourXmapping = te.transformLocationQP("DATEENDLOCATION", token);
        Assert.assertTrue("location.dateEndLocation.zuluDateTime".equals(fourXmapping));
    }

    @Test
    public void testTransformReportList() throws Exception{

        String token = getSecurityToken();

        // Query the report types using the DAL
        ISchemaRepo schemaRepo = DALFactory.get(ISchemaRepo.class);
        DALResult dalResults = schemaRepo.getWSReportList(token);

        // Transform the report list
        String reportListXML = te.transformReportList(dalResults.getResult(), "4.1", "2.3.5", token);

        Assert.assertTrue("Report types".equals(dalResults.getMessage()));
        Assert.assertTrue(reportListXML.contains("ReportList"));
        Assert.assertTrue(reportListXML.contains("Module"));
        Assert.assertTrue(reportListXML.contains("ReportType"));
        Assert.assertTrue(reportListXML.contains("ReportName"));
    }

    @Test
    public void testCopyArrayNodeToArrayNodeRule() throws Exception{

        try {
            CopyArrayNodeToArrayNode rule = new CopyArrayNodeToArrayNode(
                    "com.issinc.ges.interfaces.te.rules.copyArrayNodeToArrayNode", "details.SubtableNoSecurity.",
                    "objectId", null, "anotherLocationField", "location.longitude", "Longitude", "convertToNumber");
            String reportJson = loadFile("sampleInputReports/CIDNE/Simple/copyArrayNodeToArrayNode.json");
            JsonNode reportNode = ObjectMapperProvider.provide().readTree(reportJson);
            reportNode = rule.applyRule(getSecurityToken(), reportNode);

            Assert.assertNotNull(reportNode.get("details").get("SubtableNoSecurity").get(0).get("Longitude"));

        } catch (Exception e) {
            Assert.fail("Exception " + e);
        }
    }

    @Test
    public void testNewAddNodeRule() throws Exception{

        try {
            AddNode rule = new AddNode("location.mgrs", "convertLatLonToMGRS", "latitude", "longitude");
            String reportJson = loadFile("sampleInputReports/CIDNE/Simple/createMgrs.json");
            JsonNode reportNode = ObjectMapperProvider.provide().readTree(reportJson);
            reportNode = rule.applyRule(getSecurityToken(), reportNode);

            Assert.assertNotNull(reportNode.get("location").get(0).get("mgrs"));

        } catch (Exception e) {
            Assert.fail("Exception " + e);
        }
    }

    @Test
    public void testTransformBadFromPathException() throws Exception{

        String fromReportVersion = "Bad From Version";
        try {
            String TO_VERSION = "2.3.5";
            te.transformReport("not needed", MODULE, REPORT_TYPE, fromReportVersion,
                    TO_VERSION, getSecurityToken());
        } catch (TransformationException e) {
            Assert.assertEquals(
                    String.format("Version 'cidne-%s-%s-%s' not found",
                            fromReportVersion.toLowerCase(), MODULE.toLowerCase(), REPORT_TYPE.toLowerCase()),
                    e.getMessage());
        }
    }

    @Test
    public void testTransformBadToPathException() throws Exception {
        String toReportVersion = "Bad To Version";
        String FROM_VERSION = "4.1";
        try {
            te.transformReport("nothing needed", MODULE, REPORT_TYPE,
                    FROM_VERSION, toReportVersion, getSecurityToken());
        } catch (TransformationException e) {
            String fromVersion = String.format("CIDNE-%s-%s-%s", FROM_VERSION, MODULE, REPORT_TYPE);
            String toVersion = String.format("CIDNE-%s-%s-%s", toReportVersion, MODULE, REPORT_TYPE);
            Assert.assertEquals(String.format("There are no known conversion paths from %s to %s",
                    fromVersion.toLowerCase(), toVersion.toLowerCase()), e.getMessage());
        }
    }

}
