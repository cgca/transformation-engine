/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Validator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.issinc.ges.interfaces.te.utilities.TestUtils;
import com.issinc.ges.interfaces.te.utilities.VersionDefinitionUtilities;
import com.issinc.ges.reporting.dal.BaseTest;

@RunWith(Parameterized.class)
public class TransformReportTest extends BaseTest {

    private final static String VALIDATION_SCHEMA = "CIDNE-%s-%s-%s_WS.xsd";

    private String inputReport;
    private String wrapper;
    private String module;
    private String reportType;
    private String fromVersion;
    private String toVersion;
    private String sampleOutputReport;
    private String validationSchema;
    private String dotVersion;
    private Validator xsdValidator;

    public TransformReportTest(String module, String reportType, String fromVersion, String toVersion,
                               String inputReport, String wrapper, String sampleOutputReport) {
        this.inputReport = inputReport;
        this.wrapper = wrapper;
        this.sampleOutputReport = sampleOutputReport;
        this.module = module;
        this.reportType = reportType;
        this.fromVersion = fromVersion;
        this.toVersion = toVersion;

        this.dotVersion = toVersion.replace("_", ".");
        if ("2.3.5".equals(this.toVersion)) {
            try {
                this.validationSchema = String.format(VALIDATION_SCHEMA, module, reportType, dotVersion);
                this.xsdValidator = TestUtils
                        .getValidator("reportSchemas/" + validationSchema);
            } catch (Exception ex) {
                this.validationSchema = null;
                logger.debug("No validator found");
            }
        }
    }

    @Parameterized.Parameters
    public static Collection<Object[]> generateData() {
        return Arrays.asList(new Object[][]{
                { "cidne", "simple", "4.1", "2.3.5", "CIDNE/Simple/cidneSimpleWithLocation.json", "CIDNESimpleReports",
                        "CIDNESimple-2.3.5-WSReterun-XMLSample.xml"},
                { "cidne", "simple", "2.3.5", "4.1", "CIDNE/Simple/cidneSimpleWithLocation.xml", "CIDNESimpleReports",
                        null},
                { "cidne", "simple", "CURRENT", "2.3.5", "CIDNE/Simple/cidneSimpleWithLocation.json", "CIDNESimpleReports",
                        "CIDNESimple-2.3.5-WSReterun-XMLSample.xml"},
        });
    }

    @Test
    public void testTransformReport() throws Exception {
        // Get a CIDNE Simple report from file
        String report = loadFile("sampleInputReports/" + this.inputReport);

        // Call the transformation engine to transform it
        String result = TransformationEngine.getInstance().transformReport(report, this.module, this.reportType,
                this.fromVersion, this.toVersion, getSecurityToken());

        Assert.assertNotNull(result);

        // We are only validating xml right now
        if (xsdValidator != null) {
            String header = "<" + this.wrapper + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                    "\txsi:schemaLocation=\"urn:cidneNS https://cidne23int.field.issinc.com/ws/xsds/" + dotVersion + "/" +
                    this.validationSchema + "\"\n" +
                    "\txmlns=\"urn:cidneNS\" xmlns:sqltypes=\"../reportSchemas/SqlTypes.xsd\">";
            String footer = "</" + this.wrapper + ">";

            String fullXmlForValidation = String.format("%s%s%s", header, result, footer);
            xsdValidator.validate(new StreamSource(new java.io.StringReader(fullXmlForValidation)));
        }
    }

    @Test
    public void testPath() throws Exception {
        if (!TransformationEngine.CURRENT.equals(this.fromVersion) &&
                !TransformationEngine.CURRENT.equals(this.toVersion)) {
            String fromReportVersion = String.format(TransformationEngine.OBJECT_ID_INSTANCE, this.fromVersion,
                    this.module, this.reportType);
            String toReportVersion = String.format(TransformationEngine.OBJECT_ID_INSTANCE, this.toVersion,
                    this.module, this.reportType);
            List<String> pathList = VersionDefinitionUtilities.getPath(fromReportVersion, toReportVersion, getSecurityToken());
            Assert.assertEquals(2, pathList.size());
            Assert.assertArrayEquals(new String[]{fromReportVersion, toReportVersion}, pathList.toArray());
        }
    }

    @Test
    public void testSampleReport() throws Exception {
        if (this.sampleOutputReport != null && !this.sampleOutputReport.isEmpty() && xsdValidator != null) {
            xsdValidator.validate(
                    new StreamSource(new java.io.StringReader(
                            loadFile("sampleOutputReports/" + this.sampleOutputReport))));
        }
    }

} /* End of Class TransformerTest */
