/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */
package com.issinc.ges.interfaces.te;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Validator;
import com.issinc.ges.interfaces.te.generator.XSDGenerator;
import com.issinc.ges.interfaces.te.rules.CopyArrayNodeToNode;
import com.issinc.ges.interfaces.te.rules.MoveNode;
import org.junit.Assert;
import org.junit.Test;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.interfaces.te.components.RuleSet;
import com.issinc.ges.interfaces.te.generator.CIDNE2to4RuleSetGenerator;
import com.issinc.ges.interfaces.te.generator.CIDNE4to2RuleSetGenerator;
import com.issinc.ges.interfaces.te.utilities.TestUtils;
import com.issinc.ges.reporting.core.util.json.validator.JsonValidator;
import com.issinc.ges.reporting.dal.BaseTest;


public class RuleSetGeneratorTest extends BaseTest {

    //
    // NOTE:  These are not so much unit tests as they are drivers for the CIDNE2to4RuleSetGenerator.
    // The main purpose is to aid in debugging the CIDNE2to4RuleSetGenerator.  For unit tests, look
    // to TransformReportTest.java
    //

    private static final String CIDNE_SIMPLE_DATAMODEL = "sampleDataModels/CIDNESimple_DataModel.json";
    private static final String OPERATIONS_SIGACT_DATAMODEL = "sampleDataModels/OperationsSIGACT_DataModel.json";
    private static final String HUMINT_IIR_DATAMODEL = "sampleDataModels/HumintIIR_DataModel.json";

    @Test
    public void testSampleData4xTo2x() throws Exception {
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel("sampleDataModels/CIDNEDatamodelExample.json");
        RuleSet ruleset = generateRuleSet(dataModel, "TestModule", "TestReport",
                "CIDNE-4.1-TestModule-TestReport", "CIDNE-2.3.5-TestModule-TestReport");
        String result = applyRulesToFile(token, ruleset, "sampleInputReports/CIDNEExample.json");
        Assert.assertNotNull(result);
    }

    @Test
    public void testPmesii4xTo2x() throws Exception {
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel("sampleDataModels/TransitionPMESII_DataModel.json");
        RuleSet ruleset = generateRuleSet(dataModel, "Transition", "PMESII",
                "CIDNE-4.1-transition-pmesii", "CIDNE-2.3.5-transition-pmesii");
        String result = applyRulesToFile(token, ruleset, "sampleInputReports/TransitionPMESII.json");
        Assert.assertNotNull(result);
    }


    @Test
    public void testSampleData2xTo4x() throws Exception {
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel("sampleDataModels/CIDNEDatamodelExample.json");
        RuleSet ruleset = generate2xTo4xRuleSet(dataModel, "TestModule", "TestReport",
                "CIDNE-2.3.5-TestModule-TestReport", "CIDNE-4.1-TestModule-TestReport");
        String result = applyRulesToFile(token, ruleset, "sampleInputReports/CIDNEExample.xml");
        Assert.assertNotNull(result);

        JsonValidator.JsonValidatorResult resultGood = JsonValidator.validateJson(result, JsonUtils.toString(dataModel,false));
        Assert.assertEquals(JsonValidator.STATUS.SUCCESS, resultGood.getStatus());

    }


    @Test
    public void testCIDNESimple() throws Exception {
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel(
                CIDNE_SIMPLE_DATAMODEL);
        RuleSet ruleset = generateRuleSet(dataModel, "CIDNE", "Simple",
                "CIDNE-4.1-CIDNE-Simple", "CIDNE-2.3.5-CIDNE-Simple");
        String result = applyRulesToFile(token, ruleset, "sampleInputReports/CIDNE/Simple/cidneSimpleWithLocation.json");
        Assert.assertNotNull(result);

        //validate the XML
        Validator xsdValidator = TestUtils
                .getValidator("reportSchemas/CIDNE-CIDNE-Simple-2.3.5_WS.xsd");

        String header = "<CIDNESimpleReports xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "\txsi:schemaLocation=\"urn:cidneNS https://cidne23int.field.issinc.com/ws/xsds/2.3.5/" +
                "CIDNE-CIDNE-Simple-2.3.5_WS.xsd" + "\"\n" +
                "\txmlns=\"urn:cidneNS\" xmlns:sqltypes=\"../reportSchemas/SqlTypes.xsd\">";
        String footer = "</" + "CIDNESimpleReports" + ">";

        String fullXmlForValidation = String.format("%s%s%s", header, result, footer);
        xsdValidator.validate(new StreamSource(new java.io.StringReader(fullXmlForValidation)));

    }


    //
    // Create an XSD using the test schema, which has every type of scenario
    //
    @Test
    public void testXSDGeneration() throws Exception {
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel("sampleDataModels/CIDNEDatamodelExample.json");
        RuleSet ruleset = generateXSDRuleSet(dataModel, "SomeModule", "SomeReportType","CIDNE-4_1-JsonSchema",
                "CIDNE-2_3_5-XmlSchema");

        String result = applyRulesToFile(token, ruleset, "sampleDataModels/CIDNEDatamodelExample.json");

        Assert.assertNotNull(result);
    }

    @Test
    public void testCIDNESimpleXSDGeneration() throws Exception {
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel(CIDNE_SIMPLE_DATAMODEL);
        RuleSet ruleset = generateXSDRuleSet(dataModel, "CIDNE", "Simple","CIDNE-4_1-JsonSchema",
                "CIDNE-2_3_5-XmlSchema");

        String result = applyRulesToFile(token, ruleset,
                CIDNE_SIMPLE_DATAMODEL);

        Assert.assertNotNull(result);

        // Verify a few fields...
        Assert.assertTrue(result.contains("name=\"CIDNESimpleReports\""));
        Assert.assertTrue(result.contains("name=\"CIDNESimple\""));
        Assert.assertTrue(result.contains("name=\"WebServiceAttributes\""));
        Assert.assertTrue(result.contains("name=\"emailMeta\""));
        Assert.assertTrue(result.contains("name=\"TrackingNumber\""));
        Assert.assertTrue(result.contains("name=\"DateReported\""));
        Assert.assertTrue(result.contains("name=\"Location\""));
    }

    @Test
    public void testOperationsSIGACTXSDGeneration() throws Exception {
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel(OPERATIONS_SIGACT_DATAMODEL);
        RuleSet ruleset = generateXSDRuleSet(dataModel, "Operations", "SIGACT","CIDNE-4_1-JsonSchema",
                "CIDNE-2_3_5-XmlSchema");

        String result = applyRulesToFile(token, ruleset,
                OPERATIONS_SIGACT_DATAMODEL);

        Assert.assertNotNull(result);

        // Verify a few fields...
        Assert.assertTrue(result.contains("name=\"OperationsSIGACTReports\""));
        Assert.assertTrue(result.contains("name=\"OperationsSIGACT\""));
        Assert.assertTrue(result.contains("name=\"AltID\""));
        Assert.assertTrue(result.contains("name=\"BlownInPlace\""));
        Assert.assertTrue(result.contains("name=\"EnemyKIA\""));
        Assert.assertTrue(result.contains("name=\"UnitName\""));
        Assert.assertTrue(result.contains("name=\"Capture\""));
        Assert.assertTrue(result.contains("name=\"CaptureAlias\""));
        Assert.assertTrue(result.contains("name=\"ChemicalsFound\""));
        Assert.assertTrue(result.contains("name=\"Event\""));
        Assert.assertTrue(result.contains("name=\"MOA\""));
        Assert.assertTrue(result.contains("name=\"EventKey\""));
    }

    private RuleSet generateXSDRuleSet(JsonNode dataModel, String module, String reportType, String srcVersion,
                                       String dstVersion) {
        XSDGenerator xsdGenerator = new XSDGenerator();

        RuleSet ruleset = xsdGenerator.generateRuleset(dataModel, module, reportType, srcVersion, dstVersion);
        Assert.assertTrue(ruleset != null);
        return ruleset;
    }


    @Test
    public void testOperationsSIGACT() throws Exception {
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel(
                OPERATIONS_SIGACT_DATAMODEL);
        RuleSet ruleset = generateRuleSet(dataModel, "Operations", "SIGACT",
                "CIDNE-4.1-Operations-SIGACT", "CIDNE-2.3.5-Operations-SIGACT");
        String result = applyRulesToFile(token, ruleset, "sampleInputReports/Operations/SIGACT/sigactReport.json");
        Assert.assertNotNull(result);

        String header = "<OperationsSIGACTReports xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "\txsi:schemaLocation=\"urn:cidneNS https://cidne23int.field.issinc.com/ws/xsds/2.3.5/" +
                "CIDNE-Operations-SIGACT-2.3.5_WS.xsd" + "\"\n" +
                "\txmlns=\"urn:cidneNS\" xmlns:sqltypes=\"../reportSchemas/SqlTypes.xsd\">";
        String footer = "</" + "OperationsSIGACTReports" + ">";

        String fullXmlForValidation = String.format("%s%s%s", header, result, footer);
        //Validation not performed until parent key issue is resolved
        //validate the XML
        //Validator xsdValidator = TestUtils
        //        .getValidator("reportSchemas/CIDNE-Operations-SIGACT-2.3.5_WS.xsd");
        //xsdValidator.validate(new StreamSource(new java.io.StringReader(fullXmlForValidation)));
    }

    @Test
    public void testCIDNESimpleXmlAndXsd() throws Exception {
        testXmlAndXsd("CIDNE","Simple", "sampleInputReports/CIDNE/Simple/cidneSimpleWithLocation.json",
                CIDNE_SIMPLE_DATAMODEL);
    }

    @Test
    public void testSIGACTXmlAndXsd() throws Exception {
        testXmlAndXsd("Operations", "SIGACT",
                "sampleInputReports/Operations/SIGACT/sigactReport.json", OPERATIONS_SIGACT_DATAMODEL);
    }

    @Test
    public void testSampleXmlAndXsd() throws Exception {
        testXmlAndXsd("TestModule", "TestReportType", "sampleInputReports/CIDNEExample.json",
                "sampleDataModels/CIDNEDatamodelExample.json");
    }

    //
    // Generate the XML using a src JSON file, then validate it against the auto-generated XSD
    //
    private void testXmlAndXsd(String module, String reportType, String srcJsonFile, String dataModleSrcFile)
            throws Exception
    {

        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel(dataModleSrcFile);

        //
        // Generate the CIDNE Simple XML
        //
        RuleSet XMLruleset = generateRuleSet(dataModel, module, reportType,
                "CIDNE-4.1-TestModule-TestReport", "CIDNE-2.3.5-TestModule-TestReport");
        String XMLresult = applyRulesToFile(token, XMLruleset, srcJsonFile);
        Assert.assertNotNull(XMLresult);


        //
        // Generate the CIDNE Simple XSD
        //
        RuleSet XSDruleset = generateXSDRuleSet(dataModel, module, reportType,
                "CIDNE-4_1-JsonSchema", "CIDNE-2_3_5-XmlSchema");

        String XSDresults = applyRules(token, XSDruleset, dataModel.toString());

        Assert.assertNotNull(XSDresults);

        //validate the XML
        Validator xsdValidator = TestUtils.getValidatorFromStr(XSDresults);

        String xsdHeader = "<" + module + reportType + "Reports xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "\txsi:schemaLocation=\"urn:cidneNS https://cidne23int.field.issinc.com/ws/xsds/2.3.5/" +
                "CIDNE-Operations-SIGACT-2.3.5_WS.xsd" + "\"\n" +
                "\txmlns=\"urn:cidneNS\" xmlns:sqltypes=\"../reportSchemas/SqlTypes.xsd\">";
        String xsdFooter = "</" + module +reportType + "Reports" + ">";

        String fullXmlForValidation = String.format("%s%s%s", xsdHeader, XMLresult, xsdFooter);
        xsdValidator.validate(new StreamSource(new java.io.StringReader(fullXmlForValidation)));
    }


    private JsonNode getDataModel(String dataModelFile) throws Exception {
        JsonNode datamodel = getDocument(dataModelFile);
        Assert.assertNotNull(datamodel);
        return datamodel;
    }

    private RuleSet generateRuleSet(JsonNode datamodel, String module, String report, String srcVersion,
            String dstVersion) {
        CIDNE4to2RuleSetGenerator CIDNE4to2RuleSetGenerator = new CIDNE4to2RuleSetGenerator();

        RuleSet ruleset = CIDNE4to2RuleSetGenerator.generateRuleset(datamodel, module, report, srcVersion, dstVersion);
        Assert.assertTrue(ruleset != null);
        return ruleset;
    }

    private RuleSet generate2xTo4xRuleSet(JsonNode datamodel, String module, String report, String srcVersion,
            String dstVersion) {
        CIDNE2to4RuleSetGenerator ruleSetGenerator = new CIDNE2to4RuleSetGenerator();
        RuleSet ruleset = ruleSetGenerator.generateRuleset(datamodel, module, report, srcVersion, dstVersion);
        Assert.assertTrue(ruleset != null);
        return ruleset;
    }

    private String applyRulesToFile(String token, RuleSet ruleset, String inputFile) throws Exception {

        Object result = loadFile(inputFile);
        Assert.assertNotNull(ruleset.getRules());
        for (Rule rule : ruleset.getRules()) {
            if(rule instanceof CopyArrayNodeToNode && ((CopyArrayNodeToNode)rule).getFilterByIdPath().equalsIgnoreCase("economiclocationid")){
                logger.debug("...");
            }
            result = rule.applyRule(token, result);
        }
        return result.toString();
    }

    private String applyRules(String token, RuleSet ruleset, Object obj) throws Exception {
        Object result = obj;
        for (Rule rule : ruleset.getRules()) {
            result = rule.applyRule(token, result);
        }
        return result.toString();

    }

}
