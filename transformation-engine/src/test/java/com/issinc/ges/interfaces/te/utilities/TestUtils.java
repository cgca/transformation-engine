/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.utilities;

import java.io.*;
import java.util.List;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.commons.utils.ObjectMapperProvider;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.reporting.dal.BaseTest;

/**
 * Utility class to hold helper methods for unit tests.
 */
public final class TestUtils {

    private static final Logger logger = LoggerFactory.getLogger(TestUtils.class);

    private TestUtils() {

    }

    /**
     * Counts up the number of actual rules used by the request.
     *
     * @param rules
     *   Rules
     * @param token
     *   Security token
     * @return
     *   The total number of rules as counted by traversing all
     *   the rulesets
     * @throws Exception
     *   Bad stuff happened
     */
    public static int doCount(List<Rule> rules, String token) throws Exception {
        int retVal = 0;

        for (Rule rule : rules) {
            if (rule.getRuleSetObjectId() != null) {
                retVal += doCount(rule.getRulesForNestedRuleset(token), token);
            } else {
                retVal++;
            }
        }

        return retVal;
    }

    /**
     * Retrieves the xsd validator for the test.
     *
     * @param xsd
     *   Path of the xsd file.
     * @return
     *   Validator
     * @throws Exception
     */
    public static Validator getValidator(String xsd) throws Exception {
        Source xsdFile = new StreamSource(BaseTest.getFile(xsd));
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(xsdFile);

        return schema.newValidator();
    }

    /**
     * Get an XSD validator given a string containing the XSD contents
     * @param src XSD contents
     * @return the validator
     * @throws Exception
     */
    public static Validator getValidatorFromStr(String src) throws Exception {
        Source xsdFile = new StreamSource(new StringReader(src));
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(xsdFile);

        return schema.newValidator();
    }

    /**
     * Loads a JSON file from the specified file into a JsonNode.
     * @param filename The filename of the schema
     * @return A JsonNode tree containing the "encoded" schema
     */
    public static JsonNode getJsonFromFile(final String filename) {
        try {
            final InputStream fileIS = new FileInputStream(new File(filename));
            final String jsonString = IOUtils.toString(fileIS);
            return ObjectMapperProvider.provide().readTree(jsonString);
        } catch (IOException e) {
            logger.error("Error loading decoded schema " + filename);
            return null;
        }
    }

    public static String loadFile(String filename) {
        try {
            ClassLoader ex = Thread.currentThread().getContextClassLoader();
            InputStream errMsg1 = ex.getResourceAsStream(filename);
            return IOUtils.toString(errMsg1);
        } catch (IOException var3) {
            String errMsg = "Failed to load file: " + filename;
            throw new RuntimeException(errMsg);
        }
    }

}
