/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */
package com.issinc.ges.interfaces.te;

import com.issinc.ges.interfaces.te.rules.MoveNode;
import com.issinc.ges.interfaces.te.rules.SecurityConversionRule;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.fasterxml.jackson.databind.JsonNode;
import com.issinc.ges.commons.utils.JsonUtils;
import com.issinc.ges.interfaces.te.components.Rule;
import com.issinc.ges.interfaces.te.components.RuleSet;
import com.issinc.ges.interfaces.te.generator.CIDNE2to4RuleSetGenerator;
import com.issinc.ges.reporting.core.util.json.validator.JsonValidator;
import com.issinc.ges.reporting.dal.BaseTest;

import java.util.Arrays;
import java.util.Collection;

/**
 * Parameterized tests for various 2x to 4x transformations
 */

@RunWith(Parameterized.class)
public class RuleSet2x4xGeneratorTest extends BaseTest {

    private static final String CIDNE_SIMPLE_DATAMODEL = "sampleDataModels/CIDNESimple_DataModel.json";
    private static final String OPERATIONS_SIGACT_DATAMODEL = "sampleDataModels/OperationsSIGACT_DataModel.json";
    private static final String HUMINT_IIR_DATAMODEL = "sampleDataModels/HumintIIR_DataModel.json";
    private static final String ENTITY_FACILITY_DATAMODEL = "sampleDataModels/TargetFacility_DataModel.json";
    private static final String ENTITY_PERSON_DATAMODEL = "sampleDataModels/TargetPerson_DataModel.json";
    private static final String EXPLOITATION_UXO_DATAMODEL = "sampleDataModels/ExploitationUXO_DataModel.json";
    private static final String EXPLOITATION_CACHE_DATAMODEL = "sampleDataModels/ExploitationCache_DataModel.json";
    private static final String EXPLOITATION_IDF_DATAMODEL = "sampleDataModels/ExploitationIDF_DataModel.json";
    private static final String EXPLOITATION_ERW_DATAMODEL = "sampleDataModels/ExploitationERW_DataModel.json";
    private static final String INTEL_PRODUCTION_DATAMODEL = "sampleDataModels/IntelProduction_DataModel.json";
    private static final String PILOT_DEBRIEF_DATAMODEL = "sampleDataModels/OperationsPilotDebrief_DataModel.json";
    private static final String DAILY_SITREP_DATAMODEL = "sampleDataModels/OperationsDailySITREP_DataModel.json";
    private static final String TRANSITION_PMESII_DATAMODEL = "sampleDataModels/TransitionPMESII_DataModel.json";
    private static final String TRANSITION_FACILITY_DATAMODEL = "sampleDataModels/TransitionFacilityAssessment_DataModel.json";


    private String inputReport;
    private String module;
    private String reportType;
    private String datamodelFile;
    private String testMessage;

    public RuleSet2x4xGeneratorTest(String module, String reportType,
                               String inputReport, String datamodelFile,
                                    String testMessage) {
        this.inputReport = inputReport;
        this.module = module;
        this.reportType = reportType;
        this.datamodelFile = datamodelFile;
        this.testMessage = testMessage;

    }

    @Parameterized.Parameters( name = "{index}:{4}" )
    public static Collection<Object[]> generateData() {
        return Arrays.asList(new Object[][]{
                { "cidne", "simple", "sampleInputReports/CIDNE/Simple/cidneSimpleWithLocation.xml",
                        CIDNE_SIMPLE_DATAMODEL, "CIDNE Simple with a location"},
                { "cidne", "simple", "sampleInputReports/CIDNE/Simple/cidneSimpleCData.xml",
                        CIDNE_SIMPLE_DATAMODEL,"CData tags in CIDNE Simple"},
                { "operations", "sigact", "sampleInputReports/Operations/SIGACT/OperationsSIGACTPublish.xml",
                        OPERATIONS_SIGACT_DATAMODEL, "Operations SIGACT transformation"},
                { "Humint", "iir", "sampleInputReports/HumintIIR.xml",
                        HUMINT_IIR_DATAMODEL, "HumintIIR transformation"},
                { "entity", "facility", "sampleInputReports/TargetFacility.xml",
                       ENTITY_FACILITY_DATAMODEL, "Entity Facility transformation"},
                { "entity", "person", "sampleInputReports/TargetPerson.xml",
                        ENTITY_PERSON_DATAMODEL, "Entity Person transformation"},
                { "exploitation", "uxo", "sampleInputReports/ExploitationIedUxo.xml",
                        EXPLOITATION_UXO_DATAMODEL, "Exploitation UXO transformation"},
                { "exploitation", "cache", "sampleInputReports/ExploitationCache.xml",
                        EXPLOITATION_CACHE_DATAMODEL, "Exploitation Cache transformation"},
                { "exploitation", "idf", "sampleInputReports/ExploitationIdf.xml",
                        EXPLOITATION_IDF_DATAMODEL, "Exploitation IDF transformation"},
                { "exploitation", "erw", "sampleInputReports/ExploitationErw.xml",
                        EXPLOITATION_ERW_DATAMODEL, "Exploitation ERW transformation"},
                { "Intel", "production", "sampleInputReports/IntelProduction.xml",
                        INTEL_PRODUCTION_DATAMODEL, "Intel Production transformation"},
                { "Operations", "pilotdebrief", "sampleInputReports/PilotDebrief.xml",
                        PILOT_DEBRIEF_DATAMODEL, "Pilot Debrief transformation"},
                { "Operations", "dailysitrep", "sampleInputReports/OperationsDailySitrep.xml",
                        DAILY_SITREP_DATAMODEL, "Operations Daily SITREP transformation"},
                { "transition", "pmseii", "sampleInputReports/TransitionPMESII.xml",
                        TRANSITION_PMESII_DATAMODEL, "Transition PMESII transformation"},
                { "transition", "facilityassessment", "sampleInputReports/TransitionFacility.xml",
                        TRANSITION_FACILITY_DATAMODEL, "Transition Facility transformation"}

        });
    }

    @Test
    public void testGenerate2x4x() throws Exception {
        logger.info(testMessage);
        String token = getTestFrameworkProvider().getSecurityToken();
        JsonNode dataModel = getDataModel(this.datamodelFile);
        RuleSet ruleset = generate2xTo4xRuleSet(dataModel, module, reportType,
                "CIDNE-2.3.5-"+module+"-"+reportType, "CIDNE-4.1-"+module+"-"+reportType);
        String result = applyRulesToFile(token, ruleset, this.inputReport);
        Assert.assertNotNull(result);

        JsonValidator.JsonValidatorResult resultGood = JsonValidator.validateJson(result, JsonUtils.toString(dataModel,false));
        Assert.assertEquals(JsonValidator.STATUS.SUCCESS, resultGood.getStatus());

    }
    private JsonNode getDataModel(String dataModelFile) throws Exception {
        JsonNode datamodel = getDocument(dataModelFile);
        Assert.assertNotNull(datamodel);
        return datamodel;
    }

    private RuleSet generate2xTo4xRuleSet(JsonNode datamodel, String module, String report, String srcVersion,
                                          String dstVersion) {
        CIDNE2to4RuleSetGenerator ruleSetGenerator = new CIDNE2to4RuleSetGenerator();
        RuleSet ruleset = ruleSetGenerator.generateRuleset(datamodel, module, report, srcVersion, dstVersion);
        Assert.assertTrue(ruleset != null);
        return ruleset;
    }

    private String applyRulesToFile(String token, RuleSet ruleset, String inputFile) throws Exception {

        Object result = loadFile(inputFile);
        Assert.assertNotNull(ruleset.getRules());
        for (Rule rule : ruleset.getRules()) {
            if(rule instanceof SecurityConversionRule ){
                logger.debug("...");
            }
            result = rule.applyRule(token, result);
        }
        return result.toString();
    }



}
