/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.issinc.ges.interfaces.te.utilities.VersionDefinitionUtilities;
import com.issinc.ges.reporting.dal.BaseTest;

@RunWith(Parameterized.class)
public class TransformPicklistTest extends BaseTest {

    private String inputReport;
    private String fromVersion;
    private String toVersion;

    public TransformPicklistTest(String fromVersion, String toVersion, String inputReport) {
        this.inputReport = inputReport;
        this.fromVersion = fromVersion;
        this.toVersion = toVersion;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> generateData() {
        return Arrays.asList(new Object[][]{
                {"4.1",   "2.3.5", "4.1_2.3.5_PicklistData.json" },
                {"2.3.5", "4.1",   "2.3.5_4.1_PicklistData.xml" }
        });
    }

    @Test
    public void testPicklistTransform() throws Exception {
        // Load the sample input
        String picklist = loadFile("sampleInputReports/" + this.inputReport);

        // Call the transformation engine to transform the association
        String result = TransformationEngine.getInstance().transformPicklist(picklist, this.fromVersion,
                this.toVersion, getSecurityToken());

        Assert.assertNotNull(result);
    }

    @Test
    public void testPath() throws Exception {
        String fromReportVersion = String.format(TransformationEngine.OBJECT_ID_INSTANCE, this.fromVersion,
                TransformationEngine.ASSOCIATION, TransformationEngine.DATA);
        String toReportVersion = String.format(TransformationEngine.OBJECT_ID_INSTANCE, this.toVersion,
                TransformationEngine.ASSOCIATION, TransformationEngine.DATA);
        List<String> pathList = VersionDefinitionUtilities.getPath(fromReportVersion, toReportVersion, getSecurityToken());
        Assert.assertEquals(2, pathList.size());
        Assert.assertArrayEquals(new String[]{fromReportVersion, toReportVersion}, pathList.toArray());
    }

} /* End of Class TransformAssociationTest */
