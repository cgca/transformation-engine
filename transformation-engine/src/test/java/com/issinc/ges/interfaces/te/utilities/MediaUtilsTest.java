/*
 * Copyright (c) 2015 Intelligent Software Solutions, Inc.
 * Unpublished-all rights reserved under the copyright laws of the United States.
 *
 * This software was developed under sponsorship from
 * the CSC/42Six under:
 * xxx  xxx-xxxx-xxxx
 *
 * Contractor: Intelligent Software Solutions, Inc.
 *             5450 Tech Center Drive, Suite 400
 *             Colorado Springs, 80919
 *             http://www.issinc.com
 *
 * Intelligent Software Solutions, Inc. has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release, perform,
 * display, or disclose these technical data are restricted by paragraph (b)(1) of
 * the Rights in Technical Data-Noncommercial Items clause contained in
 * Contract No. FA8750-06-D-005, FA8750-09-D-0022, and FA8750-13-D-0013
 * Any reproduction of technical data or portions thereof marked with this legend
 * must also reproduce the markings.
 *
 * Intelligent Software Solutions, Inc. does not grant permission inconsistent with the
 * aforementioned unlimited government rights to use, disclose, copy, or make
 * derivative works of this software to parties outside the Government.
 *
 */
package com.issinc.ges.interfaces.te.utilities;


import org.junit.Assert;
import org.junit.Test;
import com.issinc.ges.reporting.core.util.json.validator.JsonValidator;
import com.issinc.ges.reporting.dal.BaseTest;

/**
 * unit tests for media util methods
 */
public class MediaUtilsTest extends BaseTest {

    @Test
    public void testCreateAssociation() throws Exception {


        String assocJson = MediaUtils.createMediaAssociation("1234", "5678", "user1", "group1");
        String dataModel = TestUtils.loadFile("sampleDataModels/AssociationSchema.json");
        JsonValidator.JsonValidatorResult resultGood = JsonValidator.validateJson(assocJson,dataModel);
        Assert.assertEquals(JsonValidator.STATUS.SUCCESS, resultGood.getStatus());
    }

    @Test
    public void testCreateMediaReport() throws Exception {
            String reportJSON = MediaUtils.createMediaReport("group1","user1","file1","reportID123", "reportID123",
                    "Document", "description field", 1200, "UNCLASSIFIED", "USA", getSecurityToken());
        String dataModel = TestUtils.loadFile("sampleDataModels/CIDNEMediaReport_DataModel.json");
        JsonValidator.JsonValidatorResult resultGood = JsonValidator.validateJson(reportJSON,dataModel);
        Assert.assertEquals(JsonValidator.STATUS.SUCCESS, resultGood.getStatus());
    }


}
