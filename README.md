# Transformation-Engine

API for transforming data to and from known CIDNE formats with the new DAE Interfaces for CIDNE

---

##  Getting Started

The transformation engine is a Java library (jar file) that can be used to convert data to and from known formats.

---

## Using the transformation engine in your application

To use it, add the following dependency to your Maven POM:

    <dependency>
      <groupId>com.issinc.ges.daei<</groupId>
      <artifactId>transformation-engine-dae</artifactId>
      <version>4.1-SNAPSHOT</version>
    </dependency>

Note that this artifact will not be found until it is compiled in Nexus. Until then, build it locally:
 
1. If you have not yet cloned the transformation engine repository, clone it: git clone https://<username>%40issinc.com@stash.issinc.com/stash/scm/daei/transformation-engine.git
1. If you have already cloned it, get the latest/greatest code: git pull origin master
1. Compile it using Maven to get the jars added to your local Maven repository: mvn clean install
1. Add the Maven dependency above to your project POM and away you go

---

## Running the tests

The transformation Engine depends on the DAE Interfaces DAL which is in the DAE Interfaces Common repository. Before
you will be able to build the Transformation Engine, you will need to pull the latest common repository and build that.
The DAE Interfaces DAL will then be in your local Maven repository for use by the Transformation Engine.

I have provided test cases (in the LoadData class) that load the rulesets, version definitions and schemas into Mongo.
You will need all 3 of these collections to be populated before you can transform anything. If you have never run them, 
run the testLoadData method. It will load all 3. If you add or update rulesets or version definitions, drop the 
appropriate collection and run either testLoadRuleSets or testLoadVersionDefinitions as appropriate. The reason that 
you have to drop the collection is that the items have unique indexes so you cannot accidentally define something
more than once.

The load data test cses will load all of the instance data in the rulesets, schemas, and versionDefinitions directories.
Therefore, the simplest way to get something new into Mongo is to drop the collection and reinsert them all.

To generate a CIDNE Simple XML schema from the current JSON schema, run the testCreate235SchemaCidneSimple test. It will 
query the schema from Mongo, convert it to XML and return it as an XML schema in string format.

To convert a CIDNE Simple from 4.1 format (JSON) to 2.3.5 format (XML), run the testTransformCidneSimpleFrom4_1To2_3_5
test.

Any test can be run by right-clicking the method name in the source code and selecting "Debug...". Note that the first
time you do this, it will create a configuration for you. It will fail though since it will not be able to find the
implementation. Just let it fail, then edit the configuration and set the classpath to that of the dae module
(ges.interfaces.te.dae), then run it again.

&nbsp;<hr>

## Building rulesets

For the most part, rulesets will be generated. However, if you need to build rulesets manually, below contains the 
information necessary to build your own rulesets.

Overall Data Manipulation Rules:

* JsonNodeToJsonString
* JsonStringToJsonNode
* JsonStringToXmlString
* XmlStringToJsonNode

JSON Node Manipulation Rules:

* AddNode
* CopyNode
* CopyArrayNodeToNode
* CopyArrayNodeToArrayNode
* DeleteNode
* MATLocation
* MoveNode
* RenameNode

Rule Attributes:

<ol>
  <li>Overall application attributes.
    <ul>
      <li>ruleName: placeholder for any required naming of the rule, ie trying to use a ruleValue is certain cases.  
          Not used currently.
      </li>
      <li>ruleValue: placeholder for any specific use cases, ie knowing the name of the root XML tag.
      </li>
    </ul>
  </li>
  <li>JSON node tree rule usage only attributes.
    <ul>
      <li>oldFieldPath: used to specify the path to a current element in the JSON node tree.
      </li>
      <li>newFieldPath: used to specify the path to the location where the node to be placed should go in the JSON node 
          tree.
      </li>
      <li>dataConversionMethodName: the name of the method to convert the node's data to an appropriate format/value for
          the transformation.
      </li>
      <li>defaultValue: a default value if so desired; in case the node being requested does not have a value or a new
          node is being placed.
      </li>
    </ul>
  </li>
  <li>JSON node CopyArrayNodeToNode rule usage only attributes.
    <ul>
      <li>filterByPath: the name of the path to filter on; this value is relative to the array specified in the
          oldFieldPath.
      </li>
      <li>filterByIdPath: the existing path in the JSON node tree to retrieve the filtering value.
      </li>
      <li>filterByValue: the specific value to filter on; this should be an either or with the filterByIdPath.
      </li>
    </ul>
    </li>
  <li>JSON node CopyArrayNodeToArrayNode rule usage only attributes.
    <ul>
      <li>toArrayPath: The path to the array to which we are copying a field. </li>
    </ul>
</ol>

All data is manipulated via JSON trees as defined by Jackson.  This means that all incoming data will need to be 
converted to JSON nodes.

<ol>
  <li>Understand what format the incoming data is in and what the outgoing format should be.  Currently, a String in a 
      format of either JSON or XML are the only acceptable incoming formats.  Rules supporting data conversion are:
    <ol>
      <li>Incoming format conversion rules
        <ul>
          <li>JsonStringToJsonNode: Takes an incoming JSON-formatted String and converts it to a JSON node tree.
            <ul>
              <li>Required attributes: "className"
              </li>
              <li>Example:
                  <pre>
                  {     
                    "className": "com.issinc.ges.interfaces.te.rules.JsonStringToJsonNode"
                  }
                  </pre>
              </li>
            </ul>
          </li>
          <li>XmlStringToJsonNode: Takes an incoming XML-formatted String and converts it to a JSON node tree.
            <ul>
              <li>Required attributes: "className" and "ruleValue" (XML root tag name).
              </li>
              <li>Example:
                  <pre>
                  {
                    "className": "com.issinc.ges.interfaces.te.rules.XmlStringToJsonNode",
                    "ruleValue": "CIDNESimple"
                  }
                  </pre>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li>Outgoing format conversion rules
        <ul>
          <li>JsonNodeToJsonString: Takes the incoming JSON node tree and converts it to a JSON-formatted String.
          </li>
          <li>JsonStringtoXmlString: Takes the incoming JSON-formatted String and converts it to an XML-formatted String.
            <ul>
              <li>Required attributes: "className" and "ruleValue" (XML root tag name).
              </li>
              <li>Example:
                  <pre>
                  { 
                    "className": "com.issinc.ges.interfaces.te.rules.JsonStringToXmlString",
                    "ruleValue" : "CIDNESimple"
                  }
                  </pre>
              </li>
            </ul>
          </li>
        </ul>
      </li>
    </ol>
  </li>
  <li>Once you have your incoming data converted to a JSON node tree, then the node manipulation rules come into play.
    <ul>
      <li>AddNode: Adds a node to the JSON node tree.  This action can be performed on a single node or an array node.
          Currently, one cannot add a new array node.  The path is expected to not already exist in the JSON node tree.
        <ul>
          <li>Required attributes: "className", "path", and "defaultValue".
          </li>
          <li>Optional attributes: "dataConversionMethodName"
          </li>
          <li>Example:
              <pre>
              {
                "className": "com.issinc.ges.interfaces.te.rules.AddNode",
                "path": "datamodel",
                "defaultValue" : "http://cidne.dae.smil.mil/schemas/CIDNE/Simple/CIDNEDM235"
              }
              </pre>
          </li>
        </ul>
      </li>
      <li>CopyNode: Copies an existing node in the JSON node tree to another location in the JSON node tree.  The 
          "newFieldPath" is relative to the root of the "oldFieldPath" meaning that if the "oldFieldPath" specifies an
          array, then that array will be the root from where the "newFieldPath" will start.
        <ul>
          <li>Required attributes: "className", "oldFieldPath", and "newFieldPath".
          </li>
          <li>Optional attributes: "dataConversionMethodName" and "defaultValue".
          </li>
          <li>Example:
              <pre>
              {
                "className": "com.issinc.ges.interfaces.te.rules.CopyNode",
                "oldFieldPath": "DateLastViewed",
                "newFieldPath": "DateLastViewedZulu"
              }
              </pre>
          </li>
        </ul>
      </li>
      <li>CopyArrayNodeToNode: Copies an existing node in an array based upon filter criteria specifying a single array 
      object to another location relative to the absolute root of a JSON node tree.
        <ul>
          <li>Required attributes: "className", "filterByPath", ("filterByIdPath" or "filterByValue"), "oldFieldPath", 
              and "newFieldPath".
          </li>
          <li>Optional attributes: "dataConversionMethodName" and "defaultValue"
          </li>
          <li>Example:
              <pre>
              {
                "className": "com.issinc.ges.interfaces.te.rules.CopyArrayNodeToNode",
                "filterByPath": "objectId",
                "filterByIdPath": "base.locationId",
                "oldFieldPath": "location.latitude",
                "newFieldPath": "Latitude"
              }
              </pre>
          </li>
        </ul>
      </li>
      <li>CopyArrayNodeToArrayNode: Copies an existing node in an array to a new node in another array based upon filter 
      criteria.
        <ul>
          <li>Required attributes: "className", "copyFromArray", "toArrayPath", "filterByPath", "equalsFieldValue", 
          "fieldToCopy" and "newFieldName". 
          </li>
          <li>Example:
              <pre>
              {
                "className": "com.issinc.ges.interfaces.te.rules.copyArrayNodeToArrayNode", 
                "copyFromArray": "location",
                "toArrayPath": "details.SubtableNoSecurity.",
                "filterByPath": "objectId", 
                "equalsFieldValue": "anotherLocationField",
                "fieldToCopy": "longitude",
                "newFieldName": "Longitude" 
              }
              </pre>
          </li>
        </ul>
      </li>
      <li>DeleteNode: Removes a node to the JSON node tree.  This action can be performed on a single node or an array 
          node.  The path is expected to exist in the JSON node tree.
        <ul>
          <li>Required attributes: "className", "oldFieldPath"
          </li>
          <li>Example:
              <pre>
              {
                "className": "com.issinc.ges.interfaces.te.rules.DeleteNode",
                "oldFieldPath": "objectHistory"
              }
              </pre>
          </li>
        </ul>
      </li>
      <li>MATLocation: Create a location record based off of a specific MAT data node.  The description field is
          created with the specified "format" and can include values of other fields listed in "descriptionPaths"
          within the MAT data node.
        <ul>
          <li>Required attributes: "className", "fromPath", "locationType", "latitudePath", "longitudePath",
              "descriptionPaths", and "format".
          </li>
          <li>Optional attributes: "idPath".
          </li>
          <li>Example:
              <pre>
              {
                    "className" : "com.issinc.ges.interfaces.te.rules.MATLocation",
                    "fromPath" : "acloss",
                    "locationType" : "A/C Loss",
                    "latitudePath" : "location.latitude",
                    "longitudePath" : "location.longitude",
                    "idPath" : "id",
                    "descriptionPaths" : "callsign,actype,cause,crewstatus",
                    "format" : "%s(%s) - %s - %s"
                  }
              </pre>
          </li>
        </ul>
      </li>
      <li>MoveNode: Moves an existing node in the JSON node tree to another location in the JSON node tree.  The 
          "newFieldPath" is relative to the root of the "oldFieldPath" meaning that if the "oldFieldPath" specifies an
          array, then that array will be the root from where the "newFieldPath" will start.
        <ul>
          <li>Required attributes: "className", "oldFieldPath", and "newFieldPath".
          </li>
          <li>Optional attributes: "dataConversionMethodName" and "defaultValue".
          </li>
          <li>Example:
              <pre>
              {
                "className": "com.issinc.ges.interfaces.te.rules.MoveNode",
                "oldFieldPath": "base.currentObjectState.actionDate.zuluDateTime",
                "newFieldPath": "DateLastViewed",
                "dataConversionMethodName" : "convertZuluDateTime"
              }
              </pre>
          </li>
        </ul>
      </li>
      <li>RenameNode: Renames a node in the JSON node tree.  This action can be performed on a single node or an array 
          node.  The "oldFieldPath" is expected to exist in the JSON node tree.  The "newFieldPath" is just the new
          name for the node.
        <ul>
          <li>Required attributes: "className", "newFieldPath", and "oldFieldPath".
          </li>
          <li>Optional attributes: "dataConversionMethodName" and "defaultValue"
          </li>
          <li>Example:
              <pre>
              {
                "className": "com.issinc.ges.interfaces.te.rules.RenameNode",
                "oldFieldPath" : "Location.ao",
                "newFieldPath" : "AO"
              }
              </pre>
          </li>
        </ul>
      </li>
    </ul>
  </li>
  <li>A rule can also point to a set of other rules.
    <ul>
      <li>Required attributes: "ruleSetObjectId"
      </li>
      <li>Example:
          <pre>
          { 
            "ruleSetObjectId" : "common_location_section"
          }
          </pre>
      </li>
    </ul>
  </li>
</ol>