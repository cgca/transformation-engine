#!/usr/bin/env bash
#
# Create a temporary directory in which to put the import files
now=$(date +'%Y-%m-%d-%I%M%S')
tempDir=mongoImport-$now
mkdir $tempDir
#
# Copy the tar file to the temp directory and extract the files
tarFileName=teData.tar
tarFile=data-loader/src/test/ppeDataLoad/${tarFileName}

if ! [ -f $tarFileName ]
then
  echo ERROR: Mongo export tar file not found: $tarFile
    exit 1 # terminate and indicate error
fi
#
cp teData.tar $tempDir
cd $tempDir
tar xvf ${tarFileName}
#
MONGO_BIN_DIR=/usr/bin
#
# Add (or update) the transformation engine schemas to the schema collection
$MONGO_BIN_DIR/mongoimport --ssl -h mongo01.ppe.lab76.org --jsonArray --upsert --db cidne --collection cidne_schema --file ./teSchema_export.json --username cidne_user --password $1
#
# Import data into transformation engine specific tables. NOTE: The commands use the "--drop" option to drop the table first
$MONGO_BIN_DIR/mongoimport --ssl -h mongo01.ppe.lab76.org --jsonArray --db cidne --collection cidne_teCapcoMappings --file ./teCapcoMappings_export.json --drop --username cidne_user --password $1
$MONGO_BIN_DIR/mongoimport --ssl -h mongo01.ppe.lab76.org --jsonArray --db cidne --collection cidne_teQueryParameters --file ./teQueryParameters_export.json --drop --username cidne_user --password $1
$MONGO_BIN_DIR/mongoimport --ssl -h mongo01.ppe.lab76.org --jsonArray --db cidne --collection cidne_teSchemaVersion --file ./teSchemaVersion_export.json --drop --username cidne_user --password $1
$MONGO_BIN_DIR/mongoimport --ssl -h mongo01.ppe.lab76.org --jsonArray --db cidne --collection cidne_teVersionDefinition --file ./teVersionDefinition_export.json --drop --username cidne_user --password $1
#
# Drop the ruleset collection
$MONGO_BIN_DIR/mongo --eval "db.cidne_teRuleset.drop()" --ssl --host mongo01.ppe.lab76.org --username cidne_user --password $1 cidne
#
# Import all of the ruleset files
counter=$((1))
rulesetFileName=teRuleset_${counter}_export.json
while [ -f $rulesetFileName ]
do
  echo Ruleset filename: $rulesetFileName
  $MONGO_BIN_DIR/mongoimport --ssl -h mongo01.ppe.lab76.org --jsonArray --db cidne --collection cidne_teRuleset --file ./$rulesetFileName --username cidne_user --password $1
  counter=$((counter+1))
  rulesetFileName=teRuleset_${counter}_export.json
done
#
# Go back to parent directory and cleanup
cd ..
rm -rf $tempDir