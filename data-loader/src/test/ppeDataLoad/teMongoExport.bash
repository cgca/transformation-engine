#!/bin/bash
#
# Create a temporary directory in which to put the export files
now=$(date +'%Y-%m-%d-%I%M%S')
dirName=mongoExport-$now
mkdir $dirName
#
MONGO_BIN_DIR=/usr/share/mongodb/current/bin/
#
# Export the data from MongoDB
$MONGO_BIN_DIR/mongoexport --jsonArray --db datasetdb --collection testapp_teCapcoMappings --out $dirName/teCapcoMappings_export.json
$MONGO_BIN_DIR/mongoexport --jsonArray --db datasetdb --collection testapp_teQueryParameters --out $dirName/teQueryParameters_export.json
$MONGO_BIN_DIR/mongoexport --jsonArray --db datasetdb --collection testapp_teSchemaVersion --out $dirName/teSchemaVersion_export.json
$MONGO_BIN_DIR/mongoexport --jsonArray --db datasetdb --collection testapp_teVersionDefinition --out $dirName/teVersionDefinition_export.json
$MONGO_BIN_DIR/mongoexport --jsonArray --db datasetdb --collection testapp_schema --query "{'project': 'DAE Interfaces'}" --out $dirName/teSchema_export.json
# Rulesets are large and have to be exported in chunks
#TODO: Get count from mongo
#count=$((mongo --eval "db.testapp_teRuleset.find().count()"))
# For now, assume there are 1200 rulesets. At the last count, there are actually 1000
count=$((1200))
loops=$((7))
batch_size=$(($count/$loops))
for (( i=0; i<loops; i++ ))
do
  skip=$(($batch_size*$i))
  $MONGO_BIN_DIR/mongoexport --jsonArray --db datasetdb --collection testapp_teRuleset --skip $skip --limit $batch_size --out $dirName/teRuleset_$((i+1))_export.json
done
#
# Put the results into a tar file
cd $dirName/ && tar -zcvf ../teData.tar . && cd ..
#
# Remove the working directory
rm -rf $dirName
echo ""
echo "Export complete. Results are in : teData.tar"
echo ""