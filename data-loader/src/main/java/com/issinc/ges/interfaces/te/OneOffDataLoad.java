/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.issinc.ges.token.api.TokenProviderAPI;
import com.issinc.ges.token.api.TokenProviderException;
import com.issinc.ges.token.api.TokenProviderFactory;

/**
 * Loads transformation engine supporting data
 */
public final class OneOffDataLoad {

    private static final Logger logger = LoggerFactory.getLogger(OneOffDataLoad.class);
    private static final String ADD_OPERATION = "-a";
    private static final String DELETE_OPERATION = "-d";
    private static final String REPLACE_OPERATION = "-r";
    private static final boolean runningFromJar =
            DataLoader.class.getProtectionDomain().getCodeSource().getLocation().getFile().endsWith(".jar");

    private OneOffDataLoad() {
    }

    /**
     * Main program for the Data Loader
     * @param args - Options defining what to load and how (see usage below)
     */
    public static void main(String[] args) {
        // For now, just reload everything when running from the jar. The parameters were not getting read correctly
        // need to put in logic to look through the args and find the load parameters rather than assuming they are
        // given in arg[0] and arg[1]
        if (runningFromJar) {
            final boolean doDelete = true;
            final boolean doInsert = true;
            final String target = "all";
            loadData(doDelete, doInsert, target);
        } else if (args != null && args.length == 2 &&
             (ADD_OPERATION.equals(args[0]) || DELETE_OPERATION.equals(args[0]) || REPLACE_OPERATION.equals(args[0]))) {
            final boolean doDelete = DELETE_OPERATION.equals(args[0]) || REPLACE_OPERATION.equals(args[0]);
            final boolean doInsert = ADD_OPERATION.equals(args[0]) || REPLACE_OPERATION.equals(args[0]);
            final String target = args[1];
            loadData(doDelete, doInsert, target);
        } else {
            printHelp();
        }

    }

    private static void loadData(boolean doDelete, boolean doInsert, String target) {
        try {
            final String token = getNPE();
            DataLoader.loadData(target, doDelete, doInsert, token);
        } catch (TransformationException e) {
            logger.error("DataLoader error: Transformation error", e);
        } catch (TokenProviderException e) {
            logger.error("DataLoader error: Error getting NPE token", e);
        }
    }

    private static void printHelp() {
        logger.error("Usage:\n"
                + "-a: Adds requested data set. Options are 'all', 'schema', 'versiondef', 'ruleset', 'qp'\n"
                + "-d: Deletes requested data set. Options are 'all', 'schema', 'versiondef', 'ruleset', qp'\n"
                + "-r: Reloads requested data set. Options are 'all', 'schema', 'versiondef', 'ruleset', 'qp'\n"
                + "Example: Adding all data sets - OneOffDataLoad -a all\n"
                + "Example: Deleting the ruleset data set - OneOffDataLoad -d ruleset\n"
        );
    }

    /**
     * Get the npe token for this class
     *
     * @return the token string
     * @throws TokenProviderException if a provider cannot be found
     */
    private static String getNPE() throws TokenProviderException {
        final TokenProviderAPI tokenProvider = TokenProviderFactory.getTokenProvider();
        return tokenProvider.getNpeToken(TokenProviderAPI.TARGET_SELF);
    }

}