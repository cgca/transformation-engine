/*
 * Copyright (c) 2015 Intelligent Software Solutions
 * Unpublished-all rights reserved under the copyright laws of the
 * United States.
 *
 * This software was developed under sponsorship from the
 * Air Force Research Laboratory under FA8750-06-D-005 and FA8750-09-R-0022.
 *
 * Contractor: Intelligent Software Solutions,
 * 5450 Tech Center Drive, Suite 400, Colorado Springs, 80919.
 * http://www.issinc.com
 * Expiration Date: June 2015
 *
 * Intelligent Software Solutions has title to the rights in this computer
 * software. The Government's rights to use, modify, reproduce, release,
 * perform, display, or disclose these technical data are restricted by
 * paragraph (b)(1) of the Rights in Technical Data-Noncommercial Items
 * clause contained in Contract No. FA8750-06-D-005 and No. FA8750-09-R-0022.
 * Any reproduction of technical data or portions thereof marked with this
 * legend must also reproduce the markings.
 *
 * Intelligent Software Solutions does not grant permission inconsistent with
 * the aforementioned unlimited government rights to use, disclose, copy,
 * or make derivative works of this software to parties outside the
 * Government.
 */

package com.issinc.ges.interfaces.te;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.issinc.ges.commons.utils.ObjectMapperProvider;
import com.issinc.ges.interfaces.te.generator.CIDNE2to4QPGenerator;
import com.issinc.ges.interfaces.te.generator.RulesetGenerator;
import com.issinc.ges.interfaces.te.generator.VersionDefinitionGenerator;
import com.issinc.ges.interfaces.te.utilities.JsonConstants;
import com.issinc.ges.reporting.dal.DALException;
import com.issinc.ges.reporting.dal.DALFactory;
import com.issinc.ges.reporting.dal.DALResult;
import com.issinc.ges.reporting.dal.ISchemaRepo;
import com.issinc.ges.reporting.dal.TestRepository;
import com.issinc.ges.reporting.dal.transformationengine.ICapcoMappingRepo;
import com.issinc.ges.reporting.dal.transformationengine.IQueryParamRepo;
import com.issinc.ges.reporting.dal.transformationengine.IRulesetRepo;
import com.issinc.ges.reporting.dal.transformationengine.ISchemaVersionRepo;
import com.issinc.ges.reporting.dal.transformationengine.IVersionDefinitionRepo;

/**
 * Load the schemas that are required by DAE Interfaces
 */
public final class DataLoader {

    private static final String ALL = "all";
    private static final String SCHEMA = "schema";
    private static final String SCHEMA_VER = "schemaVer";
    private static final String VERSION = "version";
    private static final String QP = "qp";
    private static final String RULESET = "ruleset";
    private static final String CAPCO = "capco";
    private static final String DROPPING = "Dropping ";
    private static final String COLLECTION = " collection...";
    private static final String GATHERING_STATIC = "Gathering static ";
    private static final String DATA = " data...";
    private static final String INSERTING = "Inserting ";
    private static final String WITH_OBJECT_ID = " with objectId ";
    private static final String ERROR_READING_DATA_FILE = "Error reading data file: ";
    private static final String DIR = " dir: ";

    private static final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    private static final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);
    private static final boolean runningFromJar =
            DataLoader.class.getProtectionDomain().getCodeSource().getLocation().getFile().endsWith(".jar");
    /**
     * Define all repos that are to be loaded
     */
    private enum REPOSITORY {
        SCHEMA(ISchemaRepo.class, "id", "initialLoad/staticData/schemas/transformationEngine"),
        VERSION_DEF(IVersionDefinitionRepo.class, JsonConstants.OBJECT_ID, "initialLoad/staticData/versionDefinitions"),
        RULE_SET(IRulesetRepo.class, JsonConstants.OBJECT_ID, "initialLoad/staticData/rulesets"),
        QUERY_PARAM(IQueryParamRepo.class, JsonConstants.OBJECT_ID, null),
        SCHEMA_VERSION(ISchemaVersionRepo.class, JsonConstants.OBJECT_ID, "initialLoad/staticData/schemaVersions"),
        CAPCO_MAPPING(ICapcoMappingRepo.class, JsonConstants.OBJECT_ID, "initialLoad/staticData/capcoMappings");

        private TestRepository repository;
        private String directory;
        private String idField;

        <T> REPOSITORY(Class<T> repoClass, String idField, String directory) {
            try {
                this.repository = (TestRepository) DALFactory.get(repoClass);
                this.idField = idField;
                this.directory = directory;
            } catch (DALException e) {
                throw new AssertionError(e);
            }
        }

        public TestRepository getRepository() {
            return repository;
        }

        public String getDirectory() {
            return directory;
        }

        public String getIdField() {
            return idField;
        }

    }

    private static final Logger logger = LoggerFactory.getLogger(DataLoader.class);

    private DataLoader() {
    }

    /**
     * Generates the data from the schemas then loads static data from the initialLoad directory.
     *
     * @param dataType - the second argument to the loader which tells the loader what to load
     *                 (all, schema, rulesets,...)
     * @param token - Security token
     * @param doDelete - If the data is to be deleted
     * @param doInsert - If the data is to be inserted
     * @throws TransformationException if an errors occurs
     */
    public static void loadData(String dataType, boolean doDelete, boolean doInsert, String token)
            throws TransformationException {

        if (ALL.equals(dataType) || dataType.equals(SCHEMA)) {
            processSchemas(doDelete, doInsert, token);
        }

        if (SCHEMA.equals(dataType)) {
            return;
        }

        if (doDelete) {
            dropCollections(dataType, token);
        }
        if (doInsert) {
            if (ALL.equals(dataType) || dataType.startsWith(SCHEMA_VER)) {
                insertStaticData(REPOSITORY.SCHEMA_VERSION, token);
            }
            if (ALL.equals(dataType) || dataType.startsWith(VERSION)) {
                insertStaticData(REPOSITORY.VERSION_DEF, token);
            }
            if (ALL.equals(dataType) || dataType.startsWith(RULESET)) {
                insertStaticData(REPOSITORY.RULE_SET, token);
            }
            if (ALL.equals(dataType) || dataType.startsWith(QP)) {
                final JsonNode associationParams = CIDNE2to4QPGenerator.createAssociationQueryParameters();
                insertDocument(REPOSITORY.QUERY_PARAM, associationParams, token);
                final JsonNode locationParams = CIDNE2to4QPGenerator.createLocationQueryParameters();
                insertDocument(REPOSITORY.QUERY_PARAM, locationParams, token);
            }
            if (ALL.equals(dataType) || dataType.startsWith(CAPCO)) {
                insertCapcoData(REPOSITORY.CAPCO_MAPPING, token);
            }

            // Iterate through all available schemas and run the generators
            if (runningFromJar) {
                iterateSchemasFromJar(dataType, token);
            } else {
                iterateSchemasFromDisk(dataType, token);
            }
        }
    }

    private static void iterateSchemasFromJar(String dataType, String token) throws TransformationException {
        try {
            final Resource[] resources = resolver.getResources(
                    "classpath*:/initialLoad/staticData/schemas/cidneSchemas/*/*.json");
            for (Resource resource : resources) {
                final JsonNode schema = getJsonNodeFromResource(resource);
                if (schema != null && !schema.isNull()) {
                    generateDataFromSchema(dataType, token, schema);
                }
            }
        } catch (IOException e) {
            logger.error("Error: Unable generate data from schemas");
        }
    }

    private static void iterateSchemasFromDisk(String dataType, String token) throws TransformationException {
        final URL resource = classLoader.getResource("initialLoad/staticData/schemas/cidneSchemas");
        if (resource == null) {
            throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                    "Schema directory not found");
        }
        final File schemaDir = new File(resource.getFile());
        final String[] extensions = {"json"};
        final Collection<File> schemaFiles = FileUtils.listFiles(schemaDir, extensions, true);
        for (File file : schemaFiles) {
            if (!file.isDirectory()) {
                final JsonNode schema = getJsonNodeFromFile(file.getPath());
                if (schema != null && !schema.isNull()) {
                    generateDataFromSchema(dataType, token, schema);
                }
            }
        }
    }

    private static void generateDataFromSchema(String dataType, String token, JsonNode schema)
            throws TransformationException {

        if (schema != null) {
            if (ALL.equals(dataType) || dataType.startsWith(VERSION)) {
                insertGeneratedData(schema, REPOSITORY.VERSION_DEF, token);
            }
            if (ALL.equals(dataType) || dataType.startsWith(RULESET)) {
                insertGeneratedData(schema, REPOSITORY.RULE_SET, token);
            }
            if (ALL.equals(dataType) || dataType.startsWith(QP)) {
                insertGeneratedData(schema, REPOSITORY.QUERY_PARAM, token);
            }
        }
    }

    // Schemas are special, we do not generate them and when deleting, we cannot drop the collection
    private static void processSchemas(boolean doDelete, boolean doInsert, String token)
            throws TransformationException {
        final REPOSITORY repo = REPOSITORY.SCHEMA;
        if (doDelete) {
            logger.info("Deleting transformation engine schemas...");
            deleteData(repo, getStaticData(repo), token);
        }
        if (doInsert) {
            logger.info("Inserting transformation engine schemas...");
            insertStaticData(repo, token);
        }
    }

    private static void dropCollections(String dataType, String token) {
        if (ALL.equals(dataType) || dataType.startsWith(VERSION)) {
            logger.info(DROPPING + REPOSITORY.VERSION_DEF.toString() + COLLECTION);
            REPOSITORY.VERSION_DEF.getRepository().dropCollection(token);
        }
        if (ALL.equals(dataType) || dataType.startsWith(RULESET)) {
            logger.info(DROPPING + REPOSITORY.RULE_SET.toString() + COLLECTION);
            REPOSITORY.RULE_SET.getRepository().dropCollection(token);
        }
        if (ALL.equals(dataType) || dataType.startsWith(QP)) {
            logger.info(DROPPING + REPOSITORY.QUERY_PARAM.toString() + COLLECTION);
            REPOSITORY.QUERY_PARAM.getRepository().dropCollection(token);
        }
        if (ALL.equals(dataType) || dataType.startsWith(SCHEMA_VER)) {
            logger.info(DROPPING + REPOSITORY.SCHEMA_VERSION.toString() + COLLECTION);
            REPOSITORY.SCHEMA_VERSION.getRepository().dropCollection(token);
        }
        if (ALL.equals(dataType) || dataType.startsWith(CAPCO)) {
            logger.info(DROPPING + REPOSITORY.CAPCO_MAPPING.toString() + COLLECTION);
            REPOSITORY.CAPCO_MAPPING.getRepository().dropCollection(token);
        }
    }

    private static void insertStaticData(REPOSITORY repo, String token) throws TransformationException {
        List<JsonNode> nodeList;
        nodeList = getStaticData(repo);
        insertData(repo, nodeList, token);
    }

    private static void insertCapcoData(REPOSITORY repo, String token) throws TransformationException {
        List<JsonNode> nodeList;
        logger.info(GATHERING_STATIC + repo.toString() + DATA);
        nodeList = getStaticData(repo);
        logger.info(INSERTING + repo.toString() + DATA);
        for (JsonNode node : nodeList) {
            if (node instanceof ArrayNode) {
                for (int i = 0; i < node.size(); i++) {
                    final ObjectNode capcoNode = (ObjectNode) node.get(i);
                    capcoNode.put("objectId", UUID.randomUUID().toString());
                    insertDocument(repo, capcoNode, token);
                }
            }
        }

    }

    private static void insertGeneratedData(JsonNode schema, REPOSITORY repo, String token)
            throws TransformationException {
        if (schema != null) {
            List<JsonNode> nodeList;
            logger.info("Generating dynamic " + repo.toString() + DATA);
            nodeList = getGeneratedData(repo, schema, token);
            insertData(repo, nodeList, token);
        }
    }

    private static void insertData(REPOSITORY repo, List<JsonNode> nodeList, String token)
            throws TransformationException {
        logger.info(INSERTING + repo.toString() + DATA);
        for (JsonNode node : nodeList) {
            insertDocument(repo, node, token);
        }
    }

    private static void insertDocument(REPOSITORY repo, JsonNode node, String token) throws TransformationException {
        try {
            boolean doInsert = true;
            // If this is a version definition, it may exist already. Check before inserting.
            if (repo == REPOSITORY.VERSION_DEF) {
                final DALResult dalResult =
                        repo.getRepository().getByObjectId(node.get(JsonConstants.OBJECT_ID).asText(), token);
                doInsert = dalResult.getCount() == 0;
            }
            if (doInsert) {
                repo.getRepository().insert(node.toString(), "U", token);
            }
            logger.info("Inserted: " + node.get(repo.getIdField()).asText());
        } catch (DALException e) {
            if (e.getDetailedMessage() != null && e.getDetailedMessage().contains("DuplicateKey")) {
                // Find a graceful way of checking for duplicate version definitions. Then remove this "if" stmt
                logger.warn("Document not inserted because of duplicate key " + repo.getRepository().toString() +
                        WITH_OBJECT_ID + node.get(repo.getIdField()).asText());
            } else {
                logger.error("Error inserting " + repo.getRepository().toString() +
                        WITH_OBJECT_ID + node.get(repo.getIdField()).asText());
                throw new TransformationException(TransformationException.ErrorType.UNKNOWN_ERROR,
                        "Error inserting document to " + repo.name());
            }
        }
    }

    /**
     * Deletes all of the documents in the nodeList from the specified Mongo collection
     * @param repo - The REPOSITORY to be updated
     * @param nodeList - The documents to be updated in the repo
     * @param token - The security token
     */
    private static void deleteData(REPOSITORY repo, List<JsonNode> nodeList, String token) {
        if (nodeList != null) {
            for (JsonNode node : nodeList) {
                try {
                    repo.getRepository().delete(node.get(repo.getIdField()).asText(), token);
                    logger.info("Deleted: " + node.get(repo.getIdField()).asText());
                } catch (DALException e) {
                    // Seems to delete but another error happens
                    logger.info("Document ('" + repo.getIdField() + "', '" +
                            node.get(repo.getIdField()).asText() + "') probably deleted from repository " +
                            repo.getRepository().toString());
                }
            }
        }
    }

    private static List<JsonNode> getGeneratedData(REPOSITORY repo, JsonNode schema, String token) {
        List<JsonNode> result;
        if (repo == REPOSITORY.VERSION_DEF) {
            result = VersionDefinitionGenerator.buildVersionDefinitions(schema, token);
        } else if (repo == REPOSITORY.RULE_SET) {
            result =  RulesetGenerator.buildRuleSets(schema, token);
        } else {
            final ArrayList<JsonNode> qpList = new ArrayList<>();
            final JsonNode qpNode = CIDNE2to4QPGenerator.generateQueryParameters(schema, token);
            if (qpNode != null) {
                qpList.add(qpNode);
            }
            result = qpList;
        }
        return result;
    }

    private static List<JsonNode> getStaticData(REPOSITORY repo) {
        if (runningFromJar) {
            return getStaticDataFromJar(repo);
        } else {
            return getStaticDataFromDisk(repo);
        }
    }

    private static List<JsonNode> getStaticDataFromJar(REPOSITORY repo) {
        final ArrayList<JsonNode> nodeList = new ArrayList<>();
        try {
            final Resource[] resources = resolver.getResources("classpath*:/" + repo.getDirectory() + "/**/*.json");

            for (Resource resource : resources) {
                final JsonNode node = getJsonNodeFromResource(resource);
                if (node != null && !node.isNull()) {
                    nodeList.add(node);
                }
            }
        } catch (IOException e) {
            logger.error("Error: Unable to load static " + repo.toString() + " data");
        }
        return nodeList;
    }

    private static List<JsonNode> getStaticDataFromDisk(REPOSITORY repo) {
        ArrayList<JsonNode> nodeList = new ArrayList<>();
        final String directoryName = repo.getDirectory();
        if (directoryName != null) {
            final URL directoryUrl = classLoader.getResource(directoryName);
            if (directoryUrl != null && directoryUrl.getFile() != null) {
                final File[] files = new File(directoryUrl.getFile()).listFiles();
                nodeList = loadDataFiles(files, new ArrayList<JsonNode>());
            } else {
                logger.error(repo.toString() + DIR + directoryName + " not found");
            }
        }
        return nodeList;
    }

    private static ArrayList<JsonNode> loadDataFiles(File[] files, ArrayList<JsonNode> nodeList) {
        if (files == null) {
            return nodeList;
        }

        for (File file : files) {
            if (file.isDirectory()) {
                loadDataFiles(file.listFiles(), nodeList);
            } else {
                final JsonNode node = getJsonNodeFromFile(file.getPath());
                if (node != null && !node.isNull()) {
                    nodeList.add(node);
                }
            }
        }
        return nodeList;
    }

    /**
     * Loads a JSON file from the specified file into a JsonNode.
     * @param resource A resource in the executable jar
     * @return A JsonNode tree containing the "encoded" schema
     */
    private static JsonNode getJsonNodeFromResource(final Resource resource) {
        JsonNode result = null;
        InputStream is = null;
        try {
            if (resource.isReadable()) {
                is = resource.getInputStream();
                final String jsonString = IOUtils.toString(is);
                result = ObjectMapperProvider.provide().readTree(jsonString);
            }
        } catch (IOException e) {
            logger.error(ERROR_READING_DATA_FILE + resource.getFilename());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // Do nothing
                }
            }
        }
        return result;
    }

    /**
     * Loads a JSON file from the specified file into a JsonNode.
     * @param filename The filename of the schema
     * @return A JsonNode tree containing the "encoded" schema
     */
    private static JsonNode getJsonNodeFromFile(final String filename) {
        InputStream is = null;
        try {
            is = new FileInputStream(new File(filename));
            final String jsonString = IOUtils.toString(is);
            return ObjectMapperProvider.provide().readTree(jsonString);
        } catch (IOException e) {
            logger.error(ERROR_READING_DATA_FILE + filename);
            return null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // Do nothing
                }
            }
        }
    }

}